﻿namespace AksaraJawa2015
{
    partial class Labeling
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OriginalImage = new System.Windows.Forms.GroupBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.combo_lbl = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.PictureBox_OriginalImage = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.OriginalImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).BeginInit();
            this.SuspendLayout();
            // 
            // OriginalImage
            // 
            this.OriginalImage.Controls.Add(this.button2);
            this.OriginalImage.Controls.Add(this.txtFilename);
            this.OriginalImage.Controls.Add(this.btnNext);
            this.OriginalImage.Controls.Add(this.comboBox1);
            this.OriginalImage.Controls.Add(this.combo_lbl);
            this.OriginalImage.Controls.Add(this.btnDelete);
            this.OriginalImage.Controls.Add(this.PictureBox_OriginalImage);
            this.OriginalImage.Location = new System.Drawing.Point(13, 13);
            this.OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.OriginalImage.Name = "OriginalImage";
            this.OriginalImage.Padding = new System.Windows.Forms.Padding(27, 25, 27, 25);
            this.OriginalImage.Size = new System.Drawing.Size(553, 545);
            this.OriginalImage.TabIndex = 3;
            this.OriginalImage.TabStop = false;
            this.OriginalImage.Text = "Labeled Image";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(8, 433);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(533, 42);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(86, 402);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(455, 24);
            this.comboBox1.TabIndex = 3;
            // 
            // combo_lbl
            // 
            this.combo_lbl.AutoSize = true;
            this.combo_lbl.Location = new System.Drawing.Point(5, 402);
            this.combo_lbl.Name = "combo_lbl";
            this.combo_lbl.Size = new System.Drawing.Size(75, 17);
            this.combo_lbl.TabIndex = 2;
            this.combo_lbl.Text = "Pilih huruf:";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(8, 483);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(533, 42);
            this.btnDelete.TabIndex = 1;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // PictureBox_OriginalImage
            // 
            this.PictureBox_OriginalImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBox_OriginalImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox_OriginalImage.Location = new System.Drawing.Point(8, 66);
            this.PictureBox_OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox_OriginalImage.Name = "PictureBox_OriginalImage";
            this.PictureBox_OriginalImage.Padding = new System.Windows.Forms.Padding(13, 12, 13, 12);
            this.PictureBox_OriginalImage.Size = new System.Drawing.Size(533, 332);
            this.PictureBox_OriginalImage.TabIndex = 0;
            this.PictureBox_OriginalImage.TabStop = false;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(387, 27);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(154, 31);
            this.button2.TabIndex = 8;
            this.button2.Text = "Load CSV";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtFilename
            // 
            this.txtFilename.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFilename.Location = new System.Drawing.Point(8, 27);
            this.txtFilename.Margin = new System.Windows.Forms.Padding(4);
            this.txtFilename.Multiline = true;
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtFilename.Size = new System.Drawing.Size(371, 27);
            this.txtFilename.TabIndex = 9;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Labeling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 571);
            this.Controls.Add(this.OriginalImage);
            this.Name = "Labeling";
            this.Text = "Labeling";
            this.Load += new System.EventHandler(this.Labeling_Load);
            this.OriginalImage.ResumeLayout(false);
            this.OriginalImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox OriginalImage;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label combo_lbl;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.PictureBox PictureBox_OriginalImage;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}