﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AksaraJawa2015
{
    public partial class MDIParent1 : Form
    {
        private int childFormNumber = 0;
        Segmentation SegmentationForm = null;
        HoughFeature HoughTransformForm = null;
        _88Features BondanFeaturesForm = null;
        ID3_PNN ID3_PNNForm = null;
        ID3_PNN_old ID3_PNNForm_old = null;
        FormBackprop BackpropForm = null;
        Wizard_Testing WTestingForm = null;
        Wizard_Setting WSettingForm = null;
        Labeling LabelingForm = null;
        public MDIParent1()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void printPreviewToolStripButton_Click(object sender, EventArgs e)
        {

        }

        private void segmentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SegmentationForm != null)
            {
                SegmentationForm.Dispose();
            }
            SegmentationForm = new Segmentation();
            SegmentationForm.MdiParent = this;
            //SegmentationForm.StartPosition = FormStartPosition.CenterScreen;
            //SegmentationForm.WindowState = FormWindowState.Maximized;
            SegmentationForm.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void houghTransformToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (HoughTransformForm != null)
            {
                HoughTransformForm.Dispose();
            }
            HoughTransformForm = new HoughFeature();
            HoughTransformForm.MdiParent = this;
            //HoughTransformForm.StartPosition = FormStartPosition.CenterScreen;
            //HoughTransformForm.WindowState = FormWindowState.Maximized;
            HoughTransformForm.Show();
        }

        private void featuresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (BondanFeaturesForm != null)
            {
                BondanFeaturesForm.Dispose();
            }
            BondanFeaturesForm = new _88Features();
            BondanFeaturesForm.MdiParent = this;
            //HoughTransformForm.StartPosition = FormStartPosition.CenterScreen;
            //HoughTransformForm.WindowState = FormWindowState.Maximized;
            BondanFeaturesForm.Show();
        }

        private void iD3PNNToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (ID3_PNNForm != null)
            {
                ID3_PNNForm.Dispose();
            }
            ID3_PNNForm = new ID3_PNN();
            ID3_PNNForm.MdiParent = this;
            //ID3_PNNForm.StartPosition = FormStartPosition.CenterScreen;
            //ID3_PNNForm.WindowState = FormWindowState.Maximized;
            ID3_PNNForm.Show();
        }

        private void cHI2BackpropagationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (BackpropForm != null)
            {
                BackpropForm.Dispose();
            }
            BackpropForm = new FormBackprop();
            BackpropForm.MdiParent = this;
            //BackpropForm.StartPosition = FormStartPosition.CenterScreen;
            //BackpropForm.WindowState = FormWindowState.Maximized;
            BackpropForm.Show();
        }

        private void iD3PNNToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void classificationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void houghID3PNNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ID3_PNNForm_old != null)
            {
                ID3_PNNForm_old.Dispose();
            }
            ID3_PNNForm_old = new ID3_PNN_old();
            ID3_PNNForm_old.MdiParent = this;
            //ID3_PNNForm_old.StartPosition = FormStartPosition.CenterScreen;
            //ID3_PNNForm_old.WindowState = FormWindowState.Maximized;
            ID3_PNNForm_old.Show();
        }

        private void autorunToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (WTestingForm != null)
            {
                WTestingForm.Dispose();
            }
            WTestingForm = new Wizard_Testing();
            WTestingForm.MdiParent = this;
            //ID3_PNNForm_old.StartPosition = FormStartPosition.CenterScreen;
            //ID3_PNNForm_old.WindowState = FormWindowState.Maximized;
            WTestingForm.Show();
        }

        private void testingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void labelingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LabelingForm != null)
            {
                LabelingForm.Dispose();
            }
            LabelingForm = new Labeling();
            LabelingForm.MdiParent = this;
            //LabelingForm.StartPosition = FormStartPosition.CenterScreen;
            //LabelingForm.WindowState = FormWindowState.Maximized;
            LabelingForm.Show();
        }

        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        

    }
}
