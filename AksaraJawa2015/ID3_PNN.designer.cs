﻿namespace AksaraJawa2015
{
    partial class ID3_PNN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.visualizeMethod = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.clearVisualizerBtn = new System.Windows.Forms.Button();
            this.methodVisualizer = new System.Windows.Forms.TextBox();
            this.trainingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainNewDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainExistingNetworkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTrainingDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadTrainingDataToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modifySpreadValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setCharacterLimitPerDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iD3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trainNewDataToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTrainingDataToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loadTrainingDataToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.classificationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pNNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iD3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // visualizeMethod
            // 
            this.visualizeMethod.AutoSize = true;
            this.visualizeMethod.Checked = true;
            this.visualizeMethod.CheckState = System.Windows.Forms.CheckState.Checked;
            this.visualizeMethod.Location = new System.Drawing.Point(8, 23);
            this.visualizeMethod.Margin = new System.Windows.Forms.Padding(4);
            this.visualizeMethod.Name = "visualizeMethod";
            this.visualizeMethod.Size = new System.Drawing.Size(192, 21);
            this.visualizeMethod.TabIndex = 7;
            this.visualizeMethod.Text = "Visualize Method Process";
            this.visualizeMethod.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.clearVisualizerBtn);
            this.groupBox3.Controls.Add(this.visualizeMethod);
            this.groupBox3.Controls.Add(this.methodVisualizer);
            this.groupBox3.Location = new System.Drawing.Point(16, 38);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(1013, 516);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Method Visualizer";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // clearVisualizerBtn
            // 
            this.clearVisualizerBtn.Location = new System.Drawing.Point(905, 18);
            this.clearVisualizerBtn.Margin = new System.Windows.Forms.Padding(4);
            this.clearVisualizerBtn.Name = "clearVisualizerBtn";
            this.clearVisualizerBtn.Size = new System.Drawing.Size(100, 28);
            this.clearVisualizerBtn.TabIndex = 8;
            this.clearVisualizerBtn.Text = "Clear";
            this.clearVisualizerBtn.UseVisualStyleBackColor = true;
            this.clearVisualizerBtn.Click += new System.EventHandler(this.clearVisualizerBtn_Click);
            // 
            // methodVisualizer
            // 
            this.methodVisualizer.Location = new System.Drawing.Point(8, 52);
            this.methodVisualizer.Margin = new System.Windows.Forms.Padding(4);
            this.methodVisualizer.Multiline = true;
            this.methodVisualizer.Name = "methodVisualizer";
            this.methodVisualizer.ReadOnly = true;
            this.methodVisualizer.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.methodVisualizer.Size = new System.Drawing.Size(996, 453);
            this.methodVisualizer.TabIndex = 0;
            this.methodVisualizer.WordWrap = false;
            // 
            // trainingToolStripMenuItem
            // 
            this.trainingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.iD3ToolStripMenuItem});
            this.trainingToolStripMenuItem.Name = "trainingToolStripMenuItem";
            this.trainingToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.trainingToolStripMenuItem.Text = "Training";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainNewDataToolStripMenuItem,
            this.trainExistingNetworkToolStripMenuItem,
            this.saveTrainingDataToolStripMenuItem,
            this.loadTrainingDataToolStripMenuItem1,
            this.modifySpreadValueToolStripMenuItem,
            this.setCharacterLimitPerDocumentToolStripMenuItem});
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.loadToolStripMenuItem.Text = "PNN";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // trainNewDataToolStripMenuItem
            // 
            this.trainNewDataToolStripMenuItem.Name = "trainNewDataToolStripMenuItem";
            this.trainNewDataToolStripMenuItem.Size = new System.Drawing.Size(300, 24);
            this.trainNewDataToolStripMenuItem.Text = "Train new data";
            this.trainNewDataToolStripMenuItem.Click += new System.EventHandler(this.trainNewDataToolStripMenuItem_Click);
            // 
            // trainExistingNetworkToolStripMenuItem
            // 
            this.trainExistingNetworkToolStripMenuItem.Name = "trainExistingNetworkToolStripMenuItem";
            this.trainExistingNetworkToolStripMenuItem.Size = new System.Drawing.Size(300, 24);
            this.trainExistingNetworkToolStripMenuItem.Text = "Train Existing Network";
            this.trainExistingNetworkToolStripMenuItem.Visible = false;
            this.trainExistingNetworkToolStripMenuItem.Click += new System.EventHandler(this.trainExistingNetworkToolStripMenuItem_Click);
            // 
            // saveTrainingDataToolStripMenuItem
            // 
            this.saveTrainingDataToolStripMenuItem.Name = "saveTrainingDataToolStripMenuItem";
            this.saveTrainingDataToolStripMenuItem.Size = new System.Drawing.Size(300, 24);
            this.saveTrainingDataToolStripMenuItem.Text = "Save training data";
            this.saveTrainingDataToolStripMenuItem.Click += new System.EventHandler(this.saveTrainingDataToolStripMenuItem_Click);
            // 
            // loadTrainingDataToolStripMenuItem1
            // 
            this.loadTrainingDataToolStripMenuItem1.Name = "loadTrainingDataToolStripMenuItem1";
            this.loadTrainingDataToolStripMenuItem1.Size = new System.Drawing.Size(300, 24);
            this.loadTrainingDataToolStripMenuItem1.Text = "Load training data";
            this.loadTrainingDataToolStripMenuItem1.Click += new System.EventHandler(this.loadTrainingDataToolStripMenuItem1_Click);
            // 
            // modifySpreadValueToolStripMenuItem
            // 
            this.modifySpreadValueToolStripMenuItem.Name = "modifySpreadValueToolStripMenuItem";
            this.modifySpreadValueToolStripMenuItem.Size = new System.Drawing.Size(300, 24);
            this.modifySpreadValueToolStripMenuItem.Text = "Modify Spread Value";
            this.modifySpreadValueToolStripMenuItem.Click += new System.EventHandler(this.modifySpreadValueToolStripMenuItem_Click);
            // 
            // setCharacterLimitPerDocumentToolStripMenuItem
            // 
            this.setCharacterLimitPerDocumentToolStripMenuItem.Name = "setCharacterLimitPerDocumentToolStripMenuItem";
            this.setCharacterLimitPerDocumentToolStripMenuItem.Size = new System.Drawing.Size(300, 24);
            this.setCharacterLimitPerDocumentToolStripMenuItem.Text = "Set Character Limit Per Document";
            this.setCharacterLimitPerDocumentToolStripMenuItem.Click += new System.EventHandler(this.setCharacterLimitPerDocumentToolStripMenuItem_Click);
            // 
            // iD3ToolStripMenuItem
            // 
            this.iD3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainNewDataToolStripMenuItem1,
            this.saveTrainingDataToolStripMenuItem1,
            this.loadTrainingDataToolStripMenuItem2});
            this.iD3ToolStripMenuItem.Name = "iD3ToolStripMenuItem";
            this.iD3ToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.iD3ToolStripMenuItem.Text = "ID3";
            // 
            // trainNewDataToolStripMenuItem1
            // 
            this.trainNewDataToolStripMenuItem1.Name = "trainNewDataToolStripMenuItem1";
            this.trainNewDataToolStripMenuItem1.Size = new System.Drawing.Size(200, 24);
            this.trainNewDataToolStripMenuItem1.Text = "Train new data";
            this.trainNewDataToolStripMenuItem1.Click += new System.EventHandler(this.trainNewDataToolStripMenuItem1_Click);
            // 
            // saveTrainingDataToolStripMenuItem1
            // 
            this.saveTrainingDataToolStripMenuItem1.Name = "saveTrainingDataToolStripMenuItem1";
            this.saveTrainingDataToolStripMenuItem1.Size = new System.Drawing.Size(200, 24);
            this.saveTrainingDataToolStripMenuItem1.Text = "Save training data";
            this.saveTrainingDataToolStripMenuItem1.Click += new System.EventHandler(this.saveTrainingDataToolStripMenuItem1_Click);
            // 
            // loadTrainingDataToolStripMenuItem2
            // 
            this.loadTrainingDataToolStripMenuItem2.Name = "loadTrainingDataToolStripMenuItem2";
            this.loadTrainingDataToolStripMenuItem2.Size = new System.Drawing.Size(200, 24);
            this.loadTrainingDataToolStripMenuItem2.Text = "Load training data";
            this.loadTrainingDataToolStripMenuItem2.Click += new System.EventHandler(this.loadTrainingDataToolStripMenuItem2_Click);
            // 
            // classificationToolStripMenuItem
            // 
            this.classificationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pNNToolStripMenuItem,
            this.iD3ToolStripMenuItem1});
            this.classificationToolStripMenuItem.Name = "classificationToolStripMenuItem";
            this.classificationToolStripMenuItem.Size = new System.Drawing.Size(108, 24);
            this.classificationToolStripMenuItem.Text = "Classification";
            // 
            // pNNToolStripMenuItem
            // 
            this.pNNToolStripMenuItem.Name = "pNNToolStripMenuItem";
            this.pNNToolStripMenuItem.Size = new System.Drawing.Size(108, 24);
            this.pNNToolStripMenuItem.Text = "PNN";
            this.pNNToolStripMenuItem.Click += new System.EventHandler(this.pNNToolStripMenuItem_Click);
            // 
            // iD3ToolStripMenuItem1
            // 
            this.iD3ToolStripMenuItem1.Name = "iD3ToolStripMenuItem1";
            this.iD3ToolStripMenuItem1.Size = new System.Drawing.Size(108, 24);
            this.iD3ToolStripMenuItem1.Text = "ID3";
            this.iD3ToolStripMenuItem1.Click += new System.EventHandler(this.iD3ToolStripMenuItem1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainingToolStripMenuItem,
            this.classificationToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1045, 28);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ID3_PNN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 569);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ID3_PNN";
            this.Text = "Classification Program";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox methodVisualizer;
        private System.Windows.Forms.CheckBox visualizeMethod;
        private System.Windows.Forms.Button clearVisualizerBtn;
        private System.Windows.Forms.ToolStripMenuItem trainingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainNewDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainExistingNetworkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTrainingDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadTrainingDataToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem modifySpreadValueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setCharacterLimitPerDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iD3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trainNewDataToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem saveTrainingDataToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadTrainingDataToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem classificationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pNNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iD3ToolStripMenuItem1;
        private System.Windows.Forms.MenuStrip menuStrip1;
    }
}

