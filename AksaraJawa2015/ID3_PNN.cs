﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Diagnostics;
using NUnit.Framework;

namespace AksaraJawa2015
{
    public partial class ID3_PNN : Form
    {
        #region Members

        private string[][] _trainerData, _rawClassificationData;
        private List<string> _testDataNames;
        private Dictionary<string, string> _javaUnicode;
        private List<string> _upperCharacter;
        private List<string> _letterException;
        public PNN _pnnNetwork;
        public Tree _id3Network;
        public bool TrainNewData { get; protected set; }
        public bool _enchanceReading;
        public bool _createCSVResult;
        public bool _enrichFeatureExtraction;
        public bool _matlabOptimized;

        // auto test vars
        public string[] DataTester { get; protected set; }
        public bool AutoDataTest { get; protected set; }

        // feature extraction method vars
        public bool _88Features;

        // training limit vars
        public bool LimitTrainingData { get; protected set; }
        public bool LimitPNNData { get; protected set; }
        public int MaxClassVariancePerDoc { get; set; }
        public int PNNDataLimit { get; protected set; }

        // training & classification data separator vars
        public bool SeparateTrainingData { get; protected set; }

        // separate classification vars
        public bool SeparateClassificationCategory { get; protected set; }
        public List<string> AksaraCarakanList { get; protected set; }

        #endregion

        public ID3_PNN()
        {
            InitializeComponent();
            _trainerData = _rawClassificationData = null;
            _pnnNetwork = null;
            _id3Network = null;
            _enchanceReading = false;
            _createCSVResult = true;
            _enrichFeatureExtraction = false;
            _matlabOptimized = false;
            TrainNewData = true;
            methodVisualizer.Text = "";

            // auto test settings
            AutoDataTest = true;
            DataTester = new string[] { "result - edited", "Used for training", "Excluded from trainer" };

            // feature extraction method setting
            _88Features = true;

            // training limit settings
            LimitTrainingData = false;
            LimitPNNData = false;
            MaxClassVariancePerDoc = int.MaxValue;
            PNNDataLimit = int.MaxValue;

            // training & classification data separation settings
            if (LimitTrainingData) SeparateTrainingData = true;
            else SeparateTrainingData = false;

            // separate classification data based on its category settings
            SeparateClassificationCategory = false;
            #region List of aksara carakan
            AksaraCarakanList = new List<string>();
            AksaraCarakanList.Add("ha");
            AksaraCarakanList.Add("na");
            AksaraCarakanList.Add("ca");
            AksaraCarakanList.Add("ra");
            AksaraCarakanList.Add("ka");
            AksaraCarakanList.Add("da");
            AksaraCarakanList.Add("ta");
            AksaraCarakanList.Add("sa");
            AksaraCarakanList.Add("wa");
            AksaraCarakanList.Add("la");
            AksaraCarakanList.Add("pa");
            AksaraCarakanList.Add("dha");
            AksaraCarakanList.Add("ja");
            AksaraCarakanList.Add("ya");
            AksaraCarakanList.Add("nya");
            AksaraCarakanList.Add("ma");
            AksaraCarakanList.Add("ga");
            AksaraCarakanList.Add("ba");
            AksaraCarakanList.Add("tha");
            AksaraCarakanList.Add("nga");
            #endregion

            #region Non-java test data initialization
            // set test data exceptions
            _testDataNames = new List<string>();
            _testDataNames.Add("car.csv");
            _testDataNames.Add("car_trainer.csv");
            _testDataNames.Add("iris.csv");
            _testDataNames.Add("iris_trainer.csv");
            _testDataNames.Add("gift_problem_trainer.csv");
            _testDataNames.Add("weather_prediction_trainer.csv");
            #endregion

            #region Unicode initialization
            // init java font unicodes
            _javaUnicode = new Dictionary<string, string>();
            // carakan
            _javaUnicode.Add("ha", "a");
            _javaUnicode.Add("na", "n");
            _javaUnicode.Add("ca", "c");
            _javaUnicode.Add("ra", "r");
            _javaUnicode.Add("ka", "k");
            _javaUnicode.Add("da", "f");
            _javaUnicode.Add("ta", "t");
            _javaUnicode.Add("sa", "s");
            _javaUnicode.Add("wa", "w");
            _javaUnicode.Add("la", "l");
            _javaUnicode.Add("pa", "p");
            _javaUnicode.Add("dha", "d");
            _javaUnicode.Add("ja", "j");
            _javaUnicode.Add("ya", "y");
            _javaUnicode.Add("nya", "v");
            _javaUnicode.Add("ma", "m");
            _javaUnicode.Add("ga", "g");
            _javaUnicode.Add("ba", "b");
            _javaUnicode.Add("tha", "q");
            _javaUnicode.Add("nga", "z");
            // pasangan
            _javaUnicode.Add("pasangan ha", "H");
            _javaUnicode.Add("pasangan na", "N");
            _javaUnicode.Add("pasangan ca", "C");
            _javaUnicode.Add("pasangan ra", "R");
            _javaUnicode.Add("pasangan ka", "K");
            _javaUnicode.Add("pasangan da", "F");
            _javaUnicode.Add("pasangan ta", "T");
            _javaUnicode.Add("pasangan sa", "S");
            _javaUnicode.Add("pasangan wa", "W");
            _javaUnicode.Add("pasangan la", "L");
            _javaUnicode.Add("pasangan pa", "P");
            _javaUnicode.Add("pasangan dha", "D");
            _javaUnicode.Add("pasangan ja", "J");
            _javaUnicode.Add("pasangan ya", "Y");
            _javaUnicode.Add("pasangan nya", "V");
            _javaUnicode.Add("pasangan ma", "M");
            _javaUnicode.Add("pasangan ga", "G");
            _javaUnicode.Add("pasangan ba", "B");
            _javaUnicode.Add("pasangan tha", "Q");
            _javaUnicode.Add("pasangan nga", "Z");
            // sandhangan
            _javaUnicode.Add("wulu", "i");
            _javaUnicode.Add("suku", "u");
            _javaUnicode.Add("taling", "[");
            _javaUnicode.Add("pepet", "e");
            _javaUnicode.Add("tarung", "o");
            _javaUnicode.Add("layar", "/");
            _javaUnicode.Add("wignyan", "h");
            _javaUnicode.Add("cecak", "=");
            _javaUnicode.Add("pangkon", "\\");
            _javaUnicode.Add("pangku", "\\");
            _javaUnicode.Add("paten", "\\");
            _javaUnicode.Add("pengkal", "-");
            _javaUnicode.Add("cakra", "]");
            _javaUnicode.Add("keret", "}");
            // wilangan
            _javaUnicode.Add("0", "0");
            _javaUnicode.Add("1", "1");
            _javaUnicode.Add("2", "2");
            _javaUnicode.Add("3", "3");
            _javaUnicode.Add("4", "4");
            _javaUnicode.Add("5", "5");
            _javaUnicode.Add("6", "6");
            _javaUnicode.Add("7", "7");
            _javaUnicode.Add("8", "8");
            _javaUnicode.Add("9", "9");
            // tanda baca
            _javaUnicode.Add("adeg adeg", "?");
            _javaUnicode.Add("pada lungsi", ".");
            _javaUnicode.Add("pada lingsa", ",");
            // null char
            _javaUnicode.Add("-", null);
            #endregion

            #region List of sandhangan atas
            // init sandangan atas
            _upperCharacter = new List<string>();
            _upperCharacter.Add("wulu");
            _upperCharacter.Add("pepet");
            _upperCharacter.Add("layar");
            _upperCharacter.Add("cecak");
            #endregion

            #region List of forbidden characters to be used for training & classification
            // set classification exceptions
            _letterException = new List<string>();
            _letterException.Add("+");
            _letterException.Add("murda");
            _letterException.Add("swara");
            _letterException.Add("?");
            _letterException.Add("-");
            _letterException.Add("/");
            for (int i = 1; i < 10; i++)
                _letterException.Add(i + "");
            #endregion
        }

        #region Button Click Events

        private void segmentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessHelper.startProcess("ExtApps\\Segmentation\\App.exe", this);
        }

        private void featureExtractionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessHelper.startProcess("ExtApps\\Feature Extraction\\histogram.exe", this);
        }

        private void trainNewDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TrainNewData = true;
            train("pnn");
        }

        private void trainExistingNetworkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stopwatch stopWatch = new Stopwatch();
            string displayText = "";
            stopWatch.Start();

            // load external trainer file
            TrainNewData = false;
            if (!loadTrainerFile()) return;

            // train existing PNN network
            if (_pnnNetwork.TrainExisting(_trainerData))
            {
                if (visualizeMethod.Checked) displayText += _pnnNetwork.Display();
            }
            else
            {
                if (visualizeMethod.Checked) displayText += "Fail to train existing network\r\n";
            }

            stopWatch.Stop();

            // update display text
            if(visualizeMethod.Checked)
            {
                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                displayText += "Done ! (" + elapsedTime + ")\r\n" +
                    "\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                updateVisualizer(displayText);
            }
        }

        private void saveTrainingDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            serialize("pnn");
        }

        private void loadTrainingDataToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            deserialize("pnn");
        }

        private void modifySpreadValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var displayTxt = "";

            if(_pnnNetwork != null)
            {
                // get spread from user input
                string strSpread = Interaction.InputBox("Input spread value", "Spread", _pnnNetwork.Spread + "");
                double spread;
                while (!Double.TryParse(strSpread, out spread))
                    strSpread = Interaction.InputBox("Please input a numeric value", "User Input Needed", _pnnNetwork.Spread + "");
                // set the spread
                _pnnNetwork.Spread = spread;
                // display the change
                displayTxt += "Spread changed to : " + spread
                    + "\r\nBias changed to " + _pnnNetwork.Bias + " \r\n\r\n";
            }
            else
            {
                displayTxt += "Please train the network first\r\n\r\n";
            }
            updateVisualizer(displayTxt);
        }

        private void setCharacterLimitPerDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string tempNewValue = Interaction.InputBox("Input value", "Max Variance Per Class", MaxClassVariancePerDoc + "");
            int newValue;
            while (!int.TryParse(tempNewValue, out newValue))
                tempNewValue = Interaction.InputBox("Please input a numeric value", "Max Variance Per Class", 
                    MaxClassVariancePerDoc + "");
            MaxClassVariancePerDoc = newValue;
        }

        private void trainNewDataToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            train("id3");
        }

        private void saveTrainingDataToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            serialize("id3");
        }

        private void loadTrainingDataToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            deserialize("id3");
        }

        private void pNNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            classify("pnn");
        }

        private void iD3ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            classify("id3");
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClassificationSettings settingForm = new ClassificationSettings(this, _enchanceReading, _createCSVResult);
            settingForm.ShowDialog();
        }

        private void clearVisualizerBtn_Click(object sender, EventArgs e)
        {
            methodVisualizer.Text = "";
        }

        #endregion

        #region Training Methods

        private Boolean loadTrainerFile()
        {
            int i;
            OpenFileDialog dialogBox = new OpenFileDialog();
            dialogBox.Filter = "CSV File (.csv)|*.csv";
            dialogBox.Title = "Select data to learn from";
            string displayTxt = "";
            if (dialogBox.ShowDialog() == DialogResult.OK)
            {
                // load external file
                _trainerData = Parser.CSVToArray(@dialogBox.FileName);
                if(_enrichFeatureExtraction)
                    _trainerData = ClassifierNetworkkHelper.LeverageMultipleData(_trainerData);

                if (_trainerData != null)
                {
                    // trim filename 
                    var fileName = dialogBox.FileName.Substring(dialogBox.FileName.LastIndexOf('\\') + 1);

                    // update visualizer
                    displayTxt += "Trainer data loaded : " + fileName;
                    if(visualizeMethod.Checked) displayTxt += "\r\nContent : \r\n" + Parser.ArrayToString(_trainerData);

                    // create a master copy of trainer data
                    string[][] rawTrainer = _trainerData;

                    // set columns to fetch
                    var columns = "";
                    if(_88Features)
                    {
                        columns += "F1";
                        for(i=2; i<=88; i++)
                            columns += "|F" + i;
                    }
                    else
                    {
                        columns += "Jumlah Loop pada Gambar"
                            + "|Jumlah Loop pada Segmen Atas"
                            + "|Jumlah Loop pada Segmen Bawah"
                            + "|Jumlah Line pada Segmen Atas"
                            + "|Jumlah Line pada Segmen Bawah"
                            + "|Jumlah Curve pada Segmen Atas"
                            + "|Jumlah Curve pada Segmen Bawah";
                    }
                    
                    if (_matlabOptimized) columns += "|Representasi Angka";
                    else columns += "|Nama Huruf";

                    if (_enrichFeatureExtraction) columns += "|Rho, theta";
                    
                    // check for test data
                    if (_testDataNames.Contains(fileName)) columns = null;

                    // fetch choosen columns
                    _trainerData = ArrayHelper.TrimData(_trainerData, columns);

                    // remove unlabelled rows
                    int removedRow = 0;
                    List<string[]> sanitizedTrainer = new List<string[]>();
                    List<string[]> tempSanitizedTrainer = new List<string[]>();
                    string label; bool containForbiddenLetter;
                    for (i = 0; i < _trainerData.Length; i++)
                    {
                        label = _trainerData[i][0];
                        containForbiddenLetter = false;
                        _letterException.ForEach(x => { if (label.Contains(x)) containForbiddenLetter = true; });
                        if (!string.IsNullOrEmpty(label) && !string.IsNullOrWhiteSpace(label) &&
                            !containForbiddenLetter)
                        {
                            sanitizedTrainer.Add(_trainerData[i]);
                            tempSanitizedTrainer.Add(rawTrainer[i]);
                        }
                        else removedRow++;
                    }
                    _trainerData = sanitizedTrainer.ToArray();
                    rawTrainer = tempSanitizedTrainer.ToArray();

                    displayTxt += "\r\nRemoved " + removedRow + " unnamed/forbidden letter(s)\r\n";

                    // remove duplicate features
                    List<string> featuresExist = new List<string>();
                    sanitizedTrainer = new List<string[]>();
                    tempSanitizedTrainer = new List<string[]>();
                    string[] features;
                    int initialDataCount = _trainerData.Length;
                    for (i = 0; i < _trainerData.Length; i++)
                    {
                        features = new string[_trainerData[i].Length - 1];
                        for (int j = 0; j < _trainerData[i].Length; j++)
                        {
                            if (j == 0) continue;
                            features[j - 1] = _trainerData[i][j];
                        }

                        // duplicate features check
                        if (featuresExist.Contains(Parser.ArrayToString(features)))
                        {
                            var kickStarter = sanitizedTrainer.Where(value => String.Join(" ", value).
                                Contains(String.Join(" ", features))).ToArray();
                            if (_trainerData[i][0] == kickStarter[0][0])
                                displayTxt += "\r\n\"" + _trainerData[i][0] + "\" at line " + (i + 1) + " already exist\r\n";
                            else
                                displayTxt += "\r\n\"" + _trainerData[i][0] + "\" at line " + (i + 1) + 
                                    " have same feature as \"" + kickStarter[0][0] + "\"\r\n";
                            continue;
                        }

                        featuresExist.Add(Parser.ArrayToString(features));
                        sanitizedTrainer.Add(_trainerData[i]);
                        tempSanitizedTrainer.Add(rawTrainer[i]);
                    }
                    _trainerData = sanitizedTrainer.ToArray();
                    rawTrainer = tempSanitizedTrainer.ToArray();

                    // training data limiter
                    if(LimitTrainingData)
                    {
                        #region Limit same letters in a document to be taken as training data
                        // limit same letters to be trained
                        int limit = MaxClassVariancePerDoc, tempCounter;
                        sanitizedTrainer = new List<string[]>();
                        tempSanitizedTrainer = new List<string[]>();
                        List<string[]> excludedData = new List<string[]>();
                        for (i = 0; i < _trainerData.Length; i++)
                        {
                            if (i == 0)
                            {
                                sanitizedTrainer.Add(_trainerData[i]);
                                tempSanitizedTrainer.Add(rawTrainer[i]);
                                excludedData.Add(rawTrainer[i]);
                                continue;
                            }
                            label = _trainerData[i][0];
                            tempCounter = sanitizedTrainer.Where(x => x[0] == label).Count();
                            if (tempCounter < limit)
                            {
                                sanitizedTrainer.Add(_trainerData[i]);
                                tempSanitizedTrainer.Add(rawTrainer[i]);
                            }
                            else excludedData.Add(rawTrainer[i]);
                        }
                        _trainerData = sanitizedTrainer.ToArray();
                        #endregion

                        #region PNN data limiter
                        if (LimitPNNData)
                        {
                            // limit letters in PNN network
                            if (_pnnNetwork != null)
                            {
                                string[][] sanitizedRawTrainer = tempSanitizedTrainer.ToArray();
                                List<string> storedTrainer = new List<string>();
                                string[] tempArray = new string[_pnnNetwork.ClassLabels.Count];
                                _pnnNetwork.ClassLabels.CopyTo(tempArray);
                                storedTrainer = tempArray.ToList();
                                int initialCount = _pnnNetwork.ClassLabels.Count;
                                limit = PNNDataLimit;
                                sanitizedTrainer = new List<string[]>();
                                tempSanitizedTrainer = new List<string[]>();
                                for (i = 0; i < _trainerData.Length; i++)
                                {
                                    label = _trainerData[i][0];
                                    tempCounter = storedTrainer.Where(x => x == label).Count();
                                    if (tempCounter < limit)
                                    {
                                        sanitizedTrainer.Add(_trainerData[i]);
                                        storedTrainer.Add(label);
                                        tempSanitizedTrainer.Add(sanitizedRawTrainer[i]);
                                    }
                                    else excludedData.Add(sanitizedRawTrainer[i]);
                                }
                                _trainerData = sanitizedTrainer.ToArray();

                                if (initialCount != _pnnNetwork.ClassLabels.Count) Console.WriteLine("Invalid class labels count !");
                                else Console.WriteLine("Class labels count is valid !");

                                if (_trainerData.Length <= 1)
                                {
                                    displayTxt += "no new data is added\r\n\r\n";
                                    updateVisualizer(displayTxt);
                                    return false;
                                }
                            }
                        }
                        #endregion

                        #region Training & classification data separation
                        // separate training & classification data
                        if(SeparateTrainingData)
                        {
                            // write sanitized & excluded data to separate files
                            string filePath, txtToWrite;
                            TextWriter writer;
                            // 1. sanitized data
                            txtToWrite = "";
                            filePath = Path.GetDirectoryName(dialogBox.FileName) + "\\Used for training.csv";
                            string parentPath = Directory.GetParent(Path.GetDirectoryName(dialogBox.FileName)) +
                                "\\Collective training data.csv";
                            //Console.WriteLine("path : {0}", parentPath);
                            if (TrainNewData)
                            {
                                writer = new StreamWriter(filePath);
                                tempSanitizedTrainer.ForEach(x => txtToWrite += string.Join(";", x) + "\r\n");
                                writer.Write(txtToWrite);
                                writer.Close();
                                // write all training data into 1 file
                                writer = new StreamWriter(parentPath);
                                writer.Write(txtToWrite);
                                writer.Close();
                            }
                            else
                            {
                                writer = File.AppendText(filePath);
                                for (i = 0; i < tempSanitizedTrainer.Count; i++)
                                    if (i > 0)
                                        txtToWrite += string.Join(";", tempSanitizedTrainer[i]) + "\r\n";
                                writer.Write(txtToWrite);
                                writer.Close();
                                // write all training data into 1 file
                                writer = File.AppendText(parentPath);
                                writer.Write(txtToWrite);
                                writer.Close();
                            }
                            // 2. excluded data
                            txtToWrite = "";
                            filePath = Path.GetDirectoryName(dialogBox.FileName) + "\\Excluded from trainer.csv";
                            writer = new StreamWriter(filePath);
                            excludedData.ForEach(x => txtToWrite += string.Join(";", x) + "\r\n");
                            writer.Write(txtToWrite);
                            writer.Close();
                        }
                        #endregion
                    }

                    // display process
                    if (visualizeMethod.Checked)
                    {
                        displayTxt += "\r\nINPUT TRIMMED TO :\r\n";
                        displayTxt += Parser.ArrayToString(_trainerData);
                        displayTxt += "\r\n" + (initialDataCount - _trainerData.Length) + 
                            " duplicate feature(s) removed";
                    }

                    displayTxt += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                    updateVisualizer(displayTxt);

                    return true;
                }

                displayTxt += "Failed to load data, please try again";
                displayTxt += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                updateVisualizer(displayTxt);
            }

            return false;
        }

        private void train(string method)
        {
            if (!loadTrainerFile())
                return;

            var processStart = DateTime.Now;
            var displayText = "";
                
            // PNN process
            if (method == "pnn")
            {
                displayText += "TRAINING START, METHOD USED : PNN\r\n\r\n";

                // get spread from user input
                string strSpread = Interaction.InputBox("Input spread value", "Spread", "1");
                double spread;
                while (!Double.TryParse(strSpread, out spread))
                    strSpread = Interaction.InputBox("Please input a numeric value", "Spread", "1");

                // train network
                _pnnNetwork = new PNN();
                bool trainingDone = _pnnNetwork.Train(_trainerData, spread, 0, 0);
                if(!trainingDone)
                {
                    displayText += "Training failed ! Please try again";
                    displayText += "\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                    updateVisualizer(displayText);
                    return;
                }

                // update display text
                if(visualizeMethod.Checked)
                {
                    displayText += "Spread value : " + spread + "\r\n\r\n";
                    displayText += _pnnNetwork.Display();
                }

                // enable the "train existing network" option
                trainExistingNetworkToolStripMenuItem.Visible = true;
            }
            // ID3 process 
            else if (method == "id3")
            {
                // build tree
                var treeSet = Parser.ArrayToDecisionTreeSet(_trainerData, 0, 0);
                _id3Network = treeSet.BuildTree();
                
                // display tree
                if (visualizeMethod.Checked)
                {
                    displayText += "TRAINING START, METHOD USED : DECISION TREE (ID3)\r\n\r\n";
                    displayText += _id3Network.display();
                    displayText += "\r\n";
                }

                // validate tree
                displayText += "ERRORS : \r\n";
                int totalData = 0, error = 0;
                foreach (var instance in treeSet.Instances)
                {
                    totalData++;
                    try
                    {
                        Assert.That(Tree.ProcessInstance(_id3Network, instance).Value, Is.EqualTo(instance.Output.Value));
                    }
                    catch(Exception ex)
                    {
                        error++;
                        displayText += ex.Message + ex.InnerException + ex.TargetSite + "\r\n";
                    }
                }

                displayText += error + " errors of " + totalData + " data\r\n\r\n";
            }

            // calculate total process time
            var processEnd = DateTime.Now;
            var processDuration = processEnd - processStart;
            var totalDuration = processDuration.Minutes + " min "
                            + processDuration.Seconds + " sec "
                            + processDuration.Milliseconds + " mili sec";

            // update visualizer
            displayText += "Training finished ! ( " + totalDuration + " )\r\n";
            displayText += "\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
            updateVisualizer(displayText);
        }

        #endregion

        #region Classifier Methods

        private Boolean loadFileToClassify()
        {
            var dialogBox = new OpenFileDialog();
            dialogBox.Filter = "CSV File (.csv)|*.csv";
            dialogBox.Title = "Select data to be classfied";

            if (dialogBox.ShowDialog() == DialogResult.OK)
            {
                // string to display in visualizer
                string displayTxt = "";

                // load external file
                _rawClassificationData = Parser.CSVToArray(@dialogBox.FileName);
                if(_enrichFeatureExtraction)
                    _rawClassificationData = ClassifierNetworkkHelper.LeverageMultipleData(_rawClassificationData);

                if (_rawClassificationData != null)
                {
                    // trim filename 
                    var fileName = dialogBox.FileName.Substring(dialogBox.FileName.LastIndexOf('\\') + 1);
                    // if data loeaded, update text to display
                    displayTxt += "File to classify loaded : " + fileName;
                    // display data matrix
                    if(visualizeMethod.Checked)
                        displayTxt += "\r\nContent : \r\n" + Parser.ArrayToString(_rawClassificationData);

                    // set columns to fetch
                    string columns = "";
                    if(_88Features)
                    {
                        columns += "F1";
                        for (int i = 2; i <= 88; i++)
                            columns += "|F" + i;
                    }
                    else
                    {
                        columns += "Jumlah Loop pada Gambar"
                            + "|Jumlah Loop pada Segmen Atas"
                            + "|Jumlah Loop pada Segmen Bawah"
                            + "|Jumlah Line pada Segmen Atas"
                            + "|Jumlah Line pada Segmen Bawah"
                            + "|Jumlah Curve pada Segmen Atas"
                            + "|Jumlah Curve pada Segmen Bawah";
                    }
                    
                    columns += "|X"
                        + "|Y"
                        + "|Width"
                        + "|Height"
                        + "|Image Data"
                        + "|Parent Folder"
                        + "|Segment Image";

                    if (_enrichFeatureExtraction)
                        columns += "|Rho, theta";

                    // check for test data
                    if (_testDataNames.Contains(fileName))
                        columns = null;

                    // sanitize data
                    _rawClassificationData = ArrayHelper.TrimData(_rawClassificationData, columns);

                    if(visualizeMethod.Checked)
                        displayTxt += "\r\nSanitized content : \r\n" + Parser.ArrayToString(_rawClassificationData);

                    displayTxt += "\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                    updateVisualizer(displayTxt);

                    return true;
                }

                displayTxt += "Fail to load file to classify, please try again";
                displayTxt += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                updateVisualizer(displayTxt);
            }

            return false;
        }

        private void classify(string method)
        {
            var proceedNextStep = true;
            object network;
            var displayText = "";

            if (method == "pnn") network = _pnnNetwork;
            else network = _id3Network;

            // untrained network check
            if(network == null)
            {
                MessageBox.Show("Untrained network", "Classification Error");
                proceedNextStep = false;
            }

            if(proceedNextStep)
            {
                if (!loadFileToClassify()) return;

                var processStart = DateTime.Now;
                string[] classificationRes;
                string fileName;
                int totalTrainingData = 0,
                    carakanTrainingData = 0,
                    pasanganTrainingData = 0,
                    sandhanganTrainingData = 0;

                #region PNN classification
                // PNN process
                if (method == "pnn")
                {
                    displayText += "CLASSIFICATION START, METHOD USED : PNN\r\n\r\n";

                    // remove unnecessary data
                    string columns = "";
                    if(_88Features)
                    {
                        columns += "F1";
                        for (int i = 2; i <= 88; i++)
                            columns += "|F" + i;
                    }
                    else
                    {
                        columns += "Jumlah Loop pada Gambar"
                            + "|Jumlah Loop pada Segmen Atas"
                            + "|Jumlah Loop pada Segmen Bawah"
                            + "|Jumlah Line pada Segmen Atas"
                            + "|Jumlah Line pada Segmen Bawah"
                            + "|Jumlah Curve pada Segmen Atas"
                            + "|Jumlah Curve pada Segmen Bawah";
                    }
                    
                    if (_enrichFeatureExtraction)
                        columns += "|Rho, theta";
                    var halfBakedData = ArrayHelper.TrimData(_rawClassificationData, columns, true);

                    // parse arr of str into arr of double
                    var bakedData = Parser.StringToDecimal(halfBakedData);

                    // start classification
                    classificationRes = _pnnNetwork.Classify(bakedData);

                    // set result file name
                    fileName = "PNN Classification Result";

                    // set training data count
                    totalTrainingData = _pnnNetwork.TotalTrainingData;
                    // set training data count for separate categories
                    if(SeparateClassificationCategory)
                    {
                        carakanTrainingData = _pnnNetwork.ClassLabels.Where(x => AksaraCarakanList.Contains(x.Trim())).Count();
                        pasanganTrainingData = _pnnNetwork.ClassLabels.Where(x => x.Contains("pasangan")).Count();
                        sandhanganTrainingData = totalTrainingData - (carakanTrainingData + pasanganTrainingData);
                    }
                }
                #endregion
                #region ID3 Classification
                // ID3 process
                else
                {
                    displayText += "CLASSIFICATION START, METHOD USED : ID3\r\n\r\n";

                    // remove unnecessary data
                    string columns = "";
                    if (_88Features)
                    {
                        columns += "F1";
                        for (int i = 2; i <= 88; i++)
                            columns += "|F" + i;
                    }
                    else
                    {
                        columns += "Jumlah Loop pada Gambar"
                            + "|Jumlah Loop pada Segmen Atas"
                            + "|Jumlah Loop pada Segmen Bawah"
                            + "|Jumlah Line pada Segmen Atas"
                            + "|Jumlah Line pada Segmen Bawah"
                            + "|Jumlah Curve pada Segmen Atas"
                            + "|Jumlah Curve pada Segmen Bawah";
                    }

                    if (_enrichFeatureExtraction)
                        columns += "|Rho, theta";
                    var halfBakedData = ArrayHelper.TrimData(_rawClassificationData, columns);

                    // start classification
                    classificationRes = _id3Network.Classify(halfBakedData);

                    // set result file name
                    fileName = "ID3 Classification Result";

                    totalTrainingData = _id3Network.TotalTrainingData;
                }
                #endregion

                if (!_enrichFeatureExtraction) fileName += " (standard feature)";
                else fileName += " (enriched feature)";

                if (classificationRes != null)
                {
                    #region Convert classification result into unicode representation
                    // convert result to lowercase & remove extra whitespaces
                    for (int i = 0; i < classificationRes.Length; i++)
                    {
                        if(classificationRes[i] != null)
                            classificationRes[i] = classificationRes[i].ToLower().Trim();
                    }

                    var originalClassificationRes = new string[classificationRes.Length];
                    Array.Copy(classificationRes, originalClassificationRes, classificationRes.Length);
                        
                    var characterMap = ArrayHelper.TrimData(_rawClassificationData, null, true);
                    /* character map index :
                     * - col 0 = Image Data
                     * - col 1 = Parent Folder
                     * - col 2 = Segment Image
                     * - col 3 = X
                     * - col 4 = Y
                     * - col 5 = Width
                     * - col 6 = Height
                     */
                    
                    // get character line map
                    var lineMap = new int[characterMap.Length];
                    for (int i = 0; i < lineMap.Length; i++)
                    {
                        var trimmedString = characterMap[i][1].Substring(1);
                        var splittedString = trimmedString.Split('-');
                        lineMap[i] = int.Parse(splittedString[0]);
                    }

                    // normal alphabet reading, relying on segmentation alone
                    if (!_enchanceReading)
                    {
                        // adjust alphabet position
                        for (int i = 0; i < classificationRes.Length; i++)
                        {
                            // character above main alphabet
                            if (_upperCharacter.Contains(classificationRes[i]))
                            {
                                try
                                {
                                    classificationRes[i + 1] += "+" + classificationRes[i];
                                    classificationRes[i] = "-";
                                }
                                catch(Exception ex)
                                {
                                    Console.WriteLine("Error when adjusting alphabet position\nUpper chareacter\nError message : {0}", ex.Message);
                                }
                                continue;
                            }
                        }
                    }

                    // convert result into its unicode representation
                    for (int i = 0; i < classificationRes.Length; i++)
                    {
                        try
                        {
                            if (classificationRes[i] == null) continue;
                            if (classificationRes[i].Contains('+'))
                            {
                                var split = classificationRes[i].Split('+');
                                for (int j = 0; j < split.Length; j++)
                                {
                                    if (!String.IsNullOrEmpty(split[j]))
                                        split[j] = _javaUnicode[split[j]];
                                }
                                classificationRes[i] = Parser.ArrayToString(split, "");
                                continue;
                            }
                            classificationRes[i] = _javaUnicode[classificationRes[i]];
                        }
                        catch
                        {
                            Console.WriteLine("{0} at iteration-{1} is not recognized in dictionary", classificationRes[i], i);
                        }
                    }
                    
                    // enqueue final result
                    var finalResult = "";
                    var lastLine = lineMap[0];
                    for (int i = 0; i < classificationRes.Length; i++)
                    {
                        if (classificationRes[i] == null) continue;
                        if(lineMap[i] > lastLine)
                        {
                            finalResult += "\r\n";
                            lastLine = lineMap[i];
                        }
                        finalResult += classificationRes[i];
                        if (i < classificationRes.Length - 1) finalResult += " ";
                    }
                    #endregion

                    // calculate process duration
                    var processEnd = DateTime.Now;
                    var processDuration = processEnd - processStart;
                    var totalDuration = processDuration.Minutes + " min "
                            + processDuration.Seconds + " sec "
                            + processDuration.Milliseconds + " mili sec";

                    // update display text
                    displayText += "CLASSIFICATION RESULT : \r\n";
                    displayText += finalResult + "\r\n\r\n";
                    displayText += "Classify finished ! ( " + totalDuration + " )";

                    // save result to a text file
                    var saveFile = MessageBox.Show("Save result to file ?", "Classification Done", MessageBoxButtons.YesNo);
                    if (saveFile == DialogResult.Yes)
                    {
                        SaveFileDialog savedFile = new SaveFileDialog();
                        savedFile.Filter = "Text files (*.txt)|*.txt";
                        savedFile.FileName = fileName;
                        if (savedFile.ShowDialog() == DialogResult.OK)
                        {
                            using (TextWriter writer = new StreamWriter(@savedFile.FileName))
                            {
                                writer.Write(finalResult);
                            }
                            if (_createCSVResult)
                            {
                                #region Set file name & path
                                // init filename & location
                                var index = savedFile.FileName.LastIndexOf("\\") + 1;
                                var length = index - 0;
                                var fileLocation = savedFile.FileName.Substring(0, length);
                                fileName = savedFile.FileName.Substring(savedFile.FileName.LastIndexOf('\\') + 1);
                                fileName = fileName.Substring(0, fileName.IndexOf('.'));
                                var saveFileName = fileLocation + fileName + " - CSV Result";
                                #endregion

                                #region Decide whether to use auto data testing or not
                                // init data testing
                                string[][] testerData = null;
                                double correct = 0, 
                                    carakanCorrect = 0,
                                    pasanganCorrect = 0,
                                    sandhanganCorrect = 0,
                                    wrong = 0,
                                    carakanWrong = 0,
                                    pasanganWrong = 0,
                                    sandhanganWrong = 0,
                                    totalData = 0,
                                    carakanTotal = 0,
                                    pasanganTotal = 0,
                                    sandhanganTotal = 0;
                                int headerColIndex = 3;
                                if (AutoDataTest)
                                {
                                    bool found = false;
                                    foreach(var val in DataTester)
                                    {
                                        var path = fileLocation + val + ".csv";
                                        if (File.Exists(path))
                                        {
                                            testerData = Parser.CSVToArray(path);
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found) AutoDataTest = false;
                                }
                                #endregion

                                var CSVResult = "";
                                string carakanResult = "", pasanganResult = "", sandhanganResult = "";
                                string aksaraType = "carakan";

                                // create CSV result
                                if (_createCSVResult)
                                {
                                    for (int i = 0; i < _rawClassificationData.Length; i++)
                                    {
                                        for (int j = 0; j < _rawClassificationData[i].Length; j++)
                                        {
                                            if (i == 0 && j == 0)
                                            {
                                                // write column "hasil"
                                                CSVResult += "Hasil;";
                                                if(SeparateClassificationCategory)
                                                {
                                                    // write column "hasil" for separate categories
                                                    carakanResult += "Hasil;";
                                                    pasanganResult += "Hasil;";
                                                    sandhanganResult += "Hasil;";
                                                }
                                                // write column "Nama Huruf Seharusnya" & "Benar/Salah"
                                                if (AutoDataTest)
                                                {
                                                    CSVResult += "Nama Huruf Seharusnya;Benar/Salah;";
                                                    if(SeparateClassificationCategory)
                                                    {
                                                        // write column "Nama Huruf Seharusnya" & "Benar/Salah"
                                                        // for separate categories
                                                        carakanResult += "Nama Huruf Seharusnya;Benar/Salah;";
                                                        pasanganResult += "Nama Huruf Seharusnya;Benar/Salah;";
                                                        sandhanganResult += "Nama Huruf Seharusnya;Benar/Salah;";
                                                    }
                                                }
                                            }
                                            else if (i > 0 && j == 0)
                                            {
                                                // write classification result
                                                var classificationResult = originalClassificationRes[i - 1];
                                                CSVResult += classificationResult + ";";
                                                if(SeparateClassificationCategory)
                                                {
                                                    // set what type of aksara is being classified
                                                    if (AksaraCarakanList.Contains(classificationResult.Trim()))
                                                        aksaraType = "carakan";
                                                    else if (classificationResult.Contains("pasangan"))
                                                        aksaraType = "pasangan";
                                                    else
                                                        aksaraType = "sandhangan";
                                                    // write classification result for separate categories
                                                    if (aksaraType == "carakan")
                                                        carakanResult += classificationResult + ";";
                                                    else if (aksaraType == "pasangan")
                                                        pasanganResult += classificationResult + ";";
                                                    else
                                                        sandhanganResult += classificationResult + ";";
                                                }

                                                if(AutoDataTest)
                                                {
                                                    // change header index for matlab
                                                    if (_matlabOptimized) headerColIndex = 4;

                                                    // fill the value of column "Nama Huruf Seharusnya"
                                                    var originalData = testerData[i][headerColIndex];
                                                    CSVResult += originalData + ";";
                                                    if(SeparateClassificationCategory)
                                                    {
                                                        // fill the value of column "Nama Huruf Seharusnya" 
                                                        // for separate categories
                                                        if (aksaraType == "carakan") carakanResult += originalData + ";";
                                                        else if (aksaraType == "pasangan") pasanganResult += originalData + ";";
                                                        else sandhanganResult += originalData + ";";
                                                    }

                                                    var containsForbiddenCharacter = false;
                                                    _letterException.ForEach(val => { if (originalData.Contains(val)) { containsForbiddenCharacter = true; return; } });

                                                    // the test
                                                    if (!String.IsNullOrWhiteSpace(originalData) && !containsForbiddenCharacter)
                                                    {
                                                        // count total data classified
                                                        totalData ++;
                                                        // count total data classified for separate categories
                                                        if(SeparateClassificationCategory)
                                                        {
                                                            if (aksaraType == "carakan") carakanTotal++;
                                                            else if (aksaraType == "pasangan") pasanganTotal++;
                                                            else sandhanganTotal++;
                                                        }

                                                        string answer;
                                                        if (originalData == classificationResult)
                                                        {
                                                            answer = "benar";
                                                            correct++;
                                                            // decide answer for separate categories
                                                            if(SeparateClassificationCategory)
                                                            {
                                                                if (aksaraType == "carakan") carakanCorrect++;
                                                                else if (aksaraType == "pasangan") pasanganCorrect++;
                                                                else sandhanganCorrect++;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            answer = "salah";
                                                            wrong++;
                                                            // decide answer for separate categories
                                                            if (SeparateClassificationCategory)
                                                            {
                                                                if (aksaraType == "carakan") carakanWrong++;
                                                                else if (aksaraType == "pasangan") pasanganWrong++;
                                                                else sandhanganWrong++;
                                                            }
                                                        }
                                                        // write the answer 
                                                        CSVResult += answer;
                                                        if (SeparateClassificationCategory)
                                                        {
                                                            // write the answer for separate categories
                                                            if (aksaraType == "carakan") carakanResult += answer;
                                                            else if (aksaraType == "pasangan") pasanganResult += answer;
                                                            else sandhanganResult += answer;
                                                        }
                                                    }
                                                    // add delimiter
                                                    CSVResult += ";";
                                                    if(SeparateClassificationCategory)
                                                    {
                                                        // write delimiter for separate categories
                                                        if (aksaraType == "carakan") carakanResult += ";";
                                                        else if (aksaraType == "pasangan") pasanganResult += ";";
                                                        else sandhanganResult += ";";
                                                    }
                                                }
                                            }

                                            // add the rest of columns to result file
                                            CSVResult += _rawClassificationData[i][j];
                                            if (j < _rawClassificationData[i].Length - 1)
                                                CSVResult += ";";
                                            if(SeparateClassificationCategory)
                                            {
                                                // add the rest of columns to result file for separate categories
                                                if (aksaraType == "carakan")
                                                {
                                                    carakanResult += _rawClassificationData[i][j];
                                                    if (j < _rawClassificationData[i].Length - 1)
                                                        carakanResult += ";";
                                                }
                                                else if (aksaraType == "pasangan")
                                                {
                                                    pasanganResult += _rawClassificationData[i][j];
                                                    if (j < _rawClassificationData[i].Length - 1)
                                                        pasanganResult += ";";
                                                }
                                                else
                                                {
                                                    sandhanganResult += _rawClassificationData[i][j];
                                                    if (j < _rawClassificationData[i].Length - 1)
                                                        sandhanganResult += ";";
                                                }
                                            }
                                        }

                                        // add newline
                                        if (i < _rawClassificationData.Length - 1)
                                        {
                                            CSVResult += "\r\n";
                                            // add newline for separate categories
                                            if(SeparateClassificationCategory)
                                            {
                                                if (aksaraType == "carakan") carakanResult += "\r\n";
                                                else if (aksaraType == "pasangan") pasanganResult += "\r\n";
                                                else sandhanganResult += "\r\n";
                                            }
                                        }
                                    }

                                    // calc correct/wrong
                                    double correctPercentageCalc = correct / totalData * 100;
                                    string correctPercentage = Math.Round(correctPercentageCalc, 2) + " %";
                                    double wrongPercentageCalc = wrong / totalData * 100;
                                    string wrongPercentage = Math.Round(wrongPercentageCalc, 2) + " %";

                                    // write report
                                    CSVResult += "\r\n\r\n" + "Success";
                                    if (AutoDataTest)
                                        CSVResult += ";" + correct + ";" + correctPercentage;
                                    CSVResult += "\r\n" + "Fail";
                                    if (AutoDataTest)
                                        CSVResult += ";" + wrong + ";" + wrongPercentage;
                                    CSVResult += "\r\n" + "Total data classified;" + totalData;
                                    CSVResult += "\r\n" + "Total training data;" + totalTrainingData;
                                    CSVResult += "\r\nClassification time;" + totalDuration;

                                    if(SeparateClassificationCategory)
                                    {
                                        // calc carakan correct/wrong
                                        double carakanCorrectPercentageCalc = carakanCorrect / carakanTotal * 100;
                                        string carakanCorrectPercentage = Math.Round(correctPercentageCalc, 2) + " %";
                                        double carakanWrongPercentageCalc = carakanWrong / carakanTotal * 100;
                                        string carakanWrongPercentage = Math.Round(wrongPercentageCalc, 2) + " %";
                                        // calc pasangan correct/wrong
                                        double pasanganCorrectPercentageCalc = pasanganCorrect / pasanganTotal * 100;
                                        string pasanganCorrectPercentage = Math.Round(correctPercentageCalc, 2) + " %";
                                        double pasanganWrongPercentageCalc = pasanganWrong / pasanganTotal * 100;
                                        string pasanganWrongPercentage = Math.Round(wrongPercentageCalc, 2) + " %";
                                        // calc sandhangan correct/wrong
                                        double sandhanganCorrectPercentageCalc = sandhanganCorrect / sandhanganTotal * 100;
                                        string sandhanganCorrectPercentage = Math.Round(correctPercentageCalc, 2) + " %";
                                        double sandhanganWrongPercentageCalc = sandhanganWrong / sandhanganTotal * 100;
                                        string sandhanganWrongPercentage = Math.Round(wrongPercentageCalc, 2) + " %";

                                        // write carakan report
                                        carakanResult += "\r\n\r\n" + "Success";
                                        if (AutoDataTest)
                                            carakanResult += ";" + carakanCorrect + ";" + carakanCorrectPercentage;
                                        carakanResult += "\r\n" + "Fail";
                                        if (AutoDataTest)
                                            carakanResult += ";" + carakanWrong + ";" + carakanWrongPercentage;
                                        carakanResult += "\r\n" + "Total data classified;" + carakanTotal;
                                        carakanResult += "\r\n" + "Total training data;" + carakanTrainingData;
                                        carakanResult += "\r\nClassification time;" + totalDuration;
                                        // write pasangan report
                                        pasanganResult += "\r\n\r\n" + "Success";
                                        if (AutoDataTest)
                                            pasanganResult += ";" + pasanganCorrect + ";" + pasanganCorrectPercentage;
                                        pasanganResult += "\r\n" + "Fail";
                                        if (AutoDataTest)
                                            pasanganResult += ";" + pasanganWrong + ";" + pasanganWrongPercentage;
                                        pasanganResult += "\r\n" + "Total data classified;" + pasanganTotal;
                                        pasanganResult += "\r\n" + "Total training data;" + pasanganTrainingData;
                                        pasanganResult += "\r\nClassification time;" + totalDuration;
                                        // write sandhangan report
                                        sandhanganResult += "\r\n\r\n" + "Success";
                                        if (AutoDataTest)
                                            sandhanganResult += ";" + sandhanganCorrect + ";" + sandhanganCorrectPercentage;
                                        sandhanganResult += "\r\n" + "Fail";
                                        if (AutoDataTest)
                                            sandhanganResult += ";" + sandhanganWrong + ";" + sandhanganWrongPercentage;
                                        sandhanganResult += "\r\n" + "Total data classified;" + sandhanganTotal;
                                        sandhanganResult += "\r\n" + "Total training data;" + sandhanganTrainingData;
                                        sandhanganResult += "\r\nClassification time;" + totalDuration;
                                    }
                                }

                                // save the file
                                TextWriter writer = new StreamWriter(saveFileName + ".csv");
                                writer.Write(CSVResult);
                                writer.Close();
                                // save separated categories file
                                writer = new StreamWriter(saveFileName + " carakan.csv");
                                writer.Write(carakanResult);
                                writer.Close();
                                writer = new StreamWriter(saveFileName + " pasangan.csv");
                                writer.Write(pasanganResult);
                                writer.Close();
                                writer = new StreamWriter(saveFileName + " sandhangan.csv");
                                writer.Write(sandhanganResult);
                                writer.Close();
                            }
                        }
                    }
                }
                else
                {
                    proceedNextStep = false;
                }
            }

            if(!proceedNextStep)
                displayText += "Classification failed, please try again";

            // update visualizer
            displayText += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
            updateVisualizer(displayText);
        }

        #endregion

        #region Serialization Methods

        private void serialize(string method)
        {
            // init save file dialog properties (general)
            SaveFileDialog savedFile = new SaveFileDialog();
            savedFile.Filter = "XML File (.xml)|*.xml";

            var displayText = "";
            var networkTrained = true;
            object network;

            if(method == "pnn")
            {
                network = _pnnNetwork;
                savedFile.FileName = "Serialized PNN.xml";
            }
            else
            {
                network = _id3Network;
                savedFile.FileName = "Serialized ID3.xml";
            }

            // untrained network check
            if(network == null)
            {
                MessageBox.Show("Cannot serialize untrained network !", "Serialization Error");
                networkTrained = false;
            }

            if(networkTrained)
            {
                // open the save dialog
                if (savedFile.ShowDialog() == DialogResult.OK)
                {
                    // start calculating process duration
                    DateTime start = DateTime.Now;

                    // serialize network
                    var serializer = new DataContractSerializer(network.GetType());
                    using (Stream stream = new FileStream(@savedFile.FileName, FileMode.Create, FileAccess.Write))
                    {
                        using (XmlDictionaryWriter writer = XmlDictionaryWriter.CreateTextWriter(stream, Encoding.UTF8))
                        {
                            writer.WriteStartDocument();
                            serializer.WriteObject(writer, network);
                        }
                    }

                    // calculate total process duration
                    DateTime end = DateTime.Now;
                    TimeSpan duration = end - start;
                    var totalDuration = duration.Minutes + " min "
                            + duration.Seconds + " sec "
                            + duration.Milliseconds + " mili sec";
                    // display duration in visualizer
                    displayText += savedFile.FileName + " serialized ! ( " + totalDuration + " )";
                }
                else
                {
                    return;
                }
            }
            else
            {
                displayText += "Serialization fail, please try again";
            }
            
            // update visualizer
            displayText += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
            updateVisualizer(displayText);
        }

        private void deserialize(string method)
        {
            // init dialog box
            var dialogBox = new OpenFileDialog();
            dialogBox.Filter = "XML File (.xml)|*.xml";
            dialogBox.Title = "Select data to be deserialized";

            // open dialog box
            if(dialogBox.ShowDialog() == DialogResult.OK)
            {
                // processStart calculating process duration
                DateTime processStart = DateTime.Now;

                Type networkType;
                if(method == "pnn")
                    networkType = typeof(PNN);
                else
                    networkType = typeof(Tree);

                DataContractSerializer deserializer = new DataContractSerializer(networkType);
                Stream fileStream = new FileStream(@dialogBox.FileName, FileMode.Open);
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fileStream, new XmlDictionaryReaderQuotas());

                var deserializationSuccess = true;
                try
                {
                    if (networkType == typeof(PNN))
                    {
                        _pnnNetwork = (PNN)deserializer.ReadObject(reader);
                        if (_pnnNetwork != null)
                            trainExistingNetworkToolStripMenuItem.Visible = true;
                    }
                    else
                    {
                        _id3Network = (Tree)deserializer.ReadObject(reader);
                    }
                }
                catch(SerializationException ex)
                {
                    MessageBox.Show("Data mismatch with current network\n\n" + ex.Message, "Deserialization Error");
                    deserializationSuccess = false;
                }

                reader.Close();
                fileStream.Close();

                var txt = "";

                if (deserializationSuccess)
                {
                    if (visualizeMethod.Checked)
                    {
                        txt += "Deserialized network content : \r\n";
                        if (networkType == typeof(PNN))
                            txt += _pnnNetwork.Display();
                        else
                            txt += _id3Network.display();
                    }

                    // calculate total process duration
                    DateTime processEnd = DateTime.Now;
                    TimeSpan duration = processEnd - processStart;
                    var totalDuration = duration.Minutes + " min "
                            + duration.Seconds + " sec "
                            + duration.Milliseconds + " mili sec";

                    // trim filename 
                    var fileName = dialogBox.FileName.Substring(dialogBox.FileName.LastIndexOf('\\') + 1);

                    txt += fileName + " deserialized ! ( " + totalDuration + " )";
                }
                else
                {
                    txt += "Deserialization fail, please try again";
                }

                // show confirmation in visualizer
                txt += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                updateVisualizer(txt);
            }
        }

        #endregion

        private void updateVisualizer(string txt)
        {
            methodVisualizer.Text += txt;
            methodVisualizer.Focus();
            methodVisualizer.SelectionStart = methodVisualizer.Text.Length;
            methodVisualizer.ScrollToCaret();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

    } // end of class

} // end of namespace
