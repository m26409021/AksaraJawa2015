﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Drawing;
using System.Windows.Forms;

namespace AksaraJawa2015
{
    class HoughInfo
    {
        public LineSegment2D _line;
        public int _rho, _theta;
        public HoughInfo()
        {

            _line = new LineSegment2D();
            _rho = _theta = 0;
        }
        public HoughInfo(LineSegment2D line, int rho, int theta)
        {
            _line = line;
            _rho = rho;
            _theta = theta;
        }
    }
    class HoughLine
    {
        private Image<Gray, byte> _img;
        static int MAXTHETA = 180;
        private int _w, _h, _rhoAwlLen, _rhoLen, _thetaLen, _thetastep, _numLines;
        private int[,] _accArray;
        private double[] _sin, _cos;

        public HoughLine()
        {
            _numLines = 0;
            _h = _w = 0;
            _img = new Image<Gray, byte>(_w, _h);
            _rhoAwlLen = _rhoLen = 0;
            _thetaLen = _thetastep = 0;
            //create cache
            _sin = new double[_thetaLen];
            _cos = new double[_thetaLen];
        }
        public void setImage(Image<Gray, byte> image, int step)
        {
            _numLines = 0;
            _w = image.Width+2;
            _h = image.Height+2;
            _img = new Image<Gray, byte>(_w, _h);
            for (int i = 0; i < _h; i++)
                for (int j = 0; j < _w; j++)
                    if (i > 0 && i < _h - 1 && j > 0 && j < _w - 1)
                        _img.Data[i, j, 0] = image.Data[i - 1, j - 1, 0];
                    else
                        _img.Data[i, j, 0] = 0;
            //D = -D -> D
            _rhoAwlLen = (int)Math.Sqrt(_w * _w + _h * _h);
            _rhoLen = _rhoAwlLen * 2;
            _thetastep = step;
            _thetaLen = MAXTHETA / _thetastep;
            _accArray = new int[_rhoLen, _thetaLen];
            for (int i = 0; i < _rhoLen; i++)
                for (int j = 0; j < _thetaLen; j++)
                    _accArray[i, j] = 0;
            //create cache
            _sin = new double[_thetaLen];
            _cos = new double[_thetaLen];
            for (int i = 0, j = 0; i < _thetaLen; i++, j += _thetastep)
            {
                _sin[i] = Math.Sin((j) * Math.PI / 180);
                _cos[i] = Math.Cos((j) * Math.PI / 180);
            }
        }

        private int votingRhoValue()
        {
            int numPoint = 0;
            for (int i = 0; i < _h; i++)
                for (int j = 0; j < _w; j++)
                    if (_img.Data[i, j, 0] == 1)
                    {
                        for (int t = 0; t < _thetaLen; t++)
                        {
                            //p=y*sin(0)+x*cos(0)
                            int rho = (int)(i * _sin[t] + j * _cos[t]);
                            rho = rho + _rhoAwlLen;
                            _accArray[rho, t]++;
                        }
                        numPoint++;
                    }
            return numPoint;
        }

        private List<int[]> findLocalMaxima(int threshold, int neighbor)
        {
            int i, j;
            List<int[]> rhoTheta = new List<int[]>();

            for (i = 0; i < _rhoLen; i++)
            {
                for (j = 0; j < _thetaLen; j++)
                {
                    if (_accArray[i, j] > threshold)
                    {
                        bool stat = true;
                        for (int ii = -neighbor; ii <= neighbor; ii++)
                        {
                            int y = i + ii;
                            if (y < 0) y = y + _rhoLen;
                            else if (y >= _rhoLen) y = y - _rhoLen;
                            for (int jj = -neighbor; jj <= neighbor; jj++)
                            {
                                int x = j + jj;
                                if (x < 0) x = x + _thetaLen;
                                else if (x >= _thetaLen) x = x - _thetaLen;
                                if (_accArray[y, x] > _accArray[i, j])
                                {
                                    stat = false;
                                    ii = neighbor + 1;
                                    jj = neighbor + 1;
                                }
                            }
                        }
                        if (stat)
                        {
                            //BINARY INSERTION DESCENDING
                            int start = 0, end = rhoTheta.Count - 1;
                            bool repeat = true;
                            while (repeat)
                            {
                                repeat = false;
                                if (rhoTheta.Count == 0)
                                    rhoTheta.Add(new int[]{i,j,_accArray[i,j]});
                                else if (_accArray[i, j] <= rhoTheta[end].ElementAt(2))
                                    rhoTheta.Insert(end + 1, new int[] { i, j, _accArray[i, j] });
                                else if (_accArray[i, j] >= rhoTheta[start].ElementAt(2))
                                    rhoTheta.Insert(start, new int[] { i, j, _accArray[i, j] });
                                else
                                {
                                    int mid = (int)((start + end) / 2);
                                    repeat = true;
                                    if (start >= end)
                                        repeat = false;
                                    if (_accArray[i, j] == rhoTheta[mid].ElementAt(2))
                                    {
                                        rhoTheta.Insert(mid, new int[] { i, j, _accArray[i, j] });
                                        repeat = false;
                                    }
                                    else if (_accArray[i, j] > rhoTheta[mid].ElementAt(2))
                                        end = mid - 1;
                                    else if (_accArray[i, j] < rhoTheta[mid].ElementAt(2))
                                        start = mid + 1;
                                }
                            }
                            //rhoTheta.Add(new int[] { i, j, _accArray[i, j] });
                        }
                    }
                }
            }
            return rhoTheta;
        }

        private List<HoughInfo> filterLines(List<int[]> rhoTheta, int minLineLength, int maxGap)
        {
            Point p1 = new Point(), p2 = new Point();
            int x, y;
            List<HoughInfo> temp = new List<HoughInfo>();
            List<HoughInfo> lines = new List<HoughInfo>();
            int[,] markSelect = new int[_h, _w];
            foreach (int[] rt in rhoTheta)
            {
                List<LineSegment2D> subLine = new List<LineSegment2D>();
                int numDrawPoint = 0, numDrawLine = 0;
                int[,] mark = new int[_h, _w];  //array bounding box

                if (rt[1] >= 45 && rt[1] <= 135)
                {
                    //y = (r - x cos(t)) / sin(t)
                    for (x = 0; x < _w; x++)
                    {
                        y = (int)((((rt[0] - _rhoAwlLen) - ((x) * _cos[rt[1]])) / _sin[rt[1]]));
                        if (y >= 0 && y < _h)
                        {
                            //LIST SUBLINE
                            bool cekDrawPoint = false;
                            for (int dy = -1; dy <= 1; dy++)
                                for (int dx = -1; dx <= 1; dx++)
                                    if (y + dy > 0 && y + dy < _h && x + dx > 0 && x + dx < _w)
                                        if (_img.Data[y + dy, x + dx, 0] == 1)
                                        {
                                            cekDrawPoint = true;
                                            dy = 2; dx = 2;
                                        }
                            if (cekDrawPoint)//kalo masih bagian garis
                            {
                                if (numDrawPoint == 0) { p1.X = x; p1.Y = y; }
                                else { p2.X = x; p2.Y = y; }
                                numDrawPoint++;
                                mark[y, x] = numDrawLine + 1;
                                if (x == _w - 1)
                                {
                                    subLine.Add(new LineSegment2D(p1, p2));
                                    numDrawPoint = 0;
                                    numDrawLine++;
                                }
                            }
                            else if (numDrawPoint != 0)
                            {
                                subLine.Add(new LineSegment2D(p1, p2));
                                numDrawPoint = 0;
                                numDrawLine++;
                            }
                        }
                    }
                }
                else
                {
                    //x = (r - y sin(t)) / cos(t);
                    for (y = 0; y < _h; y++)
                    {
                        x = (int)((((rt[0] - _rhoAwlLen) - ((y) * _sin[rt[1]])) / _cos[rt[1]]));
                        if (x >= 0 && x < _w)
                        {
                            //LIST SUBLINE
                            bool cekDrawPoint = false;
                            for (int dy = -1; dy <= 1; dy++)
                                for (int dx = -1; dx <= 1; dx++)
                                    if (y + dy > 0 && y + dy < _h && x + dx > 0 && x + dx < _w)
                                        if (_img.Data[y + dy, x + dx, 0] == 1)
                                        {
                                            cekDrawPoint = true;
                                            dy = 2; dx = 2;
                                        }
                            if (cekDrawPoint)//kalo masih bagian garis
                            {
                                if (numDrawPoint == 0) { p1.X = x; p1.Y = y; }
                                else { p2.X = x; p2.Y = y; }
                                numDrawPoint++;
                                mark[y, x] = numDrawLine + 1;
                                if (y == _h - 1)
                                {
                                    subLine.Add(new LineSegment2D(p1, p2));
                                    numDrawPoint = 0;
                                    numDrawLine++;
                                }
                            }
                            else if (numDrawPoint != 0)
                            {
                                subLine.Add(new LineSegment2D(p1, p2));
                                numDrawPoint = 0;
                                numDrawLine++;
                            }
                        }
                    }
                }
                if (numDrawLine != 0)
                {
                    //get potential line from subLine, find max length
                    double max = -9999; int indexMaxLine = 0;
                    int n = subLine.Count;
                    for (int a = 0; a < n; a++)
                    {
                        //lines.Add(new HoughInfo(subLine.ElementAt(a), rt[0], rt[1] * _thetastep));
                        double maxLength = subLine.ElementAt(a).Length;
                        if (maxLength > max)
                        {
                            indexMaxLine = a;
                            max = maxLength;
                        }
                    }

                    LineSegment2D selectedLine = subLine.ElementAt(indexMaxLine);
                    indexMaxLine++;
                    //CEK MINLINELENGTH
                    if (selectedLine.Length > minLineLength)
                    {
                        //CEK INTERSECTION
                        int numInter = 0, numOuter = 0;
                        for (int i = 0; i < _h; i++)
                            for (int j = 0; j < _w; j++)
                            {
                                if (mark[i, j] == indexMaxLine)
                                {
                                    numOuter++;
                                    if (markSelect[i, j] == 1)
                                        numInter++;
                                }
                            }
                        if (numInter < numOuter * 0.5)
                        {
                            lines.Add(new HoughInfo(selectedLine, rt[0], rt[1] * _thetastep));
                            _numLines++;
                            for (int i = 0; i < _h; i++)
                                for (int j = 0; j < _w; j++)
                                {
                                    if (mark[i, j] == indexMaxLine)
                                    {
                                        markSelect[i, j] = 1;
                                        for (int k = 0; k < maxGap; k++)
                                        {
                                            if (i + k >= 0 && i + k < _h)
                                                markSelect[i + k, j] = 1;
                                            if (i - k >= 0 && i - k < _w)
                                                markSelect[i - k, j] = 1;
                                            if (j + k >= 0 && j + k < _w)
                                                markSelect[i, j + k] = 1;
                                            if (j - k >= 0 && j - k < _w)
                                                markSelect[i, j - k] = 1;
                                        }
                                    }
                                    mark[i, j] = 0;
                                }
                        }
                    }
                }
            }
            return lines;
        }

        public List<HoughInfo> getLines(int pilThresh, int thresh, int neighbor, int minLineLength, int maxGap)
        {
            List<int[]> rhoTheta = new List<int[]>();
            int numPoints = votingRhoValue();
            if (numPoints != 0)
            {
                int threshold = thresh;
                if (pilThresh == 0) //by percentage
                    threshold = (int)((double)thresh / 100.0 * maxValue());
                rhoTheta = findLocalMaxima(threshold, neighbor);
            }
            List<HoughInfo> foundLines = filterLines(rhoTheta, minLineLength, (int)(maxGap / 2));
            return foundLines;
        }

        public int maxValue()
        {
            int max = 0;
            for (int i = 0; i < _rhoLen; i++)
                for (int j = 0; j < _thetaLen; j++)
                    if (_accArray[i, j] > max)
                        max = _accArray[i, j];
            return max;
        }

        public int getNumLines()
        {
            return _numLines;
        }

        public Image<Gray, byte> getAccArrayImage()
        {
            Image<Gray, byte> temp = new Image<Gray, byte>(_thetaLen, _rhoLen);
            for (int i = 0; i < _rhoLen; i++)
            {
                for (int j = 0; j < _thetaLen; j++)
                {
                    double value = 255 * ((double)_accArray[i, j]) / maxValue();
                    int v = 255 - (int)value;
                    temp.Data[i, j, 0] = (byte)v;
                }
            }
            return temp;
        }

        public Image<Gray, byte> getImageHasil(List<HoughInfo> lines)
        {
            Image<Gray, byte> hasil = new Image<Gray, byte>(_w, _h);
            foreach (HoughInfo line in lines)
            {
                hasil.Draw(line._line, new Gray(255), 1);
            }
            return hasil;
        }

        public Image<Bgr, byte> getImageUnion(Image<Gray, byte> imgGray)
        {
            int w = imgGray.Width;
            int h = imgGray.Height;
            Image<Bgr, byte> imgUnion = new Image<Bgr, byte>(w, h);

            for (int i = 0; i < h; i++)
                for (int j = 0; j < w; j++)
                {
                    if (imgGray.Data[i, j, 0] == 255)
                        imgUnion[i, j] = new Bgr(Color.Red);
                    else if (_img.Data[i, j, 0] == 1)
                        imgUnion[i, j] = new Bgr(Color.Black);
                    else
                        imgUnion[i, j] = new Bgr(Color.White);
                }

            return imgUnion;
        }
    }
}
