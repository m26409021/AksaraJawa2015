﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AksaraJawa2015
{
    public class Neuron
    {
        public float[] weights;
        float neuronValue;
        float ErrorGradient;
        float bias;
       
        public Neuron(int value,Random rand)
        {
            weights = new float[value];
            for (int i = 0; i < value; i++)
            {
                weights[i] = (float)rand.NextDouble();
                if (rand.Next() % 2 == 0)
                    weights[i] *= -1;
            }
            bias = (float)rand.NextDouble();
            if (rand.Next() % 2 == 0)
                bias *= -1;
        }
        public float ErrorGradientValue
        {
            set
            {
                ErrorGradient = value;
            }
            get
            {
                return ErrorGradient;
            }
        }
        public float biasValue
        {
            set
            {
                bias = value;
            }
            get
            {
                return bias;
            }
        }
        public float NeuronValue
        {
            set
            {
                neuronValue = value;
            }
            get
            {
                return neuronValue;
            }
        }
        public float[] WeightsValue
        {
            get
            {
                return weights;
            }
        }
        
       

    }
}
