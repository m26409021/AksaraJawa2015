﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Drawing;

namespace AksaraJawa2015
{
    class CycleDetect
    {
        private int _w, _h, _minArea, cekArea;
        Image<Gray, Byte> _img, _hsl;
        public CycleDetect()
        {
            _w = 0;
            _h = 0;
            _img = new Image<Gray, byte>(_w, _h);
            _hsl = new Image<Gray, byte>(_w,_h);
        }
        public void setImage(Image<Gray, Byte> image, int minArea)
        {
            _w = image.Width;
            _h = image.Height;
            _img = image.Clone();
            _minArea = minArea;
        }
        public Image<Gray, Byte> getHasil()
        {
            return _hsl;
        }
        public int countCycle()
        {
            Image<Gray, Byte> im;
            int c = 0, x, y, w, h;

            //image dengan pigura putih
            w = _w + 2;
            h = _h + 2;
            im = new Image<Gray, byte>(w,h);
            for (int i = 0; i < h; i++)
                for (int j = 0; j < w; j++)
                    if (i > 0 && i < h - 1 && j > 0 && j < w - 1)
                        im.Data[i, j, 0] = _img.Data[i-1, j-1, 0];
                    else
                        im.Data[i, j, 0] = 0;
            //jalankan floodFill di salah 1 corner -> 0,0
            _hsl = floodFill(im, 0, 0, h, w, 200);
            //cek fill brp kali dijalankan
            bool cek=true;
            while(cek)
            {
                cek = false;
                for (int i = 0; i < h; i++)
                    for (int j = 0; j < w; j++)
                        if (_hsl.Data[i, j, 0] == 0)
                        {
                            x = j; y = i; //save point mulai fill
                            i = h; j = w; //utk keluar loop
                            cekArea = 0;
                            _hsl = floodFill(_hsl, y, x, h, w, 50);
                            //if (cekArea >= _minArea)
                                c++;
                            cek = true;
                        }
            }
            return c;
        }

        private Image<Gray, Byte> floodFill(Image<Gray, Byte> im, int y, int x, int h, int w, byte color)
        {
            if (im.Data[y, x, 0] == 0)
            {
                im.Data[y, x, 0] = color;
                cekArea++;
                int dx, dy;
                //west
                dx = x - 1;
                dy = y;
                if (dx >= 0 && dx < w)
                    floodFill(im, dy, dx, h, w, color);
                //eastD:\Meili\TA\TA\histogram\histogram\Class\HoughLine.cs
                dx = x + 1;
                dy = y;
                if (dx >= 0 && dx < w)
                    floodFill(im, dy, dx, h, w, color);
                //north
                dx = x;
                dy = y - 1;
                if (dy >= 0 && dy < h)
                    floodFill(im, dy, dx, h, w, color);
                //south
                dx = x;
                dy = y + 1;
                if (dy >= 0 && dy < h)
                    floodFill(im, dy, dx, h, w, color);
            }
            return im;
        }
    }
}
