﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
//using System.Threading.Tasks;
//using System.Text;

namespace AksaraJawa2015
{
    static class Parser
    {
        public static DecisionTreeSet ArrayToDecisionTreeSet(string[][] array, int headerRowIndex = 0, int headerColIndex = 0)
        {
            try 
            {
                var treeSet = new DecisionTreeSet();
                treeSet.Instances = new List<Instance>();

                var header = array[headerRowIndex];

                for (int i = 0; i < array.Length; i++)
                {
                    if (i == headerRowIndex)
                        continue;

                    // init new features
                    var newFeatures = new List<Feature>();
                    for (int j = 0; j < array[i].Length; j++)
                    {
                        if (j == headerColIndex)
                            continue;
                        
                        newFeatures.Add(new Feature(array[i][j], header[j]));
                    }

                    // set output 
                    Output output;
                    if (headerColIndex > -1)
                        output = new Output(array[i][headerColIndex], header[headerColIndex]);
                    else
                        output = null;

                    // init new instance
                    var instance = new Instance
                    {
                        Output = output,
                        Features = newFeatures
                    };

                    // add instance to tree set
                    treeSet.Instances.Add(instance);
                }

                return treeSet;
            } 
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public static string[][] CSVToArray(string filePath, char delimiter = 'a')
        {
            try
            {
                // init file reader
                var reader = new StreamReader(File.OpenRead(filePath));
                var totalLines = StreamReaderHelper.CountLines(filePath);

                // check auto delimiter decider
                if (delimiter == 'a')
                {
                    // get first line of file
                    while(!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        if (line.Contains(',') && !line.Contains(';'))
                            delimiter = ',';
                        else
                            delimiter = ';';

                        break;
                    }
                    // reset reader cursor pos
                    reader.DiscardBufferedData();
                    reader.BaseStream.Seek(0, SeekOrigin.Begin);
                    reader.BaseStream.Position = 0;
                }

                var resArr = new string[totalLines][];
                int i = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    string[] lineArr = null;
                    if (line.Contains('"'))
                    {
                        ArrayList tempLineArr = new ArrayList();
                        bool newArr = false;
                        bool catchingMultipleComas = false;
                        string word = "";
                        for (int j = 0; j < line.Length; j++)
                        {
                            if (j < line.Length - 1)
                            {
                                if (line[j] == delimiter)
                                {
                                    if (!catchingMultipleComas)
                                    {
                                        newArr = true;
                                    }
                                    else
                                    {
                                        newArr = false;
                                    }
                                }
                                else if (line[j] == '"')
                                {
                                    if(!catchingMultipleComas) 
                                    {
                                        newArr = false;
                                        catchingMultipleComas = true;
                                    }
                                    else
                                    {
                                        catchingMultipleComas = false;
                                    }
                                }

                                if (!newArr)
                                {
                                    if(line[j] != '"') word += line[j];
                                }
                                else
                                {
                                    tempLineArr.Add(word);
                                    word = "";
                                    newArr = false;
                                }
                            }
                            else
                            {
                                if (line[j] != '"')
                                {
                                    word += line[j];
                                    tempLineArr.Add(word);
                                }
                            }
                        }
                        lineArr = (String[]) tempLineArr.ToArray( typeof(string) );
                    }
                    else
                    {
                        lineArr = line.Split(delimiter);
                    }

                    resArr[i] = lineArr;
                    i++;
                }
                reader.Close();
                return resArr;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public static string ArrayToString(string[][] arr, string delimiter = " | ", bool numbered = false)
        {
            // null input check
            if(arr == null || delimiter == null)
            {
                Console.WriteLine("Function : ArrayToString()\r\nMessage : Null input");
                return null;
            }

            string res = "";
            int numberStart = 1;
            for (int i = 0; i < arr.Length; i++)
            {
                if (numbered)
                {
                    res += numberStart + ".)   ";
                    numberStart++;
                }
                for (int j = 0; j < arr[i].Length; j++)
                {
                    res += arr[i][j];
                    if (j < arr[i].Length - 1)
                        res += delimiter;
                }
                res += "\r\n";
            }
            return res;
        }

        public static string ArrayToString(double[][] arr, string delimiter = " | ", bool numbered = false)
        {
            // null input check
            if (arr == null || delimiter == null)
            {
                MessageBox.Show("Function : ArrayToString()\r\nMessage : Null input", "Parser Error");
                return null;
            }

            string res = "";
            int numberStart = 1;
            for (int i = 0; i < arr.Length; i++)
            {
                if (numbered)
                {
                    res += numberStart + ".)   ";
                    numberStart++;
                }
                for (int j = 0; j < arr[i].Length; j++)
                {
                    res += arr[i][j];
                    if (j < arr[i].Length - 1)
                        res += delimiter;
                }
                res += "\r\n";
            }
            return res;
        }

        public static string ArrayToString(double[] arr, string delimiter = " ")
        {
            // null input check
            if (arr == null || delimiter == null)
            {
                MessageBox.Show("Function : ArrayToString()\r\nMessage : Null input", "Parser Error");
                return null;
            }

            string result = "";
            for (int i = 0; i < arr.Length; i++)
            {
                result += arr[i] + delimiter;
            }

            return result;
        }

        public static string ArrayToString(string[] arr, string delimiter = " ", bool numbered = false)
        {
            // null input check
            if (arr == null || delimiter == null)
            {
                Console.WriteLine("Parser Error\r\nFunction : ArrayToString()\r\nMessage : Null input");
                return null;
            }

            string result = "";
            for (int i = 0; i < arr.Length; i++)
            {
                if (numbered)
                    result += (i + 1) + ". ";
                result += arr[i];
                if (i < arr.Length - 1)
                    result += delimiter;
            }

            return result;
        }

        public static string ArrayToString(int[] arr, string delimiter = " ", bool numbered = false)
        {
            // null input check
            if (arr == null || delimiter == null)
            {
                Console.WriteLine("Parser Error\r\nFunction : ArrayToString()\r\nMessage : Null input");
                return null;
            }

            string result = "";
            for (int i = 0; i < arr.Length; i++)
            {
                if (numbered)
                    result += i + 1 + ". ";

                result += arr[i];

                if (i < arr.Length - 1)
                    result += delimiter;
            }

            return result;
        }

        public static double[][] StringToDecimal(string[][] arr)
        {
            // null input check
            if (arr == null)
                return null;

            double temp;
            var resArr = new double[arr.Length][];
            for (int i = 0; i < arr.Length; i++)
            {
                resArr[i] = new double[arr[i].Length];
                for (int j = 0; j < arr[i].Length; j++)
                {
                    if (Double.TryParse(arr[i][j], out temp))
                    {
                        resArr[i][j] = temp;
                    }
                    else
                    {
                        Console.WriteLine("Parser Error\nAt arr[{0}][{1}]\nUnable to parse {2} to double", i, j, arr[i][j]);
                        return null;
                    }
                }
            }
            return resArr;
        }

        public static string[][] DecimalToString(double[][] input)
        {
            // null input check
            if(input == null)
            {
                MessageBox.Show("Empty array inputted", "Parser Error");
                return null;
            }

            string[][] res = new string[input.Length][];
            for (int i = 0; i < input.Length; i++)
            {
                res[i] = new string[input[i].Length];
                for (int j = 0; j < input[i].Length; j++)
                {
                    res[i][j] = "" + input[i][j];
                }
            }

            return res;
        }

    } // end of class

    static class StreamReaderHelper
    {
        public static int CountLines(string filePath)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                int i = 0;
                while (reader.ReadLine() != null) { i++; }
                return i;
            }
        }

    } // end of class

    static class ArrayHelper
    {
        public static string[][] TrimData(string[][] input, string identifier = null, bool removeHeader = false, int headerIndex = 0)
        {
            try
            {
                int totalChoosenCol, i = 0, j = 0, k = 0, l = 0;
                int[] choosenIndexes;

                // trimming column
                if(identifier != null)
                {
                    string[] splittedIdentifier = identifier.Split('|');

                    // remove any duplicates in identifier
                    splittedIdentifier = (from value in splittedIdentifier
                                          select value).Distinct().ToArray();

                    // count total column selected
                    totalChoosenCol = 0;
                    foreach (var words in splittedIdentifier)
                    {
                        foreach (var headerWords in input[headerIndex])
                        {
                            if (headerWords == words)
                            {
                                totalChoosenCol++;
                            }
                        }
                    }

                    // get the indexes of choosen cols
                    choosenIndexes = new int[totalChoosenCol];
                    i = 0;
                    foreach (var choosenWord in splittedIdentifier)
                    {
                        j = 0;
                        foreach (var word in input[headerIndex])
                        {
                            if (word == choosenWord)
                            {
                                choosenIndexes[i] = j;
                                i++;
                            }
                            j++;
                        }
                    }
                }
                // no column trimmed
                else
                {
                    totalChoosenCol = input[0].Length;
                    choosenIndexes = new int[totalChoosenCol];
                    for (i = 0; i < choosenIndexes.Length; i++)
                    {
                        choosenIndexes[i] = i;
                    }
                }
                
                // init result array size
                string[][] newArr;
                if (!removeHeader) 
                    newArr = new string[input.Length][];
                else 
                    newArr = new string[input.Length - 1][];

                // construct result array
                k = 0;
                for (i = 0; i < input.Length; i++)
                {
                    newArr[k] = new string[choosenIndexes.Length];
                    l = 0;
                    for (j = 0; j < input[i].Length; j++)
                    {
                        if (choosenIndexes.Contains(j))
                        {
                            if (!removeHeader)
                            {
                                newArr[k][l] = input[i][j];
                                l++;
                            }
                            else
                            {
                                if (i != headerIndex)
                                {
                                    newArr[k][l] = input[i][j];
                                    l++;
                                }
                            }
                        }
                    }

                    if (!removeHeader)
                    {
                        k++;
                    }
                    else
                    {
                        if (i != headerIndex)
                            k++;
                    }
                }
                return newArr;
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show(ex.Message, "Array Trimming Error");
                return null;
            }
        }

        public static double[][] MinMaxNormalization(string[][] arr)
        {
            int i, j;

            // parse string input to double
            var resArr = Parser.StringToDecimal(arr);

            // find max value
            double[] max = new double[resArr[0].Length];
            for (i = 0; i < max.Length; i++)
            {
                max[i] = 0;
                for (j = 0; j < resArr.Length; j++)
                {
                    if (resArr[j][i] > max[i]) max[i] = resArr[j][i];
                }
            }
            /*// debug
            Console.WriteLine("MAX : ");
            ArrayHelper.PrintArray(max, " | ");*/

            // find min value
            double[] min = new double[max.Length];
            for (i = 0; i < resArr[0].Length; i++)
            {
                min[i] = max[i];
                for (j = 0; j < resArr.Length; j++)
                {
                    if (resArr[j][i] < min[i]) min[i] = resArr[j][i];
                }
            }
            /*// debug
            Console.WriteLine("MIN : ");
            ArrayHelper.PrintArray(min, " | ");*/

            // normalize the data
            for (i = 0; i < resArr.Length; i++)
            {
                for (j = 0; j < resArr[i].Length; j++)
                {
                    if (max[j] == min[j]) resArr[i][j] = 0.5;
                    else { resArr[i][j] = (resArr[i][j] - min[j]) / (max[j] - min[j]); }
                }
            }
            // omit the result
            return resArr;
        }

        public static void PrintArray(int[] arr, string delimiter = " ")
        {
            try
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    Console.Write(arr[i]);
                    if (i < arr.Length - 1)
                    {
                        Console.Write(delimiter);
                    }
                }
                Console.WriteLine("");
            }
            catch(NullReferenceException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void PrintArray(double[] arr, string delimiter = " ", bool newLine = true)
        {
            try
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    Console.Write(arr[i]);
                    if (i < arr.Length - 1)
                    {
                        Console.Write(delimiter);
                    }
                }
                if(newLine)
                    Console.WriteLine("");
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void PrintArray(string[] arr, string delimiter = " ")
        {
            try
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    Console.Write(arr[i]);
                    if (i < arr.Length - 1)
                    {
                        Console.Write(delimiter);
                    }
                }
                Console.WriteLine("");
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void PrintArray(double[][] array, string delimiter = " ")
        {
            try
            {
                foreach (var line in array)
                {
                    Console.WriteLine(string.Join(delimiter, line));
                }
            }
            catch (NullReferenceException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void PrintArray(string[][] array, string delimiter = " ")
        {
            try
            {
                foreach (var line in array)
                {
                    Console.WriteLine(string.Join(delimiter, line));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    } // end of class

    static class MatrixHelper
    {
        // create a rows x cols matrix with 0 value
        public static double[][] init(int rows, int cols)
        {
            // creates a matrix initialized to all 0.0s
            // do error checking here?
            double[][] result = new double[rows][];
            for (int i = 0; i < rows; ++i)
                result[i] = new double[cols]; // auto init to 0.0
            return result;
        }

        public static double[][] buildRandom(int rows, int cols, double minVal, double maxVal, int seed)
        {
            // return matrix with values between minVal and maxVal
            Random ran = new Random(seed);
            double[][] result = init(rows, cols);
            for (int i = 0; i < rows; ++i)
                for (int j = 0; j < cols; ++j)
                    result[i][j] = (maxVal - minVal) * ran.NextDouble() + minVal;
            return result;
        }

        public static double[][] buildIdentity(int n)
        {
            double[][] result = init(n, n);
            for (int i = 0; i < n; ++i)
                result[i][i] = 1.0;
            return result;
        }

        public static double[][] transpose(double[][] input)
        {
            double[][] result = new double[input[0].Length][];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new double[input.Length];
                for (int j = 0; j < result[i].Length; j++)
                {
                    result[i][j] = input[j][i];
                }
            }

            return result;
        }

        public static double[][] multiply(double[][] A, double[][] B, bool debugMode = false)
        {
            // form an A's row x B's column result matrix
            double[][] result = new double[A.Length][];
            // do calculation
            if (debugMode)
            {
                Console.Write("** MATRIX MULTIPLICATION START...\nMatrice A : ");
                print(A);
                Console.Write("Matrix B : ");
                print(B);
                Console.WriteLine("Calculation : ");
            }
            for (int i = 0; i < A.Length; i++)
            {
                result[i] = new double[B[0].Length];
                for (int j = 0; j < B[0].Length; j++)
                {
                    result[i][j] = 0;
                    for (int k = 0; k < B.Length; k++)
                    {
                        result[i][j] += A[i][k] * B[k][j];
                        if (debugMode)
                        {
                            Console.Write(A[i][k] + "x" + B[k][j]);
                            if (k < B.Length - 1) { Console.Write("+"); }
                        }
                    }
                    if (debugMode) { Console.Write(" "); }
                }
                if (debugMode == true) { Console.WriteLine(""); }
            }
            if (debugMode == true)
            {
                Console.Write("Result : ");
                print(result);
                Console.WriteLine("** MATRIX MULTIPLICATION END\n");
            }
            // return the result
            return result;
        }

        public static double[][] inverse(double[][] matrix, bool debugMode = false)
        {
            if (debugMode) { Console.WriteLine("** CALC INVERSE..."); }

            int n = matrix.Length;

            double[][] result = matrix;
            int[] perm;
            int toggle;
            double[][] lum = decompose(matrix, out perm, out toggle);
            if (lum == null)
                throw new Exception("Unable to compute inverse");
            double[] b = new double[n];
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (i == perm[j])
                        b[j] = 1.0;
                    else
                        b[j] = 0.0;
                }
                double[] x = helperSolve(lum, b);
                for (int j = 0; j < n; ++j)
                    result[j][i] = x[j];
            }

            if (debugMode) { Console.WriteLine("** CALC INVERSE END"); }

            return result;
        }

        public static double getDeterminant(double[][] matrix)
        {
            int[] perm;
            int toggle;
            double[][] lum = decompose(matrix, out perm, out toggle);
            if (lum == null)
                throw new Exception("Unable to compute MatrixDeterminant");
            double result = toggle;
            for (int i = 0; i < lum.Length; ++i)
                result *= lum[i][i];
            return result;
        }

        public static double[][] decompose(double[][] matrix, out int[] perm, out int toggle)
        {
            // Doolittle LUP decomposition.
            // assumes matrix is square.
            int n = matrix.Length; // convenience
            double[][] result = duplicate(matrix);
            perm = new int[n];
            for (int i = 0; i < n; ++i) { perm[i] = i; }
            toggle = 1;
            for (int j = 0; j < n - 1; ++j) // each column
            {
                double colMax = Math.Abs(result[j][j]); // largest val in col j
                int pRow = j;
                for (int i = j + 1; i < n; ++i)
                {
                    if (result[i][j] > colMax)
                    {
                        colMax = result[i][j];
                        pRow = i;
                    }
                }
                if (pRow != j) // swap rows
                {
                    double[] rowPtr = result[pRow];
                    result[pRow] = result[j];
                    result[j] = rowPtr;
                    int tmp = perm[pRow]; // and swap perm info
                    perm[pRow] = perm[j];
                    perm[j] = tmp;
                    toggle = -toggle; // row-swap toggle
                }
                if (Math.Abs(result[j][j]) < 1.0E-20)
                    return null; // consider a throw
                for (int i = j + 1; i < n; ++i)
                {
                    result[i][j] /= result[j][j];
                    for (int k = j + 1; k < n; ++k)
                        result[i][k] -= result[i][j] * result[j][k];
                }
            } // main j column loop
            return result;
        }

        private static double[][] duplicate(double[][] matrix)
        {
            // assumes matrix is not null.
            double[][] result = init(matrix.Length, matrix[0].Length);
            for (int i = 0; i < matrix.Length; ++i) // copy the values
                for (int j = 0; j < matrix[i].Length; ++j)
                    result[i][j] = matrix[i][j];
            return result;
        }

        private static double[] helperSolve(double[][] luMatrix, double[] b)
        {
            // solve luMatrix * x = b
            int n = luMatrix.Length;
            double[] x = new double[n];
            b.CopyTo(x, 0);
            for (int i = 1; i < n; ++i)
            {
                double sum = x[i];
                for (int j = 0; j < i; ++j)
                    sum -= luMatrix[i][j] * x[j];
                x[i] = sum;
            }
            x[n - 1] /= luMatrix[n - 1][n - 1];
            for (int i = n - 2; i >= 0; --i)
            {
                double sum = x[i];
                for (int j = i + 1; j < n; ++j)
                    sum -= luMatrix[i][j] * x[j];
                x[i] = sum / luMatrix[i][i];
            }
            return x;
        }

        // print matrix to console
        public static void print(double[][] input)
        {
            Console.WriteLine("");
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = 0; j < input[i].Length; j++)
                {
                    Console.Write(input[i][j] + " ");
                }
                Console.WriteLine("");
            }
        }

    } // end of class

    static class VectorHelper
    {
        public static void print(int[] input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                Console.Write(input[i] + " ");
            }
            Console.WriteLine("");
        }

        public static void print(double[] input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                Console.Write(input[i] + " ");
            }
            Console.WriteLine("");
        }

    } // end of class

    static class ProcessHelper
    {
        public static void startProcess(string filename, Form mainForm)
        {
            try
            {
                Process segmentation = new Process();
                segmentation.StartInfo.FileName = @filename;
                segmentation.EnableRaisingEvents = true;
                mainForm.Visible = false;
                segmentation.Start();
                segmentation.WaitForExit();
                mainForm.Visible = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    static class ClassifierNetworkkHelper
    {
        public static string[][] LeverageMultipleData(string[][] arr, bool haveHeader = true)
        {
            try
            {
                for (int i = 0; i < arr.Length; i++)
                {
                    if (haveHeader && i == 0)
                        continue;

                    for (int j = 0; j < arr[i].Length; j++)
                    {
                        var wholeWord = arr[i][j];

                        if (wholeWord.Contains('(') && wholeWord.Contains(')'))
                        {
                            var dataCount = wholeWord.Count(v => v == '(');
                            var dataHolder = new string[dataCount];
                            // debug
                            //Console.WriteLine("data count : {0}", dataCount);

                            for (int l = 0; l < dataCount; l++)
                            {
                                var takenWord = wholeWord.Substring(wholeWord.IndexOf('(') + 1, wholeWord.IndexOf(')') - 1);
                                // debug
                                //Console.WriteLine("taken word : {0}", takenWord);

                                var wordSplit = takenWord.Split(',');
                                for (int k = 0; k < wordSplit.Length; k++)
                                {
                                    wordSplit[k] = wordSplit[k].Trim();
                                }
                                // debug
                                //Console.WriteLine("word split : {0}", Parser.ArrayToString(wordSplit, ", "));

                                dataHolder[l] = Parser.ArrayToString(wordSplit, ",");

                                var keyword = "(" + takenWord + "),";
                                var tgtIndex = wholeWord.IndexOf(keyword) + keyword.Length;
                                // debug
                                //Console.WriteLine("Target index : {0}", tgtIndex);

                                wholeWord = wholeWord.Substring(tgtIndex);
                                wholeWord = wholeWord.Trim();
                                // debug
                                //Console.WriteLine("remaining word : {0}", wholeWord);
                            }

                            double outerAvg = 0;

                            for (int k = 0; k < dataCount; k++)
                            {
                                if (dataHolder[k].Contains(','))
                                {
                                    var wordSplit = dataHolder[k].Split(',');
                                    double avg = 0;

                                    for (int l = 0; l < wordSplit.Length; l++)
                                        avg += double.Parse(wordSplit[l]);

                                    avg /= wordSplit.Length;

                                    dataHolder[k] = Math.Round(avg) + "";
                                }

                                outerAvg += double.Parse(dataHolder[k]);
                            }

                            outerAvg /= dataHolder.Length;

                            arr[i][j] = Math.Round(outerAvg) + "";
                            // debug
                            //Console.WriteLine("arr[{0}][{1}] = {2}", i, j, arr[i][j]);
                        }
                    }
                }

                return arr;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }

} // end of namespace
