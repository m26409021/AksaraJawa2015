﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.Runtime.Serialization;
//using System.Text;

namespace AksaraJawa2015
{
    [DataContract]
    public class PNN
    {
        #region Members

        [DataMember]
        public double Bias { get; protected set; }

        private double _spread;
        [DataMember]
        public double Spread
        {
            get
            {
                return _spread;
            }
            set
            {
                _spread = value;
                // calc bias
                Bias = (Math.Sqrt(-(Math.Log(0.5)))) / _spread;
            }
        }

        [DataMember]
        public double[][] InitialWeight { get; protected set; }

        [DataMember]
        public double[][] FinalWeight { get; protected set; }

        [DataMember]
        public List<string> ClassLabels { get; protected set; }

        [DataMember]
        public int TotalTrainingData { get; protected set; }

        #endregion

        public PNN()
        {
            Bias = 0;
            InitialWeight = null;
            FinalWeight = null;
            ClassLabels = null;
            TotalTrainingData = 0;
        }

        public bool Train(string[][] inputData, double spread = 1, int headerRowIndex = 0, int headerColIndex = 0)
        {
            // null input check
            if (inputData == null)
            {
                Console.WriteLine("PNN Training Error\nEmpty Array Inputted");
                return false;
            }
            // row & col index inbound check
            if(headerRowIndex >= inputData.Length)
            {
                Console.WriteLine("PNN Training Error\nPNN detect an out of bound row header index");
                return false;
            }
            if (headerColIndex >= inputData[0].Length)
            {
                Console.WriteLine("PNN Training Error\nPNN detect an out of bound column header index");
                return false;
            }

            TotalTrainingData = inputData.Length - 1;

            int i, j, k; // reusable local counter

            // set bias
            Spread = spread;
            Bias = (Math.Sqrt(-(Math.Log(0.5)))) / Spread;

            // find out which columns to fetch data from
            string choosenColumns = "";
            for (i = 0; i < inputData[headerRowIndex].Length; i++)
            {
                if (i == headerColIndex)
                    continue;
                choosenColumns += inputData[headerRowIndex][i];
                if (i < inputData[headerRowIndex].Length - 1)
                    choosenColumns += "|";
            }
            // fetch choosen columns from input data 
            var trimmedInput = ArrayHelper.TrimData(inputData, choosenColumns, true, headerRowIndex);

            // set initial weight
            InitialWeight = Parser.StringToDecimal(trimmedInput);

            // get class labels from input data
            ClassLabels = new List<string>();
            for (i = 0; i < inputData.Length; i++)
            {
                if (i == headerRowIndex) continue;
                ClassLabels.Add(inputData[i][headerColIndex].Trim());
            }

            // set input data label index
            List<string> distinctClassLabels = ClassLabels.Distinct().ToList();
            k = 0;
            int[] labelIndex = new int[inputData.Length - 1];
            string inputLabel;
            for (i = 0; i < inputData.Length; i++)
            {
                if (i == headerRowIndex) continue;
                inputLabel = inputData[i][headerColIndex].Trim();
                for (j = 0; j < distinctClassLabels.Count; j++)
                {
                    if (inputLabel == distinctClassLabels[j])
                    {
                        labelIndex[k] = j;
                        k++;
                    }
                }
            }

            // set final weight
            FinalWeight = new double[distinctClassLabels.Count][];
            for (i = 0; i < FinalWeight.Length; i++)
            {
                FinalWeight[i] = new double[trimmedInput.Length];
                for (j = 0; j < FinalWeight[i].Length; j++)
                {
                    if (i == labelIndex[j]) FinalWeight[i][j] = 1;
                    else FinalWeight[i][j] = 0;
                }
            }

            return true;
        }

        public bool TrainExisting(string[][] inputData, int headerRowIndex = 0, int headerColIndex = 0)
        {
            // null input check
            if (inputData == null) { Console.WriteLine("Null input at " + this.GetType().FullName); return false; }
            // invalid header row index check
            if (headerRowIndex > inputData.Length ||
                headerRowIndex < 0) { Console.WriteLine("Invalid row header index at " + this.GetType().FullName); return false; }
            // invalid header col index check
            if (headerColIndex > inputData[0].Length ||
                headerColIndex < 0) { Console.WriteLine("Invalid col header index at " + this.GetType().FullName); return false; }

            int i, j, k, l;
            TotalTrainingData += inputData.Length;

            // find out which columns to fetch data from
            string choosenColumns = "";
            for (i = 0; i < inputData[headerRowIndex].Length; i++)
            {
                if (i == headerColIndex)
                    continue;
                choosenColumns += inputData[headerRowIndex][i];
                if (i < inputData[headerRowIndex].Length - 1)
                    choosenColumns += "|";
            }
            // fetch only choosen columns from input data
            string[][] fetchedData = ArrayHelper.TrimData(inputData, choosenColumns, true, headerRowIndex);

            // add input data to initial weight
            k = 0;
            double[][] oldWeight = InitialWeight;
            InitialWeight = new double[oldWeight.Length + fetchedData.Length][];
            for (i = 0; i < InitialWeight.Length; i++)
            {
                l = 0;
                InitialWeight[i] = new double[oldWeight[0].Length];
                for (j = 0; j < InitialWeight[i].Length; j++)
                {
                    if (i < oldWeight.Length)
                        InitialWeight[i][j] = oldWeight[i][j];
                    else
                    {
                        InitialWeight[i][j] = double.Parse(fetchedData[k][l]);
                        l++;
                    }
                }
                if (i >= oldWeight.Length)
                    k++;
            }
            // verify new initial weight
            k = 0;
            bool verified = true;
            for (i = 0; i < InitialWeight.Length; i++ )
            {
                l = 0;
                for (j = 0; j < InitialWeight[i].Length; j++)
                {
                    if (i < oldWeight.Length)
                    {
                        if (InitialWeight[i][j] != oldWeight[i][j]) { verified = false; break; }
                    }
                    else
                    {
                        if (InitialWeight[i][j] != double.Parse(fetchedData[k][l])) { verified = false; break; }
                        l++;
                    }
                }
                if (i >= oldWeight.Length)
                    k++;
            }
            if (!verified) Console.WriteLine("Initial weight didn't pass verification test");
            else Console.WriteLine("Initial weight pass verification test");

            // add new labels to label container
            List<string> distinctClassLabels = ClassLabels.Distinct().ToList();
            string inputLabel;
            int newLabelAdded = 0;
            for (i = 0; i < inputData.Length; i++)
            {
                if (i == headerRowIndex) continue;
                inputLabel = inputData[i][headerColIndex].Trim();
                ClassLabels.Add(inputLabel);
                if (!distinctClassLabels.Contains(inputLabel))
                {
                    distinctClassLabels.Add(inputLabel);
                    newLabelAdded++;
                }
            }
            // set new labels indexes
            k = 0;
            int[] labelIndex = new int[inputData.Length - 1];
            for (i = 0; i < inputData.Length; i++)
            {
                if (i == headerRowIndex) continue;
                inputLabel = inputData[i][headerColIndex].Trim();
                for (j = 0; j < distinctClassLabels.Count; j++)
                {
                    if (inputLabel == distinctClassLabels[j])
                    {
                        labelIndex[k] = j;
                        k++;
                    }
                }
            }
            // add new label to final weight
            oldWeight = FinalWeight;
            FinalWeight = new double[oldWeight.Length + newLabelAdded][];
            for (i = 0; i < FinalWeight.Length; i++)
            {
                k = 0;
                FinalWeight[i] = new double[oldWeight[0].Length + fetchedData.Length];
                for (j = 0; j < FinalWeight[i].Length; j++)
                {
                    if (j < oldWeight[0].Length)
                    {
                        if (i < oldWeight.Length) FinalWeight[i][j] = oldWeight[i][j];
                        else FinalWeight[i][j] = 0;
                    }
                    else
                    {
                        if (i == labelIndex[k]) FinalWeight[i][j] = 1;
                        else FinalWeight[i][j] = 0;
                        k++;
                    }
                }
            }
            // verify final weight
            verified = true;
            for (i = 0; i < FinalWeight.Length; i++)
            {
                k = 0;
                for (j = 0; j < FinalWeight[i].Length; j++)
                {
                    if(j < oldWeight[0].Length)
                    {
                        if(i < oldWeight.Length) { if (FinalWeight[i][j] != oldWeight[i][j]) { verified = false; break; } }
                        else { if(FinalWeight[i][j] != 0) { verified = false; break; } }
                    }
                    else
                    {
                        if (i == labelIndex[k]) { if(FinalWeight[i][j] != 1) { verified = false; break; } }
                        else { if (FinalWeight[i][j] != 0) { verified = false; break; } }
                        k++;
                    }
                }
            }
            if (!verified) Console.WriteLine("Final weight didn't pass verification test");
            else Console.WriteLine("Final weight pass verification test");

            return true;
        }

        public string[] Classify(double[][] input)
        {
            try
            {
                string[] result = new string[input.Length];
                var distinctLabels = ClassLabels.Distinct().ToList();

                for (int i = 0; i < input.Length; i++)
                {
                    int index = Classify(input[i]);
                    result[i] = distinctLabels[index];
                }

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error at classify\n{0}", ex.Message);
                return null;
            }
        }

        protected int Classify(double[] input)
        {
            var dist = new double[InitialWeight.Length];
            double total = 0;

            for (int i = 0; i < InitialWeight.Length; i++)
            {
                total = 0;

                for (int j = 0; j < input.Length; j++)
                {
                    total += Math.Pow(InitialWeight[i][j] - input[j], 2);
                }

                dist[i] = Math.Sqrt(total);
            }

            dist = Radbas(dist);

            return Competitive(dist);
        }

        protected double[] Radbas(double[] distance)
        {
            for (int i = 0; i < distance.Length; i++)
            {
                distance[i] = Math.Exp(Math.Pow((distance[i] * Bias), 2) * -1);
            }
            return distance;
        }

        protected int Competitive(double[] dist)
        {
            var d = new double[FinalWeight.Length];
            double max = double.MinValue, total;
            int index = 0;

            for (int k = 0; k < FinalWeight.Length; k++)
            {
                total = 0;
                for (int l = 0; l < FinalWeight[k].Length; l++)
                {
                    total += dist[l] * FinalWeight[k][l];
                }
                d[k] = total;

                if (d[k] > max)
                {
                    max = d[k];
                    index = k;
                }
            }

            return index;
        }

        public string Display(string arrayDelimiter = " ")
        {
            string res = "";

            // display bias value
            res += "BIAS VALUE : " + Bias + "\r\n\r\n";

            // display initial weight 
            res += "INITIAL WEIGHT : \r\n";
            if (InitialWeight != null) res += Parser.ArrayToString(InitialWeight, arrayDelimiter);
            else res += " NULL";
            res += "\r\n\r\n";

            // display final weight
            res += "FINAL WEIGHT : \r\n";
            if (FinalWeight != null) res += Parser.ArrayToString(FinalWeight, arrayDelimiter);
            else res += " NULL";
            res += "\r\n\r\n";

            // display stored classes
            List<string> distinctClassLabels = ClassLabels.Distinct().OrderBy(x => x).ToList();
            int labelCount = 0, tempCounter;
            res += "Stored Classes : ";
            distinctClassLabels.ForEach(x =>
            {
                tempCounter = ClassLabels.Where(y => y == x).Count();
                labelCount += tempCounter;
                res += "\r\n" + x + " = " + tempCounter;
            });
            if(labelCount == ClassLabels.Count)
                res += "\r\nClass label pass verification test !\r\nTotal class collected : " + labelCount;
            else
                res += "\r\nClass label  didn't pass verification test\r\nTotal class collected : " + labelCount +
                    ", should be : " + ClassLabels.Count;
            res += "\r\n\r\n";

            // omit result
            return res;
        }

    } // end of class

} // end of namespace
