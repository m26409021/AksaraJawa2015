﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;
using System.Windows.Forms;
using System.Runtime.Serialization;
//using System.Text;

namespace AksaraJawa2015
{
    [DataContract]
    [KnownType(typeof(Feature))]
    [KnownType(typeof(Output))]
    [KnownType(typeof(Tree))]
    public class Tree
    {
        [DataMember]
        public Output Leaf { get; set; }

        [DataMember]
        public Dictionary<Feature, Tree> Branches { get; set; }

        [DataMember]
        public int TotalTrainingData { get; set; }

        public static Output ProcessInstance(Tree tree, Instance i)
        {
            if (tree == null)
                return null;

            if (tree.Leaf != null)
                return tree.Leaf;

            return ProcessInstance(tree.TreeForInstance(i), i);
        }

        private Tree TreeForFeature(Feature feature)
        {
            Tree found;
            if(Branches.TryGetValue(feature, out found))
            {
                return found;
            }
            return null;
        }

        private Tree TreeForInstance(Instance instance)
        {
            var tree = instance.Features.Select(TreeForFeature).FirstOrDefault(f => f != null);

            return tree;
        }

        public string[] Classify(string[][] dataToClassify)
        {
            //try
            //{
                //string[][] result = new string[dataToClassify.Length - 1][];
                string[] result = new string[dataToClassify.Length - 1];
                int i = 0;

                // init instances to classify
                var instancesToClassify = new List<Instance>();
                for (i = 1; i < dataToClassify.Length; i++)
                {
                    var features = new List<Feature>();
                    for (int j = 0; j < dataToClassify[i].Length; j++)
                    {
                        features.Add(new Feature(dataToClassify[i][j], dataToClassify[0][j]));
                    }
                    instancesToClassify.Add(new Instance
                    {
                        Features = features
                    });
                }

                // classify the instances
                i = 0;
                foreach (var instance in instancesToClassify)
                {
                    var output = ProcessInstance(this, instance);
                    //var features = "";
                    //instance.Features.ForEach(f => features += f.Value + " ");
                    //result[i] = new string[] { "Alphabet : " + output.Value, "Feature : " + features };
                    if (output != null)
                        result[i] = output.Value;
                    else
                        result[i] = null;
                    i++;
                }

                return result;
            /*}
            catch(Exception ex)
            {
                Console.WriteLine("Invalid data to classify\r\n\r\n" + ex.Message, "Classification Error");
                return null;
            }*/
        }

        public string display(int tab = 0)
        {
            Func<string> textBoxTabWriter = () => 
            { 
                string s = ""; 
                Enumerable.Range(0, tab).ForEach(i => s += "    "); 
                return s; 
            };
            string newText = "";

            if (Branches != null)
            {
                foreach (var feature in Branches)
                {
                    newText += textBoxTabWriter();
                    newText += feature.Key + "\r\n";
                    newText += feature.Value.display(tab + 1);
                }
            }
            else
            {
                newText += textBoxTabWriter();
                newText += Leaf + "\r\n";
            }

            return newText;
        }

    } // end of 'Tree' class

    public class DecisionTreeSet
    {
        public List<Instance> Instances { get; set; }

        public int NumberOfFeatures
        {
            get
            {
                var first = Instances.FirstOrDefault();
                if(first != null)
                {
                    return first.Features.Count;
                }
                return 0;
            }
        }

        public int NumberOfInstances
        {
            get { return Instances.Count(); }
        }

        public Boolean InstancesAreSameClass
        {
            get
            {
                return Instances.Select(i => i.Output).DistinctBy(i => i.Value).Count() == 1;
            }
        }

        public DecisionTreeSet Split(Feature feature)
        {
            return Split(feature.Axis, feature.Value);
        }

        public DecisionTreeSet Split(string Axis, string Value)
        {
            return new DecisionTreeSet
            {
                Instances = Instances.Select(i => i.Split(Axis, Value))
                                     .Where(i => i.Features.Any())
                                     .ToList()
            };
        }

        public Tree BuildTree()
        {
            if (InstancesAreSameClass || Instances.All(f => f.Features.Count() == 1))
            {
                return LeafTreeForRemainingFeatures();
            }

            var best = Decider.SelectBestAxis(this);

            return SplitByAxis(best);
        }

        private Tree SplitByAxis(string Axis)
        {
            if (Axis == null)
            {
                return null;
            }

            // Split the set on each unique feature Value where the feature is
            // if of the right Axis
            var splits = (from feature in UniqueFeatures().Where(a => a.Axis == Axis)
                          select new { splitFeature = feature, set = Split(feature) }).ToList();

            var tempBranches = new Dictionary<Feature, Tree>();

            // for each Split, either recursively create a new tree
            // or Split the final feature outputs into Leaf trees
            int totalData = 0;
            foreach (var item in splits)
            {
                tempBranches[item.splitFeature] = item.set.BuildTree();
                totalData += tempBranches[item.splitFeature].TotalTrainingData;
            }

            return new Tree
            {
                Branches = tempBranches,
                TotalTrainingData = totalData
            };
        }

        private Tree LeafTreeForRemainingFeatures()
        {
            if (InstancesAreSameClass)
            {
                return GroupByClass();
            }

            if (Instances.All(f => f.Features.Count() == 1))
            {
                return LeafForEachFeature();
            }

            return null;
        }

        private Tree LeafForEachFeature()
        {
            // each feature is the last item
            var tempBranches = new Dictionary<Feature, Tree>();

            int totalData = 0;

            foreach (var instance in Instances)
            {
                foreach (var feature in instance.Features)
                {
                    if (tempBranches.Any(k => k.Key.Value == feature.Value))
                    {
                        continue;
                    }

                    tempBranches[feature] = new Tree
                    {
                        Leaf = instance.Output,
                        TotalTrainingData = 1
                    };
                    totalData += tempBranches[feature].TotalTrainingData;
                }
            }

            return new Tree
            {
                Branches = tempBranches,
                TotalTrainingData = totalData
            };
        }

        private Tree GroupByClass()
        {
            var groupings = Instances.DistinctBy(i => i.Output.Value)
                                     .ToDictionary(i => i.Features.First(), j => new Tree
                                     {
                                         Leaf = j.Output,
                                         TotalTrainingData = 1
                                     });

            if (groupings.Count() > 1)
            {
                // calc total training data
                int totalData = 0;
                groupings.ForEach(x => totalData += x.Value.TotalTrainingData);

                return new Tree
                {
                    Branches = groupings,
                    TotalTrainingData = totalData
                };
            }

            return new Tree
            {
                Leaf = groupings.First().Value.Leaf,
                TotalTrainingData = 1
            };
        }

        public IEnumerable<Feature> UniqueFeatures()
        {
            return Instances.SelectMany(f => f.Features).DistinctBy(f => f.Axis + f.Value).ToList();
        }

    } // end of 'DecisionTreeSet' class

    public class Instance
    {
        public List<Feature> Features { get; set; }

        public Output Output { get; set; }

        public Instance Split(string Axis, string Value)
        {
            var splitFeature = new Feature(Value, Axis);

            var featureSplit = Features.Where(f => !f.Equals(splitFeature)).ToList();

            // no Split happened
            if (featureSplit.Count == Features.Count)
            {
                featureSplit = new List<Feature>();
            }

            return new Instance
            {
                Output = Output,
                Features = featureSplit
            };
        }

        public override string ToString()
        {
            var s = Features.Aggregate(String.Empty, (acc, item) => acc + item.Value + ", ");
            s += Output.Value;
            return s;
        }

    } // end of 'Instance' class

    [DataContract]
    public class Feature
    {
        protected bool Equals(Feature other)
        {
            return string.Equals(Value, other.Value) && string.Equals(Axis, other.Axis);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Value != null ? Value.GetHashCode() : 0) * 397) ^ (Axis != null ? Axis.GetHashCode() : 0);
            }
        }

        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string Axis { get; set; }
 
        public Feature(string inputVal, string inputAx)
        {
            Value = inputVal;
            Axis = inputAx;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Feature)obj);
        }

        public override string ToString()
        {
            return String.Format("{0}: {1}", Axis, Value);
        }

    } // end of 'Feature' class

    [DataContract]
    public class Output : Feature
    {
        public Output (string Value, string @class): base(Value, @class)
        {

        }

    } // end of 'Output' class

    // acts as a helper class for id3
    public static class Decider
    {
        public static string SelectBestAxis(DecisionTreeSet set)
        {
            var baseEntropy = Entropy(set);

            var bestInfoGain = 0.0;

            var uniqueFeaturesByAxis = set.UniqueFeatures().GroupBy(i => i.Axis).ToList();

            string bestAxisSplit = uniqueFeaturesByAxis.First().Key;

            foreach (var Axis in uniqueFeaturesByAxis)
            {
                // calculate the total entropy based on splitting by this Axis. The total entropy
                // is the sum of the entropy of each branch that would be created by this Split 

                var newEntropy = EntropyForSplitBranches(set, Axis.ToList());

                var infoGain = baseEntropy - newEntropy;

                if (infoGain > bestInfoGain)
                {
                    bestInfoGain = infoGain;

                    bestAxisSplit = Axis.Key;
                }
            }

            return bestAxisSplit;
        }

        private static double EntropyForSplitBranches(DecisionTreeSet set, IEnumerable<Feature> allPossibleAxisValues)
        {
            return (from possibleValue in allPossibleAxisValues
                    select set.Split(possibleValue) into subset
                    let prob = (float)subset.NumberOfInstances / set.NumberOfInstances
                    select prob * Entropy(subset)).Sum();
        }

        public static double Entropy(DecisionTreeSet set)
        {
            var total = set.Instances.Count();

            var outputs = set.Instances.Select(i => i.Output).GroupBy(f => f.Value).ToList();

            var entropy = 0.0;

            foreach (var target in outputs)
            {
                var probability = (float)target.Count() / total;
                entropy -= probability * Math.Log(probability, 2);
            }

            return entropy;
        }

    } // end of 'Decider' class
}
