﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AksaraJawa2015
{
    public class Layer
    {
        public Neuron[] Nneuron;
        public Layer(int value,int weights,Random rand)
        {
            Nneuron = new Neuron[value];
            for (int i = 0; i < value; i++)
            {
                Nneuron[i] = new Neuron(weights,rand);
            }
        }
        public float[] setInputNeuron
        {
            set
            {
                for (int i = 0; i < Nneuron.Count(); i++)
                {
                    Nneuron[i].NeuronValue = value[i];
                }
            }
            
        }
        public void setNeuronValue(Layer backLayer)
        {
            for (int i = 0; i < Nneuron.Count(); i++)
            {
                Nneuron[i].NeuronValue = 0;
                for (int j = 0; j < backLayer.Nneuron.Count(); j++)
                {
                    if (Nneuron[i].WeightsValue[j] != 0)
                        Nneuron[i].NeuronValue += backLayer.Nneuron[j].NeuronValue * Nneuron[i].WeightsValue[j];
                }
                Nneuron[i].NeuronValue -= Nneuron[i].biasValue;
                Nneuron[i].NeuronValue = (float)(1.0 / (float)(1.0 + Math.Exp(Nneuron[i].NeuronValue * -1)));
            }
        }
        public void New_setNeuronValue(Layer backLayer,int index, int outputlayer)
        {
            for (int i = 0; i < Nneuron.Count(); i++)
            {
                Nneuron[i].NeuronValue = 0;
                for (int j = 0; j < backLayer.Nneuron.Count(); j++)
                {
                    Nneuron[i].NeuronValue += backLayer.Nneuron[j].NeuronValue * Nneuron[i].WeightsValue[j];
                }

                if (index != outputlayer)
                    Nneuron[i].NeuronValue = 2*Nneuron[i].NeuronValue;

                if(Math.Abs(Nneuron[i].NeuronValue) < 1.0e-16)
                    Nneuron[i].NeuronValue=(float)0.5;
                else if(Nneuron[i].NeuronValue > 100)
                    Nneuron[i].NeuronValue=1;
                else if(Nneuron[i].NeuronValue <-100)
                    Nneuron[i].NeuronValue = 0;
                else
                {
                    Nneuron[i].NeuronValue = (float)(1.0 / (float)(1.0 + Math.Exp(Nneuron[i].NeuronValue * -1)));
                }

                if(index!=outputlayer)
                    Nneuron[i].NeuronValue = 2*Nneuron[i].NeuronValue -1;
            }
        }
        public Neuron[] getNeuronValue()
        {
            return Nneuron;
        }
        
        
    }
}
