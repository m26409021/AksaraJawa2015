﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Drawing;
using System.Windows.Forms;

namespace AksaraJawa2015
{
    class HoughCurve
    {
        private Image<Gray, byte> _img;
        private List<int[]> _param;
        static int MAXTHETA = 180;
        private int _w, _h, _numCurve, _thetaStep,_thetaLen;
        private int[, ,] _accArray;
        private double[] _sin, _cos;

        public HoughCurve()
        {
            _h = _w = 0;
            _img = new Image<Gray, byte>(_w, _h);
            _thetaLen = _thetaStep = 0;
            _numCurve = 0;
            _sin = new double[_thetaLen];
            _cos = new double[_thetaLen];
            _param = new List<int[]>();
        }

        public void setImage(Image<Gray, byte> image, int step)
        {
            _param = new List<int[]>();
            _numCurve = 0;
            _img = image.Clone();
            _w = _img.Width;
            _h = _img.Height;
            _thetaStep = step;
            _thetaLen = MAXTHETA / _thetaStep;
            _accArray = new int[_w, _h, _thetaLen];
            for (int i = 0; i < _w; i++)
                for (int j = 0; j < _h; j++)
                    for (int k = 0; k < _thetaLen; k++)
                        _accArray[i, j, k] = 0;
            //create cache
            _sin = new double[_thetaLen];
            _cos = new double[_thetaLen];
            for (int i = 0, j = 0; i < _thetaLen; i++, j += _thetaStep)
            {
                _sin[i] = Math.Sin((j) * Math.PI / 180);
                _cos[i] = Math.Cos((j) * Math.PI / 180);
            }
        }

        private int maxValue()
        {
            int max = 0;
            for (int i = 0; i < _w; i++)
                for (int j = 0; j < _h; j++)
                    for (int k = 0; k < _thetaLen; k++)
                        if (_accArray[i, j, k] > max)
                            max = _accArray[i, j, k];
            return max;
        }

        public Image<Gray, byte> getImage()
        {
            return _img;
        }

        public List<int[]> getParam()
        {
            return _param;
        }

        public int getNumCurve()
        {
            return _numCurve;
        }

        public int votingYoValue()
        {
            int numPoint = 0;
            Image<Gray, float> gradX = _img.Sobel(1, 0, 3);
            Image<Gray, float> gradY = _img.Sobel(0, 1, 3);
            for (int y = 0; y < _h; y++)
                for (int x = 0; x < _w; x++)
                    if (_img.Data[y, x, 0] == 1)
                    {
                        numPoint++;
                        double dydx = Math.Atan2(gradX.Data[y, x, 0], gradY.Data[y, x, 0]);
                        for (int xo = 0; xo < _w; xo++)
                            for (int t = 0; t < _thetaLen; t++)
                            {
                                double k1 = (-_sin[t] + (dydx * _cos[t])) / (2 * (_cos[t] + (dydx * _sin[t])));
                                double a = (k1 * (x * _cos[t] + y * _sin[t])) + ((x * _sin[t]) - (y * _cos[t]));
                                double b = (k1 * _sin[t]) - _cos[t];
                                double c = ((k1 * _cos[t]) + _sin[t]) * xo;
                                int yo = (int)((a - c) / b);
                                if (yo >= 0 && yo < _h)
                                    _accArray[xo, yo, t]++;
                            }
                    }
            return numPoint;
        }

        public Image<Gray, byte> findLocalMaxima(int neighbor, int thresh, int color, int dist, int gap)
        {
            Image<Gray, float> gradX = _img.Sobel(1, 0, 3);
            Image<Gray, float> gradY = _img.Sobel(0, 1, 3);
            Image<Gray, byte> hsl = new Image<Gray, byte>(_w, _h);
            if (neighbor > _w)
                neighbor = _w;
            if (neighbor > _h)
                neighbor = _h;
            int threshold = thresh;
            for (int x = 0; x < _w; x++)
                for (int y = 0; y < _h; y++)
                    for (int t = 0; t < _thetaLen; t++)
                        if (_accArray[x, y, t] > threshold)
                        {
                            bool stat = true;
                            for (int xx = -neighbor; xx <= neighbor; xx++)
                            {
                                int dx = x + xx;
                                if (dx < 0) dx += _w;
                                else if (dx >= _w) dx -= _w;
                                for (int yy = -neighbor; yy <= neighbor; yy++)
                                {
                                    int dy = y + yy;
                                    if (dy < 0) dy = dy + _h;
                                    else if (dy >= _h) dy = dy - _h;
                                    for (int tt = -neighbor; tt <= neighbor; tt++)
                                    {
                                        int dt = t + tt;
                                        if (dt < 0) dt = dt + _thetaLen;
                                        else if (dt >= _thetaLen) dt = dt - _thetaLen;
                                        if (_accArray[dx, dy, dt] > _accArray[x, y, t])
                                        {
                                            stat = false;
                                            xx = yy = tt = neighbor + 1;
                                        }
                                    }
                                }
                            }
                            if (stat)
                            {
                                //FILTER
                                bool cek = true;
                                foreach (int[] param in _param)
                                    if (Math.Abs(param[0] - x) <= dist && Math.Abs(param[1] - y) <= dist && Math.Abs(t - param[2]) <= gap)
                                        cek = false;
                                if (cek)
                                {
                                    _numCurve++;
                                    _param.Add(new int[] { x, y, t });
                                    //DRAW VERTEX
                                    for (int ii = -1; ii <= 1; ii++)
                                        for (int jj = -1; jj <= 1; jj++)
                                            if (y + ii > 0 && y + ii < _h && x + jj > 0 && x + jj < _w)
                                                _img.Data[y + ii, x + jj, 0] = (byte)color;
                                    //DRAW PARABOLA
                                    double dydx = Math.Atan2(gradX.Data[y, x, 0], gradY.Data[y, x, 0]);
                                    double k1 = (-_sin[t] + (dydx * _cos[t])) / (2 * (_cos[t] + (dydx * _sin[t])));
                                    for (int dx = 0; dx < _w; dx++)
                                    {
                                        double a = (k1 * _cos[t]) + _sin[t];
                                        double b = (k1 * _sin[t]) - _cos[t];
                                        int dy = (int)((((dx - x) * a) - (y * b)) / (b * -1));
                                        if (dy >= 0 && dy < _h)
                                        {
                                            /*int dyAksen = (int)(-dx * _sin[t] + dy * _cos[t]);
                                            int yAksen = (int)(-x * _sin[t] + y * _cos[t]);
                                            int kuad = Math.Abs(dyAksen - yAksen);
                                            int y1 = (int)(((yAksen + kuad) + (dx * _sin[t])) / _cos[t]);
                                            int y2 = (int)(((yAksen - kuad) + (dx * _sin[t])) / _cos[t]);
                                            if (y1 >= 0 && y1 < _h)
                                                hsl.Data[y1, dx, 0] = (byte)color;
                                            if (y2 >= 0 && y2 < _h)
                                                hsl.Data[y2, dx, 0] = (byte)color;*/
                                            int kuad = Math.Abs(dy - y);
                                            if (y + kuad >= 0 && y + kuad < _h)
                                                hsl.Data[y + kuad, dx, 0] = (byte)color;
                                            if (y - kuad >= 0 && y - kuad < _h)
                                                hsl.Data[y - kuad, dx, 0] = (byte)color;
                                        }
                                    }
                                    for (int dy = 0; dy < _h; dy++)
                                    {
                                        double a = (k1 * _sin[t]) - _cos[t];
                                        double b = (k1 * _cos[t]) + _sin[t];
                                        int dx = (int)((((y - dy) * a + (x * b)) / b));
                                        if (dx >= 0 && dx < _w)
                                        {
                                            /*int dxAksen = (int)(dx * _cos[t] + dy * _sin[t]);
                                            int xAksen = (int)(x * _cos[t] + y * _sin[t]);
                                            int kuad = Math.Abs(dxAksen - xAksen);
                                            int x1 = (int)(((xAksen + kuad) - (dy * _sin[t])) / _cos[t]);
                                            int x2 = (int)(((xAksen - kuad) - (dy * _sin[t])) / _cos[t]);
                                            if (x1 >= 0 && x1 < _w)
                                                hsl.Data[dy, x1, 0] = (byte)color;
                                            if (x2 >= 0 && x2 < _w)
                                                hsl.Data[dy, x2, 0] = (byte)color;*/
                                            int kuad = Math.Abs(dx - x);
                                            if (x + kuad >= 0 && x + kuad < _w)
                                                hsl.Data[dy, x + kuad, 0] = (byte)color;
                                            if (x - kuad >= 0 && x - kuad < _w)
                                                hsl.Data[dy, x - kuad, 0] = (byte)color;
                                        }
                                    }
                                }
                                //_img.Draw(new CircleF(new Point(x, y), 3), new Gray(color), 1);
                            }
                        }

            return hsl;
        }

        public Image<Gray, byte> getCurve(int neighbor, int pilThresh, int thresh, int color, int dist,int gap)
        {
            Image<Gray, byte> hsl = new Image<Gray, byte>(_w, _h);
            int numPoints = votingYoValue();
            if (numPoints != 0)
            {
                int threshold=thresh;
                if (pilThresh == 0)
                    threshold = (int)(maxValue() * thresh / 100);
                hsl = findLocalMaxima(neighbor, threshold, color, dist, gap);
            }
            return hsl;
        }
    }
}
