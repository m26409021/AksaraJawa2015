﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Drawing;

namespace AksaraJawa2015
{
    class ZhangSuenThinning
    {
        static int NORTH = 1, SOUTH = 3;
        //private int _ra = 0, _rb = 0, _ca = 0, _cb = 0;
        private int _w, _h;
        protected Image<Gray, byte> _im;

        public ZhangSuenThinning()
        {
            _w = _h = 0;
            _im = new Image<Gray, byte>(_w, _h);
        }

        public void setImage(Image<Gray, Byte> img)
        {
            _w = img.Width;
            _h = img.Height;
            _im = new Image<Gray, byte>(_w, _h);
            //monochrome to binary
            for (int i = 0; i < _h; i++)
                for (int j = 0; j < _w; j++)
                    if (img.Data[i, j, 0] < 128)
                        _im.Data[i, j, 0] = 1;
                    else
                        _im.Data[i, j, 0] = 0;
        }
        public Image<Gray, Byte> pre_process(int winW, int winH)
        {
            int i, ii, j, jj;
            Image<Gray, byte> temp;
            int[,] mark;
            bool cek = true;

            temp = _im;
            temp = fit(_im, 0);//shrink
            _w = temp.Width;
            _h = temp.Height;
            mark = new int[_h, _w];
            for (i = winH; i < _h - winH; i++)
                for (j = winW; j < _w - winW; j++)
                {
                    cek = true;
                    for (ii = -winH; ii <= winH; ii++)
                        for (jj = -winW; jj <= winW; jj++)
                            if (temp.Data[i+ii, j+jj, 0] == 0)
                                cek = false;
                    if (cek == true)
                        mark[i, j] = 1;
                }
            for (i = winH; i < _h - winH; i++)
                for (j = winW; j < _w - winW; j++)
                    if (mark[i, j] == 1)
                        temp.Data[i, j, 0] = 0;
            //larger image
            _w += 2;
            _h += 2;
            _im = new Image<Gray, byte>(_w, _h); //larger image
            for (i = 0; i < _h; i++)
                for (j = 0; j < _w; j++)
                {
                    if (i > 0 && i < _h - 1 && j > 0 && j < _w - 1)
                        _im.Data[i, j, 0] = temp.Data[i-1, j-1, 0];
                }

            return _im;
        }

        private Image<Gray, Byte> fit(Image<Gray, Byte> img, int border)
        {
            bool cek, adaPoint = false;
            int i, j, ii, jj, w, h;
            int ra, rb, ca, cb;

            ra = rb = ca = cb = border;
            w = img.Width - border;
            h = img.Height - border;
            for (i = 0; i < h; i++)
                for (j = 0; j < w; j++)
                    if (img.Data[i, j, 0] == 1)
                    {
                        i = h + 1;
                        j = w + 1;
                        adaPoint = true;
                    }
            if (adaPoint)
            {
                //fit to up
                cek = true;
                while (cek)
                {
                    for (i = border; i < w && cek; i++)
                        if (img.Data[ra, i, 0] == 1)
                            cek = false;
                    if (cek) ra++;
                }
                //fit to bottom
                cek = true;
                j = h - 1;
                while (cek)
                {
                    for (i = border; i < w && cek; i++)
                        if (img.Data[j, i, 0] == 1)
                            cek = false;
                    if (cek)
                    {
                        j--;
                        rb++;
                    }
                }
                //fit to left
                cek = true;
                while (cek)
                {
                    for (i = border; i < h && cek; i++)
                        if (img.Data[i, ca, 0] == 1)
                            cek = false;
                    if (cek)
                        ca++;
                }
                //fit to right
                cek = true;
                j = w - 1;
                while (cek)
                {
                    for (i = border; i < h && cek; i++)
                        if (img.Data[i, j, 0] == 1)
                            cek = false;
                    if (cek)
                    {
                        j--;
                        cb++;
                    }
                }
                //shrink
                w = w + border;
                h = h + border;
                Image<Gray, byte> tmp = new Image<Gray, byte>(w - ca - cb, h - ra - rb);
                for (i = ra, ii = 0; i < h - rb; i++, ii++)
                    for (j = ca, jj = 0; j < w - cb; j++, jj++)
                        tmp.Data[ii, jj, 0] = img.Data[i, j, 0];
                return tmp;
            }
            return img;
        }

        public Image<Gray, Byte> skeletonizing()
        {
            int i, j, k, again = 1;
            Image<Gray, byte> tmp = new Image<Gray, byte>(_w, _h);

            //mark and delete
            while (again == 1)
            {
                again = 0;
                //second sub iteration
                for (i = 1; i < _h - 1; i++)
                {
                    for (j = 1; j < _w - 1; j++)
                    {
                        if (_im.Data[i, j, 0] != 1)
                            continue;
                        k = nays8(_im, i, j);
                        if ((k >= 2 && k <= 6) && connectify(_im, i, j) == 1)
                        {
                            if ((_im.Data[i, j + 1, 0] * _im.Data[i - 1, j, 0] * _im.Data[i, j - 1, 0] == 0) &&
                                (_im.Data[i - 1, j, 0] * _im.Data[i + 1, j, 0] * _im.Data[i, j - 1, 0] == 0))
                            {
                                tmp.Data[i, j, 0] = 1;
                                again = 1;
                            }
                        }
                    }
                }
                _im = delete(_im, tmp);
                if (again == 0)
                    break;

                //first sub iteration
                for (i = 1; i < _h - 1; i++)
                {
                    for (j = 1; j < _w - 1; j++)
                    {
                        if (_im.Data[i, j, 0] != 1)
                            continue;
                        k = nays8(_im, i, j);
                        if ((k >= 2 && k <= 6) && connectify(_im, i, j) == 1)
                        {
                            if (_im.Data[i - 1, j, 0] * _im.Data[i, j + 1, 0] * _im.Data[i + 1, j, 0] == 0 &&
                                _im.Data[i, j + 1, 0] * _im.Data[i + 1, j, 0] * _im.Data[i, j - 1, 0] == 0)
                            {
                                tmp.Data[i, j, 0] = 1; ;
                                again = 1;
                            }
                        }
                    }
                }
                _im = delete(_im, tmp);
            }
            //staircase removal
            stair(NORTH, _im);
            stair(SOUTH, _im);
            //post process
            _im = fit(_im, 1);

            return _im;
        }

        private Image<Gray,byte> delete(Image<Gray, Byte> im, Image<Gray, Byte> tmp) //delete pixel di im yg di mark 1 di tmp
        {
            int i, j;

            //delete pixel yg di mark
            for (i = 1; i < im.Height - 1; i++)
            {
                for (j = 1; j < im.Width - 1; j++)
                {
                    if (tmp.Data[i, j, 0] == 1)
                    {
                        im.Data[i, j, 0] = 0;
                        tmp.Data[i, j, 0] = 0;
                    }
                }
            }
            return im;
        }

        private int nays8(Image<Gray, Byte> im, int r, int c) //hitung jumlah neighbor pixel 1
        {
            int i, j, k = 0;
            for (i = r - 1; i <= r + 1; i++)
                for (j = c - 1; j <= c + 1; j++)
                    if (i != r || c != j)
                        if (im.Data[i, j, 0] >= 1)
                            k++;
            return k;
        }

        private int connectify(Image<Gray, Byte> im, int r, int c) //hitung connectivity number
        {
            int n = 0;
            if (im.Data[r, c + 1, 0] >= 1 && im.Data[r - 1, c + 1, 0] == 0)
                n++;
            if (im.Data[r - 1, c + 1, 0] >= 1 && im.Data[r - 1, c, 0] == 0)
                n++;
            if (im.Data[r - 1, c, 0] >= 1 && im.Data[r - 1, c - 1, 0] == 0)
                n++;
            if (im.Data[r - 1, c - 1, 0] >= 1 && im.Data[r, c - 1, 0] == 0)
                n++;
            if (im.Data[r, c - 1, 0] >= 1 && im.Data[r + 1, c - 1, 0] == 0)
                n++;
            if (im.Data[r + 1, c - 1, 0] >= 1 && im.Data[r + 1, c, 0] == 0)
                n++;
            if (im.Data[r + 1, c, 0] >= 1 && im.Data[r + 1, c + 1, 0] == 0)
                n++;
            if (im.Data[r + 1, c + 1, 0] >= 1 && im.Data[r, c + 1, 0] == 0)
                n++;
            return n;
        }

        private Image<Gray, byte> stair(int dir, Image<Gray, byte> im)
        {
            int i, j;
            int n, s, e, w, ne, nw, se, sw, c;
            int width = im.Width, height = im.Height;
            Image<Gray, byte> temp = new Image<Gray, byte>(width, height);

            //if (dir == NORTH)
            for (i = 1; i < height - 1; i++)
                for (j = 1; j < width - 1; j++)
                {
                    nw = im.Data[i - 1, j - 1, 0];
                    n = im.Data[i - 1, j, 0];
                    ne = im.Data[i - 1, j + 1, 0];
                    w = im.Data[i, j - 1, 0];
                    c = im.Data[i, j, 0];
                    e = im.Data[i, j + 1, 0];
                    sw = im.Data[i + 1, j - 1, 0];
                    s = im.Data[i + 1, j, 0];
                    se = im.Data[i + 1, j + 1, 0];
                    if (dir == NORTH)
                    {
                        if (c == 1 && !(n == 1 &&
                            ((e == 1 && ne != 1 && sw != 1 && (w != 1 || s != 1)) ||
                                (w == 1 && nw != 1 && se != 1 && (e != 1 || s != 1)))))
                            temp.Data[i, j, 0] = 0; //survive
                        else
                            temp.Data[i, j, 0] = 1;
                    }
                    else if (dir == SOUTH)
                    {
                        if (c == 1 && !(s == 1 &&
                            ((e == 1 && se != 1 && nw != 1 && (w != 1 || n != 1)) ||
                                (w == 1 && sw != 1 && ne != 1 && (e != 1 || n != 1)))))
                            temp.Data[i, j, 0] = 0; //survive
                        else
                            temp.Data[i, j, 0] = 1;
                    }
                }
            im = delete(im, temp);
            return im;
        }
    }
}
