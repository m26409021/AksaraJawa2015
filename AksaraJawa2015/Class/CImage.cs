﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;


namespace AksaraJawa2015
{
    class CImage
    {
        private const double phi = 3.141592653589793238462643383279502884197169399375105820974944;
        private Bitmap OriginalImage;
        private Bitmap CloneImage;
        private Color[][] originalColor;
        private Color[][] processColor;
        public String APP_PATH = System.IO.Path.GetDirectoryName(
                                    System.IO.Path.GetDirectoryName(
                                        System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory())
                                    )
                                 ) + "/Hasil Segmentasi/";
        public String imgName;
        private int image_width;
        private int image_height;

        private LinkedList<Vector3FT[]> GLOBAL_ROWLIST;
        private LinkedList<Vector3FT[]>[] GLOBAL_COLLIST;

        private Bitmap GLOBAL_TEMP_IMAGE;


        public CImage(String file_path)
        {
            // set orig image
            OriginalImage = new Bitmap(file_path);

            // set image name
            String[] temp = System.IO.Path.GetFileName(file_path).Split('.');
            imgName = temp[0];

            // clone to cloneImage as processed image
            CloneImage = new Bitmap(OriginalImage); // clone original image to processed image

            // set global image height and width
            image_height = OriginalImage.Height; // set height image;
            image_width = OriginalImage.Width; // set width image

            // set result folder
            // create folder if not exists
            bool isExists = System.IO.Directory.Exists(APP_PATH);
            if (!isExists)
                System.IO.Directory.CreateDirectory(APP_PATH);

            // set color array
            this.BitmapToArray();
        }

        protected void BitmapToArray()
        {
            originalColor = new Color[image_height][];
            for (int i = 0; i < image_height; i++)
            {
                originalColor[i] = new Color[image_width];
                for (int j = 0; j < image_width; j++)
                {
                    originalColor[i][j] = OriginalImage.GetPixel(j, i); // set the color image array
                }
            }
        }

        protected int[][] ColorToInt(Bitmap processedBitmap = null, int blackBit = 1, int whiteBit = 0)
        {
            int i, j;
            int[][] binaryImage;
            if (processedBitmap == null) processedBitmap = CloneImage;

            // initialize binary image;
            binaryImage = new int[processedBitmap.Height][];
            for (i = 0; i < processedBitmap.Height; i++)
            {
                binaryImage[i] = new int[processedBitmap.Width];
                for (j = 0; j < processedBitmap.Width; j++)
                {
                    if (processedBitmap.GetPixel(j, i) == Color.FromArgb(0, 0, 0))
                    {
                        // consider black as 1
                        binaryImage[i][j] = blackBit;
                    }
                    else
                    {
                        // consider white as 0
                        binaryImage[i][j] = whiteBit;
                    }
                }
            }

            return binaryImage;
        }

        protected void IntToProcessColor(int[][] binaryImage, int blackBit = 1, int whiteBit = 0)
        {
            int i, j;
            // convert to process
            for (i = 0; i < image_height; i++)
            {
                for (j = 0; j < image_width; j++)
                {
                    if (binaryImage[i][j] == blackBit)
                    {
                        // consider black as 1
                        this.processColor[i][j] = Color.FromArgb(0, 0, 0);
                    }
                    else if(binaryImage[i][j] == whiteBit)
                    {
                        // consider white as 0
                        this.processColor[i][j] = Color.FromArgb(255, 255, 255);
                    }
                }
            }
        }

        protected void render()
        {
            CloneImage.Dispose();
            CloneImage = new Bitmap(image_width, image_height);
            int i, j;
            // render process
            for (i = 0; i < image_height; i++)
            {
                for (j = 0; j < image_width; j++)
                {
                    this.CloneImage.SetPixel(j, i, this.processColor[i][j]);
                }
            }
        }

        public void toGrey()
        {
            int greyNumber;

            image_height = OriginalImage.Height; image_width = OriginalImage.Width;
            // init process color
            processColor = new Color[image_height][];
            for (int i = 0; i < image_height; i++)
            {
                processColor[i] = new Color[image_width];
            }

            for (int i = 0; i < image_height; i++)
            {
                for (int j = 0; j < image_width; j++)
                {
                    // calculate greynumber from original color
                    greyNumber = (int)(0.333 * originalColor[i][j].R + 0.5 * originalColor[i][j].G + 0.16 * originalColor[i][j].B);
                    Color grey = Color.FromArgb(greyNumber, greyNumber, greyNumber); // new grey value
                    processColor[i][j] = grey; // set image process color
                }
            }

            // render image
            render();
        }

        protected int[][] setKernel(int size, int mode)
        {
            int[][] result;

            if (mode == -1)
            {   
                // kernel mode 0
                /*
                 * -1 -1 -1
                 * -1 -1 -1
                 * -1 -1 -1
                 */

                result = new int[size][];
                for (int i = 0; i < size; i++)
                {
                    result[i] = new int[size];
                    for (int j = 0; j < size; j++)
                    {
                        result[i][j] = -1;
                    }
                }
                return result;
            }
            else if (mode == 0)
            {
                // kernel mode 0
                /*
                 * MEAN FILTER BLUR KERNEL
                 * 1 1 1
                 * 1 1 1
                 * 1 1 1
                 */
                result = new int[size][];
                for (int i = 0; i < size; i++)
                {
                    result[i] = new int[size];
                    for (int j = 0; j < size; j++)
                    {
                        result[i][j] = 1;
                    }
                }
                return result;
            }
            else if (mode == 1)
            {
                // kernel mode 1
                /*
                 * 0 1 0
                 * 1 1 1
                 * 0 1 0
                 */

                // all are set up to 0
                result = new int[size][];
                for (int i = 0; i < size; i++)
                    result[i] = new int[size];

                // segitiga atas
                for (int i = 0; i <= size / 2; i++)
                {
                    for (int j = 0; j < size / 2 - i; j++)
                    {
                        result[i][j] = 0;
                    }

                    for (int j = size / 2 - i; j <= size / 2; j++)
                    {
                        result[i][j] = 1;
                    }

                    for (int j = size / 2; j <= size / 2 + i; j++)
                    {
                        result[i][j] = 1;
                    }

                    for (int j = size / 2 + i + 1; j < size; j++)
                    {
                        result[i][j] = 0;
                    }
                }

                // segitiga bawah
                for (int i = size / 2 + 1; i < size; i++)
                {

                    for (int j = 0; j < i - size / 2; j++)
                    {
                        result[i][j] = 0;
                    }

                    for (int j = i - size / 2; j <= size / 2; j++)
                    {
                        result[i][j] = 1;
                    }

                    for (int j = size - i + size / 2; j > size / 2; j--)
                    {
                        result[i][j] = 1;
                    }

                    for (int j = size - i + size / 2; j < size; j++)
                    {
                        result[i][j] = 0;
                    }
                }
                return result;
            }
            else if (mode == 2)
            {
                // kernel mode 2
                /*
                 * -1 -1 -1
                 * -1  8 -1
                 * -1 -1 -1
                 * 
                 */

                result = new int[size][];
                for (int i = 0; i < size; i++)
                {
                    result[i] = new int[size];
                    for (int j = 0; j < size; j++)
                    {
                        result[i][j] = -1;
                    }
                }

                result[size / 2][size / 2] = 8;
                return result;

            }
            else
            {
                // default mode
                /*
                 * 0 0 0
                 * 0 0 0
                 * 0 0 0
                 */

                result = new int[size][];
                for (int i = 0; i < size; i++)
                {
                    result[i] = new int[size];
                    for (int j = 0; j < size; j++)
                    {
                        result[i][j] = 0;
                    }
                }
                return result;
            }
        }

        protected int calculate_kernelValue(int[][] t_kernel, int kernelSize)
        {
            int result = 0;

            for (int i = 0; i < kernelSize; i++)
            {
                for (int j = 0; j < kernelSize; j++)
                {
                    result += t_kernel[i][j];
                }
            }

            return result;
        }
        
        // monochrome
        public void mono(int threshold = 128, bool invert = false)
        {
            int i, j;
            for (i = 0; i < image_height; i++)
            {
                for (j = 0; j < image_width; j++)
                {
                    if (!invert)
                    {
                        if (processColor[i][j].R <= threshold)
                        {
                            processColor[i][j] = Color.FromArgb(0, 0, 0);
                        }
                        else
                        {
                            processColor[i][j] = Color.FromArgb(255, 255, 255);
                        }
                    }
                    else if (invert)
                    {
                        if (processColor[i][j].R <= threshold)
                        {
                            processColor[i][j] = Color.FromArgb(255, 255, 255);
                        }
                        else
                        {
                            processColor[i][j] = Color.FromArgb(0, 0, 0);
                        }
                    }
                }
            }

            // render image
            render();
        }

        public Bitmap mono(Bitmap img = null, int threshold = 128, bool invert = false)
        {
            int i, j;
            for (i = 0; i < img.Height; i++)
            {
                for (j = 0; j < img.Width; j++)
                {
                    if (!invert)
                    {
                        if (img.GetPixel(j, i).R <= threshold)
                        {
                            img.SetPixel(j, i, Color.FromArgb(0, 0, 0));
                        }
                        else
                        {
                            img.SetPixel(j, i, Color.FromArgb(255, 255, 255));
                        }
                    }
                    else if (invert)
                    {
                        if (img.GetPixel(j, i).R <= threshold)
                        {
                            img.SetPixel(j, i, Color.FromArgb(255, 255, 255));
                        }
                        else
                        {
                            img.SetPixel(j, i, Color.FromArgb(0, 0, 0));
                        }
                    }
                }
            }

            return img;
        }

        public void Histogram()
        {
            // function to generate histogram of the image.
            int[] histogram_r = new int[256];
            float max = -99999;
            int i, j;

            for (i = 0; i < image_width; i++)
            {
                for (j = 0; j < image_height; j++)
                {
                    int redValue = processColor[j][i].G;
                    histogram_r[redValue]++;
                    if (max < histogram_r[redValue])
                    {
                        max = histogram_r[redValue];
                    }
                }
            }

            int histHeight = 100;
            Bitmap img = new Bitmap(256, histHeight + 10);
            using (Graphics g = Graphics.FromImage(img))
            {
                g.Clear(Color.FromArgb(255, 255, 255));
                for (i = 0; i < histogram_r.Length; i++)
                {
                    float pct = histogram_r[i] / max;   // What percentage of the max is this value?
                    g.DrawLine(Pens.Black,
                        new Point(i, img.Height - 5),
                        new Point(i, img.Height - 5 - (int)(pct * histHeight))  // Use that percentage of the height
                        );
                }
            }

            img.Save(APP_PATH + "histogram-" +imgName + ".jpg");

            // render to processed image
            render();
        }
        
        // percuma di stretch
        public void histogramStretch()
        {
            int[] histogram_r = new int[256];
            float max = -99999;
            int i, j;

            for (i = 0; i < image_width; i++)
            {
                for (j = 0; j < image_height; j++)
                {
                    int redValue = processColor[j][i].G;
                    histogram_r[redValue]++;
                    if (max < histogram_r[redValue])
                    {
                        max = histogram_r[redValue];
                    }
                }
            }

            // apply histogram stratching
            // find histogram median
            int count = 0;
            double median = 0;

            // find median value, min value, max value, interval
            int medValue = 0;
            int minVal = 999;
            int maxVal = 0;

            count = image_height * image_width;
            for (i = 0; i < 256; i++)
            {
                if (histogram_r[i] != 0)
                {
                    if (i < minVal)
                        minVal = i;

                    if (i > maxVal)
                        maxVal = i;
                }
            }

            median = count / 2;
            int jum = 0, posisi = 0;
            while (jum < median)
            {
                jum += histogram_r[posisi++];
            }

            medValue = posisi - 1;

            //median = image_height * image_width / 4;
            //int jum2 = 0, posisi2 = 0, medValue2 = 0;
            //while (jum2 < median && posisi2 < medValue)
            //{
            //    jum2 += histogram_r[posisi2++];
            //}

            //medValue2 = posisi2 - 1;
            for (i = 0; i < 256; i++)
            {
                Console.WriteLine("Histogram ke " + i + " : " + histogram_r[i]);
            }

            //double interval1 = medValue2 - minVal;
            //double interval2 = maxVal - medValue2;
            
            //double interval3 = maxVal - minVal;
            //double newColor = 0;
            //for (i = 0; i < image_height; i++)
            //{
            //    for (j = 0; j < image_width; j++)
            //    {
            //        if (processColor[i][j].R <= medValue2)
            //        {
            //            // if color < median value stretch min 0 to 128
            //            newColor = (processColor[i][j].R - minVal) / interval1 * 128;
            //        }
            //        else
            //        {
            //            // if color > median value stretch min 129 max 255
            //            newColor = 127 + ((processColor[i][j].R - medValue2) / interval2 * 128);
            //        }

            //        // newColor = (processColor[i][j].R - minVal) / interval3 * 255;
            //        // processColor[i][j] = Color.FromArgb((int)newColor, (int)newColor, (int)newColor);
            //    }
            //}

            // Console.WriteLine("MaxVal : " + maxVal + ", MinVal : " + minVal + ", Median Value : " + medValue + ", Median Value 2 : " + medValue2);
            // display histogram
            int histHeight = 100;
            Bitmap img = new Bitmap(256, histHeight + 10);
            using (Graphics g = Graphics.FromImage(img))
            {
                g.Clear(Color.FromArgb(255, 255, 255));
                for (i = 0; i < histogram_r.Length; i++)
                {
                    float pct = histogram_r[i] / max;   // What percentage of the max is this value?
                    g.DrawLine(Pens.Black,
                        new Point(i, img.Height - 5),
                        new Point(i, img.Height - 5 - (int)(pct * histHeight))  // Use that percentage of the height
                        );
                }
            }

            img.Save(APP_PATH + "stretched-histogram-"+ imgName + ".jpg");

            // render to processed image
            render();
        }

        // mean filter blur
        #region meanFilter
        public void meanFilter(int kernel_size = 3)
        {
            int[][] kernel;
            double greyNumber;
            // dynamic kernel size
            int size = kernel_size;
            // loop variable
            int i, j, m, n;

            // inisialisasi kernel
            kernel = setKernel(size, 1);

            // calculate kernel value
            int kernelValue = calculate_kernelValue(kernel, size);

            // make temp for color
            double[][] tempColor;
            tempColor = new double[image_height][];
            for (i = 0; i < image_height; i++)
            {
                tempColor[i] = new double[image_width];
                for (j = 0; j < image_width; j++)
                {
                    tempColor[i][j] = 0; // set temp color to be 0
                }
            }

            int min = 999999999;
            int max = -99999999;
            int tot;
            for (i = (size / 2); i < image_height - (size / 2); i++) // loop for image height
            {
                for (j = (size / 2); j < image_width - (size / 2); j++) // loop for image width
                {
                    greyNumber = 0;
                    for (m = -(size / 2); m < (size / 2); m++) // loop for masking
                    {
                        for (n = -(size / 2); n < (size / 2); n++) // loop for masking
                        {
                            greyNumber += processColor[i + m][j + n].R * kernel[m + (size / 2)][n + (size / 2)]; // calculate pixel, warna sudah grey
                            // each pixel * kernel
                        }
                    }

                    tot = (int) (greyNumber / kernelValue); // div by kernel value

                    if (tot < min)
                        min = tot;
                    if(tot > max)
                        max = tot;

                    tempColor[i][j] = tot;
                }
            }

            // rescale pixel
            int temp;
            for (i = (size / 2); i < image_height - (size / 2); i++)
            {
                temp = 0;
                for (j = (size / 2); j < image_width - (size / 2); j++)
                {
                    // neutralize color
                    temp = (int)(((tempColor[i][j] - min) / (max - min)) * 255);
                    Color res = Color.FromArgb(temp, temp, temp);
                    processColor[i][j] = res;
                }
            }

            // render image
            render();
        }
        #endregion

        // bilinear resize
        #region bilinearResize
        public void bilinearResize(int size = 2)
        {
            int i,j; // looping variable

            // new width and height
            int t_height = image_height * size;
            int t_width = image_width * size;

            // create new color with new width and height
            int[] oldPixelTotal = new int[image_height * image_width];
            int cOldPixel = 0;
            for (i = 0; i < image_height; i++)
            {
                for (j = 0; j < image_width; j++)
                {
                    oldPixelTotal[cOldPixel] = processColor[i][j].R;
                    cOldPixel++;
                }
            }

            int[] newPixelTotal = new int[t_height * t_width];
            newPixelTotal = resizeBilinearGray(oldPixelTotal, image_width, image_height, t_width, t_height);

            Color[][] result = new Color[t_height][];
            int c = 0;
            for (i = 0; i < t_height; i++)
            {
                result[i] = new Color[t_width];
                for (j = 0; j < t_width; j++)
                {
                    result[i][j] = Color.FromArgb(newPixelTotal[c], newPixelTotal[c], newPixelTotal[c]);
                    c++;
                }
            }

            // create new color based on result
            processColor = new Color[t_height][];
            for (i = 0; i < t_height; i++)
            {
                processColor[i] = new Color[t_width];
                for (j = 0; j < t_width; j++)
                {
                    processColor[i][j] = result[i][j];
                }
            }

            // create new clone image based on result
            CloneImage = new Bitmap(t_width, t_height);

            // set new image width and height
            image_height = t_height;
            image_width = t_width;
            // render process color to clone image
            render();
        }

        public int[] resizeBilinearGray(int[] pixels, int w, int h, int w2, int h2)
        {
            int[] temp = new int[w2 * h2];
            int A, B, C, D, x, y, index, gray;
            float x_ratio = ((float)(w - 1)) / w2;
            float y_ratio = ((float)(h - 1)) / h2;
            float x_diff, y_diff;
            int offset = 0;
            for (int i = 0; i < h2; i++)
            {
                for (int j = 0; j < w2; j++)
                {
                    x = (int)(x_ratio * j);
                    y = (int)(y_ratio * i);
                    x_diff = (x_ratio * j) - x;
                    y_diff = (y_ratio * i) - y;
                    index = y * w + x;

                    // range is 0 to 255 thus bitwise AND with 0xff
                    A = pixels[index] & 0xff;
                    B = pixels[index + 1] & 0xff;
                    C = pixels[index + w] & 0xff;
                    D = pixels[index + w + 1] & 0xff;

                    // Y = A(1-w)(1-h) + B(w)(1-h) + C(h)(1-w) + Dwh
                    gray = (int)(
                            A * (1 - x_diff) * (1 - y_diff) + B * (x_diff) * (1 - y_diff) +
                            C * (y_diff) * (1 - x_diff) + D * (x_diff * y_diff)
                            );

                    temp[offset++] = gray;
                }
            }
            return temp;
        }
        #endregion

        // hit and miss
        #region hitandmiss
        public void hitandmiss()
        {
            int[][][] kernel;
            // initialize process color to binary image
            int[][] binaryImage = ColorToInt();
            int mask_size = 3;      // kernel size is 3
            int i, j, m, n, z;      // loop variable
            bool flag;

            // initialize array result temporarily
            int[][] result;
            result = new int[image_height][];
            for (i = 0; i < image_height; i++)
            {
                result[i] = new int[image_width];
                for (j = 0; j < image_width; j++)
                {
                    result[i][j] = 0;
                }
            }
            #region initialize structuring element
                int tot_kernel = 4;
                kernel = new int[tot_kernel][][];
                // kernel - 0
                /*
                 *     0  0
                 *  1  1  0
                 *     1   
                 * 
                 */
                
                kernel[0] = setKernel(3, -1);
                kernel[0][0][1] = 0;
                kernel[0][0][2] = 0;
                kernel[0][1][0] = 1;
                kernel[0][1][1] = 1;
                kernel[0][1][2] = 0;
                kernel[0][2][1] = 1;

                // kernel - 1
                /*
                 *     1   
                 *  1  1  0
                 *     0  0
                 * 
                 */
                
                kernel[1] = setKernel(3, -1);
                kernel[1][0][1] = 1;
                kernel[1][1][0] = 1;
                kernel[1][1][1] = 1;
                kernel[1][1][2] = 0;
                kernel[1][2][1] = 0;
                kernel[1][2][2] = 0;

                // kernel - 2
                /*
                 *     1   
                 *  0  1  1
                 *  0  0   
                 * 
                 */

                kernel[2] = setKernel(3, -1);
                kernel[2][0][1] = 1;
                kernel[2][1][0] = 0;
                kernel[2][1][1] = 1;
                kernel[2][1][2] = 1;
                kernel[2][2][0] = 0;
                kernel[2][2][1] = 0;

                // kernel - 3
                /*
                 *  0  0   
                 *  0  1  1
                 *     1   
                 * 
                 */
                
                kernel[3] = setKernel(3, -1);
                kernel[3][0][0] = 0;
                kernel[3][0][1] = 0;
                kernel[3][1][0] = 0;
                kernel[3][1][1] = 1;
                kernel[3][1][2] = 1;
                kernel[3][2][1] = 1;
            #endregion

                int bgColor = 0;
                int fgColor = 1;
            // thinning process
            for (z = 0; z < tot_kernel; z++)    // loop for total kernel
            {   
                for (i = (mask_size / 2); i < image_height - (mask_size / 2); i++) // loop for image height
                {
                    for (j = (mask_size / 2); j < image_width - (mask_size / 2); j++) // loop for image width
                    {
                        // consider as true
                        flag = true;
                        for (m = -(mask_size / 2); m < (mask_size / 2); m++) // loop for masking
                        {
                            for (n = -(mask_size / 2); n < (mask_size / 2); n++) // loop for masking
                            {
                                if (kernel[z][m + (mask_size / 2)][n + (mask_size / 2)] != -1)
                                {
                                    // check if pixel in image are match with the structuring element
                                    if (binaryImage[i + m][j + n] != kernel[z][m + (mask_size / 2)][n + (mask_size / 2)])
                                    {
                                        flag = false; break;
                                    }
                                }
                            
                            }
                            if (!flag)
                                break;
                        }

                        if (flag)
                        {
                            // if structure element match with pixel image, then origin in result is set to foreground color
                            result[i][j] = fgColor;
                        }
                        else
                        {
                            // if it doesn't match, origin set to background color
                            result[i][j] = bgColor;
                        }
                    }
                }
            }
            IntToProcessColor(result);
            render();
        }
        #endregion

        // thinning
        #region thinning
        public void thinning()
        {
            int i, j; // loop variable

            // binary image
            int[][] binaryImage;
            // initialize binary image change processColor to array of int
            binaryImage = ColorToInt();

            // temp;
            int a, b;
            bool change;

            int mask_size = 3;
            List<Point> pointsToChange = new List<Point>();
            int c = 0;
            do
            {
                c++;
                change = false;
                for (i = (mask_size / 2); i < image_height - (mask_size / 2); i++) // loop for image height
                {
                    for (j = (mask_size / 2); j < image_width - (mask_size / 2); j++) // loop for image width
                    {
                        // calculate A(p1)
                        a = getA(binaryImage, i, j);

                        // calculate B(p1)
                        b = getB(binaryImage, i, j);

                        if ( binaryImage[i][j] == 1 
                                && 2 <= b && b <= 6 && a == 1
                                && (binaryImage[i - 1][j] * binaryImage[i][j + 1] * binaryImage[i + 1][j] == 0)
                                && (binaryImage[i][j + 1] * binaryImage[i + 1][j] * binaryImage[i][j - 1] == 0)) 
                        {
                            pointsToChange.Add(new Point(j, i));
                            change= true;
                        }
                    }
                }

                for (i = 0; i < pointsToChange.Count; i++)
                {
                    Point point = (Point)pointsToChange.ElementAt(i);
                    binaryImage[point.Y][point.X] = 0;
                }

                // reset list
                pointsToChange.Clear();

                for (i = (mask_size / 2); i < image_height - (mask_size / 2); i++) // loop for image height
                {
                    for (j = (mask_size / 2); j < image_width - (mask_size / 2); j++) // loop for image width
                    {
                        // calculate A(p1)
                        a = getA(binaryImage, i, j);

                        // calculate B(p1)
                        b = getB(binaryImage, i, j);

                        if ( binaryImage[i][j] == 1 
                                && 2 <= b && b <= 6 && a == 1
                                && (binaryImage[i - 1][j] * binaryImage[i][j + 1] * binaryImage[i][j - 1] == 0)
                                && (binaryImage[i - 1][j] * binaryImage[i + 1][j] * binaryImage[i][j - 1] == 0)) 
                        {
                            pointsToChange.Add(new Point(j, i));
                            change= true;
                        }
                    }
                }

                for (i = 0; i < pointsToChange.Count; i++)
                {
                    Point point = (Point)pointsToChange.ElementAt(i);
                    binaryImage[point.Y][point.X] = 0;
                }

                // reset list
                pointsToChange.Clear();
            }
            while (change);

            // convert from binary image to process color
            this.IntToProcessColor(binaryImage);
            this.render();
        }

        private int getA(int[][] binaryImage, int y, int x)
        {
            int count = 0;
            //p2 p3
            if (binaryImage[y - 1][x] == 0 && binaryImage[y - 1][x + 1] == 1)
            {
                count++;
            }
            //p3 p4
            if (binaryImage[y - 1][x + 1] == 0 && binaryImage[y][x + 1] == 1)
            {
                count++;
            }
            //p4 p5
            if (binaryImage[y][x + 1] == 0 && binaryImage[y + 1][x + 1] == 1)
            {
                count++;
            }
            //p5 p6
            if (binaryImage[y + 1][x + 1] == 0 && binaryImage[y + 1][x] == 1)
            {
                count++;
            }
            //p6 p7
            if (binaryImage[y + 1][x] == 0 && binaryImage[y + 1][x - 1] == 1)
            {
                count++;
            }
            //p7 p8
            if (binaryImage[y + 1][x - 1] == 0 && binaryImage[y][x - 1] == 1)
            {
                count++;
            }
            //p8 p9
            if (binaryImage[y][x - 1] == 0 && binaryImage[y - 1][x - 1] == 1)
            {
                count++;
            }
            //p9 p2
            if (binaryImage[y - 1][x - 1] == 0 && binaryImage[y - 1][x] == 1)
            {
                count++;
            }

            return count;
        }

        private int getB(int[][] binaryImage, int y, int x)
        {

            return binaryImage[y - 1][x] + binaryImage[y - 1][x + 1] + binaryImage[y][x + 1]
                    + binaryImage[y + 1][x + 1] + binaryImage[y + 1][x] + binaryImage[y + 1][x - 1]
                    + binaryImage[y][x - 1] + binaryImage[y - 1][x - 1];
        }
        #endregion

        // skew detection and correction
        #region skew detection and correction
        // skew detection using hough transform
        public void houghTransform()
        {
            int i, j;
            // define counter for hough transform
            int rho;
            double theta;
            double angle = 179;
            int[][] accArray;

            /* 
             * HOUGH TRANSFORM
             * source : C:/ ---- my source ------
             * accArray[p][o] = accumulator array is multidimension array
             * p = rho, where p elment are -D,D (D is diagonal image)
             * o = angles, where o element are 0 to 180 degree
             * 
             */

            // D = -D, D;
            int rho_length = (int)Math.Sqrt(Math.Pow(image_height, 2) + Math.Pow(image_width, 2));
            int theta_length = 179;

            accArray = new int[rho_length * 2][]; // rho_length * 2 because p = -D to D
            for (i = 0; i < rho_length * 2; i++)
            {
                accArray[i] = new int[theta_length + 1]; // 0 to 180
                for (j = 0; j <= theta_length; j++)
                {
                    accArray[i][j] = 0; // set accArray to 0;
                }
            }

            // define int color image, 0 is background (white), 1 is foreground (black)
            int[][] intColorImage = ColorToInt();

            // hough transform
            // y axis
            for (i = 0; i < image_height; i++)
            {
                // x axis
                for (j = 0; j < image_width; j++)
                {
                    // check if color is object
                    if (intColorImage[i][j] == 1)
                    {
                        for (theta = 0; theta <= angle; theta += 1)
                        {
                            // p = x cos 0 + y sin 0
                            rho = (int)Math.Round((j * Math.Cos(theta * phi / 180)) + (i * Math.Sin(theta * phi / 180)));
                            accArray[rho + rho_length][(int)theta] = accArray[rho + rho_length][(int)theta] + 1;
                        }
                    }
                }
            }

            // find max value in accumulator array
            int max = -999999999;
            int pos_rho = 0; int pos_theta = 0;
            for (i = 0; i < rho_length * 2; i++)
            {
                for (j = 0; j < theta_length; j++)
                {
                    if (accArray[i][j] > max)
                    {
                        max = accArray[i][j];
                        pos_rho = i; pos_theta = j;
                    }
                }
            }

            // max value, and max position
            pos_rho = pos_rho - rho_length;

            // normalize theta degree
            pos_theta = pos_theta - 90;

            Console.WriteLine("pos theta: " + pos_theta.ToString());

            // rotate image
            CloneImage = rotateImage(CloneImage, -pos_theta);

            // set process color = CloneImage
            for (i = 0; i < image_height; i++)
            {
                for (j = 0; j < image_width; j++)
                {
                    processColor[i][j] = CloneImage.GetPixel(j, i);
                }
            }
        }
        // skew correction using graphic rotate
        public Bitmap rotateImage(Bitmap bitmap, float angle)
        {
            Bitmap returnBitmap = new Bitmap(bitmap.Width, bitmap.Height);
            Graphics graphics = Graphics.FromImage(returnBitmap);
            graphics.Clear(Color.FromArgb(255, 255, 255));
            graphics.TranslateTransform((float)bitmap.Width / 2, (float)bitmap.Height / 2);
            graphics.RotateTransform(angle);
            graphics.TranslateTransform(-(float)bitmap.Width / 2, -(float)bitmap.Height / 2);
            graphics.DrawImage(bitmap, new Point(0, 0));
            return returnBitmap;
        }
        #endregion

        // Segmentation
        #region segmentation based on histogram
        /* coba line segmentation based on "Handwritting segmentation of unconstrained Oriya text"
         * By : N Tripathy and U Pal
         * File : Drive/Skripsi/documents/kumpulan paper/dipake/segmentation/line segmentation sinhala script (paper2).pdf
         */
        public void lineSegmentTest()
        {
            // counter variable, etc variable
            int i, j, k;

            // define width of PSL (piece-wise seperating lines)
            int psl_width = 102; // berdasarkan rata2 36 data di folder /hasil percobaan/segmentation/2
            /* last width = z - w * (n - 1)
             * z = text width / document width;
             * n = number of stripe;
             * w = psl width;
             */

            // define total stripe
            int totalStripe = image_width / psl_width;
            // divide text document into vertical stripes of width psl_width
            Bitmap separateImageTemp;
            // vertical projection image
            Bitmap HProjectionProfile;
            // list to define top and bottom rows;
            LinkedList<Vector3FT[]>[] PSL = new LinkedList<Vector3FT[]>[totalStripe];

            // set result folder
            // create folder if not exists
            String filePath = APP_PATH + imgName + "/";
            bool isExists = System.IO.Directory.Exists(filePath);
            if (!isExists)
                System.IO.Directory.CreateDirectory(filePath);

            for (i = 0; i < totalStripe; i++)
            {
                // vertical stripe
                separateImageTemp = new Bitmap(CopyImage(CloneImage, (i * psl_width), 0, (i * psl_width) + psl_width, image_height));
                // find horizontal histogram
                HProjectionProfile = new Bitmap(HProjectionMethod(separateImageTemp));
                PSL[i] = ProcessHorizontalProjection(HProjectionProfile, image_height, 4, 0);
                // save vertical stripe image
                // separateImageTemp.Save(filePath + "line_segment-" + i + ".jpg");
            }

            // count potential PSL
            int[] Mpsl = new int[totalStripe];
            for (i = 0; i < totalStripe; i++)
            {
                // calculate mode of distance, and mean of distance
                // calculate array of distance
                int[] tempDistance = new int[PSL[i].Count];
                for (j = 0; j < PSL[i].Count - 1; j++)
                {
                    int nodeone = (int)PSL[i].ElementAt(j).ElementAt(1).Y;
                    int nodetwo = (int)PSL[i].ElementAt(j + 1).ElementAt(1).Y;

                    tempDistance[j] = nodetwo - nodeone;
                }

                // initial variable
                int mode, mean, median;
                int[] tempMode;
                // lumayan kalo pake mean, see the result on hasil percobaan/PSL
                // pake mode agak kacau, tetapi hampir semua bagian tulisan dapat terdeteksi
                // meskipun ada beberapa yang gak penting. but it's good to try check the result on hasil percobaan/PSL
                mean = mode = median = 0;
                tempMode = new int[tempDistance.Length];

                // sorting temp distance
                for (j = 0; j < tempDistance.Length; j++)
                {
                    for (k = 0; k < tempDistance.Length; k++)
                    {
                        if (tempDistance[k] > tempDistance[j] && j != k)
                        {
                            int temp = tempDistance[j];
                            tempDistance[j] = tempDistance[k];
                            tempDistance[k] = temp;
                        }
                    }
                }

                for (j = 0; j < tempDistance.Length; j++)
                {
                    // find mean
                    mean += tempDistance[j];

                    // find mode
                    tempMode[j] = 0;
                    for (k = 0; k < tempDistance.Length; k++)
                    {
                        if (tempDistance[j] == tempDistance[k] && j != k) tempMode[j]++;
                    }
                }

                // find mode
                int posMode = -9999;
                for (j = 0; j < tempMode.Length; j++)
                {
                    // find max value of tempMode
                    if (tempMode[j] > posMode) posMode = j;
                }

                // find mean
                if (tempDistance.Length != 0)
                {
                    // set mean
                    mean = mean / tempDistance.Length;
                    // set mode
                    mode = tempDistance[posMode];
                    // set median
                    if (tempDistance.Length % 2 == 0) median = (tempDistance[tempDistance.Length / 2] + tempDistance[(tempDistance.Length / 2) + 1]) / 2;
                    else median = tempDistance[tempDistance.Length / 2];

                    // set Mpsl use for next process, joining PSLs
                    // if using mean, then set with mean
                    // if using mode, set with mode

                    Mpsl[i] = median;
                }

                // debug mean and mode value
                Console.WriteLine("stripe ke-" + i + ", mean: " + mean.ToString() + ", mode: " + mode.ToString() + ", median: " + median.ToString());

                // delete unpotential PSL
                // using mean
                for (j = 0; j < PSL[i].Count - 1; j++)
                {
                    int nodeone = (int)PSL[i].ElementAt(j).ElementAt(1).Y;
                    int nodetwo = (int)PSL[i].ElementAt(j + 1).ElementAt(1).Y;

                    int distance = nodetwo - nodeone;
                    if (distance < median)
                    {
                        // unpotential PSL 
                        // delete
                        PSL[i].Remove(PSL[i].ElementAt(j));
                    }

                    //if (distance < mean)
                    //{
                    //    // unpotential PSL 
                    //    // delete
                    //    PSL[i].Remove(PSL[i].ElementAt(j));
                    //}

                    //if (distance < mode)
                    //{
                    //    // unpotential PSL 
                    //    // delete
                    //    PSL[i].Remove(PSL[i].ElementAt(j));
                    //}
                }
            }

            // connecting PSL
            /*
             * connectedTo is array of int, to save the PSL connected to next PSL
             * array connectedTo save the index of connected PSL
             */
            int[][] connectedTo = new int[totalStripe][];
            // temp connected to temporary save connected to
            // because element that are connected to the stripe may more than 1
            int tempConnected;
            // join the PSL from right to left
            for (i = totalStripe - 1; i > 0; i--)
            {
                connectedTo[i] = new int[PSL[i].Count];
                for (j = 0; j < PSL[i].Count; j++)
                {
                    // initialize connected to
                    connectedTo[i][j] = 0;
                    // check PSL[i-1] normal distance is less than Mpsl
                    // define Ki
                    int Ki = (int)PSL[i].ElementAt(j).ElementAt(1).Y;

                    // set tempConnected
                    tempConnected = 9999;
                    for (k = 0; k < PSL[i - 1].Count; k++)
                    {
                        int nodeToConnect = (int)PSL[i - 1].ElementAt(k).ElementAt(1).Y;
                        int distance = Ki - nodeToConnect;
                        // absolute distance
                        if (distance < 0) distance *= -1;

                        if (distance < Mpsl[i] && tempConnected > distance)
                        {
                            // connected!
                            tempConnected = distance;
                            // set connected to specific index on PSL i - 1
                            connectedTo[i][j] = k;
                        }
                    }
                    // if exist
                    // if not exist
                }
            }

            for (i = totalStripe - 1; i > 0; i--)
            {
                for (j = 0; j < PSL[i].Count; j++)
                {
                    Console.WriteLine("PSL ke-" + i + "-" + j + " Connected to: " + connectedTo[i][j]);
                    double top = PSL[i].ElementAt(j).ElementAt(0).Y;
                    double bottom = PSL[i].ElementAt(j).ElementAt(1).Y;

                    for (k = 0; k < psl_width; k++)
                    {
                        // draw top lines
                        // processColor[(int)top][k + (i * psl_width)] = Color.FromArgb(0, 0, 0);
                        // draw bottom lines of psl
                        processColor[(int)bottom][k + (i * psl_width)] = Color.FromArgb(0, 0, 0);
                    }

                    // draw connected parts
                    // check if have connectedTo parts
                    //if (connectedTo[i][j] != 0)
                    //{
                    //    // have connected PSL
                    //    // draw connected parts
                    //    // get bottom vector
                    //    Vector3FT connected = PSL[i - 1].ElementAt(connectedTo[i][j]).ElementAt(1);
                    //    if (connected.Y > bottom)
                    //    {
                    //        // lower
                    //        for (k = (int)bottom; k < connected.Y; k++)
                    //        {
                    //            processColor[k][i * psl_width] = Color.FromArgb(0, 0, 0);
                    //        }
                    //    }
                    //    else if (connected.Y < bottom)
                    //    {
                    //        // upper
                    //        for (k = (int)connected.Y; k < bottom; k++)
                    //        {
                    //            processColor[k][i * psl_width] = Color.FromArgb(0, 0, 0);
                    //        }
                    //    }
                    //}
                }
            }

            render();
        }

        public void projectionProfile(Bitmap processedImage = null, String filename = null)
        {
            int i, j, iwidth, iheight, range;
            bool isExists;
            // range is for threshold when crop image
            // create directory to save images result
            String filePath = APP_PATH + imgName + "/";
            if (processedImage == null)
            {
                range = 4;
                // create folder if not exists
                isExists = System.IO.Directory.Exists(filePath);
                if (!isExists)
                    System.IO.Directory.CreateDirectory(filePath);
            }
            else
            {
                filePath += filename + "/";
                range = 2;
            }

            // process clone image
            // step 1. get horizontal histogram
            Bitmap HProjectionProfile;
            if (processedImage == null) processedImage = CloneImage;
            HProjectionProfile = HProjectionMethod(processedImage);
            iwidth = processedImage.Width; iheight = processedImage.Height;

            // save horizontal histogram
            if(processedImage == CloneImage) HProjectionProfile.Save(filePath + "/hhistogram.jpg");
            else HProjectionProfile.Save(filePath + "segment-hhistogram.jpg");

            // step 2. calculate total rows
            // step 2.1 define linked list
            LinkedList<Vector3FT[]> rowLimitList = new LinkedList<Vector3FT[]>();

            // find all rows
            // step 2.2 define top point and bottom point of each rows
            if (processedImage == CloneImage) rowLimitList = ProcessHorizontalProjection(HProjectionProfile, iheight, range, 0);
            else rowLimitList = ProcessHorizontalProjection(HProjectionProfile, iheight, range, 1);


            // step 2.3 do connected region schemes
            // do connected regions proccess only on raw data.
            if (processedImage == CloneImage) rowLimitList = DoConnectedRegions(rowLimitList, HProjectionProfile);

            // step 3. foreach row total do
            Bitmap segmentImage;
            Bitmap VProjectionProfile;
            // define array of linked list to save total column of each row
            LinkedList<Vector3FT[]>[] colLimitList = new LinkedList<Vector3FT[]>[rowLimitList.Count];
            for (i = 0; i < rowLimitList.Count; i++)
            {
                // step 3.1 clone image
                segmentImage = new Bitmap(CopyImage(processedImage, 0, (int)rowLimitList.ElementAt(i)[0].Y + 1, iwidth, (int)rowLimitList.ElementAt(i)[1].Y));
                
                // step 3.2 generate vertical histogram
                VProjectionProfile = new Bitmap(VProjectionMethod(segmentImage));
                // step 3.3 loop to all vertical histogram image
                colLimitList[i] = new LinkedList<Vector3FT[]>();

                if(processedImage == CloneImage) colLimitList[i] = ProcessVerticalProjection(VProjectionProfile, range, 0);
                else colLimitList[i] = ProcessVerticalProjection(VProjectionProfile, range, 1);

                if(processedImage == CloneImage) VProjectionProfile.Save(filePath + "/vhistogram-" + i + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                else VProjectionProfile.Save(filePath + "/segment-vhistogram-" + i + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            /*
             * 
             * TRACE SEGMENTATION
             * 
             */

            // save each segmented image
            String linesToWrite = "";
            for (i = 0; i < rowLimitList.Count; i++)
            {
                for (j = 0; j < colLimitList[i].Count; j++)
                {
                    // save all images
                    segmentImage = new Bitmap(CopyImage(processedImage,
                                                        (int)colLimitList[i].ElementAt(j)[0].X - range,
                                                        (int)rowLimitList.ElementAt(i)[0].Y - range,
                                                        (int)colLimitList[i].ElementAt(j)[1].X + range,
                                                        (int)rowLimitList.ElementAt(i)[1].Y + range));
                    if (processedImage == CloneImage)
                    {
                        // create product folder
                        String productPath = filePath + "/product-" + i + "-" + j;
                        isExists = System.IO.Directory.Exists(productPath);
                        if (!isExists)
                            System.IO.Directory.CreateDirectory(productPath);
                        segmentImage.Save(productPath + "/orig-" + i + "-" + j + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    else
                    {
                        // save each segmented image
                        int segment_pos_x, segment_pos_y, segment_width, segment_height;
                        segment_pos_x = (int)colLimitList[i].ElementAt(j)[0].X; segment_pos_y = (int)rowLimitList.ElementAt(i)[0].Y;
                        segment_width = (int)colLimitList[i].ElementAt(j)[1].X - segment_pos_x;
                        segment_height = (int)rowLimitList.ElementAt(i)[1].Y - segment_pos_y;
                        linesToWrite += i + "-" + j + " " + segment_pos_x.ToString() + " " + segment_pos_y.ToString()
                                        + " " + segment_width + " " + segment_height + ";";
                        segmentImage.Save(filePath + "/segment-" + i + "-" + j + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }
            }

            // save temporary pos and size segment to path
            if(processedImage != CloneImage) System.IO.File.WriteAllText(filePath + "/temporary-pos.txt", linesToWrite);

            // save row and col list globaly
            if (processedImage == CloneImage)
            {
                GLOBAL_ROWLIST = rowLimitList; GLOBAL_COLLIST = colLimitList;
            }

            // draw image
            for (i = 0; i < rowLimitList.Count; i++)
            {
                // draw line vertical each row
                for (j = 0; j < iwidth; j++)
                {
                    processedImage.SetPixel(j, (int)rowLimitList.ElementAt(i)[0].Y, Color.FromArgb(0, 0, 0));
                    processedImage.SetPixel(j, (int)rowLimitList.ElementAt(i)[1].Y, Color.FromArgb(0, 0, 0));
                }

                for (j = 0; j < colLimitList[i].Count; j++)
                {

                    // draw line horizontal each column
                    for (int k = (int)rowLimitList.ElementAt(i)[0].Y; k < (int)rowLimitList.ElementAt(i)[1].Y; k++)
                    {
                        processedImage.SetPixel((int)colLimitList[i].ElementAt(j)[0].X, k, Color.FromArgb(0, 0, 0));
                        processedImage.SetPixel((int)colLimitList[i].ElementAt(j)[1].X, k, Color.FromArgb(0, 0, 0));
                    }
                }
            }

            // save the image data
            if (processedImage == CloneImage)
            {
                Bitmap imgToSave = new Bitmap(processedImage);
                processedImage.Save(filePath + "imgData.jpg", System.Drawing.Imaging.ImageFormat.Jpeg); 
            }
        }
        
        // looping for each segmented image
        public void projectionProfileOnSegment()
        {
            int i, j, k;
            String filePath = APP_PATH + imgName;

            String[] temp = System.IO.Directory.GetDirectories(filePath);
            
            for (i = 0; i < temp.Length; i++)
            {
                String[] segmentImgPath = System.IO.Directory.GetFiles(temp[i]);
                String fpath = System.IO.Path.GetFileName(System.IO.Path.GetDirectoryName(segmentImgPath[0]));

                Bitmap segmentProcessedImage;
                // load image from path folder only if the orig image just 1
                if (segmentImgPath.Length == 1)
                {
                    segmentProcessedImage = new Bitmap(segmentImgPath[0]);
                    Console.WriteLine(fpath);
                    projectionProfile(segmentProcessedImage, fpath);
                }
                else
                {
                    MessageBox.Show("This segmentation phase can't be continue because the result folder are exists");
                    break;
                }
            }
            // save csv file
            LinkedList<String[]> csvStr = new LinkedList<String[]>();
            String[] lineCsv = new String[] { "Image Data", "Parent Folder", "Segment Image", "X", "Y", "Width", "Height", "Feature" };
            // insert table title
            csvStr.AddLast(lineCsv);

            for (i = 0; i < GLOBAL_ROWLIST.Count; i++)
            {
                for (j = 0; j < GLOBAL_COLLIST[i].Count; j++)
                {
                    // insert all item
                    // search temporary files text;
                    string temporaryText = System.IO.File.ReadAllText(filePath + "/product-" + i + "-" + j + "/temporary-pos.txt");
                    string[][] temporarySegmentPos = temporaryText.Split(';').Select(s => s.Split(' ')).ToArray();
                    for (k = 0; k < temporarySegmentPos.Length - 1; k++)
                    {
                        lineCsv = new String[8];
                        lineCsv[0] = "imgData.jpg"; lineCsv[1] = "product-" + i + "-" + j;
                        lineCsv[2] = "segment-" + temporarySegmentPos[k][0] + ".jpg";
                        // X and Y
                        lineCsv[3] = Convert.ToString((int)GLOBAL_COLLIST[i].ElementAt(j)[0].X + Convert.ToInt32(temporarySegmentPos[k][1]) - 4); // 4 is range of parent segmentation, see projection profile function
                        lineCsv[4] = Convert.ToString((int)GLOBAL_ROWLIST.ElementAt(i)[0].Y + Convert.ToInt32(temporarySegmentPos[k][2]) - 4);
                        // Width and Height
                        lineCsv[5] = temporarySegmentPos[k][3]; lineCsv[6] = temporarySegmentPos[k][4];

                        // push to linked list
                        csvStr.AddLast(lineCsv);

                    }

                    // Console.WriteLine("product-" + i + "-" + j + ": " + temporarySegmentPos[0].Length + ", " + temporarySegmentPos[0][0].Length);
                }
            }


            String csvPath = filePath + "/result.csv";
            if (!System.IO.File.Exists(csvPath))
            {
                System.IO.File.Create(csvPath).Close();
            }

            string delimiter = ",";
            StringBuilder sb = new StringBuilder();

            for (i = 0; i < csvStr.Count; i++)
            {
                sb.AppendLine(string.Join(delimiter, csvStr.ElementAt(i)));
            }


            System.IO.File.WriteAllText(csvPath, sb.ToString());
        }

        // copy / crop image at pos x, y to width, height
        private Bitmap CopyImage(Bitmap based, int x, int y, int width, int height)
        {
            Bitmap original = based;
            int twidth,theight;

            if (x < 0) x = 0;
            if (width > based.Width) twidth = based.Width - x;
            else twidth = width - x;

            if (y < 0) y = 0;
            if (height > based.Height) theight = based.Height - y;
            else theight = height - y;
            Rectangle srcRect = new Rectangle(x, y, twidth, theight);
            Bitmap cropped = original.Clone(srcRect, System.Drawing.Imaging.PixelFormat.Format16bppArgb1555);
            return cropped;
        }

        // projection profile horizontal
        private Bitmap HProjectionMethod(Bitmap processedImage = null)
        {
            int i, j, counter;
            Bitmap hHistogram;
            if (processedImage == null)
            {
                hHistogram = new Bitmap(image_width, image_height);
                // horizontal projection
                for (i = 0; i < image_height; i++)
                {
                    counter = 0;
                    for (j = 0; j < image_width; j++)
                    {
                        hHistogram.SetPixel(j, i, Color.FromArgb(255, 255, 255));
                        if (processColor[i][j].R == 0)
                        {
                            hHistogram.SetPixel(counter, i, Color.FromArgb(0, 0, 0));
                            counter++;
                        }
                    }
                }
            }
            else
            {
                hHistogram = new Bitmap(processedImage.Width, processedImage.Height);
                // horizontal projection
                for (i = 0; i < processedImage.Height; i++)
                {
                    counter = 0;
                    for (j = 0; j < processedImage.Width; j++)
                    {
                        hHistogram.SetPixel(j, i, Color.FromArgb(255, 255, 255));
                        if (processedImage.GetPixel(j, i).R == 0)
                        {
                            hHistogram.SetPixel(counter, i, Color.FromArgb(0, 0, 0));
                            counter++;
                        }
                    }
                }
            }

            return hHistogram;
        }

        // define row from horizontal projection profile
        private LinkedList<Vector3FT[]> ProcessHorizontalProjection(Bitmap HProjectionProfile, int iheight, int range, int type)
        {
            int i;
            int counter = 0;
            // define temp list
            Vector3FT[] tempList = new Vector3FT[2];
            tempList[0] = new Vector3FT();
            tempList[1] = new Vector3FT();
            
            // define temp for result;
            LinkedList<Vector3FT[]> _rowLimitList = new LinkedList<Vector3FT[]>();
            for (i = range; i < iheight - range; i++)
            {
                if (type == 0)
                {
                    // find top limit of rows TOP PATTERN
                    if (HProjectionProfile.GetPixel(0, i - 1).R == 255 &&
                        HProjectionProfile.GetPixel(0, i).R == 0 &&
                        HProjectionProfile.GetPixel(0, i + 1).R == 0 &&
                        HProjectionProfile.GetPixel(0, i + 2).R == 0 &&
                        HProjectionProfile.GetPixel(0, i + 3).R == 0 &&
                        HProjectionProfile.GetPixel(0, i + 4).R == 0 &&
                        counter == 0)
                    {
                        tempList[counter].X = 0; tempList[counter].Y = i;
                        counter++;
                    }

                    // find bottom limit of rows BOTTOM PATTERN
                    if (HProjectionProfile.GetPixel(0, i - 4).R == 0 &&
                        HProjectionProfile.GetPixel(0, i - 3).R == 0 &&
                        HProjectionProfile.GetPixel(0, i - 2).R == 0 &&
                        HProjectionProfile.GetPixel(0, i - 1).R == 0 &&
                        HProjectionProfile.GetPixel(0, i).R == 0 &&
                        HProjectionProfile.GetPixel(0, i + 1).R == 255 &&
                        counter == 1)
                    {
                        tempList[counter].X = 0; tempList[counter].Y = i;
                        counter++;
                    }
                }
                else
                {
                    // find top limit of rows TOP PATTERN
                    if (HProjectionProfile.GetPixel(0, i - 1).R == 255 &&
                        HProjectionProfile.GetPixel(0, i).R == 0 &&
                        HProjectionProfile.GetPixel(0, i + 1).R == 0 &&
                        counter == 0)
                    {
                        tempList[counter].X = 0; tempList[counter].Y = i;
                        counter++;
                    }

                    // find bottom limit of rows BOTTOM PATTERN
                    if (HProjectionProfile.GetPixel(0, i - 2).R == 0 &&
                        HProjectionProfile.GetPixel(0, i - 1).R == 0 &&
                        HProjectionProfile.GetPixel(0, i).R == 255 &&
                        counter == 1)
                    {
                        tempList[counter].X = 0; tempList[counter].Y = i;
                        counter++;
                    }

                }

                if (counter == 2)
                {
                    // reset counter
                    _rowLimitList.AddLast(tempList);
                    tempList = new Vector3FT[2]; tempList[0] = new Vector3FT(); tempList[1] = new Vector3FT();
                    counter = 0;
                }
            }
            return _rowLimitList;
        }

        // projection profile horizontal
        private Bitmap VProjectionMethod(Bitmap imgSrc)
        {
            int i, j, counter;
            Bitmap vHistogram = new Bitmap(imgSrc.Width, imgSrc.Height);

            // vertical projection
            for (i = 0; i < imgSrc.Width; i++)
            {
                counter = imgSrc.Height - 1;
                for (j = imgSrc.Height - 1; j >= 0; j--)
                {
                    vHistogram.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                    if (imgSrc.GetPixel(i, j).R == 0)
                    {
                        vHistogram.SetPixel(i, counter, Color.FromArgb(0, 0, 0));
                        counter--;
                    }
                }
            }
            return vHistogram;
        }

        // define column from vertical projection profile
        private LinkedList<Vector3FT[]> ProcessVerticalProjection(Bitmap VProjectionProfile, int range, int type)
        {
            // type == 0; it's raw data
            // type == 1; it's recursive segment

            int i;
            int counter = 0;
            // define temp list
            Vector3FT[] tempList = new Vector3FT[2];
            tempList[0] = new Vector3FT();
            tempList[1] = new Vector3FT();

            // define temp for result;
            LinkedList<Vector3FT[]> _colLimitList = new LinkedList<Vector3FT[]>();
            counter = 0;
            for (i = range; i < VProjectionProfile.Width - range; i++)
            {
                if (type == 0)
                {
                    // define left point and right point of each col
                    // find left limit using LEFT PATTERN
                    if (VProjectionProfile.GetPixel(i - 1, VProjectionProfile.Height - 1).R == 255 &&
                        VProjectionProfile.GetPixel(i, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i + 1, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i + 2, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i + 3, VProjectionProfile.Height - 1).R == 0 &&
                        counter == 0)
                    {
                        tempList[counter].X = i; tempList[counter].Y = VProjectionProfile.Height;
                        counter++;
                    }

                    // find right limit using RIGHT PATTERN
                    if (VProjectionProfile.GetPixel(i - 3, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i - 2, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i - 1, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i + 1, VProjectionProfile.Height - 1).R == 255 &&
                        counter == 1)
                    {
                        tempList[counter].X = i; tempList[counter].Y = VProjectionProfile.Height;
                        counter++;
                    }
                }
                else
                {

                    // define left point and right point of each col
                    // find left limit using LEFT PATTERN
                    if (VProjectionProfile.GetPixel(i - 1, VProjectionProfile.Height - 1).R == 255 &&
                        VProjectionProfile.GetPixel(i, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i + 1, VProjectionProfile.Height - 1).R == 0 &&
                        counter == 0)
                    {
                        tempList[counter].X = i; tempList[counter].Y = VProjectionProfile.Height;
                        counter++;
                    }

                    // find right limit using RIGHT PATTERN
                    if (VProjectionProfile.GetPixel(i - 2, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i - 1, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i + 1, VProjectionProfile.Height - 1).R == 0 &&
                        VProjectionProfile.GetPixel(i + 2, VProjectionProfile.Height - 1).R == 255 &&
                        counter == 1)
                    {
                        tempList[counter].X = i; tempList[counter].Y = VProjectionProfile.Height;
                        counter++;
                    }
                }
                if (counter == 2)
                {
                    // reset counter
                    _colLimitList.AddLast(tempList);
                    tempList = new Vector3FT[2]; tempList[0] = new Vector3FT(); tempList[1] = new Vector3FT();
                    counter = 0;
                }
            }

            return _colLimitList;
        }
        
        // check connected regions
        private LinkedList<Vector3FT[]> DoConnectedRegions(LinkedList<Vector3FT[]> _rowLimitList, Bitmap HProjectionProfile)
        {
            int i;
            int f1, f2, d;
            double ratio;
            for (i = 0; i < _rowLimitList.Count - 1; i++)
            {
                // find max frequency of each rows
                // freq of row[i] : freq of row[i+1]
                // connected regions if 
                // 1. freq of row[i] / freq of row[i+1] < 0.3 (do research again)
                // 2. d(row[1] - row[2]) < -15 (do research again)
                f1 = maxFreq(_rowLimitList.ElementAt(i), HProjectionProfile);
                f2 = maxFreq(_rowLimitList.ElementAt(i + 1), HProjectionProfile);
                d = rowDistance(_rowLimitList.ElementAt(i), _rowLimitList.ElementAt(i + 1));
                ratio = (double)f1 / f2;
                if (ratio < 0.3 && d > -15)
                {
                    // are connected regions
                    // row[i].bottom = row[i+1].bottom
                    _rowLimitList.ElementAt(i)[1].Y = _rowLimitList.ElementAt(i + 1)[1].Y;
                    // row [i+1] remove
                    _rowLimitList.Remove(_rowLimitList.ElementAt(i + 1));
                }
            }

            return _rowLimitList;
        }

        // get max freq of horizontal projection profile
        private int maxFreq(Vector3FT[] temp, Bitmap HistogramSrc)
        {
            int result = -9999;
            int i, j;
            int c;
            // loop from batas atas to batas bawah, count max freq
            for (i = (int)temp[0].Y; i < (int)temp[1].Y; i++)
            {
                c = 0; // c calculate freq of each rows
                for (j = 0; j < HistogramSrc.Width; j++)
                {
                    if (HistogramSrc.GetPixel(j, i).R == 0)
                    {
                        c++; // increment c if pixel is black
                    }
                }

                if (c > result)
                    result = c;
            }
            return result;
        }

        // get distance between row
        private int rowDistance(Vector3FT[] row1, Vector3FT[] row2)
        {
            int result;
            // distance = row1.bottom - row2.top
            result = (int)row1[1].Y - (int)row2[0].Y;
            return result;
        }

        
        public void automateSegmentation()
        {
            int i, j, k, range, iwidth, iheight; Bitmap processedImage = null;
            // iwidth, iheight = temporary save iamge width and height
            // range = threshold for segment the image

            // set filePath
            String filePath = APP_PATH + imgName + "/";
            range = 4;

            // set processed image
            if (processedImage == null) processedImage = CloneImage;

            // create folder if not exists
            bool isExists = System.IO.Directory.Exists(filePath);
            if (!isExists) System.IO.Directory.CreateDirectory(filePath);

            // process clone image
            // step 1. get horizontal histogram
            Bitmap HProjectionProfile;
            HProjectionProfile = HProjectionMethod(processedImage);
            iwidth = processedImage.Width; iheight = processedImage.Height;

            // save horizontal histogram
            HProjectionProfile.Save(filePath + "hhistogram.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            // step 2. calculate total rows
            // step 2.1 define linked list
            LinkedList<Vector3FT[]> rowLimitList = new LinkedList<Vector3FT[]>();

            // find all rows
            // step 2.2 define top point and bottom point of each rows
            rowLimitList = ProcessHorizontalProjection(HProjectionProfile, iheight, range, 0);

            // step 2.3 do connected region schemes
            rowLimitList = DoConnectedRegions(rowLimitList, HProjectionProfile);

            // step 3. foreach row total do
            Bitmap segmentImage;
            Bitmap VProjectionProfile;
            // define array of linked list to save total column of each row
            LinkedList<Vector3FT[]>[] colLimitList = new LinkedList<Vector3FT[]>[rowLimitList.Count];
            for (i = 0; i < rowLimitList.Count; i++)
            {
                // step 3.1 clone image
                segmentImage = new Bitmap(CopyImage(processedImage, 0, (int)rowLimitList.ElementAt(i)[0].Y + 1,
                                                    iwidth, (int)rowLimitList.ElementAt(i)[1].Y));

                // step 3.2 generate vertical histogram
                VProjectionProfile = new Bitmap(VProjectionMethod(segmentImage));

                // step 3.3 loop to all vertical histogram image
                colLimitList[i] = new LinkedList<Vector3FT[]>();

                colLimitList[i] = ProcessVerticalProjection(VProjectionProfile, range, 0);

                // save vhistogram
                VProjectionProfile.Save(filePath + "vhistogram-" + i + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            // save each segmented image
            for (i = 0; i < rowLimitList.Count; i++)
            {
                for (j = 0; j < colLimitList[i].Count; j++)
                {
                    // save all images
                    segmentImage = new Bitmap(CopyImage(processedImage,
                                                            (int)colLimitList[i].ElementAt(j)[0].X - range,
                                                            (int)rowLimitList.ElementAt(i)[0].Y - range,
                                                            (int)colLimitList[i].ElementAt(j)[1].X + range,
                                                            (int)rowLimitList.ElementAt(i)[1].Y + range));
                    // create product folder
                    String productPath = filePath + "/product-" + i + "-" + j;
                    isExists = System.IO.Directory.Exists(productPath);
                    if (!isExists) System.IO.Directory.CreateDirectory(productPath);
                    segmentImage.Save(productPath + "/orig.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }


            // do segmentation on each segment image
            String[] temp = System.IO.Directory.GetDirectories(filePath);
            String linesToWrite = "";
            for (i = 0; i < temp.Length; i++)
            {
                String fpath = temp[i];
                String[] segmentImgPath = System.IO.Directory.GetFiles(fpath, "orig.jpg");

                Bitmap segmentProcessedImage;
                // load image from path folder only if the orig image just 1
                if (segmentImgPath.Length == 1) segmentProcessedImage = new Bitmap(segmentImgPath[0]);
                else
                {
                    MessageBox.Show("This segmentation phase can't be continue because the result folder are exists");
                    return;
                }
                int segmentRange = 2;
                int swidth, sheight;
                // process clone image
                // step 1. get horizontal histogram
                HProjectionProfile = HProjectionMethod(segmentProcessedImage);
                swidth = segmentProcessedImage.Width; sheight = segmentProcessedImage.Height;

                // save horizontal histogram
                HProjectionProfile.Save(fpath + "/segment-hhistogram.jpg");

                // step 2. calculate total rows
                // step 2.1 define linked list
                LinkedList<Vector3FT[]> segmentRow = new LinkedList<Vector3FT[]>();

                // find all rows
                // step 2.2 define top point and bottom point of each rows
                segmentRow = ProcessHorizontalProjection(HProjectionProfile, sheight, segmentRange, 1);

                // step 3. foreach row total do
                // define array of linked list to save total column of each row
                LinkedList<Vector3FT[]>[] segmentCol = new LinkedList<Vector3FT[]>[segmentRow.Count];
                for (j = 0; j < segmentRow.Count; j++)
                {
                    // step 3.1 clone image
                    segmentImage = new Bitmap(CopyImage(segmentProcessedImage, 0, (int)segmentRow.ElementAt(j)[0].Y + 1,
                                                        swidth, (int)segmentRow.ElementAt(j)[1].Y));

                    // step 3.2 generate vertical histogram
                    VProjectionProfile = new Bitmap(VProjectionMethod(segmentImage));

                    // step 3.3 loop to all vertical histogram image
                    segmentCol[j] = new LinkedList<Vector3FT[]>();

                    segmentCol[j] = ProcessVerticalProjection(VProjectionProfile, segmentRange, 1);

                    // save vhistogram
                    VProjectionProfile.Save(fpath + "/segment-vhistogram-" + j + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                // save each segmented image
                linesToWrite = "";
                for (j = 0; j < segmentRow.Count; j++)
                {
                    for (k = 0; k < segmentCol[j].Count; k++)
                    {
                        // save all images
                        segmentImage = new Bitmap(CopyImage(segmentProcessedImage,
                                                                (int)segmentCol[j].ElementAt(k)[0].X - range,
                                                                (int)segmentRow.ElementAt(j)[0].Y - range,
                                                                (int)segmentCol[j].ElementAt(k)[1].X + range,
                                                                (int)segmentRow.ElementAt(j)[1].Y + range));

                        int segment_pos_x, segment_pos_y, segment_width, segment_height;
                        segment_pos_x = (int)segmentCol[j].ElementAt(k)[0].X; segment_pos_y = (int)segmentRow.ElementAt(j)[0].Y;
                        segment_width = (int)segmentCol[j].ElementAt(k)[1].X - segment_pos_x;
                        segment_height = (int)segmentRow.ElementAt(j)[1].Y - segment_pos_y;
                        linesToWrite += j+ "-" + k + " " + segment_pos_x.ToString() + " " + segment_pos_y.ToString() 
                                        + " " + segment_width + " " + segment_height + ";";
                        segmentImage.Save(fpath + "/segment-" + j + "-" + k + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }

                // save temporary pos and size segment to path
                System.IO.File.WriteAllText(fpath + "/temporary-pos.txt", linesToWrite);
            }


            // save csv file
            LinkedList<String[]> csvStr = new LinkedList<String[]>();
            String[] lineCsv = new String[] { "Image Data", "Parent Folder", "Segment Image", "Region", "X", "Y", "Width", "Height", "Feature" };
            // insert table title
            csvStr.AddLast(lineCsv);

            for (i = 0; i < rowLimitList.Count; i++)
            {
                for (j = 0; j < colLimitList[i].Count; j++)
                {
                    // insert all item
                    // search temporary files text;
                    string temporaryText = System.IO.File.ReadAllText(filePath + "/product-" + i + "-" + j + "/temporary-pos.txt");
                    string[][] temporarySegmentPos = temporaryText.Split(';').Select(s => s.Split(' ')).ToArray();
                    for (k = 0; k < temporarySegmentPos.Length - 1; k++)
                    {
                        lineCsv = new String[8];
                        lineCsv[0] = "imgData.jpg"; lineCsv[1] = "'" + i + "-" + j;
                        lineCsv[2] = "'" + temporarySegmentPos[k][0];
                        lineCsv[3] = "--";
                        // X and Y
                        lineCsv[4] = Convert.ToString((int)colLimitList[i].ElementAt(j)[0].X + Convert.ToInt32(temporarySegmentPos[k][1]) - range);
                        lineCsv[5] = Convert.ToString((int)rowLimitList.ElementAt(i)[0].Y + Convert.ToInt32(temporarySegmentPos[k][2]) - range);
                        // Width and Height
                        lineCsv[6] = temporarySegmentPos[k][3]; 
                        lineCsv[7] = temporarySegmentPos[k][4];

                        // push to linked list
                        csvStr.AddLast(lineCsv);
                        
                    }

                    // Console.WriteLine("product-" + i + "-" + j + ": " + temporarySegmentPos[0].Length + ", " + temporarySegmentPos[0][0].Length);
                }
            }


            String csvPath = filePath + "result.csv";
            if (!System.IO.File.Exists(csvPath))
            {
                System.IO.File.Create(csvPath).Close();
            }

            string delimiter = ",";
            StringBuilder sb = new StringBuilder();

            for (i = 0; i < csvStr.Count; i++)
            {
                sb.AppendLine(string.Join(delimiter, csvStr.ElementAt(i)));
            }


            System.IO.File.WriteAllText(csvPath, sb.ToString());

            /*
             * 
             * TRACE SEGMENTATION
             * 
             */
            // draw image
            for (i = 1; i < csvStr.Count; i++)
            {
                Graphics g = Graphics.FromImage(processedImage);
                // draw segment image
                g.DrawRectangle(new Pen(Color.FromArgb(0, 0, 0), 1),
                                    new Rectangle(Int32.Parse(csvStr.ElementAt(i)[4]), Int32.Parse(csvStr.ElementAt(i)[5]),
                                                  Int32.Parse(csvStr.ElementAt(i)[6]), Int32.Parse(csvStr.ElementAt(i)[7])));
            }

            // save source data to folder
            Bitmap toSave = new Bitmap(processedImage);
            toSave.Save(filePath + "imgData.jpg");
        }
        #endregion

        // flood fill
        #region floodFill
        public void searchRegion(String fPath = "", String csvFile = null, String imgId = null)
        {
            int i, j, k;

            // set APP_PATH if not null
            if (fPath != "") fPath = fPath + "flood-fill-region/";
            else fPath = APP_PATH + "flood-fill-region/";

            bool isExists = System.IO.Directory.Exists(fPath);
            if (!isExists)
                System.IO.Directory.CreateDirectory(fPath);

            // 1. change process color to int array 0/1
            int[][] imageArray = ColorToInt();

            // save result temporary
            int[][] tempResult = new int[image_height][];

            // checking process 1
            //for (i = 0; i < image_height; i++)
            //{
            //    for (j = 0; j < image_width; j++)
            //    {
            //        if(imageArray[i][j] == 0) Console.Write(imageArray[i][j].ToString() + " ");
            //        else Console.Write("  ");
            //    }
            //    Console.WriteLine(" ");
            //}
            // checking image to array OK!

            int countObj = -1;
            // this following code can replace with function findPos
            // begin code
            for (i = 0; i < image_height; i++)
            {
                for (j = 0; j < image_width; j++)
                {
                    if (imageArray[i][j] == 1)
                    {
                        // set curpos to be check on recursive
                        // recursively check region
                        GLOBAL_TEMP_IMAGE = new Bitmap(image_width, image_height);
                        Graphics g = Graphics.FromImage(GLOBAL_TEMP_IMAGE);
                        g.Clear(Color.FromArgb(255, 255, 255));

                        // imageArray = recSearch(j, i, imageArray, countObj);
                        imageArray = RevisedQueueFloodFill(new Point(j, i), imageArray, countObj);

                        GLOBAL_TEMP_IMAGE.Save(fPath + "image" + countObj.ToString() + ".jpg");

                        // delete graphics variable and global image
                        // avoid memory lack
                        countObj--;
                    }
                }
            }
            // end code

            // load the image result to calculate the x, y & width, height
            String linesToWrite = "";
            for (i = -1; i > countObj; i--)
            {
                String imgPath = fPath + "image" + i.ToString() + ".jpg";
                Bitmap t = new Bitmap(imgPath);
                Bitmap HProjectionProfile;
                HProjectionProfile = HProjectionMethod(t);
                int iwidth = t.Width; int iheight = t.Height;
                int range = 2;

                // save horizontal histogram
                HProjectionProfile.Save(fPath + "hhistogram.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

                // step 2. calculate total rows
                // step 2.1 define linked list
                LinkedList<Vector3FT[]> rowLimitList = new LinkedList<Vector3FT[]>();

                // find all rows
                // step 2.2 define top point and bottom point of each rows
                rowLimitList = ProcessHorizontalProjection(HProjectionProfile, iheight, range, 1);

                // step 3. foreach row total do
                Bitmap segmentImage;
                Bitmap VProjectionProfile;
                // define array of linked list to save total column of each row
                LinkedList<Vector3FT[]>[] colLimitList = new LinkedList<Vector3FT[]>[rowLimitList.Count];
                for (j = 0; j < rowLimitList.Count; j++)
                {
                    // step 3.1 clone image
                    segmentImage = new Bitmap(CopyImage(t, 0, (int)rowLimitList.ElementAt(j)[0].Y + 1,
                                                        iwidth, (int)rowLimitList.ElementAt(j)[1].Y));

                    // step 3.2 generate vertical histogram
                    VProjectionProfile = new Bitmap(VProjectionMethod(segmentImage));

                    // step 3.3 loop to all vertical histogram image
                    colLimitList[j] = new LinkedList<Vector3FT[]>();

                    colLimitList[j] = ProcessVerticalProjection(VProjectionProfile, range, 1);

                    // save vhistogram
                    VProjectionProfile.Save(fPath + "vhistogram-" + j + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                // save each segmented image
                for (j = 0; j < rowLimitList.Count; j++)
                {
                    for (k = 0; k < colLimitList[j].Count; k++)
                    {
                        linesToWrite += i.ToString().Substring(1) + " ";
                        // X Y
                        linesToWrite += Convert.ToString((int)colLimitList[j].ElementAt(k)[0].X - range) + " ";
                        linesToWrite += Convert.ToString((int)rowLimitList.ElementAt(j)[0].Y - range) + " ";
                        // W H
                        linesToWrite += Convert.ToString((int)colLimitList[j].ElementAt(k)[1].X - (int)colLimitList[j].ElementAt(k)[0].X + range) + " ";
                        linesToWrite += Convert.ToString((int)rowLimitList.ElementAt(j)[1].Y - (int)rowLimitList.ElementAt(j)[0].Y + range);
                        linesToWrite += ";";
                    }
                }
            }
            System.IO.File.WriteAllText(fPath + "/temporary-pos.txt", linesToWrite);

            // append to result.csv file
            if (csvFile != null)
            {
                List<string> temp = System.IO.File.ReadAllLines(csvFile).ToList();
                List<string> csvResult = new List<string>();

                csvResult.Add(temp.ElementAt(0));
                for (i = 1; i < temp.Count; i++)
                {
                    String line = temp.ElementAt(i);
                    String[] arrLine = line.Split(',');
                    String tempImgId = arrLine[1].Substring(1) + "-" + arrLine[2].Substring(1);
                    if (tempImgId == imgId)
                    {
                        // append here
                        Console.WriteLine(line);
                        csvResult.Add(line);
                        string temporaryText = System.IO.File.ReadAllText(fPath + "/temporary-pos.txt");
                        string[][] temporarySegmentPos = temporaryText.Split(';').Select(s => s.Split(' ')).ToArray();
                        for (j = 0; j < temporarySegmentPos.Length - 1; j++)
                        {
                            String result = arrLine[0] + "," + arrLine[1] + "," + arrLine[2] + "," + temporarySegmentPos[j][0] + "," +
                                Convert.ToString(Int32.Parse(arrLine[4]) + Int32.Parse(temporarySegmentPos[j][1])) + "," +
                                Convert.ToString(Int32.Parse(arrLine[5]) + Int32.Parse(temporarySegmentPos[j][2])) + "," +
                                Convert.ToString(Int32.Parse(temporarySegmentPos[j][3])) + "," +
                                Convert.ToString(Int32.Parse(temporarySegmentPos[j][4]));
                            csvResult.Add(result);
                        }
                    }
                    else
                    {
                        csvResult.Add(line);
                    }
                }

                System.IO.File.WriteAllLines(csvFile, csvResult);
            }
        }

        private int[][] RevisedQueueFloodFill(Point node, int[][] arrToProcess, int countObj)
        {
            // Color targetColor = pixels[node.X, node.Y].CellColor;
            // if (targetColor == replaceColor) return;

            // arrToProcess[node.Y][node.X] = countObj;

            Queue<Point> q = new Queue<Point>();
            q.Enqueue(node);

            Point n, t, u;

            while (q.Count > 0)
            {
                n = q.Dequeue();
                if (arrToProcess[n.Y][n.X] == 1)
                {
                    t = n;
                    while ((t.X > 0) && (arrToProcess[t.Y][t.X] == 1))
                    {
                        arrToProcess[t.Y][t.X] = countObj;
                        GLOBAL_TEMP_IMAGE.SetPixel(t.X, t.Y, Color.FromArgb(0, 0, 0));
                        // pixels[t.X, t.Y].CellColor = replaceColor;
                        t.X--;
                    }
                    int XMin = t.X + 1;

                    t = n;
                    t.X++;
                    while ((t.X < arrToProcess[0].Length) && (arrToProcess[t.Y][t.X] == 1))
                    {
                        arrToProcess[t.Y][t.X] = countObj;
                        GLOBAL_TEMP_IMAGE.SetPixel(t.X, t.Y, Color.FromArgb(0, 0, 0));
                        // pixels[t.X, t.Y].CellColor = replaceColor;
                        t.X++;
                    }
                    int XMax = t.X - 1;

                    t = n;
                    t.Y++;

                    u = n;
                    u.Y--;

                    for (int i = XMin; i <= XMax; i++)
                    {
                        t.X = i;
                        u.X = i;

                        if ((t.Y < arrToProcess.Length - 1) &&
                            (arrToProcess[t.Y][t.X] == 1)) q.Enqueue(t);

                        if ((u.Y >= 0) &&
                            (arrToProcess[u.Y][u.X] == 1)) q.Enqueue(u);
                    }
                }
            }

            return arrToProcess;
        }
        public int[][] recSearch(int x, int y, int[][] arrToProcess, int countObj, int objBit = 1)
        {
            // set this pos to count object
            arrToProcess[y][x] = countObj;
            GLOBAL_TEMP_IMAGE.SetPixel(x, y, Color.FromArgb(0, 0, 0));

            // search region
            // search to left
            if (x - 1 > 0)
            {
                if (arrToProcess[y][x - 1] == objBit) arrToProcess = recSearch(x-1, y, arrToProcess, countObj);
            }
            // search to up
            if (y - 1 > 0)
            {
                if (arrToProcess[y - 1][x] == objBit) arrToProcess = recSearch(x, y-1, arrToProcess, countObj);
            }
            // search to right
            if (x + 1 < arrToProcess[0].Length)
            {
                if (arrToProcess[y][x + 1] == objBit) arrToProcess = recSearch(x+1, y, arrToProcess, countObj);
            }
            // search to down
            if (y + 1 < arrToProcess.Length)
            {
                if (arrToProcess[y + 1][x] == objBit) arrToProcess = recSearch(x, y+1, arrToProcess, countObj);
            }

            return arrToProcess;
        }
        #endregion

        public Bitmap getOriginalImage()
        {
            return OriginalImage;
        }

        public Bitmap getProccessedImage()
        {
            return CloneImage;
        }

        public void writeCSV()
        {
            String folderPath = APP_PATH + "_assets/Image/Hasil segmentasi/";
            String filePath = folderPath + "result.csv";
            if(!System.IO.File.Exists(filePath))
            {
                System.IO.File.Create(filePath).Close();
            }

            string delimiter = ",";
            string[][] output = new string[][]{
                new string[]{"Value1","Value2","Value3","Value4"},
                new string[]{"Value1","Value2","Value3","Value4","Value5"},
            };
            int length = output.GetLength(0);
            StringBuilder sb = new StringBuilder();
            for (int index = 0; index < length; index++)
                sb.AppendLine(string.Join(delimiter, output[index]));
            System.IO.File.AppendAllText(filePath, sb.ToString());
        }

        // ga guna ga penting
        public void searchAreaObj()
        {
            int minWidth, maxWidth, minHeight, maxHeight;
            minWidth = image_width; minHeight = image_height;

            // find 4 extreme point
            Vector3FT Xmin, Ymin, Xmax, Ymax;
            Xmin = new Vector3FT(image_width, image_height, 0); Ymin = new Vector3FT(image_width, image_height, 0);
            Xmax = new Vector3FT(0, 0, 0); Ymax = new Vector3FT(0, 0, 0);

            maxWidth = 0; maxHeight = 0;

            int i, j;

            for (i = 0; i < image_height; i++)
            {
                for (j = 0; j < image_width; j++)
                {
                    // cek apakah warna tersebut item?
                    if (processColor[i][j].R == 0)
                    {
                        // cek koordinat dari sisi height
                        if (i > maxHeight)
                        {
                            maxHeight = i;
                            // set y max point
                            Ymax.X = j; Ymax.Y = i;
                        }

                        if (i < minHeight)
                        {
                            minHeight = i;
                            // set y min point
                            Ymin.X = j; Ymin.Y = i;
                        }

                        // cek koordinat dari sisi width;
                        if (j > maxWidth)
                        {
                            maxWidth = j;
                            // set x max point
                            Xmax.X = j; Xmax.Y = i;
                        }

                        if (j < minWidth)
                        {
                            minWidth = j;
                            // set x min point
                            Xmin.X = j; Xmin.Y = i;
                        }

                    }
                }
            }

            Graphics temp = Graphics.FromImage(CloneImage);

            // drawing check for skew detection
            temp.DrawLine(new Pen(Color.Black, 2), new Point((int)Xmin.X, (int)Xmin.Y), new Point((int)Ymin.X, (int)Ymin.Y));
            temp.DrawLine(new Pen(Color.Black, 2), new Point((int)Xmin.X, (int)Xmin.Y), new Point((int)Ymax.X, (int)Ymax.Y));
            temp.DrawLine(new Pen(Color.Black, 2), new Point((int)Xmax.X, (int)Xmax.Y), new Point((int)Ymin.X, (int)Ymin.Y));
            temp.DrawLine(new Pen(Color.Black, 2), new Point((int)Xmax.X, (int)Xmax.Y), new Point((int)Ymax.X, (int)Ymax.Y));

            // drawing box
            temp.DrawLine(new Pen(Color.Black, 2), new Point(minWidth, minHeight), new Point(maxWidth, minHeight)); // draw line from top left to top right
            temp.DrawLine(new Pen(Color.Black, 2), new Point(minWidth, minHeight), new Point(minWidth, maxHeight)); // draw line from top left to bottom left
            temp.DrawLine(new Pen(Color.Black, 2), new Point(maxWidth, maxHeight), new Point(maxWidth, minHeight)); // draw line from bottom right to top right
            temp.DrawLine(new Pen(Color.Black, 2), new Point(maxWidth, maxHeight), new Point(minWidth, maxHeight)); // draw line from bottom right to bottom left
            temp.DrawImage(CloneImage, 0, 0);
            temp.Dispose();

            // if skew detect == true then
            // calculate angle
            // define vector origin of image position (0,0)
            Vector3FT origin = new Vector3FT();
            // define vector 12
            Vector3FT v12 = new Vector3FT();
            v12 = Vector3FT.subtract(Xmin, Ymin);
            // define vector 52
            Vector3FT v52 = new Vector3FT();
            v52 = Vector3FT.subtract(Xmin, origin);
            // define cos x
            // cosx = (v52 . v12) / |v52| * |v12|
            Console.WriteLine("xmin : " + Xmin.ToString() + " ymin : " + Ymin.ToString());
            Console.WriteLine("v12 : " + v12.ToString() + " v52 : " + v52.ToString());
            double cosx = Vector3FT.angle(v52, v12);
            cosx = 180 / phi * cosx;
        }
        #region bondanFeatureFunctions
        private int Factorial(int i)
        {
            if (i <= 1)
                return 1;
            return i * Factorial(i - 1);
        }

        private int Combination(int n, int r)
        {
            return Factorial(n) / (Factorial(r) * Factorial(n - r));
        }

        private int[] ExtractPattern(Bitmap img, int x, int y, int range)
        {
            int[] resultVector = new int[range * range];
            int vIterator = 0;

            for (int i = y; i < y + range; i++)
            {
                for (int j = x; j < x + range; j++)
                {
                    if (img.GetPixel(j, i).R == 0)
                        resultVector[vIterator] = 1;
                    else
                        resultVector[vIterator] = 0;

                    vIterator++;
                }
            }

            return resultVector;
        }
        private void GeneratePossiblePatterns(List<int[]> currentMap, int n, int r, int vectorLen)
        {
            //Console.WriteLine("n = {0}, r = {1}", n, r);

            // init comb matrix
            int[] combMatrix = new int[vectorLen];
            for (int j = 0; j < vectorLen; j++)
                combMatrix[j] = 0;

            int[] tempMatrix = new int[vectorLen];

            // there's only 1 pattern available for r = 0
            if (r == 0)
            {
                currentMap.Add(combMatrix);
                Console.WriteLine("Combination map created, {0} new featuresPatterns added, should be : {1}", n, n);
                return;
            }

            int newPattern = 1, lastMovingIndex = r - 1, lastStaticIndex = lastMovingIndex - 1, nextIndex = lastMovingIndex + 1;

            // try to fill combination matrix with initial value
            for (int j = 0; j < r; j++)
                if (combMatrix[j] == 0) combMatrix[j] = 1;
            // initial value always new, so always add initial value to current map
            Array.Copy(combMatrix, tempMatrix, vectorLen);
            currentMap.Add(tempMatrix);
            //Console.WriteLine("curent map before processed :");
            //currentMap.ForEach(x => Console.WriteLine(String.Join(" ", x)));

            // number of combinations possible
            for (int i = 1; i < n; i++)
            {
                //Console.WriteLine("iteration {0} of {1}", i, n-1);

                // try to iterate trough featuresPatterns
                if (nextIndex >= vectorLen)
                {
                    combMatrix[lastStaticIndex] = 0;
                    lastStaticIndex++;
                    combMatrix[lastStaticIndex] = 1;
                    combMatrix[lastMovingIndex] = 0;

                    lastMovingIndex = lastStaticIndex + 1;
                    if (lastMovingIndex >= vectorLen)
                    {
                        // reset all moving indexes
                        int cleared = 2;
                        lastMovingIndex -= 3;
                        while (true)
                        {
                            //Console.WriteLine("last index : {1}\nlast moving index val : {0}", combMatrix[lastMovingIndex], lastMovingIndex);
                            if (combMatrix[lastMovingIndex] == 0)
                                break;

                            combMatrix[lastMovingIndex] = 0;
                            lastMovingIndex--;
                            cleared++;
                            //Console.WriteLine("cleared added, now : {0}", cleared);
                        }

                        // find the most right '1' value 
                        int mostRightIndex = 0;
                        for (int j = 0; j < vectorLen; j++)
                            if (combMatrix[j] == 1) mostRightIndex = j;

                        // slide most right to right
                        combMatrix[mostRightIndex] = 0;
                        mostRightIndex++;
                        combMatrix[mostRightIndex] = 1;

                        // restore removed indexes to the right side of 'most right'
                        //Console.WriteLine("most right index : {0}, cleared : {1}", mostRightIndex, cleared);
                        for (int j = 0; j < cleared; j++)
                        {
                            mostRightIndex++;
                            //Console.WriteLine("value : {0}", combMatrix[mostRightIndex]);
                            combMatrix[mostRightIndex] = 1;
                            //Console.WriteLine("value now : {0}", combMatrix[mostRightIndex]);
                        }
                        //Console.WriteLine("most right index : {0}", mostRightIndex);

                        lastMovingIndex = mostRightIndex;
                        lastStaticIndex = lastMovingIndex - 1;

                        //Console.WriteLine(String.Join(" ", combMatrix));
                    }

                    combMatrix[lastMovingIndex] = 1;
                    nextIndex = lastMovingIndex + 1;
                }
                else
                {
                    combMatrix[lastMovingIndex] = 0;
                    combMatrix[nextIndex] = 1;
                    lastMovingIndex++;
                    nextIndex = lastMovingIndex + 1;
                }

                //Console.WriteLine(String.Join(" ", combMatrix));

                // put combination matrix in currentMap container
                tempMatrix = new int[vectorLen];
                Array.Copy(combMatrix, tempMatrix, vectorLen);
                bool exist = false;
                currentMap.ForEach(x =>
                {
                    if (!exist)
                    {
                        //Console.WriteLine("Comparing \n{0} \nand \n{1}", String.Join(" ", x), String.Join(" ", tempMatrix));
                        if (x.SequenceEqual(tempMatrix))
                            exist = true;
                        //Console.WriteLine("currentMap : {0}\n", exist);
                    }
                });
                if (!exist)
                {
                    currentMap.Add(tempMatrix);
                    newPattern++;
                }
            }

            Console.WriteLine("Combination map created, {0} new featuresPatterns added, should be : {1}", newPattern, n);
        }
        private int GetUpperEdgePixelIndex(Bitmap img)
        {
            for (int i = 0; i < img.Height; i++)
                for (int j = 0; j < img.Width; j++)
                    if (img.GetPixel(j, i).R == 0)
                        return i;
            return 0;
        }

        private int GetBottomEdgePixelIndex(Bitmap img)
        {
            for (int i = img.Height - 1; i >= 0; i--)
                for (int j = 0; j < img.Width; j++)
                    if (img.GetPixel(j, i).R == 0)
                        return i;
            return img.Height - 1;
        }

        private int GetLeftEdgePixelIndex(Bitmap img)
        {
            for (int i = 0; i < img.Width; i++)
                for (int j = 0; j < img.Height; j++)
                    if (img.GetPixel(i, j).R == 0)
                        return i;
            return 0;
        }

        private int GetRightEdgePixelIndex(Bitmap img)
        {
            for (int i = img.Width - 1; i >= 0; i--)
                for (int j = 0; j < img.Height; j++)
                    if (img.GetPixel(i, j).R == 0)
                        return i;
            return img.Width - 1;
        }
        private void generateNumberRepresentation(Dictionary<int[], int> dict, List<int[]> list, int mode = 1)
        {
            int i;

            if (mode == 1)
            {
                i = 0;
                list.ForEach(x => { dict.Add(x, i); i++; });
            }
            else if (mode == 2)
            {
                list.ForEach(x =>
                {
                    dict.Add(x, CountPatternMark(x) * 10 + NeighborMarking(x));
                });
            }
            else if (mode == 3)
            {
                list.ForEach(x =>
                {
                    dict.Add(x, CountPatternMark(x));
                });
            }
        }
        // only works for rootable vector len
        private int NeighborMarking(int[] vector)
        {
            int mark = 0;
            List<int>[] connection = FindConnection(vector);
            if (!FullyConnected(connection))
            {
                mark = 1;
                //Console.WriteLine("Separated");
            }
            else
            {
                //Console.WriteLine("Fully Connected");
            }

            return mark;
        }

        private bool FullyConnected(List<int>[] connection)
        {
            List<int>[] intersections = new List<int>[connection.Length];

            // debug
            /*Console.WriteLine("connections : ");
            for (int i = 0; i < connection.Length; i++)
            {
                Console.Write("connection {0} = ", i);
                connection[i].ForEach(x => Console.Write(x + " "));
                Console.WriteLine(" ");
            }*/

            //Console.WriteLine("Intersections : ");
            // number of connections
            for (int i = 0; i < connection.Length; i++)
            {
                intersections[i] = new List<int>();
                // find each connection intersection with each other
                for (int j = 0; j < connection.Length; j++)
                    if (j != i)
                        intersections[i].AddRange(connection[i].Intersect(connection[j]));
                // clean intersection from duplicate connection
                intersections[i] = intersections[i].Distinct().OrderBy(x => x).ToList();
                // debug
                /*Console.Write("intersection {0} = ", i);
                intersections[i].ForEach(x => Console.Write(x + " "));
                Console.WriteLine("");*/
            }

            // check if there's separate connection
            for (int i = 0; i < intersections.Length; i++)
            {
                if (intersections[i].Any())
                    if (i + 1 < intersections.Length)
                        if (intersections[i + 1].Any())
                            if (!intersections[i].SequenceEqual(intersections[i + 1]))
                                return false;
            }

            return true;
        }
        // only works for rootable vector len
        private List<int>[] FindConnection(int[] vector)
        {
            List<int>[] connection = new List<int>[vector.Length];
            int vectorIterator = 0;
            int[][] matrix = Vec2SquareMatrix(vector);

            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    connection[vectorIterator] = new List<int>();

                    // check left neighbor
                    if (j - 1 >= 0)
                        if (matrix[i][j - 1] == 1)
                            connection[vectorIterator].Add(MatrixIndex2VectorIndex(i, j - 1, matrix[i].Length));
                    // check right neighbor
                    if (j + 1 < matrix[i].Length)
                        if (matrix[i][j + 1] == 1)
                            connection[vectorIterator].Add(MatrixIndex2VectorIndex(i, j + 1, matrix[i].Length));
                    // check upper neighbor
                    if (i - 1 >= 0)
                        if (matrix[i - 1][j] == 1)
                            connection[vectorIterator].Add(MatrixIndex2VectorIndex(i - 1, j, matrix[i].Length));
                    // check lower neighbor
                    if (i + 1 < matrix.Length)
                        if (matrix[i + 1][j] == 1)
                            connection[vectorIterator].Add(MatrixIndex2VectorIndex(i + 1, j, matrix[i].Length));

                    vectorIterator++;
                }
            }

            return connection;
        }

        // for square matrix only
        private int MatrixIndex2VectorIndex(int i, int j, int matrixWidth)
        {
            return i * matrixWidth + j;
        }

        // only works for rootable vector len
        private int[][] Vec2SquareMatrix(int[] vector)
        {
            // null check
            if (vector == null) return null;
            // unsquareable matrix check
            if (Math.Sqrt(vector.Length) % 1 != 0) return null;

            int[][] matrix = new int[(int)Math.Sqrt(vector.Length)][];
            int vectorIterator = 0;

            for (int i = 0; i < matrix.Length; i++)
            {
                matrix[i] = new int[matrix.Length];
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    matrix[i][j] = vector[vectorIterator];
                    vectorIterator++;
                }
            }

            return matrix;
        }

        private int CountPatternMark(int[] vector)
        {
            int result = 0;
            foreach (var val in vector)
                if (val == 1) result++;
            return result;
        }

        #endregion
        public string BondanAutomateSegmentation(bool extractFeature = true, int featureExtractionMode = 1)
        {
            int i, j, k, l, m; // reusable counters
            int range, iwidth, iheight;
            Bitmap processedImage = null;
            int[] features = null;
            Dictionary<int[], int> pattern_numberRepresentation = null;

            // set filePath
            String filePath = APP_PATH + imgName + "/";
            range = 4;

            // set processed image
            if (processedImage == null) processedImage = CloneImage;

            // create folder if not exists
            bool isExists = System.IO.Directory.Exists(filePath);
            if (!isExists) System.IO.Directory.CreateDirectory(filePath);

            // process clone image
            // step 1. get horizontal histogram
            Bitmap HProjectionProfile;
            HProjectionProfile = HProjectionMethod(processedImage);
            iwidth = processedImage.Width; iheight = processedImage.Height;

            // save horizontal histogram
            HProjectionProfile.Save(filePath + "hhistogram.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

            // step 2. calculate total rows
            // step 2.1 define linked list
            LinkedList<Vector3FT[]> rowLimitList = new LinkedList<Vector3FT[]>();

            // find all rows
            // step 2.2 define top point and bottom point of each rows
            rowLimitList = ProcessHorizontalProjection(HProjectionProfile, iheight, range, 0);

            // step 2.3 do connected region schemes
            rowLimitList = DoConnectedRegions(rowLimitList, HProjectionProfile);

            // step 3. foreach row total do
            Bitmap segmentImage;
            Bitmap VProjectionProfile;
            // define array of linked list to save total column of each row
            LinkedList<Vector3FT[]>[] colLimitList = new LinkedList<Vector3FT[]>[rowLimitList.Count];
            for (i = 0; i < rowLimitList.Count; i++)
            {
                // step 3.1 clone image
                segmentImage = new Bitmap(CopyImage(processedImage, 0, (int)rowLimitList.ElementAt(i)[0].Y + 1,
                                                    iwidth, (int)rowLimitList.ElementAt(i)[1].Y));

                // step 3.2 generate vertical histogram
                VProjectionProfile = new Bitmap(VProjectionMethod(segmentImage));

                // step 3.3 loop to all vertical histogram image
                colLimitList[i] = new LinkedList<Vector3FT[]>();

                colLimitList[i] = ProcessVerticalProjection(VProjectionProfile, range, 0);

                // save vhistogram
                VProjectionProfile.Save(filePath + "vhistogram-" + i + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            }

            // save each segmented image
            for (i = 0; i < rowLimitList.Count; i++)
            {
                for (j = 0; j < colLimitList[i].Count; j++)
                {
                    // save all images
                    segmentImage = new Bitmap(CopyImage(processedImage,
                                                            (int)colLimitList[i].ElementAt(j)[0].X - range,
                                                            (int)rowLimitList.ElementAt(i)[0].Y - range,
                                                            (int)colLimitList[i].ElementAt(j)[1].X + range,
                                                            (int)rowLimitList.ElementAt(i)[1].Y + range));
                    // create product folder
                    String productPath = filePath + "/product-" + i + "-" + j;
                    isExists = System.IO.Directory.Exists(productPath);
                    if (!isExists) System.IO.Directory.CreateDirectory(productPath);
                    segmentImage.Save(productPath + "/orig.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }

            // calculate & set all possible combinations
            int[] possibleCombinations = new int[9];
            int possibleCombinationCount = 1;
            for (k = 1; k <= 9; k++)
            {
                var result = Combination(9, k);
                possibleCombinations[k - 1] = result;
                var previousVal = possibleCombinationCount;
                possibleCombinationCount += result;
            }
            Console.WriteLine("Possible combinations : {0}", possibleCombinationCount);

            if (extractFeature)
            {
                // create possible patterns combination in form of vector
                List<int[]> possiblePatterns = new List<int[]>();
                GeneratePossiblePatterns(possiblePatterns, 1, 0, 9);
                for (i = 0; i < 9; i++)
                    GeneratePossiblePatterns(possiblePatterns, possibleCombinations[i], i + 1, 9);
                // create number resprentation of possible patterns
                pattern_numberRepresentation = new Dictionary<int[], int>();
                generateNumberRepresentation(pattern_numberRepresentation, possiblePatterns, featureExtractionMode);
            }

            // do segmentation on each segment image
            String[] temp = System.IO.Directory.GetDirectories(filePath);
            String linesToWrite = "";
            for (i = 0; i < temp.Length; i++)
            {
                String fpath = temp[i];
                String[] segmentImgPath = System.IO.Directory.GetFiles(fpath, "orig.jpg");

                Bitmap segmentProcessedImage;
                // load image from path folder only if the orig image just 1
                if (segmentImgPath.Length == 1) segmentProcessedImage = new Bitmap(segmentImgPath[0]);
                else
                {
                    MessageBox.Show("This segmentation phase can't be continue because the result folder are exists");
                    return "none";
                }
                int segmentRange = 2;
                int swidth, sheight;
                // process clone image
                // step 1. get horizontal histogram
                HProjectionProfile = HProjectionMethod(segmentProcessedImage);
                swidth = segmentProcessedImage.Width; sheight = segmentProcessedImage.Height;

                // save horizontal histogram
                HProjectionProfile.Save(fpath + "/segment-hhistogram.jpg");

                // step 2. calculate total rows
                // step 2.1 define linked list
                LinkedList<Vector3FT[]> segmentRow = new LinkedList<Vector3FT[]>();

                // find all rows
                // step 2.2 define top point and bottom point of each rows
                segmentRow = ProcessHorizontalProjection(HProjectionProfile, sheight, segmentRange, 1);

                // step 3. foreach row total do
                // define array of linked list to save total column of each row
                LinkedList<Vector3FT[]>[] segmentCol = new LinkedList<Vector3FT[]>[segmentRow.Count];
                for (j = 0; j < segmentRow.Count; j++)
                {
                    // step 3.1 clone image
                    segmentImage = new Bitmap(CopyImage(segmentProcessedImage, 0, (int)segmentRow.ElementAt(j)[0].Y + 1,
                                                        swidth, (int)segmentRow.ElementAt(j)[1].Y));

                    // step 3.2 generate vertical histogram
                    VProjectionProfile = new Bitmap(VProjectionMethod(segmentImage));

                    // step 3.3 loop to all vertical histogram image
                    segmentCol[j] = new LinkedList<Vector3FT[]>();

                    segmentCol[j] = ProcessVerticalProjection(VProjectionProfile, segmentRange, 1);

                    // save vhistogram
                    VProjectionProfile.Save(fpath + "/segment-vhistogram-" + j + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                // save each segmented image
                linesToWrite = "";
                for (j = 0; j < segmentRow.Count; j++)
                {
                    for (k = 0; k < segmentCol[j].Count; k++)
                    {
                        // save all images
                        segmentImage = new Bitmap(CopyImage(segmentProcessedImage,
                                                                (int)segmentCol[j].ElementAt(k)[0].X - range,
                                                                (int)segmentRow.ElementAt(j)[0].Y - range,
                                                                (int)segmentCol[j].ElementAt(k)[1].X + range,
                                                                (int)segmentRow.ElementAt(j)[1].Y + range));

                        // image cropping optimization
                        segmentImage = new Bitmap(CopyImage(segmentImage,
                            GetLeftEdgePixelIndex(segmentImage),
                            GetUpperEdgePixelIndex(segmentImage),
                            GetRightEdgePixelIndex(segmentImage),
                            GetBottomEdgePixelIndex(segmentImage)));

                        if (extractFeature)
                        {
                            // resize image to 33 (width) x 24 (height)
                            segmentImage = new Bitmap(segmentImage, 33, 24);

                            // further optimize image cropping
                            segmentImage = new Bitmap(CopyImage(segmentImage,
                                GetLeftEdgePixelIndex(segmentImage),
                                GetUpperEdgePixelIndex(segmentImage),
                                GetRightEdgePixelIndex(segmentImage),
                                GetBottomEdgePixelIndex(segmentImage)));
                            segmentImage = new Bitmap(segmentImage, 33, 24);

                            // init 88 features pattern container array
                            int[][] featuresPatterns = new int[88][];

                            // extract 3 x 3 matrix from resized image,
                            // in a form of 9 length vector 
                            int patternIterator = 0;
                            for (l = 0; l < segmentImage.Height; l += 3)
                            {
                                for (m = 0; m < segmentImage.Width; m += 3)
                                {
                                    featuresPatterns[patternIterator] = ExtractPattern(segmentImage, m, l, 3);
                                    /*if (featuresPatterns[patternIterator].SequenceEqual(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 }))
                                        Console.WriteLine("zero all found at {0}, {1}", l, m);*/
                                    patternIterator++;
                                }
                            }

                            // translate features patterns into their representation numbers
                            features = new int[88];
                            for (l = 0; l < 88; l++)
                                features[l] = pattern_numberRepresentation.Where(x => x.Key.SequenceEqual(featuresPatterns[l])).Select(x => x.Value).FirstOrDefault();
                        }

                        int segment_pos_x, segment_pos_y, segment_width, segment_height;
                        segment_pos_x = (int)segmentCol[j].ElementAt(k)[0].X;
                        segment_pos_y = (int)segmentRow.ElementAt(j)[0].Y;
                        segment_width = (int)segmentCol[j].ElementAt(k)[1].X - segment_pos_x;
                        segment_height = (int)segmentRow.ElementAt(j)[1].Y - segment_pos_y;
                        linesToWrite += j + "-" + k + " " + segment_pos_x.ToString() + " " + segment_pos_y.ToString()
                                        + " " + segment_width + " " + segment_height;
                        // add features
                        if (extractFeature)
                            foreach (var val in features)
                                linesToWrite += " " + val;
                        linesToWrite += ";";

                        segmentImage.Save(fpath + "/segment-" + j + "-" + k + ".jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }

                // save temporary pos and size segment to path
                System.IO.File.WriteAllText(fpath + "/temporary-pos.txt", linesToWrite);
            }


            // save csv file
            LinkedList<String[]> csvStr = new LinkedList<String[]>();
            string strToWrite = "Image Data|Parent Folder|Segment Image|X|Y|Width|Height";
            if (extractFeature)
                for (i = 0; i < 88; i++)
                    strToWrite += "|F" + (i + 1);
            String[] lineCsv = strToWrite.Split('|');
            // insert table title
            csvStr.AddLast(lineCsv);

            for (i = 0; i < rowLimitList.Count; i++)
            {
                for (j = 0; j < colLimitList[i].Count; j++)
                {
                    // insert all item
                    // search temporary files text;
                    string temporaryText = System.IO.File.ReadAllText(filePath + "/product-" + i + "-" + j + "/temporary-pos.txt");
                    string[][] temporarySegmentPos = temporaryText.Split(';').Select(s => s.Split(' ')).ToArray();
                    for (k = 0; k < temporarySegmentPos.Length - 1; k++)
                    {
                        if (extractFeature)
                            lineCsv = new String[7 + 88];
                        else
                            lineCsv = new String[7];
                        lineCsv[0] = "imgData.jpg";
                        lineCsv[1] = "'" + i + "-" + j;
                        lineCsv[2] = "'" + temporarySegmentPos[k][0];
                        // X and Y
                        lineCsv[3] = Convert.ToString((int)colLimitList[i].ElementAt(j)[0].X + Convert.ToInt32(temporarySegmentPos[k][1]) - range);
                        lineCsv[4] = Convert.ToString((int)rowLimitList.ElementAt(i)[0].Y + Convert.ToInt32(temporarySegmentPos[k][2]) - range);
                        // Width and Height
                        lineCsv[5] = temporarySegmentPos[k][3];
                        lineCsv[6] = temporarySegmentPos[k][4];
                        // features
                        if (extractFeature)
                            for (l = 0; l < 88; l++)
                                lineCsv[7 + l] = temporarySegmentPos[k][5 + l];

                        // push to linked list
                        csvStr.AddLast(lineCsv);
                    }
                }
            }

            String csvPath = filePath + "88result.csv";
            if (!System.IO.File.Exists(csvPath))
            {
                System.IO.File.Create(csvPath).Close();
            }

            string delimiter = ";";
            StringBuilder sb = new StringBuilder();

            j = 0; k = 0;

            for (i = 0; i < csvStr.Count; i++)
            {
                if (i > 0)
                {
                    // store original letter width & height
                    j = int.Parse(csvStr.ElementAt(i)[5]);
                    k = int.Parse(csvStr.ElementAt(i)[6]);
                    // change width & height
                    csvStr.ElementAt(i)[5] = "33";
                    csvStr.ElementAt(i)[6] = "24";
                }

                sb.AppendLine(string.Join(delimiter, csvStr.ElementAt(i)));

                if (i > 0)
                {
                    // restore orininal width & height
                    csvStr.ElementAt(i)[5] = "" + j;
                    csvStr.ElementAt(i)[6] = "" + k;
                }
            }

            System.IO.File.WriteAllText(csvPath, sb.ToString());

            /*
             * 
             * TRACE SEGMENTATION
             * 
             */
            // draw image
            for (i = 1; i < csvStr.Count; i++)
            {
                Graphics g = Graphics.FromImage(processedImage);
                // draw segment image
                g.DrawRectangle(new Pen(Color.Maroon, 1),
                                    new Rectangle(Int32.Parse(csvStr.ElementAt(i)[3]), Int32.Parse(csvStr.ElementAt(i)[4]),
                                                  Int32.Parse(csvStr.ElementAt(i)[5]), Int32.Parse(csvStr.ElementAt(i)[6])));
            }

            // save source data to folder
            Bitmap toSave = new Bitmap(processedImage);
            toSave.Save(filePath + "imgData.jpg");
            
            return filePath;
        }
    }
}
