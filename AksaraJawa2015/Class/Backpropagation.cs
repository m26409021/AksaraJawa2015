﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AksaraJawa2015
{
    class Backpropagation
    {
        public Network CNetwork;
   
        public int inputrow, inputcol, numberofFiles, classcounter,rowcounter,colcounter,epochcounter,maxiteration;
        int CVcounter,CVinputrow,Nvalue;
        int[] totaleachclass,CVeachclass;//total data for each class
        float[][] inputdata,CVdata,CopyInputdata;
        int[][] TargetOutput;
        public float ThresholdValue,GradientF,Epsilon,SSE,SSEcv,SSEfuture,LearningRate;
        bool finish,discrete,auto,p_aksara,p_houghfeatures; //p_aksara == FormBackprop.aksara, p_houghfeatures == FormBackprop.houghfeatures
        float trainAcc;
        float[] statusPrun;
        float[][] Ktable,tempInputData;//for Ksquare
        float[] Ksquarevalue;
        bool[][] ksquareStatus;
        Dictionary<string, int> aksara_dict;
        List<int> houghCol, _88featuresCol;
        private Dictionary<string, string> _javaUnicode;
        
        public Backpropagation(int value=1)
        {
            CNetwork = new Network();
            numberofFiles = value;
            totaleachclass = new int[value];
            CVeachclass = new int[value];
            inputrow = 0;
            CVcounter = 0;
            CVinputrow = 0;
            inputcol = 0;
            rowcounter = 0;
            classcounter = 0;//to know each classification total
            ThresholdValue = 0;
            GradientF = 0;
            maxiteration = 0;
            Epsilon = 0;
            epochcounter = 0;
            SSE = 0;//big random number
            SSEcv = 0;
            SSEfuture = 0;
            finish = false;

            #region _88features_used_columns
            _88featuresCol = new List<int>();
            _88featuresCol.Add(3);
            _88featuresCol.Add(8);
            _88featuresCol.Add(9);
            _88featuresCol.Add(10);
            _88featuresCol.Add(11);
            _88featuresCol.Add(12);
            _88featuresCol.Add(13);
            _88featuresCol.Add(14);
            _88featuresCol.Add(15);
            _88featuresCol.Add(16);
            _88featuresCol.Add(17);
            _88featuresCol.Add(18);
            _88featuresCol.Add(19);
            _88featuresCol.Add(20);
            _88featuresCol.Add(21);
            _88featuresCol.Add(22);
            _88featuresCol.Add(23);
            _88featuresCol.Add(24);
            _88featuresCol.Add(25);
            _88featuresCol.Add(26);
            _88featuresCol.Add(27);
            _88featuresCol.Add(28);
            _88featuresCol.Add(29);
            _88featuresCol.Add(30);
            _88featuresCol.Add(31);
            _88featuresCol.Add(32);
            _88featuresCol.Add(33);
            _88featuresCol.Add(34);
            _88featuresCol.Add(35);
            _88featuresCol.Add(36);
            _88featuresCol.Add(37);
            _88featuresCol.Add(38);
            _88featuresCol.Add(39);
            _88featuresCol.Add(40);
            _88featuresCol.Add(41);
            _88featuresCol.Add(42);
            _88featuresCol.Add(43);
            _88featuresCol.Add(44);
            _88featuresCol.Add(45);
            _88featuresCol.Add(46);
            _88featuresCol.Add(47);
            _88featuresCol.Add(48);
            _88featuresCol.Add(49);
            _88featuresCol.Add(50);
            _88featuresCol.Add(51);
            _88featuresCol.Add(52);
            _88featuresCol.Add(53);
            _88featuresCol.Add(54);
            _88featuresCol.Add(55);
            _88featuresCol.Add(56);
            _88featuresCol.Add(57);
            _88featuresCol.Add(58);
            _88featuresCol.Add(59);
            _88featuresCol.Add(60);
            _88featuresCol.Add(61);
            _88featuresCol.Add(62);
            _88featuresCol.Add(63);
            _88featuresCol.Add(64);
            _88featuresCol.Add(65);
            _88featuresCol.Add(66);
            _88featuresCol.Add(67);
            _88featuresCol.Add(68);
            _88featuresCol.Add(69);
            _88featuresCol.Add(70);
            _88featuresCol.Add(71);
            _88featuresCol.Add(72);
            _88featuresCol.Add(73);
            _88featuresCol.Add(74);
            _88featuresCol.Add(75);
            _88featuresCol.Add(76);
            _88featuresCol.Add(77);
            _88featuresCol.Add(78);
            _88featuresCol.Add(79);
            _88featuresCol.Add(80);
            _88featuresCol.Add(81);
            _88featuresCol.Add(82);
            _88featuresCol.Add(83);
            _88featuresCol.Add(84);
            _88featuresCol.Add(85);
            _88featuresCol.Add(86);
            _88featuresCol.Add(87);
            _88featuresCol.Add(88);
            _88featuresCol.Add(89);
            _88featuresCol.Add(90);
            _88featuresCol.Add(91);
            _88featuresCol.Add(92);
            _88featuresCol.Add(93);
            _88featuresCol.Add(94);
            _88featuresCol.Add(95);
            #endregion

            #region Hough_used_colums
            houghCol = new List<int>();
            houghCol.Add(3);
            houghCol.Add(8);
            houghCol.Add(9);
            houghCol.Add(10);
            houghCol.Add(11);
            houghCol.Add(14);
            houghCol.Add(17);
            houghCol.Add(19);
            #endregion

            
            #region Unicode initialization
            // init java font unicodes
            _javaUnicode = new Dictionary<string, string>();
            // carakan
            _javaUnicode.Add("ha", "a");
            _javaUnicode.Add("na", "n");
            _javaUnicode.Add("ca", "c");
            _javaUnicode.Add("ra", "r");
            _javaUnicode.Add("ka", "k");
            _javaUnicode.Add("da", "f");
            _javaUnicode.Add("ta", "t");
            _javaUnicode.Add("sa", "s");
            _javaUnicode.Add("wa", "w");
            _javaUnicode.Add("la", "l");
            _javaUnicode.Add("pa", "p");
            _javaUnicode.Add("dha", "d");
            _javaUnicode.Add("ja", "j");
            _javaUnicode.Add("ya", "y");
            _javaUnicode.Add("nya", "v");
            _javaUnicode.Add("ma", "m");
            _javaUnicode.Add("ga", "g");
            _javaUnicode.Add("ba", "b");
            _javaUnicode.Add("tha", "q");
            _javaUnicode.Add("nga", "z");
            // pasangan
            _javaUnicode.Add("pasangan ha", "H");
            _javaUnicode.Add("pasangan na", "N");
            _javaUnicode.Add("pasangan ca", "C");
            _javaUnicode.Add("pasangan ra", "R");
            _javaUnicode.Add("pasangan ka", "K");
            _javaUnicode.Add("pasangan da", "F");
            _javaUnicode.Add("pasangan ta", "T");
            _javaUnicode.Add("pasangan sa", "S");
            _javaUnicode.Add("pasangan wa", "W");
            _javaUnicode.Add("pasangan la", "L");
            _javaUnicode.Add("pasangan pa", "P");
            _javaUnicode.Add("pasangan dha", "D");
            _javaUnicode.Add("pasangan ja", "J");
            _javaUnicode.Add("pasangan ya", "Y");
            _javaUnicode.Add("pasangan nya", "V");
            _javaUnicode.Add("pasangan ma", "M");
            _javaUnicode.Add("pasangan ga", "G");
            _javaUnicode.Add("pasangan ba", "B");
            _javaUnicode.Add("pasangan tha", "Q");
            _javaUnicode.Add("pasangan nga", "Z");
            // sandhangan
            _javaUnicode.Add("wulu", "i");
            _javaUnicode.Add("suku", "u");
            _javaUnicode.Add("taling", "[");
            _javaUnicode.Add("pepet", "e");
            _javaUnicode.Add("tarung", "o");
            _javaUnicode.Add("layar", "/");
            _javaUnicode.Add("wignyan", "h");
            _javaUnicode.Add("cecak", "=");
            _javaUnicode.Add("pangkon", "\\");
            _javaUnicode.Add("pangku", "\\");
            _javaUnicode.Add("paten", "\\");
            _javaUnicode.Add("pengkal", "-");
            _javaUnicode.Add("cakra", "]");
            _javaUnicode.Add("keret", "}");
            // wilangan
            _javaUnicode.Add("0", "0");
            _javaUnicode.Add("1", "1");
            _javaUnicode.Add("2", "2");
            _javaUnicode.Add("3", "3");
            _javaUnicode.Add("4", "4");
            _javaUnicode.Add("5", "5");
            _javaUnicode.Add("6", "6");
            _javaUnicode.Add("7", "7");
            _javaUnicode.Add("8", "8");
            _javaUnicode.Add("9", "9");
            // tanda baca
            _javaUnicode.Add("adeg adeg", "?");
            _javaUnicode.Add("pada lungsi", ".");
            _javaUnicode.Add("pada lingsa", ",");
            // null char
            _javaUnicode.Add("-", null);
            #endregion
        }
        public void resetValue(int value = 1, bool aksara = false, bool testing = false)
        {
            numberofFiles = value;
            totaleachclass = new int[value];
            CVeachclass = new int[value];
            inputrow = 0;
            CVcounter = 0;
            CVinputrow = 0;
            inputcol = 0;
            rowcounter = 0;
            //if (aksara == false || testing == false)
                classcounter = 0; //to know each classification total
            //TresholdValue = 0;
            GradientF = 0;
            //maxiteration = 0;
            //Epsilon = 0;
            epochcounter = 0;
            SSE = 0;//big random number
            SSEcv = 0;
            SSEfuture = 0;
            finish = false;
        }
        public void roundUP(int decimals)
        {
            for (int i = 0; i < inputdata.Count(); i++)
            {
                for (int j = 0; j < inputdata[i].Count() - 1; j++)
                {
                    float asd = (float)Math.Round(inputdata[i][j], decimals);
                    float qwe = (float)Math.Round(inputdata[i][j], decimals+1);
                    inputdata[i][j] += Math.Abs(asd - qwe);
                }
            }
        }
        public void wizardInputFile(String path)
        {
            int counter = 0;
            string[] column = null;
            bool skip_first_row = true;
            using (System.IO.StreamReader sr = System.IO.File.OpenText(path))
            {
                String input;
                bool readcol = false;
                while ((input = sr.ReadLine()) != null)
                {
                    if (skip_first_row == true)
                    {
                        skip_first_row = false;
                        continue;
                    }
                    column = input.Split(';');
                    if (readcol == false)
                    {
                        int tempcol = 0;
                        for (int i = 0; i < column.Count(); i++)
                        {
                            if (column[i] != "" && _88featuresCol.Contains(i + 1) && i+1 != _88featuresCol.First())
                            {
                                tempcol++;
                            }
                        }
                        readcol = true;
                        if (discrete == true)
                        {
                            if (auto == false)
                                inputcol = tempcol + (Nvalue * tempcol);
                            else
                            {
                                int sum = 0;
                                for (int i = 0; i < Ktable.Count(); i++)
                                    sum += Ktable[i].Count();
                                inputcol = tempcol + sum;
                            }
                        }
                    }
                    counter++;
                }
            }
            if (counter > 0)
            {
                inputrow = counter;
            }
        }
        public void InputFile(String path, int tab = 0, bool aksara = false, bool testing = false, bool HoughFeatures = false)
        {
            int counter = 0;
            string[] column = null;
            bool skip_first_row = true;
            using (System.IO.StreamReader sr = System.IO.File.OpenText(path))
            {
                String input;
                bool readcol = false;
                int index_nama_huruf = 0;
                if (HoughFeatures == true)
                {
                    index_nama_huruf = houghCol.First();
                }
                else
                {
                    index_nama_huruf = _88featuresCol.First();
                }
                if(testing == false && aksara == true)
                    aksara_dict = new Dictionary<string, int>(); //untuk mapping class assignment dengan huruf, misal: ha -> 0
                while ((input = sr.ReadLine()) != null)
                {
                    if (aksara == true)
                    {
                        if (skip_first_row == true)
                        {
                            skip_first_row = false;
                            continue;
                        }
                        column = input.Split(';');
                    }
                    else
                    {
                        column = input.Split(' ');
                    }
                    
                    if (readcol == false)
                    {
                        if ((aksara == true && testing == false))
                        {
                            aksara_dict.Add(column[index_nama_huruf], classcounter);
                        }
                        if (aksara == true && testing == true)
                        {
                            classcounter = aksara_dict.Count;
                            if (!aksara_dict.ContainsKey(column[index_nama_huruf]))
                                aksara_dict.Add(column[index_nama_huruf], classcounter);
                        }
                        

                        int tempcol = 0;
                        if (aksara == true)
                        {
                            for (int i = 0; i < column.Count(); i++)
                            {
                                if (column[i] != "" && HoughFeatures == true && houghCol.Contains(i) && i != houghCol.First())
                                {
                                    tempcol++;
                                }
                                else if (column[i] != "" && HoughFeatures == false && _88featuresCol.Contains(i) && i != _88featuresCol.First())
                                {
                                    tempcol++;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < column.Count(); i++)
                            {
                                if (column[i] != "")
                                {
                                    tempcol++;
                                }
                            }
                        }
                        
                        readcol = true;
                        if (discrete == true)
                        {
                            if (auto == false)
                                inputcol = tempcol + (Nvalue * tempcol);
                            else
                            {
                                int sum = 0;
                                for (int i = 0; i < Ktable.Count(); i++)
                                    sum += Ktable[i].Count();
                                inputcol = tempcol + sum;
                            }
                        }
                        else
                            inputcol = tempcol;

                    }


                    if (aksara == true && !aksara_dict.ContainsKey(column[index_nama_huruf]))
                    {
                        aksara_dict.Add(column[index_nama_huruf], classcounter + 1);
                        inputrow += counter;
                        totaleachclass[classcounter] = counter;
                        classcounter++;
                        counter = 1;
                    }
                    else
                    {
                        counter++; //hitung banyak data dalam 1 class
                    }
                    
                }

                if (aksara == false)
                {
                    inputrow += counter;
                    totaleachclass[classcounter] = counter;
                    classcounter++;
                }
                //if (tab == 0 || tab == 2)//if it's for training then generate Cross-validation(0 == backprop-tab; 1== pruning-tab)
                //{
                //    //CVeachclass[classcounter] = (int)Math.Floor(0.2 * counter);
                //    //CVinputrow += (int)Math.Floor(0.2 * counter);
                //    //counter = (int)Math.Ceiling(0.8 * counter);
                //    //inputrow += counter;
                //    //totaleachclass[classcounter] = counter;
                //}
                //else
                //{
                    //inputrow += counter;
                    //totaleachclass[classcounter] = counter;
                //}
                //classcounter++;
            }
            if (aksara == true && counter > 0)
            {
                inputrow += counter;
                if (classcounter < totaleachclass.Count())
                {
                    totaleachclass[classcounter] = counter;
                    classcounter++;
                }
            }
        }
        public void wizardReadInputData(string path)
        {
            bool skip_first_row = true;
            using (System.IO.StreamReader sr = System.IO.File.OpenText(path))
            {
                String input;
                while ((input = sr.ReadLine()) != null)
                {
                    if (skip_first_row == true)
                    {
                        skip_first_row = false;
                        continue;
                    }
                    string[] value = input.Split(';');
                    colcounter = 0;
                    for (int i = 0; i < value.Count(); i++) //mulai dari 1 karena column 0 = hurufnya
                    {
                        if (value[i] != "" && _88featuresCol.Contains(i+1) && i+1 != _88featuresCol.First())
                        {
                            inputdata[rowcounter][colcounter] = float.Parse(value[i]);
                            colcounter++;
                        }
                    }
                    if (discrete == true && auto == true)
                    {
                        int index = colcounter;
                        for (int j = 0; j < colcounter; j++)
                        {
                            for (int k = 0; k < Ktable[j].Count(); k++)
                            {
                                if (inputdata[rowcounter][j] <= Ktable[j][k])
                                {
                                    //for (int counter = k; counter < Ktable[j].Count(); counter++)
                                    //{
                                    //    inputdata[rowcounter][index + counter] = 1;
                                    //}
                                    //break;
                                    inputdata[rowcounter][index + k] = 0;
                                }
                                else
                                {
                                    inputdata[rowcounter][index + k] = 1;
                                }
                            }
                            index += Ktable[j].Count();
                        }
                    }
                    inputdata[rowcounter][inputdata[rowcounter].Count() - 1] = float.Parse(value[1].Split('-')[0].Split('\'')[1]);
                    rowcounter++;
                }
            }
        }
        public void ReadInputData(String path, bool aksara = false, bool HoughFeatures = false)
        {
            int helpcounter = 0;
            bool skip_first_row = true;
            int index_nama_huruf = 0;
            if (HoughFeatures == true)
            {
                index_nama_huruf = houghCol.First();
            }
            else
            {
                index_nama_huruf = _88featuresCol.First();
            }
            using (System.IO.StreamReader sr = System.IO.File.OpenText(path))
            {
                String input;
                while ((input = sr.ReadLine()) != null)
                {
                    if (aksara == false)
                    {
                        string[] value = input.Split(' ');
                        colcounter = 0;
                        for (int i = 0; i < value.Count(); i++)
                        {
                            if (helpcounter < totaleachclass[classcounter])
                            {
                                if (value[i] != "")
                                {
                                    inputdata[rowcounter][colcounter] = float.Parse(value[i]);
                                    colcounter++;
                                }
                            }
                            else
                            {
                                if (value[i] != "")
                                {
                                    CVdata[CVcounter][colcounter] = float.Parse(value[i]);
                                    colcounter++;
                                }
                            }
                        }
                        if (helpcounter < totaleachclass[classcounter])
                        {
                            if (discrete == true && auto == true)
                            {
                                int index = colcounter;
                                for (int j = 0; j < colcounter; j++)
                                {
                                    for (int k = 0; k < Ktable[j].Count(); k++)
                                    {
                                        if (inputdata[rowcounter][j] < Ktable[j][k])
                                        {
                                            for (int counter = k; counter < Ktable[j].Count(); counter++)
                                            {
                                                inputdata[rowcounter][index + counter] = 1;
                                            }
                                            break;
                                        }
                                    }
                                    index += Ktable[j].Count();
                                }
                            }
                            inputdata[rowcounter][inputdata[rowcounter].Count() - 1] = classcounter;
                            rowcounter++;
                        }
                        else
                        {
                            if (discrete == true && auto == true)
                            {
                                int index = colcounter;
                                for (int j = 0; j < colcounter; j++)
                                {
                                    for (int k = 0; k < Ktable[j].Count(); k++)
                                    {
                                        if (CVdata[CVcounter][j] < Ktable[j][k])
                                        {
                                            for (int counter = k; counter < Ktable[j].Count(); counter++)
                                            {
                                                CVdata[CVcounter][index + counter] = 1;
                                            }
                                            break;
                                        }
                                    }
                                    index += Ktable[j].Count();
                                }
                            }
                            CVdata[CVcounter][CVdata[CVcounter].Count() - 1] = classcounter;
                            CVcounter++;
                        }
                        helpcounter++;
                    }
                    else
                    {
                        if (skip_first_row == true)
                        {
                            skip_first_row = false;
                            continue;
                        }
                        string[] value = input.Split(';');
                        colcounter = 0;
                        for (int i = 0; i < value.Count(); i++) //mulai dari 1 karena column 0 = hurufnya
                        {
                            if (value[i] != "" && HoughFeatures == true && houghCol.Contains(i) && i != houghCol.First())
                            {
                                inputdata[rowcounter][colcounter] = float.Parse(value[i]);
                                colcounter++;
                            }
                            else if (value[i] != "" && HoughFeatures == false && _88featuresCol.Contains(i) && i != _88featuresCol.First())
                            {
                                inputdata[rowcounter][colcounter] = float.Parse(value[i]);
                                colcounter++;
                            }
                        }
                        if (discrete == true && auto == true)
                        {
                            int index = colcounter;
                            for (int j = 0; j < colcounter; j++)
                            {
                                for (int k = 0; k < Ktable[j].Count(); k++)
                                {
                                    if (inputdata[rowcounter][j] <= Ktable[j][k])
                                    {
                                        //for (int counter = k; counter < Ktable[j].Count(); counter++)
                                        //{
                                        //    inputdata[rowcounter][index + counter] = 1;
                                        //}
                                        //break;
                                        inputdata[rowcounter][index + k] = 0;
                                    }
                                    else
                                    {
                                        inputdata[rowcounter][index + k] = 1;
                                    }
                                }
                                index += Ktable[j].Count();
                            }
                        }
                        inputdata[rowcounter][inputdata[rowcounter].Count() - 1] = aksara_dict[value[index_nama_huruf]];
                        rowcounter++;
                    }
                }
            }
            if (aksara == false)
            {
                //1 file = 1 class, selesai 1 file, classcounter++
                classcounter++;
            }
            else
            {
                classcounter = aksara_dict.Count;
            }

        }
        public void addManualDiscreteVariable()
        {
            if (discrete == true && auto == false)
            {
                if (Nvalue != 0)
                {
                    float tempN = (float)1.0 / Nvalue;
                    for (int i = 0; i < inputdata.Count(); i++)
                    {
                        for (int j = 0; j < colcounter; j++)
                        {
                            float posN = (float)Math.Floor(inputdata[i][j] / tempN);
                            for (int k = j * Nvalue, counter = Nvalue; k < (j + 1) * Nvalue; k++, counter--)
                            {
                                if (counter > posN)
                                    inputdata[i][colcounter + k] = 0;
                                else
                                    inputdata[i][colcounter + k] = 1;
                            }
                        }
                    }
                    for (int i = 0; i < CVdata.Count(); i++)
                    {
                        for (int j = 0; j < colcounter; j++)
                        {
                            float posN = (float)Math.Floor(CVdata[i][j] / tempN);
                            for (int k = j * Nvalue, counter = Nvalue; k < (j + 1) * Nvalue; k++, counter--)
                            {
                                if (counter > posN)
                                    CVdata[i][colcounter + k] = 0;
                                else
                                    CVdata[i][colcounter + k] = 1;
                            }
                        }
                    }
                }
            }
        }
        public void wizardNormalizeData()
        {
            float max = inputdata[0][0], min = inputdata[0][0];
            for (int k = 0; k < colcounter; k++)
            {
                for (int i = 0; i < inputdata.Count(); i++)
                {
                    if (max < inputdata[i][k])
                        max = inputdata[i][k];
                    if (min > inputdata[i][k])
                        min = inputdata[i][k];
                }
                if (min < 0)
                {
                    for (int i = 0; i < inputdata.Count(); i++)
                    {
                        inputdata[i][k] = (inputdata[i][k] - (max + min) / 2) / ((max - min) / 2);
                    }
                }
                else
                {
                    for (int i = 0; i < inputdata.Count(); i++)
                    {
                        inputdata[i][k] = (inputdata[i][k] - min) / (max - min);
                    }
                }
            }
        }
        public void NormalizeData()
        {
            float max=inputdata[0][0], min=inputdata[0][0];
            for (int k = 0; k < colcounter; k++)
            {
                for (int i = 0; i < inputdata.Count(); i++)
                {
                    if (max < inputdata[i][k])
                        max = inputdata[i][k];
                    if (min > inputdata[i][k])
                        min = inputdata[i][k];
                }
                for (int i = 0; i < CVdata.Count(); i++)
                {
                    if (max < CVdata[i][k])
                        max = CVdata[i][k];
                    if (min > CVdata[i][k])
                        min = CVdata[i][k];
                }
                if (min < 0)
                {
                    for (int i = 0; i < inputdata.Count(); i++)
                    {
                        inputdata[i][k] = (inputdata[i][k] - (max + min) / 2) / ((max - min) / 2);
                    }
                    for (int i = 0; i < CVdata.Count(); i++)
                    {
                        CVdata[i][k] = (CVdata[i][k] - (max + min) / 2) / ((max - min) / 2);
                    }
                }
                else
                {
                    for (int i = 0; i < inputdata.Count(); i++)
                    {
                        inputdata[i][k] = (inputdata[i][k] - min) / (max - min);
                    }
                    for (int i = 0; i < CVdata.Count(); i++)
                    {
                        CVdata[i][k] = (CVdata[i][k] - min) / (max - min);
                    }

                }
            }
        }
        public void wizardGenerateTestingData()
        {
            inputdata = new float[inputrow][];
            for (int i = 0; i < inputrow; i++)
            {
                inputdata[i] = new float[inputcol+1];//+1 for row separator
            }
            numberofFiles = aksara_dict.Count;
            TargetOutput = new int[numberofFiles][];
            for (int i = 0; i < numberofFiles; i++)
            {
                TargetOutput[i] = new int[numberofFiles];
            }
            for (int i = 0; i < numberofFiles; i++)
            {
                for (int j = 0; j < numberofFiles; j++)
                {
                    if (j == i)
                        TargetOutput[i][j] = 1;
                    else
                        TargetOutput[i][j] = 0;
                }
            }
        }
        public void GenerateTestingData(bool aksara = false, bool testing = false)
        {
            CVdata = new float[CVinputrow][];
            for (int i = 0; i < CVinputrow; i++)
            {
                CVdata[i] = new float[inputcol+1];//+1 for class label
            }
           
            inputdata = new float[inputrow][];
            for (int i = 0; i < inputrow; i++)
            {
                inputdata[i] = new float[inputcol + 1];//+1 for class label
            }
            
            //Generate TargetOutput Pattern
            if (aksara == false || testing == false)
            {
                TargetOutput = new int[numberofFiles][];
                for (int i = 0; i < numberofFiles; i++)
                {
                    TargetOutput[i] = new int[numberofFiles];
                }
                for (int i = 0; i < numberofFiles; i++)
                {
                    for (int j = 0; j < numberofFiles; j++)
                    {
                        if (j == i)
                            TargetOutput[i][j] = 1;
                        else
                            TargetOutput[i][j] = 0;
                    }
                }
            }
            else
            {
                numberofFiles = aksara_dict.Count;
                TargetOutput = new int[numberofFiles][];
                for (int i = 0; i < numberofFiles; i++)
                {
                    TargetOutput[i] = new int[numberofFiles];
                }
                for (int i = 0; i < numberofFiles; i++)
                {
                    for (int j = 0; j < numberofFiles; j++)
                    {
                        if (j == i)
                            TargetOutput[i][j] = 1;
                        else
                            TargetOutput[i][j] = 0;
                    }
                }
            }
        }
        public void sortinputdata(int numberofattrib=0)
        {
            float[] tempvalue;
            int inc=1;
            do
            {
                inc *= 3;
                inc++;
            } while (inc <= rowcounter);
            do
            {
                inc /= 3;
                for (int i = inc; i < rowcounter; i++)
                {
                    tempvalue = inputdata[i];
                    int j = i;
                    while (inputdata[j - inc][numberofattrib] > tempvalue[numberofattrib])
                    {
                        inputdata[j] = inputdata[j - inc];
                        j -= inc;
                        if (j<inc)
                            break;
                    }
                    inputdata[j] = tempvalue;
                }
            } while (inc > 1);

        }
        public void Ksquaretable(int numberofattrib = 0)
        {
            int changes = 0;
            for (int i = 0; i < rowcounter-1; i++)
            {
                if (inputdata[i][numberofattrib] !=inputdata[i + 1][numberofattrib])
                {
                    changes++;
                }
            }
            changes++;//last value cannot be checked during the loop above     

            Ktable = new float[changes][];
            for (int i = 0; i < Ktable.Count(); i++)
            {
                Ktable[i] = new float[classcounter+1];
            }
            changes=0;
            for (int i = 0; i < rowcounter - 1; i++)//-1 because i+1 below
            {
                Ktable[changes][classcounter] = inputdata[i][numberofattrib];
                if (inputdata[i][numberofattrib] != inputdata[i + 1][numberofattrib])
                {
                    Ktable[changes][(int)inputdata[i][inputdata[i].Count()-1]]++;
                    changes++;
                }
                else
                {
                    Ktable[changes][(int)inputdata[i][inputdata[i].Count()-1]]++;
                }
            }
            Ktable[changes][classcounter] = inputdata[rowcounter - 1][numberofattrib];
            Ktable[changes][(int)inputdata[rowcounter - 1][inputdata[rowcounter - 1].Count() - 1]]++;
            
        }
        public float getKsquaretable(int j)
        {
            return Ktable[j][classcounter];
        }
        public float[][] PInputData
        {
            set
            {
                inputdata = new float[value.Count()][];
                
                for (int i = 0; i < value.Count(); i++)
                {
                    inputdata[i] = new float[value[i].Count()];
                    for (int j = 0; j < value[i].Count(); j++)
                    {
                        inputdata[i][j] = value[i][j];
                    }
                }
            }
            get
            {
                return inputdata;
            }
        }
        public void CalKsquare()
        {
            float[] Kcols, Krows;
            
            Ksquarevalue = new float[Ktable.Count()];
            for (int i = 0; i < Ktable.Count()-1; i++)
            {
                Krows = new float[2];
                Kcols = new float[classcounter];
                for (int j = 0; j < Kcols.Count(); j++)
                {
                    Krows[0] += Ktable[i][j];
                    Krows[1] += Ktable[i+1][j];
                }
                int checkclass =0;
                for (int j = 0; j < Kcols.Count(); j++) // -1 to exclude the int value
                {
                    Kcols[j] += Ktable[i][j];
                    Kcols[j] += Ktable[i+1][j];
                    if (Kcols[j] != 0)
                        checkclass++;
                }
                if (checkclass == 1)//it only belongs to one class set it to minus value, so it can be merged first rather than others
                    Ksquarevalue[i] = (float)-1.0;
                else
                {
                    int total = 0;
                    for (int j = 0; j < Krows.Count(); j++)
                    {
                        total += (int)Krows[j];
                    }
                    for (int j = 0; j < Krows.Count(); j++)
                    {
                        for (int k = 0; k < Kcols.Count(); k++)
                        {
                            if (Krows[j] == 0 || Kcols[k] == 0)
                                Ksquarevalue[i] += (float)0.05;
                            else
                                Ksquarevalue[i] += ((float)Math.Pow(Ktable[i + j][k] - (Krows[j] * Kcols[k]) / total, 2)) / ((Krows[j] * Kcols[k]) / total);
                        }
                    }
                }
            }
        }
        public void printtofile(int numberofattrib)
        {
            TextWriter tw = new StreamWriter("D:\\Tahap Pruning\\"+numberofattrib.ToString());
            for (int i = 0; i < Ktable.Count(); i++)
            {
                tw.Write(Math.Round(Ksquarevalue[i],1) + "\t\t\t\t" );
                for(int j=0;j<Ktable[i].Count();j++)
                {
                    tw.Write(Ktable[i][j] + "\t\t");
                }
                tw.WriteLine();
            }
            tw.Close();
        }
        public int lookfor(float value, int numberofattrib)
        {
            int imin = 0;
            int imax = inputdata.Count()-1;
            int imid = 0;
            do
            {
                imid = (imin + imax)/2;
                float temp = inputdata[imid][numberofattrib];
                if (temp < value)
                    imin = imid + 1;
                else if (temp > value)
                    imax = imid;
                else
                    break;
            } while (imin < imax);

            while (imid > 0)
            {
                if (inputdata[imid][numberofattrib] == inputdata[imid - 1][numberofattrib])
                    imid--;
                else
                    break;
            }
            return imid;
        }
        public void saveInputData()
        {
            tempInputData = new float[inputdata.Count()][];
            for (int i = 0; i < inputdata.Count(); i++)
                tempInputData[i] = new float[inputdata[i].Count()];

            for (int i = 0; i < inputdata.Count(); i++)
            {
                for (int j = 0; j < inputdata[i].Count(); j++)
                    tempInputData[i][j] = inputdata[i][j];
            }
        }
        public void testing(float attr)
        {
            //Console.WriteLine("Attribute : " + attr);
            //for (int i = 0; i < Ktable.Count(); i++)
            //{
            //    for (int j = 0; j < Ktable[i].Count(); j++)
            //    {
            //        Console.Write(Ktable[i][j] + " ");
            //    }
            //    Console.WriteLine();
            //}
            for (int i = 0; i < inputdata.Count(); i++)
            {
                for (int j = 0; j < inputdata[i].Count(); j++)
                {
                    Console.Write(inputdata[i][j] + " ");
                }
                Console.WriteLine();
            }
            //for (int i = 0; i < inputdata.Count(); i++)
            //{
            //    if (inputdata[i][inputdata[i].Count() - 1] == 0)
            //    {
            //        if ((inputdata[i][0] <= 0.5 && inputdata[i][1] <= 0.5) || (inputdata[i][0] >= 0.5 && inputdata[i][1] >= 0.5))
            //        {
            //            Console.Write("there");
            //        }
            //    }
            //}
        }
        public void initializeKsquareStatus()
        {
            ksquareStatus = new bool[numberofAttribs][];
        }
        public void KsquareStatus(int numberofattribs)
        {
            ksquareStatus[numberofattribs] = new bool[Ktable.Count()];
        }
        public void updateKsquareStatus(int numberofattribs,int minIndex)
        {
            for (int i = minIndex; i < Ksquarevalue.Count() - 1; i++)
            {
                ksquareStatus[numberofattribs][i] = ksquareStatus[numberofattribs][i+1];
            }
        }
        public void retrieveInputData()
        {
            for (int i = 0; i < tempInputData.Count(); i++)
            {
                for (int j = 0; j < tempInputData[i].Count(); j++)
                    inputdata[i][j] = tempInputData[i][j];
            }
        }
        public int calInconsistency(int numberofattrib=0)
        {
            int[] classinconsistency;
            classinconsistency = new int[classcounter];
            int totalInconsistency=0;
            bool ExistInconsistency = false;//just for optimization
            bool[] statusIncons = new bool[inputdata.Count()];

            for (int i = 0; i < inputdata.Count(); i++)
            {
                if (statusIncons[i] == false)
                {
                    statusIncons[i] = true;
                    ExistInconsistency = false;//just for optimization
                    classinconsistency = new int[classcounter];
                    classinconsistency[(int)inputdata[i][inputdata[i].Count() - 1]]++;
                    for (int j = i + 1; j < inputdata.Count(); j++)
                    {
                        if (statusIncons[j] == false)
                        {
                            if (inputdata[i][numberofattrib] == inputdata[j][numberofattrib])
                            {
                                if (inputdata[i][inputdata[i].Count() - 1] != inputdata[j][inputdata[j].Count() - 1])
                                {
                                    float distance = 0;
                                    for (int k = 0; k < inputdata[j].Count() - 1; k++)
                                    {
                                        distance += (float)Math.Pow(inputdata[i][k] - inputdata[j][k], 2);
                                    }
                                    if (distance == 0)
                                    {
                                        classinconsistency[(int)inputdata[j][inputdata[j].Count() - 1]]++;
                                        statusIncons[j] = true;
                                        ExistInconsistency = true;
                                    }
                                }
                                else
                                {
                                    float distance = 0;
                                    for (int k = 0; k < inputdata[j].Count() - 1; k++)
                                    {
                                        distance += (float)Math.Pow(inputdata[i][k] - inputdata[j][k], 2);
                                    }
                                    if (distance == 0)
                                    {
                                        classinconsistency[(int)inputdata[j][inputdata[j].Count() - 1]]++;
                                        statusIncons[j] = true;
                                    }
                                }
                            }
                            else
                                break;
                        }
                    }
                    if (ExistInconsistency == true)
                    {
                        int max = classinconsistency[0];
                        for (int k = 1; k < classinconsistency.Count(); k++)
                        {
                            if (max < classinconsistency[k])
                                max = classinconsistency[k];
                        }
                        int sumAllClass = 0;
                        for (int k = 0; k < classinconsistency.Count(); k++)
                        {
                            sumAllClass += classinconsistency[k];
                        }
                        totalInconsistency += (sumAllClass - max);
                    }
                }
            }
            
            return totalInconsistency;
        }
        public void removeMinKsquare(int numberofattrib=0,float threshold=0,bool status=false,int minIndex=0)
        {
            ksquareStatus[numberofattrib][minIndex] = status;
            Ksquarevalue[minIndex] = 1 + threshold;
        }
        public void mergeByIndex(int numberofattrib = 0,int minIndex=0)
        {
            int counter1 = 0, counter2 = 0;//since all inputdata has been sorted we can assume that the are placed side by side
            for (int j = 0; j < Ktable[minIndex].Count() - 1; j++)
                counter1 += (int)Ktable[minIndex][j];
            for (int j = 0; j < Ktable[minIndex + 1].Count() - 1; j++)
                counter2 += (int)Ktable[minIndex + 1][j];

            float value1 = Ktable[minIndex][Ktable[minIndex].Count() - 1];//get the value that has minimum ksquarevalue from Ktable

            int index1 = lookfor(value1, numberofattrib);

            for (int i = index1 + counter1; i < index1 + counter1 + counter2; i++)
                inputdata[i][numberofattrib] = Ktable[minIndex][Ktable[minIndex].Count() - 1];
        }
        public bool mergeKvalue(int numberofattrib = 0,float threshold=0,int phase=0)
        {
         
            float min = Ksquarevalue[0];
            int minIndex = 0;
            for (int i = 1; i < Ksquarevalue.Count()-1; i++)//-1 because the last one doesn't have K value
            {
                if (min > Ksquarevalue[i] && (phase == 1 || phase ==2))//because phase 1 or 2 doesn't have ksquareStatus
                {
                    min = Ksquarevalue[i];
                    minIndex = i;
                }
                else if (min > Ksquarevalue[i] && ksquareStatus[numberofattrib][i] == false && phase == 3)     
                {
                    min = Ksquarevalue[i];
                    minIndex = i;
                }
            }

            if (min > threshold)
                return false;
            bool print=false;
            //if (Math.Round(min, 1) == 1.4)
            //     print = true;

            int counter1 = 0,counter2= 0;//since all inputdata has been sorted we can assume that the are placed side by side
            for (int j = 0; j < Ktable[minIndex].Count() - 1; j++)
                counter1 += (int)Ktable[minIndex][j];
            for (int j = 0; j < Ktable[minIndex + 1].Count() - 1; j++)
                counter2 += (int)Ktable[minIndex + 1][j];

            float value1 = Ktable[minIndex][Ktable[minIndex].Count() - 1];//get the value that has minimum ksquarevalue from Ktable

            int index1 = lookfor(value1, numberofattrib);

            for (int i = index1+counter1; i < index1 + counter1+counter2; i++)
                inputdata[i][numberofattrib] = Ktable[minIndex][Ktable[minIndex].Count() - 1];

            if (print == true)
            {
                Console.WriteLine("here");
                for (int i = index1 + counter1; i < index1 + counter1 + counter2; i++)
                {
                    for (int j = 0; j < inputdata[i].Count(); j++)
                        Console.Write(inputdata[i][j] + " ");
                    Console.WriteLine();
                }

            }
            return true;
        }
        public int numberofAttribs
        {
            get
            {
                return inputdata[0].Count()-1;
            }
        }
        public float getMaxKsquare(int numberofattrib)
        {
            float max = 0;
            for (int i = 0; i < Ksquarevalue.Count() - 1; i++)
            {
                if (max < Ksquarevalue[i] && ksquareStatus[numberofattrib][i]==false)
                    max = Ksquarevalue[i];
            }
            return max;
        }
        public float getKsquareByIndex(int index)
        {
            return Ksquarevalue[index];
        }
        public int getMinIndexKsquare(int numberofattrib)
        {
            float min = Ksquarevalue[0];
            int minIndex = 0;
            for (int i = 1; i < Ksquarevalue.Count() - 1; i++)
            {
                if (min > Ksquarevalue[i] && ksquareStatus[numberofattrib][i] == false)
                {
                    min = Ksquarevalue[i];
                    minIndex = i;
                }
            }
            return minIndex;
        }
        public float[][] PKtableCopy
        {
            set
            {
                Ktable = new float[value.Count()][];
                for (int i = 0; i < value.Count(); i++)
                {
                    Ktable[i] = new float[value[i].Count()];
                    for (int j = 0; j < value[i].Count(); j++)
                        Ktable[i][j] = value[i][j];
                }
            }
            get
            {
                return Ktable;
            }
        }
        public int PKtableCount
        {
            set
            {
            }
            get
            {
                return Ktable.Count();
            }
        }
        public void GenerateNetwork(int layers, int hiddens, float tresholdvalue, float learningrate, int maxiter)
        {
            Random rand = new Random();
            if(layers !=-1 && hiddens!=-1) //it means the user want to generate new network otherwise the user want to use loaded network
                CNetwork.setAttrib(layers, inputcol, hiddens, numberofFiles,rand);

            ThresholdValue = tresholdvalue;
            maxiteration = maxiter;
            LearningRate = learningrate;
        }
        public float getED()
        {
            float EDistance = 0;
            EDistance=CNetwork.getNeuronED();
            return EDistance;
        }
        public int getEpochs()
        {
            return epochcounter;
        }
        public float getSSE()
        {
            return SSE;
        }
        public void resetclasscounter()
        {
            classcounter = 0;
        }
        public bool PAuto
        {
            set
            {
                auto = value;
            }
            get
            {
                return auto;
            }

        }
        public bool PAksara
        {
            set
            {
                p_aksara = value;
            }
            get
            {
                return p_aksara;
            }

        }
        public bool PHoughFeatures
        {
            set
            {
                p_houghfeatures = value;
            }
            get
            {
                return p_houghfeatures;
            }

        }
        public int totalData
        {
            get
            {
                return inputdata.Count();
            }
        }
        public void CopyInput()
        {
            CopyInputdata = new float[inputdata.Count()][];
            for (int i = 0; i < CopyInputdata.Count(); i++)
            {
                CopyInputdata[i] = new float[inputdata[i].Count()];
                for (int j = 0; j < CopyInputdata[i].Count(); j++)
                {
                    CopyInputdata[i][j] = inputdata[i][j];
                }
            }

        }
        public void StartTraining()
        {
            Console.WriteLine("--------------Start Training---------------");
            RandomInputData();//random data position
            int failure = 0;//if SSEfuture>SSE failure++;
            while (finish == false)
            {
                int i;
                for (i = 0; i < inputdata.Count(); i++)
                {
                    CNetwork.FeedForward(inputdata[i]);
                    Layer temp = CNetwork.GetOutputLayer();
                    int tempclass = (int)inputdata[i][inputdata[i].Count() - 1];//untuk menentukan target dari inputdata

                    for (int k = 0; k < temp.Nneuron.Count(); k++)
                    {
                        SSE += (float)0.5 * (float)Math.Pow(TargetOutput[tempclass][k] - temp.Nneuron[k].NeuronValue, 2);
                        CNetwork.UpdateWeights(LearningRate, TargetOutput[tempclass], k);
                    }

                    //CNetwork.FeedForward(inputdata[i]);
                    //temp = CNetwork.GetOutputLayer();
                    //for (int k = 0; k < temp.Nneuron.Count(); k++)
                    //{
                    //    SSEfuture += (float)0.5 * (float)Math.Pow(TargetOutput[tempclass][k] - temp.Nneuron[k].NeuronValue, 2);
                    //}
                }

                //if (SSE >= tempSSE && epochcounter > 0) //don't compare in the first epoch
                //    failure++;

                tempSSE = SSE;

                //formbackprop.SetText(tempSSE.ToString());

                /*if (formbackprop.textBox8.InvokeRequired)
                {
                    formbackprop.SetText(formbackprop.textBox8, SSE.ToString());
                    formbackprop.textBox8.BeginInvoke(new Action(() =>//gak boleh pindah posisi
                    {
                        if (SSE > 0)
                        {
                            formbackprop.textBox8.Text = SSE.ToString();
                        }
                    }));
                }
                else
                {
                    formbackprop.textBox8.Text = SSE.ToString();
                }*/

                for (i = 0; i < CVdata.Count(); i++)
                {
                    CNetwork.FeedForward(CVdata[i]);
                    Layer temp = CNetwork.GetOutputLayer();
                    int tempclass = (int)CVdata[i][CVdata[i].Count() - 1];
                    for (int k = 0; k < temp.Nneuron.Count(); k++)
                    {
                        SSEcv += (float)0.5 * (float)Math.Pow(TargetOutput[tempclass][k] - temp.Nneuron[k].NeuronValue, 2);
                    }
                }

                Console.WriteLine(SSE.ToString());
                //Console.WriteLine("CV :" + SSEcv.ToString());

                epochcounter++;
                tempEpochs = epochcounter;//this is for threading
                if (SSE <= ThresholdValue || failure > 5 || SSEcv >= SSE || (epochcounter >= maxiteration && maxiteration !=0) )
                {
                    finish = true;
                }
                else if (SSE >= ThresholdValue)
                {
                    if (finish == false)
                    {

                        SSE = 0;
                        SSEcv = 0;
                        SSEfuture = 0;
                    }
                }
            }
        }
        public void wizardStartTesting(TextBox result)
        {
            int row = (int)inputdata[0][inputdata[0].Count() - 1];
            for (int i = 0; i < inputdata.Count(); i++)
            {
                CNetwork.FeedForward(inputdata[i]);
                Layer temp = CNetwork.GetOutputLayer();
                float max = 0, maxindex = 0;
                for (int j = 0; j < temp.Nneuron.Count(); j++)
                {
                    if (max <= temp.Nneuron[j].NeuronValue)
                    {
                        max = temp.Nneuron[j].NeuronValue;
                        maxindex = j;
                    }
                }
                if (row != (int)inputdata[i][inputdata[i].Count() - 1])
                {
                    row = (int)inputdata[i][inputdata[i].Count() - 1];
                    result.AppendText("\r\n");
                }
                result.AppendText(" "+_javaUnicode[aksara_dict.FirstOrDefault(x => x.Value == maxindex).Key]);
                
            }
        }
        int WrongAnswer = 0;
        public int StartTesting(DataGridView GridIO, int added_class = 0)
        {

            WrongAnswer = 0;
            SSE = 0;
            GridIO.Columns.Clear();
            GridIO.Columns.Add("number", "# ");
            for (int i = 0; i < inputcol; i++)
                GridIO.Columns.Add("inp" + i.ToString(), "Input " + i.ToString());
            for (int i = 0; i < numberofFiles; i++)
                GridIO.Columns.Add("out" + i.ToString(), "Output " + i.ToString());
            for (int i = 0; i < numberofFiles; i++)
                GridIO.Columns.Add("tar" + i.ToString(), "Target " + i.ToString());

            for (int i = 0; i < inputdata.Count(); i++)
            {
                GridIO.Rows.Add();
                GridIO.Rows[GridIO.Rows.Count - 2].Cells[0].Value = i.ToString();
                for (int n = 1, data = 0; n < inputcol + 1; n++, data++)
                    GridIO.Rows[GridIO.Rows.Count - 2].Cells[n].Value = inputdata[i][data];//to show real sample values before it's normalized

                //if (backproptype == 2)
                //    CNetwork.New_FeedForward(inputdata[i]);
                //else if (backproptype == 1)
                    CNetwork.FeedForward(inputdata[i]);

                Layer temp = CNetwork.GetOutputLayer();

                if(added_class > 0)
                    for (int n = inputcol + 1, data = 0; n < inputcol + (numberofFiles - added_class) + 1; n++, data++)
                        GridIO.Rows[GridIO.Rows.Count - 2].Cells[n].Value = temp.Nneuron[data].NeuronValue;
                else
                    for (int n = inputcol + 1, data = 0; n < inputcol + numberofFiles + 1; n++, data++)
                        GridIO.Rows[GridIO.Rows.Count - 2].Cells[n].Value = temp.Nneuron[data].NeuronValue;

                float max = 0, maxindex = 0;
                for (int j = 0; j < temp.Nneuron.Count(); j++)
                {
                    if (max <= temp.Nneuron[j].NeuronValue)
                    {
                        max = temp.Nneuron[j].NeuronValue;
                        maxindex = j;
                    }
                }
                for (int k = 0; k < temp.Nneuron.Count(); k++)//winner takes all
                {
                    if (k == maxindex)
                        temp.Nneuron[k].NeuronValue = 1;
                    else
                        temp.Nneuron[k].NeuronValue = 0;
                }
                for (int k = 0; k < temp.Nneuron.Count(); k++)
                {
                    if (TargetOutput.Count() > (int)inputdata[i][inputdata[i].Count() - 1])
                    {
                        if ((int)temp.Nneuron[k].NeuronValue != TargetOutput[(int)inputdata[i][inputdata[i].Count() - 1]][k])
                        {
                            GridIO.Rows[GridIO.Rows.Count - 2].DefaultCellStyle.BackColor = Color.Red;
                            WrongAnswer++;
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
                if (added_class > 0 && TargetOutput.Count() > (int)inputdata[i][inputdata[i].Count() - 1])
                    for (int n = inputcol + (numberofFiles - added_class) + 1, data = 0; n < inputcol + (numberofFiles - added_class) + (numberofFiles - added_class) + 1; n++, data++)
                        GridIO.Rows[GridIO.Rows.Count - 2].Cells[n].Value = TargetOutput[(int)inputdata[i][inputdata[i].Count() - 1]][data];
                else
                    for (int n = inputcol + numberofFiles + 1, data = 0; n < inputcol + numberofFiles + numberofFiles + 1; n++, data++)
                        GridIO.Rows[GridIO.Rows.Count - 2].Cells[n].Value = TargetOutput[(int)inputdata[i][inputdata[i].Count() - 1]][data];
            }
            return WrongAnswer;
        }   
        public void stopTraining()
        {
            finish = true;
        }
        public float[][] getInput()
        {
            return inputdata;
        }
        public int getWrongAnswer()
        {
            return WrongAnswer;
        }
        public void RandomInputData()
        {
            Random rand = new Random();
            for (int i = 0; i < inputdata.Count(); i++)
            {
                int dest = rand.Next(inputdata.Count() - 1);
                float[] temp;
                temp = inputdata[i];
                inputdata[i] = inputdata[dest];
                inputdata[dest] = temp;
            }
        }
        public float tempSSE = 0,tempEpochs=0;
        public float PtempSSE
        {
            set
            {
            }
            get
            {
                return tempSSE;
            }
        }
        public float PtempEpochs
        {
            set
            {
            }
            get
            {
                return tempEpochs;
            }
        }    
        public void prunTesting(ref int maxprunindex, ref float maxprunacc,int prunstatus)//prunstatus 0 = just testing, 1 = pruning input, 2 = pruning hidden
        {
            if (prunstatus == 1)
            {
                float[] tempinput = new float[inputdata[0].Count()];
                for (int i = 0; i < inputdata[0].Count() - 1; i++)//-1 to exclude the target
                {
                    if (statusPrun[i] == 1)
                    {
                        WrongAnswer = 0;
                        for (int n = 0; n < inputdata.Count(); n++)
                        {
                            for (int x = 0; x < tempinput.Count(); x++)
                            {
                                tempinput[x] = inputdata[n][x];
                            }
                            tempinput[i] = 0;
                            //if (type == true)//slow
                                CNetwork.FeedForward(tempinput);
                            //else
                            //    CNetwork.New_FeedForward(tempinput);

                            Layer temp = CNetwork.GetOutputLayer();

                            float max = 0, maxindex = 0;
                            for (int j = 0; j < temp.Nneuron.Count(); j++)
                            {
                                if (max <= temp.Nneuron[j].NeuronValue)
                                {
                                    max = temp.Nneuron[j].NeuronValue;
                                    maxindex = j;
                                }
                            }
                            for (int k = 0; k < temp.Nneuron.Count(); k++)//winner takes all
                            {
                                if (k == maxindex)
                                    temp.Nneuron[k].NeuronValue = 1;
                                else
                                    temp.Nneuron[k].NeuronValue = 0;
                            }
                            for (int k = 0; k < temp.Nneuron.Count(); k++)
                            {
                                if ((int)temp.Nneuron[k].NeuronValue != TargetOutput[(int)inputdata[n][inputdata[n].Count() - 1]][k])
                                {
                                    WrongAnswer++;
                                    break;
                                }
                            }
                        }
                        float compacc = (float)(inputdata.Count() - WrongAnswer) / inputdata.Count() * 100;
                        if (compacc > maxprunacc)
                        {
                            maxprunacc = compacc;
                            maxprunindex = i;
                        }
                    }
                }
            }
            else if (prunstatus == 2)
            {
                for (int i = 0; i < CNetwork.Nlayer[1].Nneuron.Count(); i++)
                {
                    if (statusPrun[i] == 1)
                    {
                        Random rand = new Random();
                        Layer Output = new Layer(CNetwork.Nlayer[2].Nneuron.Count(), CNetwork.Nlayer[1].Nneuron.Count(), rand);
                        for (int j = 0; j < CNetwork.Nlayer[2].Nneuron.Count(); j++)
                        {
                            Output.Nneuron[j].weights[i] = CNetwork.Nlayer[2].Nneuron[j].weights[i];
                            CNetwork.Nlayer[2].Nneuron[j].weights[i] = 0;
                        }
                        WrongAnswer = 0;
                        for (int n = 0; n < inputdata.Count(); n++)
                        {
                            //if (type == true)//slow
                                CNetwork.FeedForward(inputdata[n]);
                            //else
                            //    CNetwork.New_FeedForward(inputdata[n]);

                            Layer temp = CNetwork.GetOutputLayer();

                            float max = 0, maxindex = 0;
                            for (int j = 0; j < temp.Nneuron.Count(); j++)
                            {
                                if (max <= temp.Nneuron[j].NeuronValue)
                                {
                                    max = temp.Nneuron[j].NeuronValue;
                                    maxindex = j;
                                }
                            }
                            for (int k = 0; k < temp.Nneuron.Count(); k++)//winner takes all
                            {
                                if (k == maxindex)
                                    temp.Nneuron[k].NeuronValue = 1;
                                else
                                    temp.Nneuron[k].NeuronValue = 0;
                            }
                            for (int k = 0; k < temp.Nneuron.Count(); k++)
                            {
                                if ((int)temp.Nneuron[k].NeuronValue != TargetOutput[(int)inputdata[n][inputdata[n].Count() - 1]][k])
                                {
                                    WrongAnswer++;
                                    break;
                                }
                            }
                        }
                        float compacc = (float)(inputdata.Count() - WrongAnswer) / inputdata.Count() * 100;
                        if (compacc > maxprunacc)
                        {
                            maxprunacc = compacc;
                            maxprunindex = i;

                        }
                        for (int j = 0; j < CNetwork.Nlayer[2].Nneuron.Count(); j++)
                        {
                            CNetwork.Nlayer[2].Nneuron[j].weights[i] = Output.Nneuron[j].weights[i];
                        }
                    }
                }
            }
            else if (prunstatus == 3)
            {
                float min = 0;
                int index = 0;
                for (int i = 1; i < CNetwork.Nlayer.Count(); i++)
                {
                    for (int j = 0; j < CNetwork.Nlayer[i].Nneuron.Count(); j++)
                    {
                        for (int k = 0; k < CNetwork.Nlayer[i].Nneuron[j].weights.Count(); k++)
                        {
                            if (CNetwork.Nlayer[i].Nneuron[j].weights[k] != 0 && statusPrun[index]==1)
                            {
                                if (min == 0 || min > Math.Abs(CNetwork.Nlayer[i].Nneuron[j].weights[k]))
                                {
                                    min = Math.Abs(CNetwork.Nlayer[i].Nneuron[j].weights[k]);
                                    maxprunindex = index;
                                    
                                }
                            }
                            index++;
                        }
                    }
                }
            }
            else if (prunstatus == 0)
            {
                WrongAnswer = 0;
                for (int n = 0; n < inputdata.Count(); n++)
                {
                    //if (type == true)//slow
                        CNetwork.FeedForward(inputdata[n]);
                    //else
                    //    CNetwork.New_FeedForward(inputdata[n]);

                    Layer temp = CNetwork.GetOutputLayer();

                    float max = 0, maxindex = 0;
                    for (int j = 0; j < temp.Nneuron.Count(); j++)
                    {
                        if (max <= temp.Nneuron[j].NeuronValue)
                        {
                            max = temp.Nneuron[j].NeuronValue;
                            maxindex = j;
                        }
                    }
                    for (int k = 0; k < temp.Nneuron.Count(); k++)//winner takes all
                    {
                        if (k == maxindex)
                            temp.Nneuron[k].NeuronValue = 1;
                        else
                            temp.Nneuron[k].NeuronValue = 0;
                    }
                    for (int k = 0; k < temp.Nneuron.Count(); k++)
                    {
                        if ((int)temp.Nneuron[k].NeuronValue != TargetOutput[(int)inputdata[n][inputdata[n].Count() - 1]][k])
                        {
                            WrongAnswer++;
                            break;
                        }
                    }
                }
                maxprunacc = (float)(inputdata.Count() - WrongAnswer) / inputdata.Count() * 100;
            }
        }
        public int countAvailWeight()
        {
            int weights=0;
            for (int l = 1; l < CNetwork.Nlayer.Count(); l++)
            {
                for (int i = 0; i < CNetwork.Nlayer[l].Nneuron.Count(); i++)
                {
                    for (int j = 0; j < CNetwork.Nlayer[l].Nneuron[i].weights.Count(); j++)
                    {
                        if (CNetwork.Nlayer[l].Nneuron[i].weights[j] != 0)
                        {
                            weights++;
                        }
                    }
                }
            }
            return weights;
            
        }
        public int countAvailInput()
        {
            int inputs = 0;
            for (int i = 0; i < CNetwork.Nlayer[1].Nneuron[0].weights.Count(); i++)
            {
                for (int j = 0; j < CNetwork.Nlayer[1].Nneuron.Count(); j++)
                {
                    if (CNetwork.Nlayer[1].Nneuron[j].weights[i] != 0)
                    {
                        inputs++;
                        break;
                    }
                }
            }
            return inputs;
        }
        public int countAvailHidden()
        {
            int hiddens = 0;
            for (int i = 0; i < CNetwork.Nlayer[2].Nneuron[0].weights.Count(); i++)
            {
                for (int j = 0; j < CNetwork.Nlayer[2].Nneuron.Count(); j++)
                {
                    if (CNetwork.Nlayer[2].Nneuron[j].weights[i] != 0)
                    {
                        hiddens++;
                        break;
                    }
                }
            }
            return hiddens;
        }
        int totalprunInput = 0, totalprunHidden = 0, totalprunWeight = 0;
        public int PtotalprunInput
        {
            get
            {
                return totalprunInput;
            }
        }
        public int PtotalprunHidden
        {
            get
            {
                return totalprunHidden;
            }
        }
        public int PtotalprunWeight
        {
            get
            {
                return totalprunWeight;
            }
        }
        public float[][][] CreateWeightList()
        {
            float[][][] Weight;
            Weight = new float[2][][];
            for (int i = 1; i < CNetwork.Nlayer.Count(); i++)
            {
                Weight[i] = new float[CNetwork.Nlayer[i].Nneuron.Count()][];
                for (int j = 0; j < CNetwork.Nlayer[i].Nneuron.Count(); j++)
                {
                    Weight[i][j] = new float[CNetwork.Nlayer[i].Nneuron[j].weights.Count()];
                }
            }
            for (int i = 1; i < CNetwork.Nlayer.Count(); i++)
            {
                for (int j = 0; j < CNetwork.Nlayer[i].Nneuron.Count(); j++)
                {
                    for (int k = 0; k < CNetwork.Nlayer[i].Nneuron.Count(); k++)
                    {
                        Weight[i][j][k] = CNetwork.Nlayer[i].Nneuron[j].weights[k];
                    }
                }
            }
            return Weight;
        }
        public void prunInput(object data)
        {
            float errTolerance = (float)data;
            bool canbeprun = true;

            totalprunInput = 0;//for textbox15
            totalprunHidden = 0;//for textbox14
            totalprunWeight = 0;//for textbox16

            statusPrun = new float[inputdata[0].Count()-1];
            for (int i = 0; i < statusPrun.Count(); i++)
            {
                bool alreadypruned = true;
                for (int j = 0; j < CNetwork.Nlayer[1].Nneuron.Count(); j++)
                {
                    if (CNetwork.Nlayer[1].Nneuron[j].weights[i] != 0)
                    {
                        alreadypruned = false;
                        break;
                    }
                }
                if(alreadypruned == false)
                    statusPrun[i] = 1;
                else
                    statusPrun[i] = 0;
            }
            Network saveCurrent = new Network();
            Random rand = new Random();
            saveCurrent.setAttrib(CNetwork.Nlayer.Count()-2, CNetwork.Nlayer[0].Nneuron.Count(), CNetwork.Nlayer[1].Nneuron.Count(), CNetwork.Nlayer[2].Nneuron.Count(), rand);//-2 look setattrib method
            while (canbeprun)
            {
                for (int i = 1; i < saveCurrent.Nlayer.Count(); i++)
                {
                    for (int j = 0; j < saveCurrent.Nlayer[i].Nneuron.Count(); j++)
                    {
                        for (int k = 0; k < saveCurrent.Nlayer[i].Nneuron[j].weights.Count(); k++)
                        {
                            saveCurrent.Nlayer[i].Nneuron[j].weights[k] = CNetwork.Nlayer[i].Nneuron[j].weights[k];
                        }
                        saveCurrent.Nlayer[i].Nneuron[j].biasValue = CNetwork.Nlayer[i].Nneuron[j].biasValue;
                    }
                }
                float maxprunacc = 0;
                int maxprunindex = -1;
                prunTesting(ref maxprunindex,ref maxprunacc, 1);
                statusPrun[maxprunindex] = 0;
                CNetwork.zeroWeight(0, maxprunindex);
                StartTraining();
                prunTesting(ref maxprunindex, ref maxprunacc, 0);
                if (maxprunacc <= trainAcc - errTolerance)
                {
                    canbeprun = false;
                    for (int i = 1; i < saveCurrent.Nlayer.Count(); i++)
                    {
                        for (int j = 0; j < saveCurrent.Nlayer[i].Nneuron.Count(); j++)
                        {
                            for (int k = 0; k < saveCurrent.Nlayer[i].Nneuron[j].weights.Count(); k++)
                            {
                                CNetwork.Nlayer[i].Nneuron[j].weights[k] = saveCurrent.Nlayer[i].Nneuron[j].weights[k];
                            }
                            CNetwork.Nlayer[i].Nneuron[j].biasValue = saveCurrent.Nlayer[i].Nneuron[j].biasValue;
                        }
                    }
                }
                else
                {
                    totalprunInput++;

                    resetValue();
                }
            }
            if (finish == false)
                prunHidden(errTolerance);//lanjut prun hidden unit

            //prunWeight(errTolerance);
        }
        public void prunHidden(object data)
        {
            float errTolerance = (float)data;
            bool canbeprun = true;
            totalprunHidden = 0;
            statusPrun = new float[CNetwork.Nlayer[1].Nneuron.Count()];
            for (int i = 0; i < statusPrun.Count(); i++)
            {
                bool alreadypruned = true;
                for (int j = 0; j < CNetwork.Nlayer[2].Nneuron.Count(); j++)
                {
                    if (CNetwork.Nlayer[2].Nneuron[j].weights[i] != 0)
                    {
                        alreadypruned = false;
                        break;
                    }
                }
                if(alreadypruned == false)
                    statusPrun[i] = 1;
                else
                    statusPrun[i] = 0;
            }
            Network saveCurrent = new Network();
            Random rand = new Random();
            saveCurrent.setAttrib(CNetwork.Nlayer.Count() - 2, CNetwork.Nlayer[0].Nneuron.Count(), CNetwork.Nlayer[1].Nneuron.Count(), CNetwork.Nlayer[2].Nneuron.Count(), rand);//-2 look setattrib method
            while (canbeprun)
            {
                for (int i = 1; i < saveCurrent.Nlayer.Count(); i++)
                {
                    for (int j = 0; j < saveCurrent.Nlayer[i].Nneuron.Count(); j++)
                    {
                        for (int k = 0; k < saveCurrent.Nlayer[i].Nneuron[j].weights.Count(); k++)
                        {
                            saveCurrent.Nlayer[i].Nneuron[j].weights[k] = CNetwork.Nlayer[i].Nneuron[j].weights[k];
                        }
                        saveCurrent.Nlayer[i].Nneuron[j].biasValue = CNetwork.Nlayer[i].Nneuron[j].biasValue;
                    }
                }
                float maxprunacc = 0;
                int maxprunindex = -1;
                prunTesting(ref maxprunindex, ref maxprunacc, 2);
                statusPrun[maxprunindex] = 0;
                CNetwork.zeroWeight(1, maxprunindex);
                StartTraining();
                prunTesting(ref maxprunindex, ref maxprunacc, 0);
                if (maxprunacc <= trainAcc - errTolerance)
                {
                    canbeprun = false;
                    for (int i = 1; i < saveCurrent.Nlayer.Count(); i++)
                    {
                        for (int j = 0; j < saveCurrent.Nlayer[i].Nneuron.Count(); j++)
                        {
                            for (int k = 0; k < saveCurrent.Nlayer[i].Nneuron[j].weights.Count(); k++)
                            {
                                CNetwork.Nlayer[i].Nneuron[j].weights[k] = saveCurrent.Nlayer[i].Nneuron[j].weights[k];
                            }
                            CNetwork.Nlayer[i].Nneuron[j].biasValue = saveCurrent.Nlayer[i].Nneuron[j].biasValue;
                        }
                    }
                }
                else
                {
                    totalprunHidden++;
                    resetValue();
                }
            }
            if (finish == false)
                prunWeight(errTolerance);
            //prunInput(errTolerance);
        }
        public void prunWeight(object data)
        {
            float errTolerance = (float)data;
            bool canbeprun = true;
            totalprunWeight = 0;
            statusPrun = new float[CNetwork.Nlayer[0].Nneuron.Count()*CNetwork.Nlayer[1].Nneuron.Count()+CNetwork.Nlayer[1].Nneuron.Count()*CNetwork.Nlayer[2].Nneuron.Count()];
            for (int i = 0; i < statusPrun.Count(); i++)
            {
                statusPrun[i] = 1;
            }
            Network saveCurrent = new Network();
            Random rand = new Random();
            saveCurrent.setAttrib(CNetwork.Nlayer.Count() - 2, CNetwork.Nlayer[0].Nneuron.Count(), CNetwork.Nlayer[1].Nneuron.Count(), CNetwork.Nlayer[2].Nneuron.Count(), rand);//-2 look setattrib method
            while (canbeprun)
            {
                for (int i = 1; i < saveCurrent.Nlayer.Count(); i++)
                {
                    for (int j = 0; j < saveCurrent.Nlayer[i].Nneuron.Count(); j++)
                    {
                        for (int k = 0; k < saveCurrent.Nlayer[i].Nneuron[j].weights.Count(); k++)
                        {
                            saveCurrent.Nlayer[i].Nneuron[j].weights[k] = CNetwork.Nlayer[i].Nneuron[j].weights[k];
                        }
                        saveCurrent.Nlayer[i].Nneuron[j].biasValue = CNetwork.Nlayer[i].Nneuron[j].biasValue;
                    }
                }
                float maxprunacc = 0;
                int maxprunindex = -1;
                prunTesting(ref maxprunindex, ref maxprunacc, 3);
                //CreateWeightList();
                statusPrun[maxprunindex] = 0;
                CNetwork.zeroWeight(2, maxprunindex);
                StartTraining();
                prunTesting(ref maxprunindex, ref maxprunacc, 0);
                if (maxprunacc <= trainAcc - errTolerance)
                {
                    canbeprun = false;
                    for (int i = 1; i < saveCurrent.Nlayer.Count(); i++)
                    {
                        for (int j = 0; j < saveCurrent.Nlayer[i].Nneuron.Count(); j++)
                        {
                            for (int k = 0; k < saveCurrent.Nlayer[i].Nneuron[j].weights.Count(); k++)
                            {
                                CNetwork.Nlayer[i].Nneuron[j].weights[k] = saveCurrent.Nlayer[i].Nneuron[j].weights[k];
                            }
                            CNetwork.Nlayer[i].Nneuron[j].biasValue = saveCurrent.Nlayer[i].Nneuron[j].biasValue;
                        }
                    }
                }
                else
                {
                    totalprunWeight++;
                    resetValue();
                }
            }
        }
        public void New_StartTraining()
        {
            float f = 0, EDg = 0, SSE = 0;
            float[] error,g,wv;
            error = new float[CNetwork.Nlayer[CNetwork.Nlayer.Count() - 1].Nneuron.Count()];
            g = new float[CNetwork.Nlayer[0].Nneuron.Count() * CNetwork.Nlayer[1].Nneuron.Count() + CNetwork.Nlayer[2].Nneuron.Count() * CNetwork.Nlayer[1].Nneuron.Count()];
            wv = new float[CNetwork.Nlayer[0].Nneuron.Count() * CNetwork.Nlayer[1].Nneuron.Count() + CNetwork.Nlayer[2].Nneuron.Count() * CNetwork.Nlayer[1].Nneuron.Count()];
            CNetwork.copyWeight(ref wv);
            Console.WriteLine("--------------Start Training---------------");
            do
            {
                f = 0;
                SSE = 0;
                for (int i = 0; i < g.Count(); i++)
                {
                    g[i] = 0;
                }
                for (int i = 0; i < inputdata.Count(); i++)
                {
                    CNetwork.New_FeedForward(inputdata[i]);
                    Layer tempOutput = CNetwork.GetOutputLayer();
                    int tempclass = (int)inputdata[i][inputdata[i].Count() - 1];//untuk menentukan target dari inputdata
                    for (int j = 0; j < tempOutput.Nneuron.Count(); j++)
                    {
                        error[j] = tempOutput.Nneuron[j].NeuronValue - TargetOutput[tempclass][j];
                        SSE+= (float)0.5 * (float)Math.Pow(TargetOutput[tempclass][j] - tempOutput.Nneuron[j].NeuronValue, 2);
                        if (Math.Abs(error[j]) > (1.0 - 1.0e-16))
                            f += (float)40.0;
                        else if (TargetOutput[tempclass][j] == 0)
                            f -= (float)Math.Log(1 - tempOutput.Nneuron[j].NeuronValue);
                        else
                            f -= (float)Math.Log(tempOutput.Nneuron[j].NeuronValue);
                    }
                    for (int j = 0; j < CNetwork.Nlayer[1].Nneuron.Count(); j++)
                    {
                        g = CNetwork.calGradient(j, g, error);
                    }
                }
                tempSSE = SSE;
                CNetwork.New_updateWeight(LearningRate, g);
                EDg = CNetwork.New_gradientDecent(g);
                Console.WriteLine("F value : " + f.ToString() + " ED value : " + EDg.ToString());
                epochcounter++;
                tempEpochs = epochcounter;
            } while (f >= ThresholdValue && epochcounter < maxiteration && EDg >= Epsilon && finish == false);
        }
        public void NewNew_StartTraining()
        {
            float f = 0, EDg = 0, SSE = 0;
            float[] error, g, wv;
            error = new float[CNetwork.Nlayer[CNetwork.Nlayer.Count() - 1].Nneuron.Count()];
            g = new float[CNetwork.Nlayer[0].Nneuron.Count() * CNetwork.Nlayer[1].Nneuron.Count() + CNetwork.Nlayer[2].Nneuron.Count() * CNetwork.Nlayer[1].Nneuron.Count()];
            wv = new float[CNetwork.Nlayer[0].Nneuron.Count() * CNetwork.Nlayer[1].Nneuron.Count() + CNetwork.Nlayer[2].Nneuron.Count() * CNetwork.Nlayer[1].Nneuron.Count()];
            int io, ih, inp;
            io = CNetwork.Nlayer[2].Nneuron.Count();
            ih = CNetwork.Nlayer[1].Nneuron.Count();
            inp = CNetwork.Nlayer[0].Nneuron.Count();
            
            int inih = (inp * ih);
            float[] sum, tan;
            float sxw,xw,tanh;
            sum = new float[CNetwork.Nlayer[2].Nneuron.Count()];
            tan = new float[CNetwork.Nlayer[1].Nneuron.Count()];

            for (int i = 0; i < wv.Count(); i++)
            {
                wv[i] = (float)(i / 10.0);
                if (i % 2 == 1)
                    wv[i] *= -1;
            }

            Console.WriteLine("--------------Start Training---------------");
            do
            {
                f = 0; SSE = 0;
                for (int i = 0; i < g.Count(); i++)
                {
                    g[i] = 0;
                }
                for (int i = 0; i < inputdata.Count(); i++)
                {
                    for (int j = 0; j < io; j++)
                    {
                        sum[j] = 0;
                    }
                    for (int j = 0; j < ih; j++)
                    {
                        xw = 0;
                        int k1 = j * inp;
                        for (int k = 0; k < inp; k++)
                        {
                            xw = xw + wv[k1 + k] * inputdata[i][k];
                        }
                        xw = 2 * xw;
                        
                        if (Math.Abs(xw) < 1e-16)
                            sxw = (float)0.5;
                        else if (xw > 100)
                            sxw = 1;
                        else if (xw < -100)
                            sxw = 0;
                        else
                            sxw = (float)(1.0 / (1.0 + Math.Exp(-1 * xw)));
                        tanh = 2*sxw-1;
                        tan[j] = tanh;
                        int index = inih + j * io;
                        for (int kk = 0; kk < io; kk++)
                        {
                            float sxwv = tanh * wv[index + kk];
                            sum[kk] = sum[kk] + sxwv;
                        }
                    }
                    for (int kk = 0; kk < io; kk++)
                    {
                        sxw = sum[kk];
                        if (Math.Abs(sxw) < 1e-16)
                            sum[kk] = (float)0.5;
                        else if (sxw > 100)
                            sum[kk] = 1;
                        else if (sxw < -100)
                            sum[kk] = 0;
                        else
                            sum[kk] = (float)(1.0 / (1.0 + Math.Exp(-1 * sxw)));

                        int tempclass = (int)inputdata[i][inputdata[i].Count()-1];
                        SSE += (float)0.5 * (float)Math.Pow(TargetOutput[tempclass][kk] - sum[kk], 2);
                        
                        error[kk] = sum[kk] - TargetOutput[tempclass][kk];
                        if (Math.Abs(error[kk]) > (1 - 1e-16))
                            f += 40;
                        else if (TargetOutput[tempclass][kk] == 0)
                            f -=(float) Math.Log(1 - sum[kk]);
                        else
                            f -=(float) Math.Log(sum[kk]);
                        
                    }
                    for (int k = 0; k < ih; k++)
                    {
                        int index = inih + k *io;
                        tanh = tan[k];
                        for (int jj = 0; jj<io; jj++)
                        {
                            int iloc0 = index + jj;
                            float deriv1 = error[jj] * (float)(1 - Math.Pow(tanh, 2))*wv[iloc0];
                            for (int kk = 0; kk < inp; kk++)
                            {
                                int iloc = k * inp + kk;
                                g[iloc] += deriv1 * inputdata[i][kk];
                            }
                            g[iloc0] += tanh * error[jj];
                        }
                    }
                }
                tempSSE = SSE;
                for (int i = 0; i < wv.Count(); i++)
                {
                    wv[i] -= LearningRate * g[i];   
                }
                EDg = CNetwork.New_gradientDecent(g);
                Console.WriteLine("F value : " + f.ToString() + " ED value : " + EDg.ToString());
                epochcounter++;
                tempEpochs = epochcounter;
            } while (f >= ThresholdValue && epochcounter < maxiteration && EDg >= Epsilon && finish == false);
            CNetwork.pasteWeight(wv);
        }
        public void saveNetworks(string filename,bool discrete,string Nval, bool type, bool aksara, bool houghfeatures)
        {
            CNetwork.saveNetworks(filename, discrete, Nval, auto, aksara, houghfeatures, trainAcc, Ktable);
            using (StreamWriter sw = File.AppendText(filename))
            {
                foreach (var item in aksara_dict)
                {
                    sw.WriteLine(item.Key);
                }
            }
        }
        public bool Pdiscrete
        {
            set
            {
                discrete = value;
            }
            get
            {
                return discrete;
            }
        }
        public int PNvalue
        {
            set
            {
                Nvalue = value;
            }
            get
            {
                return Nvalue;
            }
        }
        public float PtrainAcc
        {
            set
            {
                trainAcc = value;
            }
            get
            {
                return trainAcc;
            }
        }
        public void LoadNetworks(string filename)
        {
            TextReader tr = new StreamReader(filename);
            int layers,inputs,hiddens,outputs;
            
            string line;
            string[] mstats;
            char[] delimeterChars = { ' ' };
            try
            {
                line = tr.ReadLine();
                mstats = line.Split(delimeterChars);
                layers = int.Parse(mstats[0]);
                inputs = int.Parse(mstats[1]);
                hiddens = int.Parse(mstats[2]);
                int hidden_counter = layers;
                outputs = int.Parse(mstats[hidden_counter]);
                hidden_counter++;
                discrete = bool.Parse(mstats[hidden_counter]);
                hidden_counter++;
                Nvalue = int.Parse(mstats[hidden_counter]);
                hidden_counter++;
                auto = bool.Parse(mstats[hidden_counter]);
                hidden_counter++;
                p_aksara = bool.Parse(mstats[hidden_counter]);
                hidden_counter++;
                p_houghfeatures = bool.Parse(mstats[hidden_counter]);
                hidden_counter++;
                trainAcc = float.Parse(mstats[hidden_counter]);
                Random rand = new Random();
                CNetwork.setAttrib(layers-2, inputs, hiddens, outputs,rand);   
                for (int i = 1; i < layers - 1; i++)//start from 1st hidden layer until 1 before output layer
                {        
                    for (int j = 0; j < hiddens; j++)
                    {
                        if ((line = tr.ReadLine()) != null)
                        {
                            mstats = line.Split(delimeterChars);
                            int numofWeight = int.Parse(mstats[0]);

                            if ((line = tr.ReadLine()) != null)
                                mstats = line.Split(delimeterChars);
                            for (int k = 0; k < numofWeight+1; k++)//mstats[0] is number weight for each neuron,+1 for the bias
                            {
                                if (mstats[k] != "")
                                {
                                    if(k<numofWeight-1)
                                        CNetwork.Nlayer[i].Nneuron[j].weights[k] = float.Parse(mstats[k]);
                                    else
                                        CNetwork.Nlayer[i].Nneuron[j].biasValue = float.Parse(mstats[k]);
                                }
                            }
                        }
                    }
                }

                for (int j = 0; j < outputs; j++)//weight for hidden layer
                {
                    if ((line = tr.ReadLine()) != null)
                    {
                        mstats = line.Split(delimeterChars);
                        int numofWeight = int.Parse(mstats[0]);

                        if ((line = tr.ReadLine()) != null)
                            mstats = line.Split(delimeterChars);
                        for (int k = 0; k < numofWeight+1; k++)//mstats[0] is number weight for each neuron,+1 for the bias
                        {
                            if (mstats[k] != "")
                            {
                                if (k < numofWeight - 1)
                                    CNetwork.Nlayer[layers-1].Nneuron[j].weights[k] = float.Parse(mstats[k]);
                                else
                                    CNetwork.Nlayer[layers-1].Nneuron[j].biasValue = float.Parse(mstats[k]);
                            }
                        }
                    }
                }
                if (auto == true)
                {
                    line = tr.ReadLine();
                    mstats = line.Split(delimeterChars);
                    Ktable = new float[int.Parse(mstats[0])][];
                    for (int i = 0; i < Ktable.Count(); i++)
                    {
                        line = tr.ReadLine();//to read the number of columns
                        mstats = line.Split(delimeterChars);
                        Ktable[i] = new float[int.Parse(mstats[0])];

                        line = tr.ReadLine();//to read the data
                        mstats = line.Split(delimeterChars);
                        for (int j = 0; j < Ktable[i].Count(); j++)
                        {
                            Ktable[i][j] = float.Parse(mstats[j]);
                        }
                    }
                }
                if(p_aksara == true){
                    aksara_dict = new Dictionary<string,int>();
                    int counter = 0;
                    while ((line = tr.ReadLine()) != null)
                    {
                        aksara_dict.Add(line, counter);
                        counter++;
                    }
                }
                tr.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("You Load a Wrong File !\n" + err.ToString());
            }
            

            
            

        }
    }
}
