﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AksaraJawa2015
{
    public class Network
    {
        public Layer[] Nlayer;
       
        public Network()
        {
           
        }
        public void setAttrib(int layers,int inputs, int hiddens,int outputs,Random rand)
        {
            layers += 2;//+2 for input and output layer, layers just for number of hidden layer
            Nlayer = new Layer[layers];

            Nlayer[0] = new Layer(inputs,0,rand);
            for (int i = 1; i < layers-1; i++)
            {
                if(i==1)
                    Nlayer[i] = new Layer(hiddens, inputs, rand);
                else
                    Nlayer[i] = new Layer(hiddens, hiddens, rand);
            }
            Nlayer[layers - 1] = new Layer(outputs, hiddens, rand);
        }
        public void FeedForward(float[] inputdata)
        {
            Nlayer[0].setInputNeuron=inputdata;
            for (int i = 1; i < Nlayer.Count(); i++)
            {
                Nlayer[i].setNeuronValue(Nlayer[i-1]);
            }
        }
        public void New_FeedForward(float[] inputdata)
        {
            Nlayer[0].setInputNeuron = inputdata;
            for (int i = 1; i < Nlayer.Count(); i++)
            {
                Nlayer[i].New_setNeuronValue(Nlayer[i - 1],i,Nlayer.Count()-1);
            }
        }
        public Layer GetOutputLayer()
        {
            return Nlayer[Nlayer.Count() - 1];
        }
        public float getNeuronED()
        {
            float EDvalue = 0;
            for (int i = 1; i < Nlayer.Count(); i++)//start from first hidden layer
            {
                for (int j = 0; j < Nlayer[i].Nneuron.Count(); j++)
                {
                    EDvalue += (float)Math.Pow(Nlayer[i].Nneuron[j].biasValue,2);
                    for (int k = 0; k < Nlayer[i].Nneuron[j].weights.Count(); k++)
                    {
                        EDvalue += (float)Math.Pow(Nlayer[i].Nneuron[j].WeightsValue[k],2);
                    }
                }
            }
            EDvalue = (float)Math.Sqrt(EDvalue);
            return EDvalue;
        }
        public void CalculateErrors()
        {
            Layer[] duplicate_Nlayer = new Layer[Nlayer.Count()];
        }
        public void UpdateWeights(float LearningRate, int[] TargetOutput, int k)//k = the output neuron that need to be updated
        {
            Layer duplicateLayer=null;
            Random rand = new Random();
            int i = 0;
            for (i = Nlayer.Count() - 1; i > 0; i--)
            {
                if (i == Nlayer.Count() - 1)
                {
                    duplicateLayer = new Layer(Nlayer[i].Nneuron.Count(), Nlayer[i].Nneuron[k].weights.Count(), rand);
                    for(int n=0;n<Nlayer[i].Nneuron.Count();n++)
                    {
                        Nlayer[i].Nneuron[n].ErrorGradientValue = 0;
                        Nlayer[i].Nneuron[n].ErrorGradientValue = Nlayer[i].Nneuron[n].NeuronValue * (1 - Nlayer[i].Nneuron[n].NeuronValue) * (TargetOutput[n] - Nlayer[i].Nneuron[n].NeuronValue);    
                    }
                    for (int j = 0; j < Nlayer[i].Nneuron[k].weights.Count(); j++)
                    {
                        duplicateLayer.Nneuron[k].weights[j] = LearningRate * Nlayer[i - 1].Nneuron[j].NeuronValue * Nlayer[i].Nneuron[k].ErrorGradientValue + (float)0.000001 * Nlayer[i].Nneuron[k].weights[j]; 
                    }
                    float deltaBias = 0;
                    deltaBias = LearningRate * -1 * Nlayer[i].Nneuron[k].ErrorGradientValue;
                    Nlayer[i].Nneuron[k].biasValue += deltaBias;
                }
                else
                {
                    for(int j=0;j<Nlayer[i].Nneuron.Count();j++)
                    {
                        Nlayer[i].Nneuron[j].ErrorGradientValue = 0;
                        for (int n = 0; n < Nlayer[i + 1].Nneuron.Count(); n++)
                        {
                            Nlayer[i].Nneuron[j].ErrorGradientValue += Nlayer[i + 1].Nneuron[n].ErrorGradientValue * Nlayer[i + 1].Nneuron[n].weights[j];
                        }
                        Nlayer[i].Nneuron[j].ErrorGradientValue *= Nlayer[i].Nneuron[j].NeuronValue * (1 - Nlayer[i].Nneuron[j].NeuronValue);    
                    }
                    if (i == Nlayer.Count() - 2)//update weight from particular output neuron
                    {
                        for (int n = 0; n < Nlayer[i + 1].Nneuron[k].weights.Count(); n++)
                        {
                            if(Nlayer[i + 1].Nneuron[k].weights[n] !=0)//efek pruning
                                Nlayer[i + 1].Nneuron[k].weights[n] += duplicateLayer.Nneuron[k].weights[n];
                        }
                    }
                    else
                    {
                        for (int j = 0; j < Nlayer[i + 1].Nneuron.Count(); j++)
                        {
                            for (int n = 0; n < Nlayer[i + 1].Nneuron[j].weights.Count(); n++)
                            {
                                if (Nlayer[i + 1].Nneuron[j].weights[n] != 0)//efek pruning
                                    Nlayer[i + 1].Nneuron[j].weights[n] += duplicateLayer.Nneuron[j].weights[n];
                            }
                        }
                    }
                    duplicateLayer = new Layer(Nlayer[i].Nneuron.Count(), Nlayer[i].Nneuron[0].weights.Count(), rand);
                    for(int j=0;j<Nlayer[i].Nneuron.Count();j++)
                    {
                        for(int n=0;n<Nlayer[i].Nneuron[j].weights.Count();n++)
                        {
                            duplicateLayer.Nneuron[j].weights[n] = LearningRate * Nlayer[i - 1].Nneuron[n].NeuronValue * Nlayer[i].Nneuron[j].ErrorGradientValue + (float)0.000001 * Nlayer[i].Nneuron[j].weights[n];
                        }
                        float deltaBias = 0;
                        
                        deltaBias = LearningRate * -1 * Nlayer[i].Nneuron[j].ErrorGradientValue;
                        Nlayer[i].Nneuron[j].biasValue += deltaBias;
                    }   
                }
            }
            for (int j = 0; j < Nlayer[i + 1].Nneuron.Count(); j++)
            {
                for (int n = 0; n < Nlayer[i + 1].Nneuron[j].weights.Count(); n++)
                {
                    if (Nlayer[i + 1].Nneuron[j].weights[n] != 0)//efek pruning
                        Nlayer[i + 1].Nneuron[j].weights[n] += duplicateLayer.Nneuron[j].weights[n];
                }
            }
        }
        public void copyWeight(ref float[] wv)
        {
            int index = 0;
            for (int j = 0; j < Nlayer[1].Nneuron.Count(); j++)
            {
                for (int k = 0; k < Nlayer[1].Nneuron[j].weights.Count(); k++)
                {
                    wv[index + k] = Nlayer[1].Nneuron[j].weights[k];
                }
                index += Nlayer[1].Nneuron[j].weights.Count();
            }
            for(int k=0;k<Nlayer[2].Nneuron[0].weights.Count();k++)
            {
                for (int j = 0; j < Nlayer[2].Nneuron.Count(); j++)
                {
                    wv[index + j] = Nlayer[2].Nneuron[j].weights[k];
                }
                index += Nlayer[2].Nneuron.Count();
            }
        }
        public void pasteWeight(float[] wv)
        {
            int index = 0;
            for (int j = 0; j < Nlayer[1].Nneuron.Count(); j++)
            {
                for (int k = 0; k < Nlayer[1].Nneuron[j].weights.Count(); k++)
                {
                    Nlayer[1].Nneuron[j].weights[k] = wv[index + k];
                }
                index += Nlayer[1].Nneuron[j].weights.Count();
            }
            for (int k = 0; k < Nlayer[2].Nneuron[0].weights.Count(); k++)
            {
                for (int j = 0; j < Nlayer[2].Nneuron.Count(); j++)
                {
                    Nlayer[2].Nneuron[j].weights[k] = wv[index + j];
                }
                index += Nlayer[2].Nneuron.Count();
            }
        }
        public float[] calGradient(int j,float[] g,float[] error)
        {
            int index = Nlayer[0].Nneuron.Count() * Nlayer[1].Nneuron.Count() + j * Nlayer[2].Nneuron.Count();
            float tanh = Nlayer[1].Nneuron[j].NeuronValue;
            for (int i = 0; i < Nlayer[2].Nneuron.Count(); i++)
            {
                int iloc0 = index + i;
                float deriv1 = (float)(error[i] * (1 - Math.Pow(tanh, 2)) * Nlayer[2].Nneuron[i].weights[j]);
                for (int k = 0; k < Nlayer[0].Nneuron.Count(); k++)
                {
                    int iloc = j * Nlayer[0].Nneuron.Count() + k;
                    g[iloc] = g[iloc] + deriv1 * Nlayer[0].Nneuron[k].NeuronValue;
                }
                g[iloc0] = g[iloc0] + tanh * error[i];
            }
            return g;
        }
        public void New_updateWeight(float LearningRate, float[] g)
        {
            //int index = 0;
            //for (int i = 1; i<Nlayer.Count(); i++)
            //{
            //    for (int j = 0; j < Nlayer[i].Nneuron.Count(); j++)
            //    {
            //        for (int k = 0; k < Nlayer[i].Nneuron[j].weights.Count(); k++)
            //        {
            //            Nlayer[i].Nneuron[j].weights[k] -= LearningRate*g[index + k];
            //        }
            //        index += Nlayer[i].Nneuron[j].weights.Count();
            //    }
            //}
            int index = 0;
            for (int j = 0; j < Nlayer[1].Nneuron.Count(); j++)
            {
                for (int k = 0; k < Nlayer[1].Nneuron[j].weights.Count(); k++)
                {
                    Nlayer[1].Nneuron[j].weights[k] -= LearningRate * g[index + k];
                }
                index += Nlayer[1].Nneuron[j].weights.Count();
            }
            for (int k = 0; k < Nlayer[2].Nneuron[0].weights.Count(); k++)
            {
                for (int j = 0; j < Nlayer[2].Nneuron.Count(); j++)
                {
                    Nlayer[2].Nneuron[j].weights[k] -= LearningRate * g[index + j];
                }
                index += Nlayer[2].Nneuron.Count();
            }
        }
        public float New_gradientDecent(float[] g)
        {
            float d = 0;
            for (int i = 0; i < g.Count(); i++)
            {
                d += (float)(0.5 * Math.Pow(g[i], 2));
            }
            d = (float)Math.Sqrt(d);
            return d;
        }
        public void zeroWeight(int status,int maxprunindex)//status 0 = prun input, 1 = prun hidden,2 = prun weight
        {
            if (status == 0)
            {
                for (int i = 0; i < Nlayer[1].Nneuron.Count(); i++)
                {
                    Nlayer[1].Nneuron[i].weights[maxprunindex] = 0;
                }
            }
            else if(status == 1)
            {
                for (int i = 0; i < Nlayer[2].Nneuron.Count(); i++)
                {
                    Nlayer[2].Nneuron[i].weights[maxprunindex] = 0;
                }
                for (int i = 0; i < Nlayer[1].Nneuron[maxprunindex].weights.Count(); i++)
                {
                    Nlayer[1].Nneuron[maxprunindex].weights[i] = 0;
                }
            }
            else if (status == 2)
            {
                int layer,neurons=-1,weight;
                int addition;//for indexing and finding the correct number of neuron

                if (maxprunindex < Nlayer[0].Nneuron.Count() * Nlayer[1].Nneuron.Count())
                {
                    layer = 1;
                    addition = 0;
                }
                else
                {
                    layer = 2;
                    addition = Nlayer[0].Nneuron.Count() * Nlayer[1].Nneuron.Count();
                }
                for (int i = 1; i <= Nlayer[layer].Nneuron.Count(); i++)
                {
                    
                    if (maxprunindex < i * Nlayer[layer-1].Nneuron.Count()+addition && maxprunindex >= (i - 1) * Nlayer[layer-1].Nneuron.Count()+addition)
                    {
                        neurons = i - 1;
                    }
                }
                weight = maxprunindex % Nlayer[layer - 1].Nneuron.Count();
                Nlayer[layer].Nneuron[neurons].weights[weight] = 0;
            }
        }
        public void saveNetworks(string filename, bool discrete, string Nval,bool Auto, bool aksara, bool houghfeatures, float TrainAcc, float[][] Ktable)
        {
            TextWriter tw = new StreamWriter(filename);
            tw.Write(Nlayer.Count()+" ");
            for (int i = 0; i < Nlayer.Count(); i++)
            {
                tw.Write(Nlayer[i].Nneuron.Count() + " ");
            }
           
            tw.Write(discrete + " ");
            tw.Write(Nval + " ");
            tw.Write(Auto + " ");
            tw.Write(aksara + " ");
            tw.Write(houghfeatures + " ");
            tw.Write(TrainAcc + " ");
            tw.WriteLine();

            for(int i=1;i<Nlayer.Count();i++)
            {
                for(int j=0;j<Nlayer[i].Nneuron.Count();j++)
                {
                    tw.WriteLine(Nlayer[i].Nneuron[j].weights.Count()+1);//+1 for the bias value
                    float[] tempW = Nlayer[i].Nneuron[j].WeightsValue;
                    for(int k=0;k<tempW.Count();k++)
                    {
                        tw.Write(tempW[k]+" ");
                    }
                    float tempbias = Nlayer[i].Nneuron[j].biasValue;
                    tw.Write(tempbias + " ");
                    tw.WriteLine();
                }
            }
            if (Auto == true)
            {
                tw.WriteLine(Ktable.Count());
                for (int i = 0; i < Ktable.Count(); i++)
                {
                    tw.WriteLine(Ktable[i].Count());
                    for (int j = 0; j < Ktable[i].Count(); j++)
                    {
                        tw.Write(Ktable[i][j] + " ");
                    }
                    tw.WriteLine();
                }
            }
            tw.Close();
        }
    }
}
