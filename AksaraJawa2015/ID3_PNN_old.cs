﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Diagnostics;
using NUnit.Framework;

namespace AksaraJawa2015
{
    public partial class ID3_PNN_old : Form
    {
        private string[][] _trainerData, _rawClassificationData;
        private List<string> _testDataNames;
        private Dictionary<string, string> _javaUnicode;
        private List<string> _upperCharacter;
        private List<string> _classificationException;
        public PNN _pnnNetwork;
        public Tree _id3Network;
        public bool _enchanceReading;
        public bool _createCSVResult;
        public bool _enrichFeatureExtraction;
        public bool _autoDataTest;

        public ID3_PNN_old()
        {
            InitializeComponent();
            _trainerData = _rawClassificationData = null;
            _pnnNetwork = null;
            _id3Network = null;
            _enchanceReading = false;
            _createCSVResult = true;
            _enrichFeatureExtraction = false;
            _autoDataTest = true;
            methodVisualizer.Text = "";

            // set test data exceptions
            _testDataNames = new List<string>();
            _testDataNames.Add("car.csv");
            _testDataNames.Add("car_trainer.csv");
            _testDataNames.Add("iris.csv");
            _testDataNames.Add("iris_trainer.csv");
            _testDataNames.Add("gift_problem_trainer.csv");
            _testDataNames.Add("weather_prediction_trainer.csv");

            // init java font unicodes
            _javaUnicode = new Dictionary<string, string>();
            // carakan
            _javaUnicode.Add("ha", "a");
            _javaUnicode.Add("na", "n");
            _javaUnicode.Add("ca", "c");
            _javaUnicode.Add("ra", "r");
            _javaUnicode.Add("ka", "k");
            _javaUnicode.Add("da", "f");
            _javaUnicode.Add("ta", "t");
            _javaUnicode.Add("sa", "s");
            _javaUnicode.Add("wa", "w");
            _javaUnicode.Add("la", "l");
            _javaUnicode.Add("pa", "p");
            _javaUnicode.Add("dha", "d");
            _javaUnicode.Add("ja", "j");
            _javaUnicode.Add("ya", "y");
            _javaUnicode.Add("nya", "v");
            _javaUnicode.Add("ma", "m");
            _javaUnicode.Add("ga", "g");
            _javaUnicode.Add("ba", "b");
            _javaUnicode.Add("tha", "q");
            _javaUnicode.Add("nga", "z");
            // pasangan
            _javaUnicode.Add("pasangan ha", "H");
            _javaUnicode.Add("pasangan na", "N");
            _javaUnicode.Add("pasangan ca", "C");
            _javaUnicode.Add("pasangan ra", "R");
            _javaUnicode.Add("pasangan ka", "K");
            _javaUnicode.Add("pasangan da", "F");
            _javaUnicode.Add("pasangan ta", "T");
            _javaUnicode.Add("pasangan sa", "S");
            _javaUnicode.Add("pasangan wa", "W");
            _javaUnicode.Add("pasangan la", "L");
            _javaUnicode.Add("pasangan pa", "P");
            _javaUnicode.Add("pasangan dha", "D");
            _javaUnicode.Add("pasangan ja", "J");
            _javaUnicode.Add("pasangan ya", "Y");
            _javaUnicode.Add("pasangan nya", "V");
            _javaUnicode.Add("pasangan ma", "M");
            _javaUnicode.Add("pasangan ga", "G");
            _javaUnicode.Add("pasangan ba", "B");
            _javaUnicode.Add("pasangan tha", "Q");
            _javaUnicode.Add("pasangan nga", "Z");
            // sandhangan
            _javaUnicode.Add("wulu", "i");
            _javaUnicode.Add("suku", "u");
            _javaUnicode.Add("taling", "[");
            _javaUnicode.Add("pepet", "e");
            _javaUnicode.Add("tarung", "o");
            _javaUnicode.Add("layar", "/");
            _javaUnicode.Add("wignyan", "h");
            _javaUnicode.Add("cecak", "=");
            _javaUnicode.Add("pangkon", "\\");
            _javaUnicode.Add("pangku", "\\");
            _javaUnicode.Add("paten", "\\");
            _javaUnicode.Add("pengkal", "-");
            _javaUnicode.Add("cakra", "]");
            _javaUnicode.Add("keret", "}");
            // wilangan
            _javaUnicode.Add("0", "0");
            _javaUnicode.Add("1", "1");
            _javaUnicode.Add("2", "2");
            _javaUnicode.Add("3", "3");
            _javaUnicode.Add("4", "4");
            _javaUnicode.Add("5", "5");
            _javaUnicode.Add("6", "6");
            _javaUnicode.Add("7", "7");
            _javaUnicode.Add("8", "8");
            _javaUnicode.Add("9", "9");
            // tanda baca
            _javaUnicode.Add("adeg adeg", "?");
            _javaUnicode.Add("pada lungsi", ".");
            _javaUnicode.Add("pada lingsa", ",");
            // null char
            _javaUnicode.Add("-", null);

            // init sandangan atas
            _upperCharacter = new List<string>();
            _upperCharacter.Add("wulu");
            _upperCharacter.Add("pepet");
            _upperCharacter.Add("layar");
            _upperCharacter.Add("cecak");

            // set classification exceptions
            _classificationException = new List<string>();
            _classificationException.Add("+");
            _classificationException.Add("murda");
            _classificationException.Add("swara");
            _classificationException.Add("?");
            _classificationException.Add("-");
        }

        private void segmentationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessHelper.startProcess("ExtApps\\Segmentation\\App.exe", this);
        }

        private void featureExtractionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessHelper.startProcess("ExtApps\\Feature Extraction\\histogram.exe", this);
        }

        private void trainNewDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            train("pnn");
        }

        private void saveTrainingDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            serialize("pnn");
        }

        private void loadTrainingDataToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            deserialize("pnn");
        }

        private void modifySpreadValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var displayTxt = "";

            if(_pnnNetwork != null)
            {
                // get spread from user input
                string strSpread = Interaction.InputBox("Input spread value", "Spread", _pnnNetwork.Spread + "");
                double spread;
                while (!Double.TryParse(strSpread, out spread))
                    strSpread = Interaction.InputBox("Please input a numeric value", "User Input Needed", _pnnNetwork.Spread + "");
                // set the spread
                _pnnNetwork.Spread = spread;
                // display the change
                displayTxt += "Spread changed to : " + spread
                    + "\r\nBias changed to " + _pnnNetwork.Bias + " \r\n\r\n";
            }
            else
            {
                displayTxt += "Please train the network first\r\n\r\n";
            }
            updateVisualizer(displayTxt);
        }

        private void trainNewDataToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            train("id3");
        }

        private void saveTrainingDataToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            serialize("id3");
        }

        private void loadTrainingDataToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            deserialize("id3");
        }

        private void pNNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            classify("pnn");
        }

        private void iD3ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            classify("id3");
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClassificationSettings settingForm = new ClassificationSettings(this, _enchanceReading, _createCSVResult);
            settingForm.ShowDialog();
        }

        private void clearVisualizerBtn_Click(object sender, EventArgs e)
        {
            methodVisualizer.Text = "";
        }

        private Boolean loadTrainerFile()
        {
            var dialogBox = new OpenFileDialog();
            dialogBox.Filter = "CSV File (.csv)|*.csv";
            dialogBox.Title = "Select data to learn from";
            var displayTxt = "";
            if (dialogBox.ShowDialog() == DialogResult.OK)
            {
                // load external file
                _trainerData = Parser.CSVToArray(@dialogBox.FileName);
                if(_enrichFeatureExtraction)
                    _trainerData = ClassifierNetworkkHelper.LeverageMultipleData(_trainerData);

                if (_trainerData != null)
                {
                    // trim filename 
                    var fileName = dialogBox.FileName.Substring(dialogBox.FileName.LastIndexOf('\\') + 1);
                    // update visualizer
                    displayTxt += "Trainer data loaded : " + fileName;
                    // display data matrix
                    if(visualizeMethod.Checked)
                        displayTxt += "\r\nContent : \r\n" + Parser.ArrayToString(_trainerData);

                    // set columns to fetch
                    var columns = "Jumlah Loop pada Gambar"
                        + "|Jumlah Loop pada Segmen Atas"
                        + "|Jumlah Loop pada Segmen Bawah"
                        + "|Jumlah Line pada Segmen Atas"
                        + "|Jumlah Line pada Segmen Bawah"
                        + "|Jumlah Curve pada Segmen Atas"
                        + "|Jumlah Curve pada Segmen Bawah"
                        + "|Nama Huruf";

                    if (_enrichFeatureExtraction)
                        columns += "|Rho, theta";
                    
                    // check for test data
                    if (_testDataNames.Contains(fileName))
                        columns = null;

                    // trim array content
                    _trainerData = ArrayHelper.TrimData(_trainerData, columns);

                    // remove duplicate features
                    List<string> featuresExist = new List<string>();
                    List<string[]> dataContainer = new List<string[]>();
                    int initialDataCount = _trainerData.Length;
                    for (int i = 0; i < _trainerData.Length; i++)
                    {
                        var features = new string[_trainerData[i].Length - 1];
                        for(int j=0; j<_trainerData[i].Length; j++)
                        {
                            if (j == 0)
                                continue;

                            features[j-1] = _trainerData[i][j];
                        }

                        // duplicate features check
                        if (featuresExist.Contains(Parser.ArrayToString(features)))
                        {
                            var kickStarter = dataContainer.Where(value => String.Join(" ", value).Contains(String.Join(" ", features))).ToArray();

                            if (_trainerData[i][0] == kickStarter[0][0])
                                displayTxt += "\r\n\"" + _trainerData[i][0] + "\" at line " + (i + 1) + " already exist\r\n";
                            else
                                displayTxt += "\r\n\"" + _trainerData[i][0] + "\" at line " + (i + 1) + " have same feature as \"" + kickStarter[0][0] + "\"\r\n";

                            continue;
                        }

                        // aksara murda check
                        if (_trainerData[i][0].Contains("murda"))
                            continue;

                        featuresExist.Add(Parser.ArrayToString(features));
                        dataContainer.Add(_trainerData[i]);
                    }
                    _trainerData = dataContainer.ToArray();

                    // display process
                    if (visualizeMethod.Checked)
                    {
                        displayTxt += "\r\nINPUT TRIMMED TO :\r\n";
                        displayTxt += Parser.ArrayToString(_trainerData);
                        displayTxt += "\r\n"+(initialDataCount-_trainerData.Length)+" duplicate feature(s) & aksara murda removed";
                    }

                    displayTxt += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                    updateVisualizer(displayTxt);

                    return true;
                }

                displayTxt += "Failed to load data, please try again";
                displayTxt += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                updateVisualizer(displayTxt);
            }

            return false;
        }

        private void train(string method)
        {
            if (!loadTrainerFile())
                return;

            var processStart = DateTime.Now;
            var displayText = "";
                
            // PNN process
            if (method == "pnn")
            {
                displayText += "TRAINING START, METHOD USED : PNN\r\n\r\n";

                // get spread from user input
                string strSpread = Interaction.InputBox("Input spread value", "Spread", "0,05");
                double spread;
                while (!Double.TryParse(strSpread, out spread))
                    strSpread = Interaction.InputBox("Please input a numeric value", "User Input Needed", "0,05");

                // train network
                _pnnNetwork = new PNN();
                bool trainingDone = _pnnNetwork.Train(_trainerData, spread, 0, 0);
                if(!trainingDone)
                {
                    displayText += "Training failed ! Please try again";
                    displayText += "\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                    updateVisualizer(displayText);
                    return;
                }

                // update display text
                if(visualizeMethod.Checked)
                {
                    displayText += "Spread value : " + spread + "\r\n\r\n";
                    displayText += _pnnNetwork.Display();
                }
            }
            // ID3 process 
            else if (method == "id3")
            {
                // build tree
                var treeSet = Parser.ArrayToDecisionTreeSet(_trainerData, 0, 0);
                _id3Network = treeSet.BuildTree();
                
                // display tree
                if (visualizeMethod.Checked)
                {
                    displayText += "TRAINING START, METHOD USED : DECISION TREE (ID3)\r\n\r\n";
                    displayText += _id3Network.display();
                    displayText += "\r\n";
                }

                // validate tree
                displayText += "ERRORS : \r\n";
                int totalData = 0, error = 0;
                foreach (var instance in treeSet.Instances)
                {
                    totalData++;
                    try
                    {
                        Assert.That(Tree.ProcessInstance(_id3Network, instance).Value, Is.EqualTo(instance.Output.Value));
                    }
                    catch(Exception ex)
                    {
                        error++;
                        displayText += ex.Message + "\r\n";
                    }
                }

                displayText += error + " errors of " + totalData + " data\r\n\r\n";
            }

            // calculate total process time
            var processEnd = DateTime.Now;
            var processDuration = processEnd - processStart;
            var totalDuration = processDuration.Minutes + " min "
                            + processDuration.Seconds + " sec "
                            + processDuration.Milliseconds + " mili sec";

            // update visualizer
            displayText += "Training finished ! ( " + totalDuration + " )\r\n"
                + "Total data trained : " + (_trainerData.Length - 1) + "\r\n";
            displayText += "\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
            updateVisualizer(displayText);
        }

        private Boolean loadFileToClassify()
        {
            var dialogBox = new OpenFileDialog();
            dialogBox.Filter = "CSV File (.csv)|*.csv";
            dialogBox.Title = "Select data to be classfied";

            if (dialogBox.ShowDialog() == DialogResult.OK)
            {
                // string to display in visualizer
                string displayTxt = "";

                // load external file
                _rawClassificationData = Parser.CSVToArray(@dialogBox.FileName);
                if(_enrichFeatureExtraction)
                    _rawClassificationData = ClassifierNetworkkHelper.LeverageMultipleData(_rawClassificationData);

                if (_rawClassificationData != null)
                {
                    // trim filename 
                    var fileName = dialogBox.FileName.Substring(dialogBox.FileName.LastIndexOf('\\') + 1);
                    // if data loeaded, update text to display
                    displayTxt += "File to classify loaded : " + fileName;
                    // display data matrix
                    if(visualizeMethod.Checked)
                        displayTxt += "\r\nContent : \r\n" + Parser.ArrayToString(_rawClassificationData);

                    // set columns to fetch
                    string columns = "Jumlah Loop pada Gambar"
                        + "|Jumlah Loop pada Segmen Atas"
                        + "|Jumlah Loop pada Segmen Bawah"
                        + "|Jumlah Line pada Segmen Atas"
                        + "|Jumlah Line pada Segmen Bawah"
                        + "|Jumlah Curve pada Segmen Atas"
                        + "|Jumlah Curve pada Segmen Bawah"
                        + "|X"
                        + "|Y"
                        + "|Width"
                        + "|Height"
                        + "|Image Data"
                        + "|Parent Folder"
                        + "|Segment Image";

                    if (_enrichFeatureExtraction)
                        columns += "|Rho, theta";

                    // check for test data
                    if (_testDataNames.Contains(fileName))
                        columns = null;

                    // sanitize data
                    _rawClassificationData = ArrayHelper.TrimData(_rawClassificationData, columns);

                    if(visualizeMethod.Checked)
                        displayTxt += "\r\nSanitized content : \r\n" + Parser.ArrayToString(_rawClassificationData);

                    displayTxt += "\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                    updateVisualizer(displayTxt);

                    return true;
                }

                displayTxt += "Fail to load file to classify, please try again";
                displayTxt += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                updateVisualizer(displayTxt);
            }

            return false;
        }

        private void classify(string method)
        {
            var proceedNextStep = true;
            object network;
            var displayText = "";

            if (method == "pnn")
                network = _pnnNetwork;
            else
                network = _id3Network;

            // untrained network check
            if(network == null)
            {
                MessageBox.Show("Untrained network", "Classification Error");
                proceedNextStep = false;
            }

            if(proceedNextStep)
            {
                if (!loadFileToClassify())
                    return;

                var processStart = DateTime.Now;
                string[] classificationRes;
                string fileName;
                int totalTrainingData = 0;

                // PNN process
                if (method == "pnn")
                {
                    displayText += "CLASSIFICATION START, METHOD USED : PNN\r\n\r\n";

                    // remove unnecessary data
                    string columns = "Jumlah Loop pada Gambar"
                        + "|Jumlah Loop pada Segmen Atas"
                        + "|Jumlah Loop pada Segmen Bawah"
                        + "|Jumlah Line pada Segmen Atas"
                        + "|Jumlah Line pada Segmen Bawah"
                        + "|Jumlah Curve pada Segmen Atas"
                        + "|Jumlah Curve pada Segmen Bawah";
                    if (_enrichFeatureExtraction)
                        columns += "|Rho, theta";
                    var halfBakedData = ArrayHelper.TrimData(_rawClassificationData, columns, true);

                    // parse arr of str into arr of double
                    var bakedData = Parser.StringToDecimal(halfBakedData);

                    // start classification
                    classificationRes = _pnnNetwork.Classify(bakedData);

                    // set result file name
                    fileName = "PNN Classification Result";

                    totalTrainingData = _pnnNetwork.TotalTrainingData;
                }
                // ID3 process
                else
                {
                    displayText += "CLASSIFICATION START, METHOD USED : ID3\r\n\r\n";

                    // remove unnecessary data
                    string columns = "Jumlah Loop pada Gambar"
                        + "|Jumlah Loop pada Segmen Atas"
                        + "|Jumlah Loop pada Segmen Bawah"
                        + "|Jumlah Line pada Segmen Atas"
                        + "|Jumlah Line pada Segmen Bawah"
                        + "|Jumlah Curve pada Segmen Atas"
                        + "|Jumlah Curve pada Segmen Bawah";
                    if (_enrichFeatureExtraction)
                        columns += "|Rho, theta";
                    var halfBakedData = ArrayHelper.TrimData(_rawClassificationData, columns);

                    // start classification
                    classificationRes = _id3Network.Classify(halfBakedData);

                    // set result file name
                    fileName = "ID3 Classification Result";

                    totalTrainingData = _id3Network.TotalTrainingData;
                }

                if (!_enrichFeatureExtraction)
                    fileName += " (standard feature)";
                else
                    fileName += " (enriched feature)";

                if (classificationRes != null)
                {
                    // convert result to lowercase & remove extra whitespaces
                    for (int i = 0; i < classificationRes.Length; i++)
                    {
                        if(classificationRes[i] != null)
                            classificationRes[i] = classificationRes[i].ToLower().Trim();
                    }

                    var originalClassificationRes = new string[classificationRes.Length];
                    Array.Copy(classificationRes, originalClassificationRes, classificationRes.Length);
                        
                    var characterMap = ArrayHelper.TrimData(_rawClassificationData, null, true);
                    /* character map index :
                     * - col 0 = Image Data
                     * - col 1 = Parent Folder
                     * - col 2 = Segment Image
                     * - col 3 = X
                     * - col 4 = Y
                     * - col 5 = Width
                     * - col 6 = Height
                     */
                    
                    // get character line map
                    var lineMap = new int[characterMap.Length];
                    for (int i = 0; i < lineMap.Length; i++)
                    {
                        var trimmedString = characterMap[i][1].Substring(1);
                        var splittedString = trimmedString.Split('-');
                        lineMap[i] = int.Parse(splittedString[0]);
                    }

                    // normal alphabet reading, relying on segmentation alone
                    if (!_enchanceReading)
                    {
                        // adjust alphabet position
                        for (int i = 0; i < classificationRes.Length; i++)
                        {
                            // debug
                            //Console.Write("iteration-{0}", i + 1);

                            // character above main alphabet
                            if (_upperCharacter.Contains(classificationRes[i]))
                            {
                                try
                                {
                                    classificationRes[i + 1] += "+" + classificationRes[i];
                                    classificationRes[i] = "-";
                                }
                                catch(Exception ex)
                                {
                                    Console.WriteLine("Error when adjusting alphabet position\nUpper chareacter\nError message : {0}", ex.Message);
                                }

                                // debug 
                                //Console.Write(" : sandangan atas !\nmerged to : {0}", classificationRes[i + 1]);

                                continue;
                            }

                            // debug
                            //Console.Write("\n");
                        }

                        // debug
                        //Console.WriteLine("");
                    }

                    // convert result into its unicode representation
                    for (int i = 0; i < classificationRes.Length; i++)
                    {
                        try
                        {
                            if (classificationRes[i] == null)
                                continue;

                            if (classificationRes[i].Contains('+'))
                            {
                                var split = classificationRes[i].Split('+');

                                for (int j = 0; j < split.Length; j++)
                                {
                                    if (!String.IsNullOrEmpty(split[j]))
                                        split[j] = _javaUnicode[split[j]];
                                }

                                classificationRes[i] = Parser.ArrayToString(split, "");

                                continue;
                            }

                            classificationRes[i] = _javaUnicode[classificationRes[i]];
                        }
                        catch
                        {
                            Console.WriteLine("{0} at iteration-{1} is not recognized in dictionary", classificationRes[i], i);
                        }
                    }
                    
                    // enqueue final result
                    var finalResult = "";
                    var lastLine = lineMap[0];
                    for (int i = 0; i < classificationRes.Length; i++)
                    {
                        if (classificationRes[i] == null)
                            continue;

                        if(lineMap[i] > lastLine)
                        {
                            finalResult += "\r\n";
                            lastLine = lineMap[i];
                        }

                        finalResult += classificationRes[i];

                        if (i < classificationRes.Length - 1)
                            finalResult += " ";
                    }

                    // calculate process duration
                    var processEnd = DateTime.Now;
                    var processDuration = processEnd - processStart;
                    var totalDuration = processDuration.Minutes + " min "
                            + processDuration.Seconds + " sec "
                            + processDuration.Milliseconds + " mili sec";

                    // update display text
                    displayText += "CLASSIFICATION RESULT : \r\n";
                    displayText += finalResult + "\r\n\r\n";
                    displayText += "Classify finished ! ( " + totalDuration + " )";

                    // save result to a text file
                    var saveFile = MessageBox.Show("Save result to file ?", "Classification Done", MessageBoxButtons.YesNo);
                    if (saveFile == DialogResult.Yes)
                    {
                        SaveFileDialog savedFile = new SaveFileDialog();
                        savedFile.Filter = "Text files (*.txt)|*.txt";
                        savedFile.FileName = fileName;
                        if (savedFile.ShowDialog() == DialogResult.OK)
                        {
                            using (TextWriter writer = new StreamWriter(@savedFile.FileName))
                                writer.Write(finalResult);

                            if (_createCSVResult)
                            {
                                // init filename & location
                                var index = savedFile.FileName.LastIndexOf("\\") + 1;
                                var length = index - 0;
                                var fileLocation = savedFile.FileName.Substring(0, length);
                                fileName = savedFile.FileName.Substring(savedFile.FileName.LastIndexOf('\\') + 1);
                                fileName = fileName.Substring(0, fileName.IndexOf('.'));
                                var saveFileName = fileLocation + fileName + " - CSV Result.csv";

                                // init data testing
                                string[][] testerData = null;
                                double correct = 0, wrong = 0, totalData = 0;
                                if (_autoDataTest)
                                {
                                    testerData = Parser.CSVToArray(fileLocation + "result - edited.csv");
                                    if (testerData == null || testerData.Length != _rawClassificationData.Length)
                                    {
                                        testerData = Parser.CSVToArray(fileLocation + "result - edited .csv");
                                        if (testerData == null || testerData.Length != _rawClassificationData.Length)
                                        {
                                            testerData = Parser.CSVToArray(fileLocation + " result - edited.csv");
                                            if (testerData == null || testerData.Length != _rawClassificationData.Length)
                                            {
                                                testerData = Parser.CSVToArray(fileLocation + " result - edited .csv");
                                                if (testerData == null || testerData.Length != _rawClassificationData.Length)
                                                    _autoDataTest = false;
                                            }
                                        }
                                    }
                                }

                                // create CSV result
                                var CSVResult = "";
                                if (_createCSVResult)
                                {
                                    for (int i = 0; i < _rawClassificationData.Length; i++)
                                    {
                                        for (int j = 0; j < _rawClassificationData[i].Length; j++)
                                        {
                                            if (i == 0 && j == 0)
                                            {
                                                CSVResult += "Hasil;";

                                                if (_autoDataTest)
                                                    CSVResult += "Nama Huruf Seharusnya;Benar/Salah;";
                                            }
                                            else if (i > 0 && j == 0)
                                            {
                                                var classificationResult = originalClassificationRes[i - 1];
                                                CSVResult += classificationResult + ";";

                                                if(_autoDataTest)
                                                {
                                                    var originalData = testerData[i][3];
                                                    CSVResult += originalData + ";";

                                                    var containsForbiddenCharacter = false;
                                                    _classificationException.ForEach(val => { if (originalData.Contains(val)) { containsForbiddenCharacter = true; return; } });

                                                    // the test
                                                    if (!String.IsNullOrWhiteSpace(originalData) && !containsForbiddenCharacter)
                                                    {
                                                        totalData ++;

                                                        string answer;

                                                        if (originalData == classificationResult)
                                                        {
                                                            answer = "benar";
                                                            correct++;
                                                        }
                                                        else
                                                        {
                                                            answer = "salah";
                                                            wrong++;
                                                        }

                                                        CSVResult += answer;
                                                    }

                                                    CSVResult += ";";
                                                }
                                            }

                                            CSVResult += _rawClassificationData[i][j];

                                            if (j < _rawClassificationData[i].Length - 1)
                                                CSVResult += ";";
                                        }

                                        if (i < _rawClassificationData.Length - 1)
                                            CSVResult += "\r\n";
                                    }

                                    // init report data
                                    double correctPercentageCalc = correct / totalData * 100;
                                    string correctPercentage = Math.Round(correctPercentageCalc, 2) + " %";
                                    double wrongPercentageCalc = wrong / totalData * 100;
                                    string wrongPercentage = Math.Round(wrongPercentageCalc, 2) + " %";

                                    // write report
                                    CSVResult += "\r\n\r\n" + "Success";
                                    if (_autoDataTest)
                                        CSVResult += ";" + correct + ";" + correctPercentage;
                                    CSVResult += "\r\n" + "Fail";
                                    if (_autoDataTest)
                                        CSVResult += ";" + wrong + ";" + wrongPercentage;
                                    CSVResult += "\r\n" + "Total data classified;" + totalData;
                                    CSVResult += "\r\n" + "Total training data;" + totalTrainingData;
                                    CSVResult += "\r\nClassification time;" + totalDuration;
                                }

                                // save the file
                                using (TextWriter writer = new StreamWriter(saveFileName))
                                    writer.Write(CSVResult);
                            }
                        }
                    }
                }
                else
                {
                    proceedNextStep = false;
                }
            }

            if(!proceedNextStep)
                displayText += "Classification failed, please try again";

            // update visualizer
            displayText += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
            updateVisualizer(displayText);
        }

        private void serialize(string method)
        {
            // init save file dialog properties (general)
            SaveFileDialog savedFile = new SaveFileDialog();
            savedFile.Filter = "XML File (.xml)|*.xml";

            var displayText = "";
            var networkTrained = true;
            object network;

            if(method == "pnn")
            {
                network = _pnnNetwork;
                savedFile.FileName = "Serialized PNN.xml";
            }
            else
            {
                network = _id3Network;
                savedFile.FileName = "Serialized ID3.xml";
            }

            // untrained network check
            if(network == null)
            {
                MessageBox.Show("Cannot serialize untrained network !", "Serialization Error");
                networkTrained = false;
            }

            if(networkTrained)
            {
                // open the save dialog
                if (savedFile.ShowDialog() == DialogResult.OK)
                {
                    // start calculating process duration
                    DateTime start = DateTime.Now;

                    // serialize network
                    var serializer = new DataContractSerializer(network.GetType());
                    using (Stream stream = new FileStream(@savedFile.FileName, FileMode.Create, FileAccess.Write))
                    {
                        using (XmlDictionaryWriter writer = XmlDictionaryWriter.CreateTextWriter(stream, Encoding.UTF8))
                        {
                            writer.WriteStartDocument();
                            serializer.WriteObject(writer, network);
                        }
                    }

                    // calculate total process duration
                    DateTime end = DateTime.Now;
                    TimeSpan duration = end - start;
                    var totalDuration = duration.Minutes + " min "
                            + duration.Seconds + " sec "
                            + duration.Milliseconds + " mili sec";
                    // display duration in visualizer
                    displayText += savedFile.FileName + " serialized ! ( " + totalDuration + " )";
                }
                else
                {
                    return;
                }
            }
            else
            {
                displayText += "Serialization fail, please try again";
            }
            
            // update visualizer
            displayText += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
            updateVisualizer(displayText);
        }

        private void deserialize(string method)
        {
            // init dialog box
            var dialogBox = new OpenFileDialog();
            dialogBox.Filter = "XML File (.xml)|*.xml";
            dialogBox.Title = "Select data to be deserialized";

            // open dialog box
            if(dialogBox.ShowDialog() == DialogResult.OK)
            {
                // processStart calculating process duration
                DateTime processStart = DateTime.Now;

                Type networkType;
                if(method == "pnn")
                    networkType = typeof(PNN);
                else
                    networkType = typeof(Tree);

                DataContractSerializer deserializer = new DataContractSerializer(networkType);
                Stream fileStream = new FileStream(@dialogBox.FileName, FileMode.Open);
                XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(fileStream, new XmlDictionaryReaderQuotas());

                var deserializationSuccess = true;
                try
                {
                    if (networkType == typeof(PNN))
                        _pnnNetwork = (PNN)deserializer.ReadObject(reader);
                    else
                        _id3Network = (Tree)deserializer.ReadObject(reader);
                }
                catch(SerializationException ex)
                {
                    MessageBox.Show("Data mismatch with current network\n\n" + ex.Message, "Deserialization Error");
                    deserializationSuccess = false;
                }

                reader.Close();
                fileStream.Close();

                var txt = "";

                if (deserializationSuccess)
                {
                    if (visualizeMethod.Checked)
                    {
                        txt += "Deserialized network content : \r\n";
                        if (networkType == typeof(PNN))
                            txt += _pnnNetwork.Display();
                        else
                            txt += _id3Network.display();
                    }

                    // calculate total process duration
                    DateTime processEnd = DateTime.Now;
                    TimeSpan duration = processEnd - processStart;
                    var totalDuration = duration.Minutes + " min "
                            + duration.Seconds + " sec "
                            + duration.Milliseconds + " mili sec";

                    // trim filename 
                    var fileName = dialogBox.FileName.Substring(dialogBox.FileName.LastIndexOf('\\') + 1);

                    txt += fileName + " deserialized ! ( " + totalDuration + " )";
                }
                else
                {
                    txt += "Deserialization fail, please try again";
                }

                // show confirmation in visualizer
                txt += "\r\n\r\n\r\n--------------------------------------------------\r\n\r\n\r\n";
                updateVisualizer(txt);
            }
        }

        private void updateVisualizer(string txt)
        {
            methodVisualizer.Text += txt;
            methodVisualizer.Focus();
            methodVisualizer.SelectionStart = methodVisualizer.Text.Length;
            methodVisualizer.ScrollToCaret();
        }

    } // end of class

} // end of namespace
