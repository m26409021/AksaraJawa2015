﻿namespace AksaraJawa2015
{
    partial class Wizard_Training
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OriginalImage = new System.Windows.Forms.GroupBox();
            this.browse_train_btn = new System.Windows.Forms.Button();
            this.PictureBox_OriginalImage = new System.Windows.Forms.PictureBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.save_load_btn = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.OriginalImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).BeginInit();
            this.SuspendLayout();
            // 
            // OriginalImage
            // 
            this.OriginalImage.Controls.Add(this.save_load_btn);
            this.OriginalImage.Controls.Add(this.label7);
            this.OriginalImage.Controls.Add(this.label25);
            this.OriginalImage.Controls.Add(this.textBox5);
            this.OriginalImage.Controls.Add(this.browse_train_btn);
            this.OriginalImage.Controls.Add(this.textBox10);
            this.OriginalImage.Controls.Add(this.PictureBox_OriginalImage);
            this.OriginalImage.Controls.Add(this.label22);
            this.OriginalImage.Controls.Add(this.textBox8);
            this.OriginalImage.Location = new System.Drawing.Point(13, 13);
            this.OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.OriginalImage.Name = "OriginalImage";
            this.OriginalImage.Padding = new System.Windows.Forms.Padding(27, 25, 27, 25);
            this.OriginalImage.Size = new System.Drawing.Size(553, 636);
            this.OriginalImage.TabIndex = 3;
            this.OriginalImage.TabStop = false;
            this.OriginalImage.Text = "Original Image";
            // 
            // browse_train_btn
            // 
            this.browse_train_btn.Location = new System.Drawing.Point(8, 530);
            this.browse_train_btn.Margin = new System.Windows.Forms.Padding(4);
            this.browse_train_btn.Name = "browse_train_btn";
            this.browse_train_btn.Size = new System.Drawing.Size(533, 42);
            this.browse_train_btn.TabIndex = 1;
            this.browse_train_btn.Text = "Browse Image and Training";
            this.browse_train_btn.UseVisualStyleBackColor = true;
            // 
            // PictureBox_OriginalImage
            // 
            this.PictureBox_OriginalImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBox_OriginalImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox_OriginalImage.Location = new System.Drawing.Point(8, 23);
            this.PictureBox_OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox_OriginalImage.Name = "PictureBox_OriginalImage";
            this.PictureBox_OriginalImage.Padding = new System.Windows.Forms.Padding(13, 12, 13, 12);
            this.PictureBox_OriginalImage.Size = new System.Drawing.Size(533, 467);
            this.PictureBox_OriginalImage.TabIndex = 0;
            this.PictureBox_OriginalImage.TabStop = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(198, 501);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 17);
            this.label25.TabIndex = 26;
            this.label25.Text = "Epochs :";
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.ForeColor = System.Drawing.SystemColors.MenuText;
            this.textBox10.Location = new System.Drawing.Point(271, 497);
            this.textBox10.Margin = new System.Windows.Forms.Padding(4);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(91, 23);
            this.textBox10.TabIndex = 25;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 501);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(78, 17);
            this.label22.TabIndex = 24;
            this.label22.Text = "Curr. SSE :";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.ForeColor = System.Drawing.SystemColors.MenuText;
            this.textBox8.Location = new System.Drawing.Point(92, 499);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(91, 23);
            this.textBox8.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(379, 501);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 17);
            this.label7.TabIndex = 26;
            this.label7.Text = "Process:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.ForeColor = System.Drawing.SystemColors.MenuText;
            this.textBox5.Location = new System.Drawing.Point(450, 499);
            this.textBox5.Margin = new System.Windows.Forms.Padding(4);
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(91, 23);
            this.textBox5.TabIndex = 25;
            // 
            // save_load_btn
            // 
            this.save_load_btn.Location = new System.Drawing.Point(8, 580);
            this.save_load_btn.Margin = new System.Windows.Forms.Padding(4);
            this.save_load_btn.Name = "save_load_btn";
            this.save_load_btn.Size = new System.Drawing.Size(533, 42);
            this.save_load_btn.TabIndex = 27;
            this.save_load_btn.Text = "Save Network";
            this.save_load_btn.UseVisualStyleBackColor = true;
            // 
            // Wizard_Training
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 656);
            this.Controls.Add(this.OriginalImage);
            this.Name = "Wizard_Training";
            this.Text = "Wizard_Training";
            this.OriginalImage.ResumeLayout(false);
            this.OriginalImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox OriginalImage;
        private System.Windows.Forms.Button browse_train_btn;
        private System.Windows.Forms.PictureBox PictureBox_OriginalImage;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button save_load_btn;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}