﻿namespace AksaraJawa2015
{
    partial class HoughFeature
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup9 = new System.Windows.Forms.ListViewGroup("Segmen 1", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup10 = new System.Windows.Forms.ListViewGroup("Segmen 2", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup11 = new System.Windows.Forms.ListViewGroup("Segmen 3", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup12 = new System.Windows.Forms.ListViewGroup("Segmen 4", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup13 = new System.Windows.Forms.ListViewGroup("Segmen 1", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup14 = new System.Windows.Forms.ListViewGroup("Segmen 2", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup15 = new System.Windows.Forms.ListViewGroup("Segmen 1 - Library", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup16 = new System.Windows.Forms.ListViewGroup("Segmen 2 - Library", System.Windows.Forms.HorizontalAlignment.Left);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HoughFeature));
            this.single = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.filesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageProcessingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thinningImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.featureExtractionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loopDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lineDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.curveDetectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multi = new System.Windows.Forms.TabPage();
            this.lblMultiResult = new System.Windows.Forms.Label();
            this.listMulti = new System.Windows.Forms.ListView();
            this.no = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.path = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.loopfull = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.loop1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.loop2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.line1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.coord1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rhoTheta1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.line2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.coord2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rhoTheta2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.curve1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.vertex1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.curve2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.vertex2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listImg = new System.Windows.Forms.ListView();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.processImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnRunTime = new roundedPanel();
            this.lblRunTime = new System.Windows.Forms.Label();
            this.pnCurve = new roundedPanel();
            this.label41 = new System.Windows.Forms.Label();
            this.ibCurve2 = new Emgu.CV.UI.ImageBox();
            this.ibCurveUnion2 = new Emgu.CV.UI.ImageBox();
            this.label42 = new System.Windows.Forms.Label();
            this.ibCurve1 = new Emgu.CV.UI.ImageBox();
            this.ibCurveUnion1 = new Emgu.CV.UI.ImageBox();
            this.roundedPanel12 = new roundedPanel();
            this.label15 = new System.Windows.Forms.Label();
            this.numThrPercC = new System.Windows.Forms.NumericUpDown();
            this.rbValC = new System.Windows.Forms.RadioButton();
            this.rbPercC = new System.Windows.Forms.RadioButton();
            this.numThrValC = new System.Windows.Forms.NumericUpDown();
            this.numGapC = new System.Windows.Forms.NumericUpDown();
            this.label58 = new System.Windows.Forms.Label();
            this.numDistC = new System.Windows.Forms.NumericUpDown();
            this.label57 = new System.Windows.Forms.Label();
            this.listCurve = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label14 = new System.Windows.Forms.Label();
            this.numNeighborC = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.numStepC = new System.Windows.Forms.NumericUpDown();
            this.btnCurve = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.roundedPanel13 = new roundedPanel();
            this.label36 = new System.Windows.Forms.Label();
            this.pnLine = new roundedPanel();
            this.label37 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.ibHoughLine2 = new Emgu.CV.UI.ImageBox();
            this.ibHoughLine1 = new Emgu.CV.UI.ImageBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ibDetLine2 = new Emgu.CV.UI.ImageBox();
            this.ibLineUni2 = new Emgu.CV.UI.ImageBox();
            this.ibDetLine1 = new Emgu.CV.UI.ImageBox();
            this.ibLineUni1 = new Emgu.CV.UI.ImageBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.ibLine2 = new Emgu.CV.UI.ImageBox();
            this.ibLineUnion2 = new Emgu.CV.UI.ImageBox();
            this.label19 = new System.Windows.Forms.Label();
            this.ibLine1 = new Emgu.CV.UI.ImageBox();
            this.ibLineUnion1 = new Emgu.CV.UI.ImageBox();
            this.roundedPanel1 = new roundedPanel();
            this.numThrPerc = new System.Windows.Forms.NumericUpDown();
            this.rbPilThrVal = new System.Windows.Forms.RadioButton();
            this.rbPilThrPer = new System.Windows.Forms.RadioButton();
            this.numMinLength = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.numNeighbor = new System.Windows.Forms.NumericUpDown();
            this.listLine = new System.Windows.Forms.ListView();
            this.Nomor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.P1X = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.P1Y = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.P2X = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.P2Y = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rho = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.theta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label33 = new System.Windows.Forms.Label();
            this.numThrVal = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this.numMaxGap = new System.Windows.Forms.NumericUpDown();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.numThetaStep = new System.Windows.Forms.NumericUpDown();
            this.btnHT = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.roundedPanel9 = new roundedPanel();
            this.label27 = new System.Windows.Forms.Label();
            this.pnLoop = new roundedPanel();
            this.label43 = new System.Windows.Forms.Label();
            this.numMinArea = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.ibCycleFull = new Emgu.CV.UI.ImageBox();
            this.ibCycle2 = new Emgu.CV.UI.ImageBox();
            this.ibCycle1 = new Emgu.CV.UI.ImageBox();
            this.tbCycle = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnLoop = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.roundedPanel5 = new roundedPanel();
            this.label26 = new System.Windows.Forms.Label();
            this.pnThinned = new roundedPanel();
            this.ibS2 = new Emgu.CV.UI.ImageBox();
            this.ibS1 = new Emgu.CV.UI.ImageBox();
            this.ibImgReady = new Emgu.CV.UI.ImageBox();
            this.lblHeight = new System.Windows.Forms.Label();
            this.lblWidth = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.roundedPanel3 = new roundedPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.pnThinning = new roundedPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numWinH = new System.Windows.Forms.NumericUpDown();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.numWinW = new System.Windows.Forms.NumericUpDown();
            this.btnThinning = new System.Windows.Forms.Button();
            this.ibThinning = new Emgu.CV.UI.ImageBox();
            this.ibPreProcess = new Emgu.CV.UI.ImageBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.roundedPanel4 = new roundedPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbThinning = new System.Windows.Forms.TextBox();
            this.tbPreProcess = new System.Windows.Forms.TextBox();
            this.pnImage = new roundedPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkRH = new System.Windows.Forms.CheckBox();
            this.chkRW = new System.Windows.Forms.CheckBox();
            this.btnResize = new System.Windows.Forms.Button();
            this.numH = new System.Windows.Forms.NumericUpDown();
            this.numW = new System.Windows.Forms.NumericUpDown();
            this.pbResize = new Emgu.CV.UI.ImageBox();
            this.pbDefault = new Emgu.CV.UI.ImageBox();
            this.lblImgH = new System.Windows.Forms.Label();
            this.lblImgW = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.roundedPanel2 = new roundedPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnSettingParam = new roundedPanel();
            this.roundedPanel18 = new roundedPanel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.roundedPanel19 = new roundedPanel();
            this.label56 = new System.Windows.Forms.Label();
            this.chkImgH = new System.Windows.Forms.CheckBox();
            this.chkImgW = new System.Windows.Forms.CheckBox();
            this.numParam2 = new System.Windows.Forms.NumericUpDown();
            this.numParam3 = new System.Windows.Forms.NumericUpDown();
            this.btnResetParam = new System.Windows.Forms.Button();
            this.btnSetParam = new System.Windows.Forms.Button();
            this.roundedPanel16 = new roundedPanel();
            this.numParam18 = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.numParam17 = new System.Windows.Forms.NumericUpDown();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.numParam15 = new System.Windows.Forms.NumericUpDown();
            this.rbValMC = new System.Windows.Forms.RadioButton();
            this.rbPercMC = new System.Windows.Forms.RadioButton();
            this.numParam16 = new System.Windows.Forms.NumericUpDown();
            this.numParam14 = new System.Windows.Forms.NumericUpDown();
            this.label52 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.numParam13 = new System.Windows.Forms.NumericUpDown();
            this.panel12 = new System.Windows.Forms.Panel();
            this.roundedPanel17 = new roundedPanel();
            this.label59 = new System.Windows.Forms.Label();
            this.btnSaveParam = new System.Windows.Forms.Button();
            this.roundedPanel14 = new roundedPanel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.numParam11 = new System.Windows.Forms.NumericUpDown();
            this.rbVal = new System.Windows.Forms.RadioButton();
            this.rbPerc = new System.Windows.Forms.RadioButton();
            this.numParam12 = new System.Windows.Forms.NumericUpDown();
            this.numParam9 = new System.Windows.Forms.NumericUpDown();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.numParam8 = new System.Windows.Forms.NumericUpDown();
            this.numParam10 = new System.Windows.Forms.NumericUpDown();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.numParam7 = new System.Windows.Forms.NumericUpDown();
            this.panel11 = new System.Windows.Forms.Panel();
            this.roundedPanel15 = new roundedPanel();
            this.label55 = new System.Windows.Forms.Label();
            this.roundedPanel6 = new roundedPanel();
            this.label60 = new System.Windows.Forms.Label();
            this.numParam6 = new System.Windows.Forms.NumericUpDown();
            this.panel10 = new System.Windows.Forms.Panel();
            this.roundedPanel11 = new roundedPanel();
            this.label50 = new System.Windows.Forms.Label();
            this.roundedPanel8 = new roundedPanel();
            this.label46 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.numParam5 = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.numParam4 = new System.Windows.Forms.NumericUpDown();
            this.roundedPanel10 = new roundedPanel();
            this.label51 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.roundedPanel7 = new roundedPanel();
            this.btnExitParam = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.single.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.multi.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.pnRunTime.SuspendLayout();
            this.pnCurve.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibCurve2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCurveUnion2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCurve1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCurveUnion1)).BeginInit();
            this.roundedPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numThrPercC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThrValC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGapC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDistC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNeighborC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStepC)).BeginInit();
            this.roundedPanel13.SuspendLayout();
            this.pnLine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibHoughLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibHoughLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibDetLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLineUni2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibDetLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLineUni1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLineUnion2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLineUnion1)).BeginInit();
            this.roundedPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numThrPerc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNeighbor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThrVal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxGap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThetaStep)).BeginInit();
            this.roundedPanel9.SuspendLayout();
            this.pnLoop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMinArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCycleFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCycle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCycle1)).BeginInit();
            this.roundedPanel5.SuspendLayout();
            this.pnThinned.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibS2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibS1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibImgReady)).BeginInit();
            this.roundedPanel3.SuspendLayout();
            this.pnThinning.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWinH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWinW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibThinning)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibPreProcess)).BeginInit();
            this.roundedPanel4.SuspendLayout();
            this.pnImage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbResize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDefault)).BeginInit();
            this.roundedPanel2.SuspendLayout();
            this.pnSettingParam.SuspendLayout();
            this.roundedPanel18.SuspendLayout();
            this.roundedPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam3)).BeginInit();
            this.roundedPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam17)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam13)).BeginInit();
            this.roundedPanel17.SuspendLayout();
            this.roundedPanel14.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam7)).BeginInit();
            this.roundedPanel15.SuspendLayout();
            this.roundedPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam6)).BeginInit();
            this.roundedPanel11.SuspendLayout();
            this.roundedPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam4)).BeginInit();
            this.roundedPanel10.SuspendLayout();
            this.roundedPanel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // single
            // 
            this.single.Controls.Add(this.tabPage1);
            this.single.Controls.Add(this.multi);
            this.single.Location = new System.Drawing.Point(53, 0);
            this.single.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.single.Name = "single";
            this.single.SelectedIndex = 0;
            this.single.Size = new System.Drawing.Size(1812, 862);
            this.single.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.pnRunTime);
            this.tabPage1.Controls.Add(this.pnCurve);
            this.tabPage1.Controls.Add(this.pnLine);
            this.tabPage1.Controls.Add(this.pnLoop);
            this.tabPage1.Controls.Add(this.pnThinned);
            this.tabPage1.Controls.Add(this.pnThinning);
            this.tabPage1.Controls.Add(this.pnImage);
            this.tabPage1.Controls.Add(this.menuStrip1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(1804, 833);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Single Image";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filesToolStripMenuItem,
            this.imageProcessingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(4, 4);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1796, 31);
            this.menuStrip1.TabIndex = 51;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // filesToolStripMenuItem
            // 
            this.filesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadImageToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.filesToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filesToolStripMenuItem.Name = "filesToolStripMenuItem";
            this.filesToolStripMenuItem.Size = new System.Drawing.Size(57, 27);
            this.filesToolStripMenuItem.Text = "Files";
            // 
            // loadImageToolStripMenuItem
            // 
            this.loadImageToolStripMenuItem.Name = "loadImageToolStripMenuItem";
            this.loadImageToolStripMenuItem.Size = new System.Drawing.Size(174, 28);
            this.loadImageToolStripMenuItem.Text = "Load Image";
            this.loadImageToolStripMenuItem.Click += new System.EventHandler(this.loadImageToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(174, 28);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // imageProcessingToolStripMenuItem
            // 
            this.imageProcessingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thinningImageToolStripMenuItem,
            this.featureExtractionToolStripMenuItem});
            this.imageProcessingToolStripMenuItem.Enabled = false;
            this.imageProcessingToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imageProcessingToolStripMenuItem.Name = "imageProcessingToolStripMenuItem";
            this.imageProcessingToolStripMenuItem.Size = new System.Drawing.Size(161, 27);
            this.imageProcessingToolStripMenuItem.Text = "Image Processing";
            // 
            // thinningImageToolStripMenuItem
            // 
            this.thinningImageToolStripMenuItem.Name = "thinningImageToolStripMenuItem";
            this.thinningImageToolStripMenuItem.Size = new System.Drawing.Size(226, 28);
            this.thinningImageToolStripMenuItem.Text = "Thinning Image";
            this.thinningImageToolStripMenuItem.Click += new System.EventHandler(this.thinningImageToolStripMenuItem_Click);
            // 
            // featureExtractionToolStripMenuItem
            // 
            this.featureExtractionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loopDetectionToolStripMenuItem,
            this.lineDetectionToolStripMenuItem,
            this.curveDetectionToolStripMenuItem});
            this.featureExtractionToolStripMenuItem.Enabled = false;
            this.featureExtractionToolStripMenuItem.Name = "featureExtractionToolStripMenuItem";
            this.featureExtractionToolStripMenuItem.Size = new System.Drawing.Size(226, 28);
            this.featureExtractionToolStripMenuItem.Text = "Feature Extraction";
            // 
            // loopDetectionToolStripMenuItem
            // 
            this.loopDetectionToolStripMenuItem.Name = "loopDetectionToolStripMenuItem";
            this.loopDetectionToolStripMenuItem.Size = new System.Drawing.Size(210, 28);
            this.loopDetectionToolStripMenuItem.Text = "Loop Detection";
            this.loopDetectionToolStripMenuItem.Click += new System.EventHandler(this.loopDetectionToolStripMenuItem_Click);
            // 
            // lineDetectionToolStripMenuItem
            // 
            this.lineDetectionToolStripMenuItem.Name = "lineDetectionToolStripMenuItem";
            this.lineDetectionToolStripMenuItem.Size = new System.Drawing.Size(210, 28);
            this.lineDetectionToolStripMenuItem.Text = "Line Detection";
            this.lineDetectionToolStripMenuItem.Click += new System.EventHandler(this.lineDetectionToolStripMenuItem_Click);
            // 
            // curveDetectionToolStripMenuItem
            // 
            this.curveDetectionToolStripMenuItem.Name = "curveDetectionToolStripMenuItem";
            this.curveDetectionToolStripMenuItem.Size = new System.Drawing.Size(210, 28);
            this.curveDetectionToolStripMenuItem.Text = "Curve Detection";
            this.curveDetectionToolStripMenuItem.Click += new System.EventHandler(this.curveDetectionToolStripMenuItem_Click);
            // 
            // multi
            // 
            this.multi.BackColor = System.Drawing.SystemColors.Control;
            this.multi.Controls.Add(this.pnSettingParam);
            this.multi.Controls.Add(this.lblMultiResult);
            this.multi.Controls.Add(this.listMulti);
            this.multi.Controls.Add(this.listImg);
            this.multi.Controls.Add(this.menuStrip2);
            this.multi.Location = new System.Drawing.Point(4, 25);
            this.multi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.multi.Name = "multi";
            this.multi.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.multi.Size = new System.Drawing.Size(1804, 833);
            this.multi.TabIndex = 1;
            this.multi.Text = "Multiple Image";
            // 
            // lblMultiResult
            // 
            this.lblMultiResult.AutoSize = true;
            this.lblMultiResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMultiResult.ForeColor = System.Drawing.Color.White;
            this.lblMultiResult.Location = new System.Drawing.Point(23, 46);
            this.lblMultiResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMultiResult.Name = "lblMultiResult";
            this.lblMultiResult.Size = new System.Drawing.Size(23, 17);
            this.lblMultiResult.TabIndex = 17;
            this.lblMultiResult.Text = "...";
            this.lblMultiResult.Visible = false;
            // 
            // listMulti
            // 
            this.listMulti.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.no,
            this.path,
            this.loopfull,
            this.loop1,
            this.loop2,
            this.line1,
            this.coord1,
            this.rhoTheta1,
            this.line2,
            this.coord2,
            this.rhoTheta2,
            this.curve1,
            this.vertex1,
            this.curve2,
            this.vertex2,
            this.columnHeader8,
            this.columnHeader5});
            this.listMulti.Location = new System.Drawing.Point(705, 71);
            this.listMulti.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listMulti.Name = "listMulti";
            this.listMulti.Size = new System.Drawing.Size(1065, 725);
            this.listMulti.TabIndex = 15;
            this.listMulti.UseCompatibleStateImageBehavior = false;
            this.listMulti.View = System.Windows.Forms.View.Details;
            this.listMulti.Visible = false;
            // 
            // no
            // 
            this.no.Text = "No";
            this.no.Width = 30;
            // 
            // path
            // 
            this.path.Text = "Path File";
            this.path.Width = 150;
            // 
            // loopfull
            // 
            this.loopfull.Text = "Loop Full";
            this.loopfull.Width = 50;
            // 
            // loop1
            // 
            this.loop1.Text = "Loop 1";
            this.loop1.Width = 50;
            // 
            // loop2
            // 
            this.loop2.Text = "Loop 2";
            this.loop2.Width = 50;
            // 
            // line1
            // 
            this.line1.Text = "Line 1";
            this.line1.Width = 50;
            // 
            // coord1
            // 
            this.coord1.Text = "Koordinat Titik Ujung Garis";
            this.coord1.Width = 50;
            // 
            // rhoTheta1
            // 
            this.rhoTheta1.Text = "Rho, Theta";
            this.rhoTheta1.Width = 50;
            // 
            // line2
            // 
            this.line2.Text = "Line 2";
            this.line2.Width = 50;
            // 
            // coord2
            // 
            this.coord2.Text = "Koordinat Titik Ujung Garis";
            this.coord2.Width = 50;
            // 
            // rhoTheta2
            // 
            this.rhoTheta2.Text = "Rho, Theta";
            this.rhoTheta2.Width = 50;
            // 
            // curve1
            // 
            this.curve1.Text = "Curve 1";
            this.curve1.Width = 50;
            // 
            // vertex1
            // 
            this.vertex1.Text = "Koordinat Vertex, Orientasi";
            this.vertex1.Width = 50;
            // 
            // curve2
            // 
            this.curve2.Text = "Curve 2";
            this.curve2.Width = 50;
            // 
            // vertex2
            // 
            this.vertex2.Text = "Koordinat Vertex, Orientasi";
            this.vertex2.Width = 50;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Suggestion";
            this.columnHeader8.Width = 200;
            // 
            // listImg
            // 
            this.listImg.Location = new System.Drawing.Point(27, 71);
            this.listImg.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listImg.Name = "listImg";
            this.listImg.Size = new System.Drawing.Size(652, 725);
            this.listImg.TabIndex = 13;
            this.listImg.UseCompatibleStateImageBehavior = false;
            this.listImg.Visible = false;
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingParameterToolStripMenuItem,
            this.processImagesToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(4, 4);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip2.Size = new System.Drawing.Size(1796, 31);
            this.menuStrip2.TabIndex = 12;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadImagesToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 27);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadImagesToolStripMenuItem
            // 
            this.loadImagesToolStripMenuItem.Name = "loadImagesToolStripMenuItem";
            this.loadImagesToolStripMenuItem.Size = new System.Drawing.Size(181, 28);
            this.loadImagesToolStripMenuItem.Text = "Load Images";
            this.loadImagesToolStripMenuItem.Click += new System.EventHandler(this.loadImagesToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(181, 28);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // settingParameterToolStripMenuItem
            // 
            this.settingParameterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewParameterToolStripMenuItem,
            this.loadParameterToolStripMenuItem});
            this.settingParameterToolStripMenuItem.Name = "settingParameterToolStripMenuItem";
            this.settingParameterToolStripMenuItem.Size = new System.Drawing.Size(169, 27);
            this.settingParameterToolStripMenuItem.Text = "Setting Parameter";
            // 
            // addNewParameterToolStripMenuItem
            // 
            this.addNewParameterToolStripMenuItem.Name = "addNewParameterToolStripMenuItem";
            this.addNewParameterToolStripMenuItem.Size = new System.Drawing.Size(243, 28);
            this.addNewParameterToolStripMenuItem.Text = "Add New Parameter";
            this.addNewParameterToolStripMenuItem.Click += new System.EventHandler(this.addNewParameterToolStripMenuItem_Click);
            // 
            // loadParameterToolStripMenuItem
            // 
            this.loadParameterToolStripMenuItem.Name = "loadParameterToolStripMenuItem";
            this.loadParameterToolStripMenuItem.Size = new System.Drawing.Size(243, 28);
            this.loadParameterToolStripMenuItem.Text = "Load Parameter";
            this.loadParameterToolStripMenuItem.Click += new System.EventHandler(this.loadParameterToolStripMenuItem_Click);
            // 
            // processImagesToolStripMenuItem
            // 
            this.processImagesToolStripMenuItem.Enabled = false;
            this.processImagesToolStripMenuItem.Name = "processImagesToolStripMenuItem";
            this.processImagesToolStripMenuItem.Size = new System.Drawing.Size(142, 27);
            this.processImagesToolStripMenuItem.Text = "Process Images";
            this.processImagesToolStripMenuItem.Click += new System.EventHandler(this.processImagesToolStripMenuItem_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.Tag = "";
            // 
            // pnRunTime
            // 
            this.pnRunTime.BackColor = System.Drawing.Color.LightGray;
            this.pnRunTime.Controls.Add(this.lblRunTime);
            this.pnRunTime.Location = new System.Drawing.Point(8, 782);
            this.pnRunTime.Margin = new System.Windows.Forms.Padding(4);
            this.pnRunTime.Name = "pnRunTime";
            this.pnRunTime.Size = new System.Drawing.Size(536, 37);
            this.pnRunTime.TabIndex = 59;
            this.pnRunTime.Visible = false;
            // 
            // lblRunTime
            // 
            this.lblRunTime.AutoSize = true;
            this.lblRunTime.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRunTime.ForeColor = System.Drawing.Color.Black;
            this.lblRunTime.Location = new System.Drawing.Point(24, 7);
            this.lblRunTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRunTime.Name = "lblRunTime";
            this.lblRunTime.Size = new System.Drawing.Size(33, 25);
            this.lblRunTime.TabIndex = 57;
            this.lblRunTime.Text = "...";
            // 
            // pnCurve
            // 
            this.pnCurve.BackColor = System.Drawing.Color.SandyBrown;
            this.pnCurve.Controls.Add(this.label41);
            this.pnCurve.Controls.Add(this.ibCurve2);
            this.pnCurve.Controls.Add(this.ibCurveUnion2);
            this.pnCurve.Controls.Add(this.label42);
            this.pnCurve.Controls.Add(this.ibCurve1);
            this.pnCurve.Controls.Add(this.ibCurveUnion1);
            this.pnCurve.Controls.Add(this.roundedPanel12);
            this.pnCurve.Controls.Add(this.panel6);
            this.pnCurve.Controls.Add(this.roundedPanel13);
            this.pnCurve.Location = new System.Drawing.Point(955, 89);
            this.pnCurve.Margin = new System.Windows.Forms.Padding(4);
            this.pnCurve.Name = "pnCurve";
            this.pnCurve.Size = new System.Drawing.Size(1240, 775);
            this.pnCurve.TabIndex = 55;
            this.pnCurve.Visible = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(655, 260);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(72, 17);
            this.label41.TabIndex = 93;
            this.label41.Text = "Segmen 2";
            // 
            // ibCurve2
            // 
            this.ibCurve2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibCurve2.BackColor = System.Drawing.Color.White;
            this.ibCurve2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibCurve2.Location = new System.Drawing.Point(860, 279);
            this.ibCurve2.Margin = new System.Windows.Forms.Padding(4);
            this.ibCurve2.Name = "ibCurve2";
            this.ibCurve2.Size = new System.Drawing.Size(200, 123);
            this.ibCurve2.TabIndex = 92;
            this.ibCurve2.TabStop = false;
            // 
            // ibCurveUnion2
            // 
            this.ibCurveUnion2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibCurveUnion2.BackColor = System.Drawing.Color.White;
            this.ibCurveUnion2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibCurveUnion2.Location = new System.Drawing.Point(652, 279);
            this.ibCurveUnion2.Margin = new System.Windows.Forms.Padding(4);
            this.ibCurveUnion2.Name = "ibCurveUnion2";
            this.ibCurveUnion2.Size = new System.Drawing.Size(200, 123);
            this.ibCurveUnion2.TabIndex = 91;
            this.ibCurveUnion2.TabStop = false;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(655, 96);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(72, 17);
            this.label42.TabIndex = 90;
            this.label42.Text = "Segmen 1";
            // 
            // ibCurve1
            // 
            this.ibCurve1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibCurve1.BackColor = System.Drawing.Color.White;
            this.ibCurve1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibCurve1.Location = new System.Drawing.Point(860, 116);
            this.ibCurve1.Margin = new System.Windows.Forms.Padding(4);
            this.ibCurve1.Name = "ibCurve1";
            this.ibCurve1.Size = new System.Drawing.Size(200, 123);
            this.ibCurve1.TabIndex = 89;
            this.ibCurve1.TabStop = false;
            // 
            // ibCurveUnion1
            // 
            this.ibCurveUnion1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibCurveUnion1.BackColor = System.Drawing.Color.White;
            this.ibCurveUnion1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibCurveUnion1.Location = new System.Drawing.Point(652, 116);
            this.ibCurveUnion1.Margin = new System.Windows.Forms.Padding(4);
            this.ibCurveUnion1.Name = "ibCurveUnion1";
            this.ibCurveUnion1.Size = new System.Drawing.Size(200, 123);
            this.ibCurveUnion1.TabIndex = 88;
            this.ibCurveUnion1.TabStop = false;
            // 
            // roundedPanel12
            // 
            this.roundedPanel12.BackColor = System.Drawing.Color.NavajoWhite;
            this.roundedPanel12.Controls.Add(this.label15);
            this.roundedPanel12.Controls.Add(this.numThrPercC);
            this.roundedPanel12.Controls.Add(this.rbValC);
            this.roundedPanel12.Controls.Add(this.rbPercC);
            this.roundedPanel12.Controls.Add(this.numThrValC);
            this.roundedPanel12.Controls.Add(this.numGapC);
            this.roundedPanel12.Controls.Add(this.label58);
            this.roundedPanel12.Controls.Add(this.numDistC);
            this.roundedPanel12.Controls.Add(this.label57);
            this.roundedPanel12.Controls.Add(this.listCurve);
            this.roundedPanel12.Controls.Add(this.label14);
            this.roundedPanel12.Controls.Add(this.numNeighborC);
            this.roundedPanel12.Controls.Add(this.label16);
            this.roundedPanel12.Controls.Add(this.label34);
            this.roundedPanel12.Controls.Add(this.label35);
            this.roundedPanel12.Controls.Add(this.numStepC);
            this.roundedPanel12.Controls.Add(this.btnCurve);
            this.roundedPanel12.Location = new System.Drawing.Point(20, 73);
            this.roundedPanel12.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel12.Name = "roundedPanel12";
            this.roundedPanel12.Size = new System.Drawing.Size(600, 676);
            this.roundedPanel12.TabIndex = 68;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(24, 166);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 24);
            this.label15.TabIndex = 94;
            this.label15.Text = "Threshold";
            // 
            // numThrPercC
            // 
            this.numThrPercC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numThrPercC.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numThrPercC.Location = new System.Drawing.Point(325, 164);
            this.numThrPercC.Margin = new System.Windows.Forms.Padding(4);
            this.numThrPercC.Name = "numThrPercC";
            this.numThrPercC.Size = new System.Drawing.Size(80, 29);
            this.numThrPercC.TabIndex = 93;
            this.toolTip1.SetToolTip(this.numThrPercC, "nilai minimum dalam Accumulator Array yang dianggap garis (% terhadap nilai maks " +
        "dalam accumulator array)");
            this.numThrPercC.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // rbValC
            // 
            this.rbValC.AutoSize = true;
            this.rbValC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.rbValC.Location = new System.Drawing.Point(147, 198);
            this.rbValC.Margin = new System.Windows.Forms.Padding(4);
            this.rbValC.Name = "rbValC";
            this.rbValC.Size = new System.Drawing.Size(106, 28);
            this.rbValC.TabIndex = 92;
            this.rbValC.Text = "By Value";
            this.rbValC.UseVisualStyleBackColor = true;
            this.rbValC.CheckedChanged += new System.EventHandler(this.rbValC_CheckedChanged);
            // 
            // rbPercC
            // 
            this.rbPercC.AutoSize = true;
            this.rbPercC.Checked = true;
            this.rbPercC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.rbPercC.Location = new System.Drawing.Point(147, 164);
            this.rbPercC.Margin = new System.Windows.Forms.Padding(4);
            this.rbPercC.Name = "rbPercC";
            this.rbPercC.Size = new System.Drawing.Size(154, 28);
            this.rbPercC.TabIndex = 91;
            this.rbPercC.TabStop = true;
            this.rbPercC.Text = "By Percentage";
            this.rbPercC.UseVisualStyleBackColor = true;
            this.rbPercC.CheckedChanged += new System.EventHandler(this.rbPercC_CheckedChanged);
            // 
            // numThrValC
            // 
            this.numThrValC.Enabled = false;
            this.numThrValC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numThrValC.Location = new System.Drawing.Point(325, 198);
            this.numThrValC.Margin = new System.Windows.Forms.Padding(4);
            this.numThrValC.Name = "numThrValC";
            this.numThrValC.Size = new System.Drawing.Size(80, 29);
            this.numThrValC.TabIndex = 90;
            this.toolTip1.SetToolTip(this.numThrValC, "nilai minimum dalam Accumulator Array yang dianggap garis (fixed value, dalam sat" +
        "uan pixel)");
            this.numThrValC.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // numGapC
            // 
            this.numGapC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numGapC.Location = new System.Drawing.Point(493, 121);
            this.numGapC.Margin = new System.Windows.Forms.Padding(4);
            this.numGapC.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numGapC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numGapC.Name = "numGapC";
            this.numGapC.Size = new System.Drawing.Size(80, 29);
            this.numGapC.TabIndex = 87;
            this.toolTip1.SetToolTip(this.numGapC, "Beda sudut minium antar kurva");
            this.numGapC.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(316, 123);
            this.label58.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(136, 24);
            this.label58.TabIndex = 86;
            this.label58.Text = "Min Gap Angle";
            // 
            // numDistC
            // 
            this.numDistC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numDistC.Location = new System.Drawing.Point(493, 75);
            this.numDistC.Margin = new System.Windows.Forms.Padding(4);
            this.numDistC.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numDistC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numDistC.Name = "numDistC";
            this.numDistC.Size = new System.Drawing.Size(80, 29);
            this.numDistC.TabIndex = 85;
            this.toolTip1.SetToolTip(this.numDistC, "Jarak minimum antar kurva");
            this.numDistC.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(316, 78);
            this.label57.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(118, 24);
            this.label57.TabIndex = 84;
            this.label57.Text = "Min Distance";
            // 
            // listCurve
            // 
            this.listCurve.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listCurve.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listCurve.GridLines = true;
            listViewGroup9.Header = "Segmen 1";
            listViewGroup9.Name = "Segmen1";
            listViewGroup10.Header = "Segmen 2";
            listViewGroup10.Name = "Segmen2";
            listViewGroup11.Header = "Segmen 3";
            listViewGroup11.Name = "Segmen3";
            listViewGroup12.Header = "Segmen 4";
            listViewGroup12.Name = "Segmen4";
            this.listCurve.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup9,
            listViewGroup10,
            listViewGroup11,
            listViewGroup12});
            this.listCurve.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listCurve.Location = new System.Drawing.Point(20, 308);
            this.listCurve.Margin = new System.Windows.Forms.Padding(4);
            this.listCurve.Name = "listCurve";
            this.listCurve.Size = new System.Drawing.Size(559, 344);
            this.listCurve.TabIndex = 83;
            this.listCurve.UseCompatibleStateImageBehavior = false;
            this.listCurve.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Number";
            this.columnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Xo";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Yo";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Angle";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(24, 288);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 17);
            this.label14.TabIndex = 81;
            this.label14.Text = "Result";
            // 
            // numNeighborC
            // 
            this.numNeighborC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numNeighborC.Location = new System.Drawing.Point(201, 121);
            this.numNeighborC.Margin = new System.Windows.Forms.Padding(4);
            this.numNeighborC.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numNeighborC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numNeighborC.Name = "numNeighborC";
            this.numNeighborC.Size = new System.Drawing.Size(80, 29);
            this.numNeighborC.TabIndex = 11;
            this.toolTip1.SetToolTip(this.numNeighborC, "nilai N x N tetangga dalam menentukan local maxima");
            this.numNeighborC.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(24, 123);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(143, 24);
            this.label16.TabIndex = 75;
            this.label16.Text = "Neighbor Value";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(24, 75);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(101, 24);
            this.label34.TabIndex = 72;
            this.label34.Text = "Theta Step";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(23, 20);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(187, 29);
            this.label35.TabIndex = 71;
            this.label35.Text = "PARAMETERS";
            // 
            // numStepC
            // 
            this.numStepC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numStepC.Location = new System.Drawing.Point(201, 75);
            this.numStepC.Margin = new System.Windows.Forms.Padding(4);
            this.numStepC.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numStepC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numStepC.Name = "numStepC";
            this.numStepC.Size = new System.Drawing.Size(80, 29);
            this.numStepC.TabIndex = 10;
            this.toolTip1.SetToolTip(this.numStepC, "Nilai hitung setiap n derajat theta");
            this.numStepC.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // btnCurve
            // 
            this.btnCurve.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCurve.Location = new System.Drawing.Point(201, 246);
            this.btnCurve.Margin = new System.Windows.Forms.Padding(4);
            this.btnCurve.Name = "btnCurve";
            this.btnCurve.Size = new System.Drawing.Size(171, 41);
            this.btnCurve.TabIndex = 68;
            this.btnCurve.Text = "Detect Curve";
            this.btnCurve.UseVisualStyleBackColor = true;
            this.btnCurve.Click += new System.EventHandler(this.btnCurve_Click);
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(-1, 54);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1243, 12);
            this.panel6.TabIndex = 50;
            // 
            // roundedPanel13
            // 
            this.roundedPanel13.BackColor = System.Drawing.Color.Chocolate;
            this.roundedPanel13.Controls.Add(this.label36);
            this.roundedPanel13.Location = new System.Drawing.Point(-1, 0);
            this.roundedPanel13.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel13.Name = "roundedPanel13";
            this.roundedPanel13.Size = new System.Drawing.Size(1243, 62);
            this.roundedPanel13.TabIndex = 50;
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(539, 15);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(209, 29);
            this.label36.TabIndex = 50;
            this.label36.Text = "CURVE DETECTION";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnLine
            // 
            this.pnLine.BackColor = System.Drawing.Color.Violet;
            this.pnLine.Controls.Add(this.label37);
            this.pnLine.Controls.Add(this.label30);
            this.pnLine.Controls.Add(this.label25);
            this.pnLine.Controls.Add(this.label24);
            this.pnLine.Controls.Add(this.ibHoughLine2);
            this.pnLine.Controls.Add(this.ibHoughLine1);
            this.pnLine.Controls.Add(this.label7);
            this.pnLine.Controls.Add(this.label8);
            this.pnLine.Controls.Add(this.ibDetLine2);
            this.pnLine.Controls.Add(this.ibLineUni2);
            this.pnLine.Controls.Add(this.ibDetLine1);
            this.pnLine.Controls.Add(this.ibLineUni1);
            this.pnLine.Controls.Add(this.groupBox3);
            this.pnLine.Controls.Add(this.roundedPanel1);
            this.pnLine.Controls.Add(this.panel5);
            this.pnLine.Controls.Add(this.roundedPanel9);
            this.pnLine.Location = new System.Drawing.Point(551, 438);
            this.pnLine.Margin = new System.Windows.Forms.Padding(4);
            this.pnLine.Name = "pnLine";
            this.pnLine.Size = new System.Drawing.Size(1240, 784);
            this.pnLine.TabIndex = 54;
            this.pnLine.Visible = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(860, 236);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(103, 17);
            this.label37.TabIndex = 101;
            this.label37.Text = "Detected Lines";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(860, 73);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(103, 17);
            this.label30.TabIndex = 100;
            this.label30.Text = "Detected Lines";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(1117, 236);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(88, 17);
            this.label25.TabIndex = 99;
            this.label25.Text = "Hough Array";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(1117, 73);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(88, 17);
            this.label24.TabIndex = 98;
            this.label24.Text = "Hough Array";
            // 
            // ibHoughLine2
            // 
            this.ibHoughLine2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibHoughLine2.BackColor = System.Drawing.Color.White;
            this.ibHoughLine2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibHoughLine2.Location = new System.Drawing.Point(1019, 256);
            this.ibHoughLine2.Margin = new System.Windows.Forms.Padding(4);
            this.ibHoughLine2.Name = "ibHoughLine2";
            this.ibHoughLine2.Size = new System.Drawing.Size(187, 135);
            this.ibHoughLine2.TabIndex = 97;
            this.ibHoughLine2.TabStop = false;
            // 
            // ibHoughLine1
            // 
            this.ibHoughLine1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibHoughLine1.BackColor = System.Drawing.Color.White;
            this.ibHoughLine1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibHoughLine1.Location = new System.Drawing.Point(1019, 92);
            this.ibHoughLine1.Margin = new System.Windows.Forms.Padding(4);
            this.ibHoughLine1.Name = "ibHoughLine1";
            this.ibHoughLine1.Size = new System.Drawing.Size(187, 135);
            this.ibHoughLine1.TabIndex = 96;
            this.ibHoughLine1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(628, 238);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 17);
            this.label7.TabIndex = 95;
            this.label7.Text = "Segmen 2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(628, 73);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 17);
            this.label8.TabIndex = 94;
            this.label8.Text = "Segmen 1";
            // 
            // ibDetLine2
            // 
            this.ibDetLine2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibDetLine2.BackColor = System.Drawing.Color.White;
            this.ibDetLine2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibDetLine2.Location = new System.Drawing.Point(824, 256);
            this.ibDetLine2.Margin = new System.Windows.Forms.Padding(4);
            this.ibDetLine2.Name = "ibDetLine2";
            this.ibDetLine2.Size = new System.Drawing.Size(187, 135);
            this.ibDetLine2.TabIndex = 93;
            this.ibDetLine2.TabStop = false;
            // 
            // ibLineUni2
            // 
            this.ibLineUni2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibLineUni2.BackColor = System.Drawing.Color.White;
            this.ibLineUni2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibLineUni2.Location = new System.Drawing.Point(629, 256);
            this.ibLineUni2.Margin = new System.Windows.Forms.Padding(4);
            this.ibLineUni2.Name = "ibLineUni2";
            this.ibLineUni2.Size = new System.Drawing.Size(187, 135);
            this.ibLineUni2.TabIndex = 92;
            this.ibLineUni2.TabStop = false;
            // 
            // ibDetLine1
            // 
            this.ibDetLine1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibDetLine1.BackColor = System.Drawing.Color.White;
            this.ibDetLine1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibDetLine1.Location = new System.Drawing.Point(824, 92);
            this.ibDetLine1.Margin = new System.Windows.Forms.Padding(4);
            this.ibDetLine1.Name = "ibDetLine1";
            this.ibDetLine1.Size = new System.Drawing.Size(187, 135);
            this.ibDetLine1.TabIndex = 91;
            this.ibDetLine1.TabStop = false;
            // 
            // ibLineUni1
            // 
            this.ibLineUni1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibLineUni1.BackColor = System.Drawing.Color.White;
            this.ibLineUni1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibLineUni1.Location = new System.Drawing.Point(629, 92);
            this.ibLineUni1.Margin = new System.Windows.Forms.Padding(4);
            this.ibLineUni1.Name = "ibLineUni1";
            this.ibLineUni1.Size = new System.Drawing.Size(187, 135);
            this.ibLineUni1.TabIndex = 90;
            this.ibLineUni1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.ibLine2);
            this.groupBox3.Controls.Add(this.ibLineUnion2);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.ibLine1);
            this.groupBox3.Controls.Add(this.ibLineUnion1);
            this.groupBox3.Location = new System.Drawing.Point(683, 410);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(457, 353);
            this.groupBox3.TabIndex = 88;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Hasil Library";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(43, 183);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(72, 17);
            this.label20.TabIndex = 81;
            this.label20.Text = "Segmen 2";
            // 
            // ibLine2
            // 
            this.ibLine2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibLine2.BackColor = System.Drawing.Color.White;
            this.ibLine2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibLine2.Location = new System.Drawing.Point(235, 203);
            this.ibLine2.Margin = new System.Windows.Forms.Padding(4);
            this.ibLine2.Name = "ibLine2";
            this.ibLine2.Size = new System.Drawing.Size(187, 135);
            this.ibLine2.TabIndex = 80;
            this.ibLine2.TabStop = false;
            // 
            // ibLineUnion2
            // 
            this.ibLineUnion2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibLineUnion2.BackColor = System.Drawing.Color.White;
            this.ibLineUnion2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibLineUnion2.Location = new System.Drawing.Point(40, 203);
            this.ibLineUnion2.Margin = new System.Windows.Forms.Padding(4);
            this.ibLineUnion2.Name = "ibLineUnion2";
            this.ibLineUnion2.Size = new System.Drawing.Size(187, 135);
            this.ibLineUnion2.TabIndex = 79;
            this.ibLineUnion2.TabStop = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(43, 20);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 17);
            this.label19.TabIndex = 78;
            this.label19.Text = "Segmen 1";
            // 
            // ibLine1
            // 
            this.ibLine1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibLine1.BackColor = System.Drawing.Color.White;
            this.ibLine1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibLine1.Location = new System.Drawing.Point(235, 38);
            this.ibLine1.Margin = new System.Windows.Forms.Padding(4);
            this.ibLine1.Name = "ibLine1";
            this.ibLine1.Size = new System.Drawing.Size(187, 135);
            this.ibLine1.TabIndex = 77;
            this.ibLine1.TabStop = false;
            // 
            // ibLineUnion1
            // 
            this.ibLineUnion1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibLineUnion1.BackColor = System.Drawing.Color.White;
            this.ibLineUnion1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibLineUnion1.Location = new System.Drawing.Point(40, 39);
            this.ibLineUnion1.Margin = new System.Windows.Forms.Padding(4);
            this.ibLineUnion1.Name = "ibLineUnion1";
            this.ibLineUnion1.Size = new System.Drawing.Size(187, 135);
            this.ibLineUnion1.TabIndex = 76;
            this.ibLineUnion1.TabStop = false;
            // 
            // roundedPanel1
            // 
            this.roundedPanel1.BackColor = System.Drawing.Color.PowderBlue;
            this.roundedPanel1.Controls.Add(this.numThrPerc);
            this.roundedPanel1.Controls.Add(this.rbPilThrVal);
            this.roundedPanel1.Controls.Add(this.rbPilThrPer);
            this.roundedPanel1.Controls.Add(this.numMinLength);
            this.roundedPanel1.Controls.Add(this.label18);
            this.roundedPanel1.Controls.Add(this.label13);
            this.roundedPanel1.Controls.Add(this.numNeighbor);
            this.roundedPanel1.Controls.Add(this.listLine);
            this.roundedPanel1.Controls.Add(this.label33);
            this.roundedPanel1.Controls.Add(this.numThrVal);
            this.roundedPanel1.Controls.Add(this.label32);
            this.roundedPanel1.Controls.Add(this.numMaxGap);
            this.roundedPanel1.Controls.Add(this.label31);
            this.roundedPanel1.Controls.Add(this.label29);
            this.roundedPanel1.Controls.Add(this.label28);
            this.roundedPanel1.Controls.Add(this.numThetaStep);
            this.roundedPanel1.Controls.Add(this.btnHT);
            this.roundedPanel1.Location = new System.Drawing.Point(20, 73);
            this.roundedPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel1.Name = "roundedPanel1";
            this.roundedPanel1.Size = new System.Drawing.Size(600, 676);
            this.roundedPanel1.TabIndex = 68;
            // 
            // numThrPerc
            // 
            this.numThrPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numThrPerc.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numThrPerc.Location = new System.Drawing.Point(335, 160);
            this.numThrPerc.Margin = new System.Windows.Forms.Padding(4);
            this.numThrPerc.Name = "numThrPerc";
            this.numThrPerc.Size = new System.Drawing.Size(80, 29);
            this.numThrPerc.TabIndex = 89;
            this.toolTip1.SetToolTip(this.numThrPerc, "nilai minimum dalam Accumulator Array yang dianggap garis (% terhadap nilai maks " +
        "dalam accumulator array)");
            this.numThrPerc.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // rbPilThrVal
            // 
            this.rbPilThrVal.AutoSize = true;
            this.rbPilThrVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.rbPilThrVal.Location = new System.Drawing.Point(156, 194);
            this.rbPilThrVal.Margin = new System.Windows.Forms.Padding(4);
            this.rbPilThrVal.Name = "rbPilThrVal";
            this.rbPilThrVal.Size = new System.Drawing.Size(106, 28);
            this.rbPilThrVal.TabIndex = 88;
            this.rbPilThrVal.Text = "By Value";
            this.rbPilThrVal.UseVisualStyleBackColor = true;
            this.rbPilThrVal.CheckedChanged += new System.EventHandler(this.rbPilThrVal_CheckedChanged);
            // 
            // rbPilThrPer
            // 
            this.rbPilThrPer.AutoSize = true;
            this.rbPilThrPer.Checked = true;
            this.rbPilThrPer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.rbPilThrPer.Location = new System.Drawing.Point(156, 160);
            this.rbPilThrPer.Margin = new System.Windows.Forms.Padding(4);
            this.rbPilThrPer.Name = "rbPilThrPer";
            this.rbPilThrPer.Size = new System.Drawing.Size(154, 28);
            this.rbPilThrPer.TabIndex = 87;
            this.rbPilThrPer.TabStop = true;
            this.rbPilThrPer.Text = "By Percentage";
            this.rbPilThrPer.UseVisualStyleBackColor = true;
            this.rbPilThrPer.CheckedChanged += new System.EventHandler(this.rbPilThrPer_CheckedChanged);
            // 
            // numMinLength
            // 
            this.numMinLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numMinLength.Location = new System.Drawing.Point(489, 71);
            this.numMinLength.Margin = new System.Windows.Forms.Padding(4);
            this.numMinLength.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMinLength.Name = "numMinLength";
            this.numMinLength.Size = new System.Drawing.Size(80, 29);
            this.numMinLength.TabIndex = 85;
            this.toolTip1.SetToolTip(this.numMinLength, "minimum nilai panjang garis");
            this.numMinLength.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(312, 74);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(145, 24);
            this.label18.TabIndex = 86;
            this.label18.Text = "Min Line Length";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(25, 117);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(163, 24);
            this.label13.TabIndex = 84;
            this.label13.Text = "Neighbor Window";
            // 
            // numNeighbor
            // 
            this.numNeighbor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numNeighbor.Location = new System.Drawing.Point(201, 114);
            this.numNeighbor.Margin = new System.Windows.Forms.Padding(4);
            this.numNeighbor.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numNeighbor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numNeighbor.Name = "numNeighbor";
            this.numNeighbor.Size = new System.Drawing.Size(80, 29);
            this.numNeighbor.TabIndex = 83;
            this.toolTip1.SetToolTip(this.numNeighbor, "Nilai  NxN window untuk  menghitung Local Maxima");
            this.numNeighbor.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // listLine
            // 
            this.listLine.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Nomor,
            this.P1X,
            this.P1Y,
            this.P2X,
            this.P2Y,
            this.rho,
            this.theta});
            this.listLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listLine.GridLines = true;
            listViewGroup13.Header = "Segmen 1";
            listViewGroup13.Name = "Segmen1";
            listViewGroup14.Header = "Segmen 2";
            listViewGroup14.Name = "Segmen2";
            listViewGroup15.Header = "Segmen 1 - Library";
            listViewGroup15.Name = "Segmen3";
            listViewGroup16.Header = "Segmen 2 - Library";
            listViewGroup16.Name = "Segmen4";
            this.listLine.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup13,
            listViewGroup14,
            listViewGroup15,
            listViewGroup16});
            this.listLine.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listLine.Location = new System.Drawing.Point(20, 313);
            this.listLine.Margin = new System.Windows.Forms.Padding(4);
            this.listLine.Name = "listLine";
            this.listLine.Size = new System.Drawing.Size(559, 344);
            this.listLine.TabIndex = 82;
            this.listLine.UseCompatibleStateImageBehavior = false;
            this.listLine.View = System.Windows.Forms.View.Details;
            // 
            // Nomor
            // 
            this.Nomor.Text = "No";
            this.Nomor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Nomor.Width = 40;
            // 
            // P1X
            // 
            this.P1X.Text = "X1";
            this.P1X.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P1Y
            // 
            this.P1Y.Text = "Y1";
            this.P1Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P2X
            // 
            this.P2X.Text = "X2";
            this.P2X.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P2Y
            // 
            this.P2Y.Text = "Y2";
            this.P2Y.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // rho
            // 
            this.rho.Text = "Rho";
            this.rho.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // theta
            // 
            this.theta.Text = "Theta";
            this.theta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(24, 293);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(48, 17);
            this.label33.TabIndex = 81;
            this.label33.Text = "Result";
            // 
            // numThrVal
            // 
            this.numThrVal.Enabled = false;
            this.numThrVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numThrVal.Location = new System.Drawing.Point(335, 194);
            this.numThrVal.Margin = new System.Windows.Forms.Padding(4);
            this.numThrVal.Name = "numThrVal";
            this.numThrVal.Size = new System.Drawing.Size(80, 29);
            this.numThrVal.TabIndex = 80;
            this.toolTip1.SetToolTip(this.numThrVal, "nilai minimum dalam Accumulator Array yang dianggap garis (fixed value, dalam sat" +
        "uan pixel)");
            this.numThrVal.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(24, 160);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(96, 24);
            this.label32.TabIndex = 76;
            this.label32.Text = "Threshold";
            // 
            // numMaxGap
            // 
            this.numMaxGap.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numMaxGap.Location = new System.Drawing.Point(489, 114);
            this.numMaxGap.Margin = new System.Windows.Forms.Padding(4);
            this.numMaxGap.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMaxGap.Name = "numMaxGap";
            this.numMaxGap.Size = new System.Drawing.Size(80, 29);
            this.numMaxGap.TabIndex = 11;
            this.toolTip1.SetToolTip(this.numMaxGap, "maksimum jarak antar garis");
            this.numMaxGap.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(312, 117);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(127, 24);
            this.label31.TabIndex = 75;
            this.label31.Text = "Max Line Gap";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(24, 74);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(152, 24);
            this.label29.TabIndex = 72;
            this.label29.Text = "Theta Resolution";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(23, 20);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(187, 29);
            this.label28.TabIndex = 71;
            this.label28.Text = "PARAMETERS";
            // 
            // numThetaStep
            // 
            this.numThetaStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numThetaStep.Location = new System.Drawing.Point(201, 71);
            this.numThetaStep.Margin = new System.Windows.Forms.Padding(4);
            this.numThetaStep.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numThetaStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numThetaStep.Name = "numThetaStep";
            this.numThetaStep.Size = new System.Drawing.Size(80, 29);
            this.numThetaStep.TabIndex = 10;
            this.toolTip1.SetToolTip(this.numThetaStep, "Nilai hitung setiap n derajat theta (degrees)");
            this.numThetaStep.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnHT
            // 
            this.btnHT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHT.Location = new System.Drawing.Point(201, 250);
            this.btnHT.Margin = new System.Windows.Forms.Padding(4);
            this.btnHT.Name = "btnHT";
            this.btnHT.Size = new System.Drawing.Size(171, 41);
            this.btnHT.TabIndex = 68;
            this.btnHT.Text = "Detect Line";
            this.btnHT.UseVisualStyleBackColor = true;
            this.btnHT.Click += new System.EventHandler(this.btnHT_Click);
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(-1, 54);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1243, 12);
            this.panel5.TabIndex = 50;
            // 
            // roundedPanel9
            // 
            this.roundedPanel9.BackColor = System.Drawing.Color.Purple;
            this.roundedPanel9.Controls.Add(this.label27);
            this.roundedPanel9.Location = new System.Drawing.Point(-1, -1);
            this.roundedPanel9.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel9.Name = "roundedPanel9";
            this.roundedPanel9.Size = new System.Drawing.Size(1243, 62);
            this.roundedPanel9.TabIndex = 50;
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(539, 15);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(193, 29);
            this.label27.TabIndex = 50;
            this.label27.Text = "LINE DETECTION";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnLoop
            // 
            this.pnLoop.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.pnLoop.Controls.Add(this.label43);
            this.pnLoop.Controls.Add(this.numMinArea);
            this.pnLoop.Controls.Add(this.label12);
            this.pnLoop.Controls.Add(this.ibCycleFull);
            this.pnLoop.Controls.Add(this.ibCycle2);
            this.pnLoop.Controls.Add(this.ibCycle1);
            this.pnLoop.Controls.Add(this.tbCycle);
            this.pnLoop.Controls.Add(this.panel4);
            this.pnLoop.Controls.Add(this.btnLoop);
            this.pnLoop.Controls.Add(this.label6);
            this.pnLoop.Controls.Add(this.label5);
            this.pnLoop.Controls.Add(this.roundedPanel5);
            this.pnLoop.Location = new System.Drawing.Point(651, 681);
            this.pnLoop.Margin = new System.Windows.Forms.Padding(4);
            this.pnLoop.Name = "pnLoop";
            this.pnLoop.Size = new System.Drawing.Size(533, 775);
            this.pnLoop.TabIndex = 53;
            this.pnLoop.Visible = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(124, 78);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(133, 24);
            this.label43.TabIndex = 86;
            this.label43.Text = "Minimum Area";
            // 
            // numMinArea
            // 
            this.numMinArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numMinArea.Location = new System.Drawing.Point(301, 75);
            this.numMinArea.Margin = new System.Windows.Forms.Padding(4);
            this.numMinArea.Name = "numMinArea";
            this.numMinArea.Size = new System.Drawing.Size(80, 29);
            this.numMinArea.TabIndex = 85;
            this.toolTip1.SetToolTip(this.numMinArea, "Nilai minimal luas cycle yang dapat di deteksi");
            this.numMinArea.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(124, 263);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 17);
            this.label12.TabIndex = 75;
            this.label12.Text = "Full Image";
            // 
            // ibCycleFull
            // 
            this.ibCycleFull.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibCycleFull.BackColor = System.Drawing.Color.White;
            this.ibCycleFull.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibCycleFull.Location = new System.Drawing.Point(100, 283);
            this.ibCycleFull.Margin = new System.Windows.Forms.Padding(4);
            this.ibCycleFull.Name = "ibCycleFull";
            this.ibCycleFull.Size = new System.Drawing.Size(133, 123);
            this.ibCycleFull.TabIndex = 72;
            this.ibCycleFull.TabStop = false;
            // 
            // ibCycle2
            // 
            this.ibCycle2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibCycle2.BackColor = System.Drawing.Color.White;
            this.ibCycle2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibCycle2.Location = new System.Drawing.Point(300, 364);
            this.ibCycle2.Margin = new System.Windows.Forms.Padding(4);
            this.ibCycle2.Name = "ibCycle2";
            this.ibCycle2.Size = new System.Drawing.Size(133, 123);
            this.ibCycle2.TabIndex = 72;
            this.ibCycle2.TabStop = false;
            // 
            // ibCycle1
            // 
            this.ibCycle1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibCycle1.BackColor = System.Drawing.Color.White;
            this.ibCycle1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibCycle1.Location = new System.Drawing.Point(300, 207);
            this.ibCycle1.Margin = new System.Windows.Forms.Padding(4);
            this.ibCycle1.Name = "ibCycle1";
            this.ibCycle1.Size = new System.Drawing.Size(133, 123);
            this.ibCycle1.TabIndex = 71;
            this.ibCycle1.TabStop = false;
            // 
            // tbCycle
            // 
            this.tbCycle.Font = new System.Drawing.Font("Comic Sans MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCycle.Location = new System.Drawing.Point(100, 519);
            this.tbCycle.Margin = new System.Windows.Forms.Padding(4);
            this.tbCycle.Multiline = true;
            this.tbCycle.Name = "tbCycle";
            this.tbCycle.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbCycle.Size = new System.Drawing.Size(332, 221);
            this.tbCycle.TabIndex = 70;
            this.tbCycle.WordWrap = false;
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(-1, 55);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(536, 12);
            this.panel4.TabIndex = 69;
            // 
            // btnLoop
            // 
            this.btnLoop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoop.Location = new System.Drawing.Point(168, 126);
            this.btnLoop.Margin = new System.Windows.Forms.Padding(4);
            this.btnLoop.Name = "btnLoop";
            this.btnLoop.Size = new System.Drawing.Size(172, 41);
            this.btnLoop.TabIndex = 60;
            this.btnLoop.Text = "Detect Loop";
            this.btnLoop.UseVisualStyleBackColor = true;
            this.btnLoop.Click += new System.EventHandler(this.btnLoop_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(323, 187);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 17);
            this.label6.TabIndex = 68;
            this.label6.Text = "Segment 1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(329, 345);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 17);
            this.label5.TabIndex = 67;
            this.label5.Text = "Segment 2";
            // 
            // roundedPanel5
            // 
            this.roundedPanel5.BackColor = System.Drawing.Color.LimeGreen;
            this.roundedPanel5.Controls.Add(this.label26);
            this.roundedPanel5.Location = new System.Drawing.Point(-1, 0);
            this.roundedPanel5.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel5.Name = "roundedPanel5";
            this.roundedPanel5.Size = new System.Drawing.Size(536, 62);
            this.roundedPanel5.TabIndex = 50;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(163, 14);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(203, 29);
            this.label26.TabIndex = 50;
            this.label26.Text = "CYCLE DETECTION";
            // 
            // pnThinned
            // 
            this.pnThinned.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(99)))), ((int)(((byte)(133)))));
            this.pnThinned.Controls.Add(this.ibS2);
            this.pnThinned.Controls.Add(this.ibS1);
            this.pnThinned.Controls.Add(this.ibImgReady);
            this.pnThinned.Controls.Add(this.lblHeight);
            this.pnThinned.Controls.Add(this.lblWidth);
            this.pnThinned.Controls.Add(this.label23);
            this.pnThinned.Controls.Add(this.label22);
            this.pnThinned.Controls.Add(this.label21);
            this.pnThinned.Controls.Add(this.panel3);
            this.pnThinned.Controls.Add(this.roundedPanel3);
            this.pnThinned.Location = new System.Drawing.Point(7, 39);
            this.pnThinned.Margin = new System.Windows.Forms.Padding(4);
            this.pnThinned.Name = "pnThinned";
            this.pnThinned.Size = new System.Drawing.Size(533, 738);
            this.pnThinned.TabIndex = 52;
            this.pnThinned.Visible = false;
            // 
            // ibS2
            // 
            this.ibS2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibS2.BackColor = System.Drawing.Color.White;
            this.ibS2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibS2.Location = new System.Drawing.Point(156, 528);
            this.ibS2.Margin = new System.Windows.Forms.Padding(4);
            this.ibS2.Name = "ibS2";
            this.ibS2.Size = new System.Drawing.Size(200, 185);
            this.ibS2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ibS2.TabIndex = 68;
            this.ibS2.TabStop = false;
            // 
            // ibS1
            // 
            this.ibS1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibS1.BackColor = System.Drawing.Color.White;
            this.ibS1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibS1.Location = new System.Drawing.Point(156, 309);
            this.ibS1.Margin = new System.Windows.Forms.Padding(4);
            this.ibS1.Name = "ibS1";
            this.ibS1.Size = new System.Drawing.Size(200, 185);
            this.ibS1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ibS1.TabIndex = 67;
            this.ibS1.TabStop = false;
            // 
            // ibImgReady
            // 
            this.ibImgReady.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibImgReady.BackColor = System.Drawing.Color.White;
            this.ibImgReady.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibImgReady.Location = new System.Drawing.Point(53, 89);
            this.ibImgReady.Margin = new System.Windows.Forms.Padding(4);
            this.ibImgReady.Name = "ibImgReady";
            this.ibImgReady.Size = new System.Drawing.Size(200, 185);
            this.ibImgReady.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ibImgReady.TabIndex = 66;
            this.ibImgReady.TabStop = false;
            // 
            // lblHeight
            // 
            this.lblHeight.AutoSize = true;
            this.lblHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeight.Location = new System.Drawing.Point(276, 180);
            this.lblHeight.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHeight.Name = "lblHeight";
            this.lblHeight.Size = new System.Drawing.Size(25, 24);
            this.lblHeight.TabIndex = 62;
            this.lblHeight.Text = "...";
            // 
            // lblWidth
            // 
            this.lblWidth.AutoSize = true;
            this.lblWidth.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWidth.Location = new System.Drawing.Point(276, 145);
            this.lblWidth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWidth.Name = "lblWidth";
            this.lblWidth.Size = new System.Drawing.Size(25, 24);
            this.lblWidth.TabIndex = 61;
            this.lblWidth.Text = "...";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(217, 508);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 17);
            this.label23.TabIndex = 58;
            this.label23.Text = "Segmen 2";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(217, 289);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 17);
            this.label22.TabIndex = 57;
            this.label22.Text = "Segmen 1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(101, 70);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(102, 17);
            this.label21.TabIndex = 56;
            this.label21.Text = "Thinned Image";
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(-1, 54);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(536, 12);
            this.panel3.TabIndex = 50;
            // 
            // roundedPanel3
            // 
            this.roundedPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(86)))), ((int)(((byte)(70)))));
            this.roundedPanel3.Controls.Add(this.label11);
            this.roundedPanel3.Location = new System.Drawing.Point(-1, -1);
            this.roundedPanel3.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel3.Name = "roundedPanel3";
            this.roundedPanel3.Size = new System.Drawing.Size(536, 62);
            this.roundedPanel3.TabIndex = 50;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(175, 16);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(159, 29);
            this.label11.TabIndex = 50;
            this.label11.Text = "IMAGE READY";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnThinning
            // 
            this.pnThinning.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(99)))), ((int)(((byte)(133)))));
            this.pnThinning.Controls.Add(this.groupBox2);
            this.pnThinning.Controls.Add(this.btnThinning);
            this.pnThinning.Controls.Add(this.ibThinning);
            this.pnThinning.Controls.Add(this.ibPreProcess);
            this.pnThinning.Controls.Add(this.panel2);
            this.pnThinning.Controls.Add(this.roundedPanel4);
            this.pnThinning.Controls.Add(this.label2);
            this.pnThinning.Controls.Add(this.label1);
            this.pnThinning.Controls.Add(this.tbThinning);
            this.pnThinning.Controls.Add(this.tbPreProcess);
            this.pnThinning.Location = new System.Drawing.Point(597, 740);
            this.pnThinning.Margin = new System.Windows.Forms.Padding(4);
            this.pnThinning.Name = "pnThinning";
            this.pnThinning.Size = new System.Drawing.Size(1240, 775);
            this.pnThinning.TabIndex = 3;
            this.pnThinning.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numWinH);
            this.groupBox2.Controls.Add(this.label45);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Controls.Add(this.numWinW);
            this.groupBox2.Location = new System.Drawing.Point(497, 94);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(235, 107);
            this.groupBox2.TabIndex = 87;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Window Size";
            this.toolTip1.SetToolTip(this.groupBox2, "Untuk deteksi garis berhimpit");
            // 
            // numWinH
            // 
            this.numWinH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numWinH.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numWinH.Location = new System.Drawing.Point(112, 62);
            this.numWinH.Margin = new System.Windows.Forms.Padding(4);
            this.numWinH.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numWinH.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numWinH.Name = "numWinH";
            this.numWinH.Size = new System.Drawing.Size(80, 29);
            this.numWinH.TabIndex = 90;
            this.toolTip1.SetToolTip(this.numWinH, "Tinggi window (bilangan ganjil)");
            this.numWinH.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(23, 64);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(65, 24);
            this.label45.TabIndex = 89;
            this.label45.Text = "Height";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(23, 30);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 24);
            this.label44.TabIndex = 88;
            this.label44.Text = "Width";
            // 
            // numWinW
            // 
            this.numWinW.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numWinW.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numWinW.Location = new System.Drawing.Point(112, 27);
            this.numWinW.Margin = new System.Windows.Forms.Padding(4);
            this.numWinW.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numWinW.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numWinW.Name = "numWinW";
            this.numWinW.Size = new System.Drawing.Size(80, 29);
            this.numWinW.TabIndex = 87;
            this.toolTip1.SetToolTip(this.numWinW, "lebar wiindow (bilangan ganjil)");
            this.numWinW.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // btnThinning
            // 
            this.btnThinning.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThinning.Location = new System.Drawing.Point(532, 208);
            this.btnThinning.Margin = new System.Windows.Forms.Padding(4);
            this.btnThinning.Name = "btnThinning";
            this.btnThinning.Size = new System.Drawing.Size(171, 41);
            this.btnThinning.TabIndex = 69;
            this.btnThinning.Text = "Thinning";
            this.btnThinning.UseVisualStyleBackColor = true;
            this.btnThinning.Click += new System.EventHandler(this.btnThinning_Click);
            // 
            // ibThinning
            // 
            this.ibThinning.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibThinning.BackColor = System.Drawing.Color.White;
            this.ibThinning.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibThinning.Location = new System.Drawing.Point(896, 100);
            this.ibThinning.Margin = new System.Windows.Forms.Padding(4);
            this.ibThinning.Name = "ibThinning";
            this.ibThinning.Size = new System.Drawing.Size(267, 246);
            this.ibThinning.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ibThinning.TabIndex = 68;
            this.ibThinning.TabStop = false;
            // 
            // ibPreProcess
            // 
            this.ibPreProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ibPreProcess.BackColor = System.Drawing.Color.White;
            this.ibPreProcess.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ibPreProcess.Location = new System.Drawing.Point(63, 94);
            this.ibPreProcess.Margin = new System.Windows.Forms.Padding(4);
            this.ibPreProcess.Name = "ibPreProcess";
            this.ibPreProcess.Size = new System.Drawing.Size(267, 246);
            this.ibPreProcess.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ibPreProcess.TabIndex = 67;
            this.ibPreProcess.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(-1, 54);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1243, 12);
            this.panel2.TabIndex = 50;
            // 
            // roundedPanel4
            // 
            this.roundedPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(86)))), ((int)(((byte)(70)))));
            this.roundedPanel4.Controls.Add(this.label10);
            this.roundedPanel4.Location = new System.Drawing.Point(-1, -1);
            this.roundedPanel4.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel4.Name = "roundedPanel4";
            this.roundedPanel4.Size = new System.Drawing.Size(1243, 62);
            this.roundedPanel4.TabIndex = 50;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(480, 16);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(265, 29);
            this.label10.TabIndex = 50;
            this.label10.Text = "SKELETONIZING IMAGE";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1059, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 17);
            this.label2.TabIndex = 50;
            this.label2.Text = "Thinned Image";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 74);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 17);
            this.label1.TabIndex = 50;
            this.label1.Text = "Images ready for Thinning";
            // 
            // tbThinning
            // 
            this.tbThinning.BackColor = System.Drawing.Color.White;
            this.tbThinning.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbThinning.Location = new System.Drawing.Point(629, 353);
            this.tbThinning.Margin = new System.Windows.Forms.Padding(4);
            this.tbThinning.Multiline = true;
            this.tbThinning.Name = "tbThinning";
            this.tbThinning.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbThinning.Size = new System.Drawing.Size(532, 399);
            this.tbThinning.TabIndex = 6;
            this.tbThinning.WordWrap = false;
            // 
            // tbPreProcess
            // 
            this.tbPreProcess.BackColor = System.Drawing.Color.White;
            this.tbPreProcess.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPreProcess.Location = new System.Drawing.Point(63, 353);
            this.tbPreProcess.Margin = new System.Windows.Forms.Padding(4);
            this.tbPreProcess.Multiline = true;
            this.tbPreProcess.Name = "tbPreProcess";
            this.tbPreProcess.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbPreProcess.Size = new System.Drawing.Size(532, 399);
            this.tbPreProcess.TabIndex = 5;
            this.tbPreProcess.WordWrap = false;
            // 
            // pnImage
            // 
            this.pnImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(155)))), ((int)(((byte)(65)))));
            this.pnImage.Controls.Add(this.groupBox1);
            this.pnImage.Controls.Add(this.pbResize);
            this.pnImage.Controls.Add(this.pbDefault);
            this.pnImage.Controls.Add(this.lblImgH);
            this.pnImage.Controls.Add(this.lblImgW);
            this.pnImage.Controls.Add(this.panel7);
            this.pnImage.Controls.Add(this.panel1);
            this.pnImage.Controls.Add(this.roundedPanel2);
            this.pnImage.Controls.Add(this.label4);
            this.pnImage.Controls.Add(this.label3);
            this.pnImage.Location = new System.Drawing.Point(8, 39);
            this.pnImage.Margin = new System.Windows.Forms.Padding(4);
            this.pnImage.Name = "pnImage";
            this.pnImage.Size = new System.Drawing.Size(533, 738);
            this.pnImage.TabIndex = 1;
            this.pnImage.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkRH);
            this.groupBox1.Controls.Add(this.chkRW);
            this.groupBox1.Controls.Add(this.btnResize);
            this.groupBox1.Controls.Add(this.numH);
            this.groupBox1.Controls.Add(this.numW);
            this.groupBox1.Location = new System.Drawing.Point(236, 633);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(280, 94);
            this.groupBox1.TabIndex = 85;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resize";
            // 
            // chkRH
            // 
            this.chkRH.AutoSize = true;
            this.chkRH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkRH.Location = new System.Drawing.Point(11, 54);
            this.chkRH.Margin = new System.Windows.Forms.Padding(4);
            this.chkRH.Name = "chkRH";
            this.chkRH.Size = new System.Drawing.Size(87, 28);
            this.chkRH.TabIndex = 91;
            this.chkRH.Text = "Height";
            this.chkRH.UseVisualStyleBackColor = true;
            this.chkRH.CheckedChanged += new System.EventHandler(this.chkRH_CheckedChanged);
            // 
            // chkRW
            // 
            this.chkRW.AutoSize = true;
            this.chkRW.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkRW.Location = new System.Drawing.Point(11, 20);
            this.chkRW.Margin = new System.Windows.Forms.Padding(4);
            this.chkRW.Name = "chkRW";
            this.chkRW.Size = new System.Drawing.Size(80, 28);
            this.chkRW.TabIndex = 90;
            this.chkRW.Text = "Width";
            this.chkRW.UseVisualStyleBackColor = true;
            this.chkRW.CheckedChanged += new System.EventHandler(this.chkRW_CheckedChanged);
            // 
            // btnResize
            // 
            this.btnResize.Enabled = false;
            this.btnResize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResize.Location = new System.Drawing.Point(177, 31);
            this.btnResize.Margin = new System.Windows.Forms.Padding(4);
            this.btnResize.Name = "btnResize";
            this.btnResize.Size = new System.Drawing.Size(95, 41);
            this.btnResize.TabIndex = 89;
            this.btnResize.Text = "Resize";
            this.btnResize.UseVisualStyleBackColor = true;
            this.btnResize.Click += new System.EventHandler(this.btnResize_Click);
            // 
            // numH
            // 
            this.numH.Enabled = false;
            this.numH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numH.Location = new System.Drawing.Point(105, 53);
            this.numH.Margin = new System.Windows.Forms.Padding(4);
            this.numH.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numH.Name = "numH";
            this.numH.Size = new System.Drawing.Size(64, 29);
            this.numH.TabIndex = 86;
            this.toolTip1.SetToolTip(this.numH, "nilai minimum dalam Accumulator Array yang dianggap garis");
            this.numH.Value = new decimal(new int[] {
            22,
            0,
            0,
            0});
            // 
            // numW
            // 
            this.numW.Enabled = false;
            this.numW.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numW.Location = new System.Drawing.Point(105, 17);
            this.numW.Margin = new System.Windows.Forms.Padding(4);
            this.numW.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numW.Name = "numW";
            this.numW.Size = new System.Drawing.Size(64, 29);
            this.numW.TabIndex = 85;
            this.toolTip1.SetToolTip(this.numW, "Nilai hitung setiap n derajat theta (degrees)");
            this.numW.Value = new decimal(new int[] {
            22,
            0,
            0,
            0});
            // 
            // pbResize
            // 
            this.pbResize.BackColor = System.Drawing.Color.White;
            this.pbResize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbResize.Location = new System.Drawing.Point(28, 542);
            this.pbResize.Margin = new System.Windows.Forms.Padding(4);
            this.pbResize.Name = "pbResize";
            this.pbResize.Size = new System.Drawing.Size(200, 185);
            this.pbResize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbResize.TabIndex = 65;
            this.pbResize.TabStop = false;
            // 
            // pbDefault
            // 
            this.pbDefault.BackColor = System.Drawing.Color.White;
            this.pbDefault.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbDefault.Location = new System.Drawing.Point(29, 87);
            this.pbDefault.Margin = new System.Windows.Forms.Padding(4);
            this.pbDefault.Name = "pbDefault";
            this.pbDefault.Size = new System.Drawing.Size(467, 431);
            this.pbDefault.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbDefault.TabIndex = 2;
            this.pbDefault.TabStop = false;
            // 
            // lblImgH
            // 
            this.lblImgH.AutoSize = true;
            this.lblImgH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImgH.Location = new System.Drawing.Point(251, 590);
            this.lblImgH.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblImgH.Name = "lblImgH";
            this.lblImgH.Size = new System.Drawing.Size(65, 24);
            this.lblImgH.TabIndex = 64;
            this.lblImgH.Text = "Height";
            // 
            // lblImgW
            // 
            this.lblImgW.AutoSize = true;
            this.lblImgW.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImgW.Location = new System.Drawing.Point(251, 561);
            this.lblImgW.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblImgW.Name = "lblImgW";
            this.lblImgW.Size = new System.Drawing.Size(58, 24);
            this.lblImgW.TabIndex = 63;
            this.lblImgW.Text = "Width";
            // 
            // panel7
            // 
            this.panel7.Location = new System.Drawing.Point(28, 650);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(0, 0);
            this.panel7.TabIndex = 58;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(0, 55);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(536, 12);
            this.panel1.TabIndex = 50;
            // 
            // roundedPanel2
            // 
            this.roundedPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(231)))), ((int)(((byte)(14)))));
            this.roundedPanel2.Controls.Add(this.label9);
            this.roundedPanel2.Location = new System.Drawing.Point(-1, 0);
            this.roundedPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel2.Name = "roundedPanel2";
            this.roundedPanel2.Size = new System.Drawing.Size(536, 62);
            this.roundedPanel2.TabIndex = 50;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(169, 15);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(185, 29);
            this.label9.TabIndex = 50;
            this.label9.Text = "DEFAULT IMAGE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 522);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 17);
            this.label4.TabIndex = 50;
            this.label4.Text = "Resized Image";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 68);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 50;
            this.label3.Text = "Default Image";
            // 
            // pnSettingParam
            // 
            this.pnSettingParam.BackColor = System.Drawing.Color.Turquoise;
            this.pnSettingParam.Controls.Add(this.roundedPanel18);
            this.pnSettingParam.Controls.Add(this.btnResetParam);
            this.pnSettingParam.Controls.Add(this.btnSetParam);
            this.pnSettingParam.Controls.Add(this.roundedPanel16);
            this.pnSettingParam.Controls.Add(this.btnSaveParam);
            this.pnSettingParam.Controls.Add(this.roundedPanel14);
            this.pnSettingParam.Controls.Add(this.roundedPanel6);
            this.pnSettingParam.Controls.Add(this.roundedPanel8);
            this.pnSettingParam.Controls.Add(this.panel8);
            this.pnSettingParam.Controls.Add(this.roundedPanel7);
            this.pnSettingParam.Location = new System.Drawing.Point(8, 743);
            this.pnSettingParam.Margin = new System.Windows.Forms.Padding(4);
            this.pnSettingParam.Name = "pnSettingParam";
            this.pnSettingParam.Size = new System.Drawing.Size(1773, 775);
            this.pnSettingParam.TabIndex = 18;
            this.pnSettingParam.Visible = false;
            // 
            // roundedPanel18
            // 
            this.roundedPanel18.BackColor = System.Drawing.Color.LightCyan;
            this.roundedPanel18.Controls.Add(this.panel13);
            this.roundedPanel18.Controls.Add(this.roundedPanel19);
            this.roundedPanel18.Controls.Add(this.chkImgH);
            this.roundedPanel18.Controls.Add(this.chkImgW);
            this.roundedPanel18.Controls.Add(this.numParam2);
            this.roundedPanel18.Controls.Add(this.numParam3);
            this.roundedPanel18.Location = new System.Drawing.Point(533, 74);
            this.roundedPanel18.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel18.Name = "roundedPanel18";
            this.roundedPanel18.Size = new System.Drawing.Size(707, 110);
            this.roundedPanel18.TabIndex = 95;
            // 
            // panel13
            // 
            this.panel13.Location = new System.Drawing.Point(-1, 46);
            this.panel13.Margin = new System.Windows.Forms.Padding(4);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(709, 12);
            this.panel13.TabIndex = 97;
            // 
            // roundedPanel19
            // 
            this.roundedPanel19.BackColor = System.Drawing.Color.LightSeaGreen;
            this.roundedPanel19.Controls.Add(this.label56);
            this.roundedPanel19.Location = new System.Drawing.Point(-1, 0);
            this.roundedPanel19.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel19.Name = "roundedPanel19";
            this.roundedPanel19.Size = new System.Drawing.Size(709, 49);
            this.roundedPanel19.TabIndex = 96;
            // 
            // label56
            // 
            this.label56.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(277, 10);
            this.label56.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(143, 29);
            this.label56.TabIndex = 52;
            this.label56.Text = "IMAGE SIZE";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkImgH
            // 
            this.chkImgH.AutoSize = true;
            this.chkImgH.Checked = true;
            this.chkImgH.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkImgH.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkImgH.Location = new System.Drawing.Point(376, 68);
            this.chkImgH.Margin = new System.Windows.Forms.Padding(4);
            this.chkImgH.Name = "chkImgH";
            this.chkImgH.Size = new System.Drawing.Size(87, 28);
            this.chkImgH.TabIndex = 95;
            this.chkImgH.Text = "Height";
            this.chkImgH.UseVisualStyleBackColor = true;
            this.chkImgH.CheckedChanged += new System.EventHandler(this.chkImgH_CheckedChanged);
            // 
            // chkImgW
            // 
            this.chkImgW.AutoSize = true;
            this.chkImgW.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkImgW.Location = new System.Drawing.Point(141, 68);
            this.chkImgW.Margin = new System.Windows.Forms.Padding(4);
            this.chkImgW.Name = "chkImgW";
            this.chkImgW.Size = new System.Drawing.Size(80, 28);
            this.chkImgW.TabIndex = 94;
            this.chkImgW.Text = "Width";
            this.chkImgW.UseVisualStyleBackColor = true;
            this.chkImgW.CheckedChanged += new System.EventHandler(this.chkImgW_CheckedChanged);
            // 
            // numParam2
            // 
            this.numParam2.Enabled = false;
            this.numParam2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam2.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numParam2.Location = new System.Drawing.Point(233, 65);
            this.numParam2.Margin = new System.Windows.Forms.Padding(4);
            this.numParam2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numParam2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam2.Name = "numParam2";
            this.numParam2.Size = new System.Drawing.Size(80, 29);
            this.numParam2.TabIndex = 92;
            this.numParam2.Value = new decimal(new int[] {
            22,
            0,
            0,
            0});
            // 
            // numParam3
            // 
            this.numParam3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam3.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numParam3.Location = new System.Drawing.Point(476, 66);
            this.numParam3.Margin = new System.Windows.Forms.Padding(4);
            this.numParam3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numParam3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam3.Name = "numParam3";
            this.numParam3.Size = new System.Drawing.Size(80, 29);
            this.numParam3.TabIndex = 93;
            this.toolTip1.SetToolTip(this.numParam3, "Tinggi wiindow (bilangan ganjil)");
            this.numParam3.Value = new decimal(new int[] {
            22,
            0,
            0,
            0});
            // 
            // btnResetParam
            // 
            this.btnResetParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetParam.Location = new System.Drawing.Point(1017, 672);
            this.btnResetParam.Margin = new System.Windows.Forms.Padding(4);
            this.btnResetParam.Name = "btnResetParam";
            this.btnResetParam.Size = new System.Drawing.Size(227, 74);
            this.btnResetParam.TabIndex = 93;
            this.btnResetParam.Text = "Reset";
            this.btnResetParam.UseVisualStyleBackColor = true;
            this.btnResetParam.Click += new System.EventHandler(this.btnResetParam_Click);
            // 
            // btnSetParam
            // 
            this.btnSetParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetParam.Location = new System.Drawing.Point(767, 672);
            this.btnSetParam.Margin = new System.Windows.Forms.Padding(4);
            this.btnSetParam.Name = "btnSetParam";
            this.btnSetParam.Size = new System.Drawing.Size(227, 74);
            this.btnSetParam.TabIndex = 92;
            this.btnSetParam.Text = "Set and Back";
            this.btnSetParam.UseVisualStyleBackColor = true;
            this.btnSetParam.Click += new System.EventHandler(this.btnSetParam_Click);
            // 
            // roundedPanel16
            // 
            this.roundedPanel16.BackColor = System.Drawing.Color.LightCyan;
            this.roundedPanel16.Controls.Add(this.numParam18);
            this.roundedPanel16.Controls.Add(this.label17);
            this.roundedPanel16.Controls.Add(this.numParam17);
            this.roundedPanel16.Controls.Add(this.label49);
            this.roundedPanel16.Controls.Add(this.groupBox5);
            this.roundedPanel16.Controls.Add(this.numParam14);
            this.roundedPanel16.Controls.Add(this.label52);
            this.roundedPanel16.Controls.Add(this.label54);
            this.roundedPanel16.Controls.Add(this.numParam13);
            this.roundedPanel16.Controls.Add(this.panel12);
            this.roundedPanel16.Controls.Add(this.roundedPanel17);
            this.roundedPanel16.Location = new System.Drawing.Point(1147, 192);
            this.roundedPanel16.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel16.Name = "roundedPanel16";
            this.roundedPanel16.Size = new System.Drawing.Size(493, 462);
            this.roundedPanel16.TabIndex = 91;
            // 
            // numParam18
            // 
            this.numParam18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam18.Location = new System.Drawing.Point(279, 389);
            this.numParam18.Margin = new System.Windows.Forms.Padding(4);
            this.numParam18.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numParam18.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam18.Name = "numParam18";
            this.numParam18.Size = new System.Drawing.Size(80, 29);
            this.numParam18.TabIndex = 111;
            this.toolTip1.SetToolTip(this.numParam18, "Beda sudut minium antar kurva");
            this.numParam18.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(101, 391);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(136, 24);
            this.label17.TabIndex = 110;
            this.label17.Text = "Min Gap Angle";
            // 
            // numParam17
            // 
            this.numParam17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam17.Location = new System.Drawing.Point(279, 343);
            this.numParam17.Margin = new System.Windows.Forms.Padding(4);
            this.numParam17.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numParam17.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam17.Name = "numParam17";
            this.numParam17.Size = new System.Drawing.Size(80, 29);
            this.numParam17.TabIndex = 109;
            this.toolTip1.SetToolTip(this.numParam17, "Jarak minimum antar kurva");
            this.numParam17.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(101, 346);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(118, 24);
            this.label49.TabIndex = 108;
            this.label49.Text = "Min Distance";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.numParam15);
            this.groupBox5.Controls.Add(this.rbValMC);
            this.groupBox5.Controls.Add(this.rbPercMC);
            this.groupBox5.Controls.Add(this.numParam16);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(80, 199);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(305, 128);
            this.groupBox5.TabIndex = 107;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Threshold";
            // 
            // numParam15
            // 
            this.numParam15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam15.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam15.Location = new System.Drawing.Point(197, 34);
            this.numParam15.Margin = new System.Windows.Forms.Padding(4);
            this.numParam15.Name = "numParam15";
            this.numParam15.Size = new System.Drawing.Size(80, 29);
            this.numParam15.TabIndex = 105;
            this.toolTip1.SetToolTip(this.numParam15, "nilai minimum dalam Accumulator Array yang dianggap garis (% terhadap nilai maks " +
        "dalam accumulator array)");
            this.numParam15.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // rbValMC
            // 
            this.rbValMC.AutoSize = true;
            this.rbValMC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.rbValMC.Location = new System.Drawing.Point(19, 81);
            this.rbValMC.Margin = new System.Windows.Forms.Padding(4);
            this.rbValMC.Name = "rbValMC";
            this.rbValMC.Size = new System.Drawing.Size(106, 28);
            this.rbValMC.TabIndex = 104;
            this.rbValMC.Text = "By Value";
            this.rbValMC.UseVisualStyleBackColor = true;
            this.rbValMC.CheckedChanged += new System.EventHandler(this.rbValMC_CheckedChanged);
            // 
            // rbPercMC
            // 
            this.rbPercMC.AutoSize = true;
            this.rbPercMC.Checked = true;
            this.rbPercMC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.rbPercMC.Location = new System.Drawing.Point(19, 34);
            this.rbPercMC.Margin = new System.Windows.Forms.Padding(4);
            this.rbPercMC.Name = "rbPercMC";
            this.rbPercMC.Size = new System.Drawing.Size(154, 28);
            this.rbPercMC.TabIndex = 103;
            this.rbPercMC.TabStop = true;
            this.rbPercMC.Text = "By Percentage";
            this.rbPercMC.UseVisualStyleBackColor = true;
            this.rbPercMC.CheckedChanged += new System.EventHandler(this.rbPercMC_CheckedChanged);
            // 
            // numParam16
            // 
            this.numParam16.Enabled = false;
            this.numParam16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam16.Location = new System.Drawing.Point(197, 81);
            this.numParam16.Margin = new System.Windows.Forms.Padding(4);
            this.numParam16.Name = "numParam16";
            this.numParam16.Size = new System.Drawing.Size(80, 29);
            this.numParam16.TabIndex = 98;
            this.toolTip1.SetToolTip(this.numParam16, "nilai minimum dalam Accumulator Array yang dianggap garis (fixed value, dalam sat" +
        "uan pixel)");
            this.numParam16.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // numParam14
            // 
            this.numParam14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam14.Location = new System.Drawing.Point(279, 149);
            this.numParam14.Margin = new System.Windows.Forms.Padding(4);
            this.numParam14.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam14.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam14.Name = "numParam14";
            this.numParam14.Size = new System.Drawing.Size(80, 29);
            this.numParam14.TabIndex = 82;
            this.toolTip1.SetToolTip(this.numParam14, "nilai N x N tetangga dalam menentukan local maxima");
            this.numParam14.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(101, 151);
            this.label52.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(143, 24);
            this.label52.TabIndex = 86;
            this.label52.Text = "Neighbor Value";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(101, 91);
            this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(101, 24);
            this.label54.TabIndex = 84;
            this.label54.Text = "Theta Step";
            // 
            // numParam13
            // 
            this.numParam13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam13.Location = new System.Drawing.Point(279, 91);
            this.numParam13.Margin = new System.Windows.Forms.Padding(4);
            this.numParam13.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam13.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam13.Name = "numParam13";
            this.numParam13.Size = new System.Drawing.Size(80, 29);
            this.numParam13.TabIndex = 81;
            this.toolTip1.SetToolTip(this.numParam13, "Nilai hitung setiap n derajat theta");
            this.numParam13.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // panel12
            // 
            this.panel12.Location = new System.Drawing.Point(-1, 54);
            this.panel12.Margin = new System.Windows.Forms.Padding(4);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(496, 12);
            this.panel12.TabIndex = 50;
            // 
            // roundedPanel17
            // 
            this.roundedPanel17.BackColor = System.Drawing.Color.LightSeaGreen;
            this.roundedPanel17.Controls.Add(this.label59);
            this.roundedPanel17.Location = new System.Drawing.Point(-1, -1);
            this.roundedPanel17.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel17.Name = "roundedPanel17";
            this.roundedPanel17.Size = new System.Drawing.Size(496, 62);
            this.roundedPanel17.TabIndex = 50;
            // 
            // label59
            // 
            this.label59.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(73, 16);
            this.label59.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(341, 29);
            this.label59.TabIndex = 50;
            this.label59.Text = "CURVE DETECTION PARAMETER";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSaveParam
            // 
            this.btnSaveParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveParam.Location = new System.Drawing.Point(512, 672);
            this.btnSaveParam.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveParam.Name = "btnSaveParam";
            this.btnSaveParam.Size = new System.Drawing.Size(227, 74);
            this.btnSaveParam.TabIndex = 69;
            this.btnSaveParam.Text = "Save To File";
            this.btnSaveParam.UseVisualStyleBackColor = true;
            this.btnSaveParam.Click += new System.EventHandler(this.btnSaveParam_Click);
            // 
            // roundedPanel14
            // 
            this.roundedPanel14.BackColor = System.Drawing.Color.LightCyan;
            this.roundedPanel14.Controls.Add(this.groupBox4);
            this.roundedPanel14.Controls.Add(this.numParam9);
            this.roundedPanel14.Controls.Add(this.label47);
            this.roundedPanel14.Controls.Add(this.label48);
            this.roundedPanel14.Controls.Add(this.numParam8);
            this.roundedPanel14.Controls.Add(this.numParam10);
            this.roundedPanel14.Controls.Add(this.label61);
            this.roundedPanel14.Controls.Add(this.label62);
            this.roundedPanel14.Controls.Add(this.numParam7);
            this.roundedPanel14.Controls.Add(this.panel11);
            this.roundedPanel14.Controls.Add(this.roundedPanel15);
            this.roundedPanel14.Location = new System.Drawing.Point(640, 192);
            this.roundedPanel14.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel14.Name = "roundedPanel14";
            this.roundedPanel14.Size = new System.Drawing.Size(493, 462);
            this.roundedPanel14.TabIndex = 90;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.numParam11);
            this.groupBox4.Controls.Add(this.rbVal);
            this.groupBox4.Controls.Add(this.rbPerc);
            this.groupBox4.Controls.Add(this.numParam12);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(93, 306);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(305, 128);
            this.groupBox4.TabIndex = 106;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Threshold";
            // 
            // numParam11
            // 
            this.numParam11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam11.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam11.Location = new System.Drawing.Point(197, 34);
            this.numParam11.Margin = new System.Windows.Forms.Padding(4);
            this.numParam11.Name = "numParam11";
            this.numParam11.Size = new System.Drawing.Size(80, 29);
            this.numParam11.TabIndex = 105;
            this.toolTip1.SetToolTip(this.numParam11, "nilai minimum dalam Accumulator Array yang dianggap garis (% terhadap nilai maks " +
        "dalam accumulator array)");
            this.numParam11.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // rbVal
            // 
            this.rbVal.AutoSize = true;
            this.rbVal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.rbVal.Location = new System.Drawing.Point(19, 81);
            this.rbVal.Margin = new System.Windows.Forms.Padding(4);
            this.rbVal.Name = "rbVal";
            this.rbVal.Size = new System.Drawing.Size(106, 28);
            this.rbVal.TabIndex = 104;
            this.rbVal.Text = "By Value";
            this.rbVal.UseVisualStyleBackColor = true;
            this.rbVal.CheckedChanged += new System.EventHandler(this.rbVal_CheckedChanged);
            // 
            // rbPerc
            // 
            this.rbPerc.AutoSize = true;
            this.rbPerc.Checked = true;
            this.rbPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.rbPerc.Location = new System.Drawing.Point(19, 34);
            this.rbPerc.Margin = new System.Windows.Forms.Padding(4);
            this.rbPerc.Name = "rbPerc";
            this.rbPerc.Size = new System.Drawing.Size(154, 28);
            this.rbPerc.TabIndex = 103;
            this.rbPerc.TabStop = true;
            this.rbPerc.Text = "By Percentage";
            this.rbPerc.UseVisualStyleBackColor = true;
            this.rbPerc.CheckedChanged += new System.EventHandler(this.rbPerc_CheckedChanged);
            // 
            // numParam12
            // 
            this.numParam12.Enabled = false;
            this.numParam12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam12.Location = new System.Drawing.Point(197, 81);
            this.numParam12.Margin = new System.Windows.Forms.Padding(4);
            this.numParam12.Name = "numParam12";
            this.numParam12.Size = new System.Drawing.Size(80, 29);
            this.numParam12.TabIndex = 98;
            this.toolTip1.SetToolTip(this.numParam12, "nilai minimum dalam Accumulator Array yang dianggap garis (fixed value, dalam sat" +
        "uan pixel)");
            this.numParam12.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // numParam9
            // 
            this.numParam9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam9.Location = new System.Drawing.Point(292, 207);
            this.numParam9.Margin = new System.Windows.Forms.Padding(4);
            this.numParam9.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam9.Name = "numParam9";
            this.numParam9.Size = new System.Drawing.Size(80, 29);
            this.numParam9.TabIndex = 101;
            this.toolTip1.SetToolTip(this.numParam9, "minimum nilai panjang garis");
            this.numParam9.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(103, 209);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(145, 24);
            this.label47.TabIndex = 102;
            this.label47.Text = "Min Line Length";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(103, 154);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(163, 24);
            this.label48.TabIndex = 100;
            this.label48.Text = "Neighbor Window";
            // 
            // numParam8
            // 
            this.numParam8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam8.Location = new System.Drawing.Point(292, 154);
            this.numParam8.Margin = new System.Windows.Forms.Padding(4);
            this.numParam8.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam8.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam8.Name = "numParam8";
            this.numParam8.Size = new System.Drawing.Size(80, 29);
            this.numParam8.TabIndex = 99;
            this.toolTip1.SetToolTip(this.numParam8, "Nilai  NxN window untuk  menghitung Local Maxima");
            this.numParam8.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // numParam10
            // 
            this.numParam10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam10.Location = new System.Drawing.Point(292, 261);
            this.numParam10.Margin = new System.Windows.Forms.Padding(4);
            this.numParam10.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam10.Name = "numParam10";
            this.numParam10.Size = new System.Drawing.Size(80, 29);
            this.numParam10.TabIndex = 94;
            this.toolTip1.SetToolTip(this.numParam10, "maksimum jarak antar garis");
            this.numParam10.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(103, 263);
            this.label61.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(127, 24);
            this.label61.TabIndex = 96;
            this.label61.Text = "Max Line Gap";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(103, 101);
            this.label62.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(152, 24);
            this.label62.TabIndex = 95;
            this.label62.Text = "Theta Resolution";
            // 
            // numParam7
            // 
            this.numParam7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam7.Location = new System.Drawing.Point(292, 98);
            this.numParam7.Margin = new System.Windows.Forms.Padding(4);
            this.numParam7.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam7.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam7.Name = "numParam7";
            this.numParam7.Size = new System.Drawing.Size(80, 29);
            this.numParam7.TabIndex = 93;
            this.toolTip1.SetToolTip(this.numParam7, "Nilai hitung setiap n derajat theta (degrees)");
            this.numParam7.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // panel11
            // 
            this.panel11.Location = new System.Drawing.Point(-1, 54);
            this.panel11.Margin = new System.Windows.Forms.Padding(4);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(496, 12);
            this.panel11.TabIndex = 50;
            // 
            // roundedPanel15
            // 
            this.roundedPanel15.BackColor = System.Drawing.Color.LightSeaGreen;
            this.roundedPanel15.Controls.Add(this.label55);
            this.roundedPanel15.Location = new System.Drawing.Point(-1, -1);
            this.roundedPanel15.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel15.Name = "roundedPanel15";
            this.roundedPanel15.Size = new System.Drawing.Size(496, 62);
            this.roundedPanel15.TabIndex = 50;
            // 
            // label55
            // 
            this.label55.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(84, 16);
            this.label55.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(325, 29);
            this.label55.TabIndex = 50;
            this.label55.Text = "LINE DETECTION PARAMETER";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // roundedPanel6
            // 
            this.roundedPanel6.BackColor = System.Drawing.Color.LightCyan;
            this.roundedPanel6.Controls.Add(this.label60);
            this.roundedPanel6.Controls.Add(this.numParam6);
            this.roundedPanel6.Controls.Add(this.panel10);
            this.roundedPanel6.Controls.Add(this.roundedPanel11);
            this.roundedPanel6.Location = new System.Drawing.Point(135, 446);
            this.roundedPanel6.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel6.Name = "roundedPanel6";
            this.roundedPanel6.Size = new System.Drawing.Size(493, 208);
            this.roundedPanel6.TabIndex = 89;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(92, 117);
            this.label60.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(133, 24);
            this.label60.TabIndex = 94;
            this.label60.Text = "Minimum Area";
            // 
            // numParam6
            // 
            this.numParam6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam6.Location = new System.Drawing.Point(252, 114);
            this.numParam6.Margin = new System.Windows.Forms.Padding(4);
            this.numParam6.Name = "numParam6";
            this.numParam6.Size = new System.Drawing.Size(80, 29);
            this.numParam6.TabIndex = 93;
            this.toolTip1.SetToolTip(this.numParam6, "Nilai minimal luas cycle yang dapat di deteksi");
            this.numParam6.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // panel10
            // 
            this.panel10.Location = new System.Drawing.Point(-1, 54);
            this.panel10.Margin = new System.Windows.Forms.Padding(4);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(496, 12);
            this.panel10.TabIndex = 50;
            // 
            // roundedPanel11
            // 
            this.roundedPanel11.BackColor = System.Drawing.Color.LightSeaGreen;
            this.roundedPanel11.Controls.Add(this.label50);
            this.roundedPanel11.Location = new System.Drawing.Point(-1, -1);
            this.roundedPanel11.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel11.Name = "roundedPanel11";
            this.roundedPanel11.Size = new System.Drawing.Size(496, 62);
            this.roundedPanel11.TabIndex = 50;
            // 
            // label50
            // 
            this.label50.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(72, 12);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(335, 29);
            this.label50.TabIndex = 50;
            this.label50.Text = "CYCLE DETECTION PARAMETER";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // roundedPanel8
            // 
            this.roundedPanel8.BackColor = System.Drawing.Color.LightCyan;
            this.roundedPanel8.Controls.Add(this.label46);
            this.roundedPanel8.Controls.Add(this.label39);
            this.roundedPanel8.Controls.Add(this.numParam5);
            this.roundedPanel8.Controls.Add(this.label38);
            this.roundedPanel8.Controls.Add(this.panel9);
            this.roundedPanel8.Controls.Add(this.numParam4);
            this.roundedPanel8.Controls.Add(this.roundedPanel10);
            this.roundedPanel8.Location = new System.Drawing.Point(133, 192);
            this.roundedPanel8.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel8.Name = "roundedPanel8";
            this.roundedPanel8.Size = new System.Drawing.Size(493, 246);
            this.roundedPanel8.TabIndex = 88;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(169, 84);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(131, 24);
            this.label46.TabIndex = 91;
            this.label46.Text = "Window Size";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(164, 177);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(65, 24);
            this.label39.TabIndex = 92;
            this.label39.Text = "Height";
            // 
            // numParam5
            // 
            this.numParam5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam5.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numParam5.Location = new System.Drawing.Point(253, 175);
            this.numParam5.Margin = new System.Windows.Forms.Padding(4);
            this.numParam5.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam5.Name = "numParam5";
            this.numParam5.Size = new System.Drawing.Size(80, 29);
            this.numParam5.TabIndex = 91;
            this.toolTip1.SetToolTip(this.numParam5, "Tinggi wiindow (bilangan ganjil)");
            this.numParam5.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(164, 127);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(58, 24);
            this.label38.TabIndex = 90;
            this.label38.Text = "Width";
            // 
            // panel9
            // 
            this.panel9.Location = new System.Drawing.Point(-1, 54);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(496, 12);
            this.panel9.TabIndex = 50;
            // 
            // numParam4
            // 
            this.numParam4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numParam4.Increment = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numParam4.Location = new System.Drawing.Point(253, 124);
            this.numParam4.Margin = new System.Windows.Forms.Padding(4);
            this.numParam4.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numParam4.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numParam4.Name = "numParam4";
            this.numParam4.Size = new System.Drawing.Size(80, 29);
            this.numParam4.TabIndex = 89;
            this.toolTip1.SetToolTip(this.numParam4, "lebar wiindow (bilangan ganjil)");
            this.numParam4.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // roundedPanel10
            // 
            this.roundedPanel10.BackColor = System.Drawing.Color.LightSeaGreen;
            this.roundedPanel10.Controls.Add(this.label51);
            this.roundedPanel10.Location = new System.Drawing.Point(-1, -1);
            this.roundedPanel10.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel10.Name = "roundedPanel10";
            this.roundedPanel10.Size = new System.Drawing.Size(496, 62);
            this.roundedPanel10.TabIndex = 50;
            // 
            // label51
            // 
            this.label51.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(128, 16);
            this.label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(258, 29);
            this.label51.TabIndex = 50;
            this.label51.Text = "THINNING PARAMETER";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel8
            // 
            this.panel8.Location = new System.Drawing.Point(-1, 54);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1787, 12);
            this.panel8.TabIndex = 50;
            // 
            // roundedPanel7
            // 
            this.roundedPanel7.BackColor = System.Drawing.Color.Teal;
            this.roundedPanel7.Controls.Add(this.btnExitParam);
            this.roundedPanel7.Controls.Add(this.label40);
            this.roundedPanel7.Location = new System.Drawing.Point(-1, -1);
            this.roundedPanel7.Margin = new System.Windows.Forms.Padding(4);
            this.roundedPanel7.Name = "roundedPanel7";
            this.roundedPanel7.Size = new System.Drawing.Size(1787, 62);
            this.roundedPanel7.TabIndex = 50;
            // 
            // btnExitParam
            // 
            this.btnExitParam.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExitParam.BackgroundImage")));
            this.btnExitParam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnExitParam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExitParam.Location = new System.Drawing.Point(1709, 14);
            this.btnExitParam.Margin = new System.Windows.Forms.Padding(4);
            this.btnExitParam.Name = "btnExitParam";
            this.btnExitParam.Size = new System.Drawing.Size(44, 25);
            this.btnExitParam.TabIndex = 94;
            this.btnExitParam.UseVisualStyleBackColor = true;
            this.btnExitParam.Click += new System.EventHandler(this.btnExitParam_Click);
            // 
            // label40
            // 
            this.label40.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(740, 14);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(240, 29);
            this.label40.TabIndex = 50;
            this.label40.Text = "SETTING PARAMETER";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FeatureExtractApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1805, 912);
            this.Controls.Add(this.single);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FeatureExtractApp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aplikasi Ekstraksi Fitur Citra Huruf Jawa";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FeatureExtractApp_Load);
            this.single.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.multi.ResumeLayout(false);
            this.multi.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.pnRunTime.ResumeLayout(false);
            this.pnRunTime.PerformLayout();
            this.pnCurve.ResumeLayout(false);
            this.pnCurve.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibCurve2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCurveUnion2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCurve1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCurveUnion1)).EndInit();
            this.roundedPanel12.ResumeLayout(false);
            this.roundedPanel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numThrPercC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThrValC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGapC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDistC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNeighborC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStepC)).EndInit();
            this.roundedPanel13.ResumeLayout(false);
            this.roundedPanel13.PerformLayout();
            this.pnLine.ResumeLayout(false);
            this.pnLine.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibHoughLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibHoughLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibDetLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLineUni2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibDetLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLineUni1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLineUnion2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibLineUnion1)).EndInit();
            this.roundedPanel1.ResumeLayout(false);
            this.roundedPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numThrPerc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNeighbor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThrVal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxGap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numThetaStep)).EndInit();
            this.roundedPanel9.ResumeLayout(false);
            this.roundedPanel9.PerformLayout();
            this.pnLoop.ResumeLayout(false);
            this.pnLoop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMinArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCycleFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCycle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibCycle1)).EndInit();
            this.roundedPanel5.ResumeLayout(false);
            this.roundedPanel5.PerformLayout();
            this.pnThinned.ResumeLayout(false);
            this.pnThinned.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibS2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibS1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibImgReady)).EndInit();
            this.roundedPanel3.ResumeLayout(false);
            this.roundedPanel3.PerformLayout();
            this.pnThinning.ResumeLayout(false);
            this.pnThinning.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numWinH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWinW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibThinning)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ibPreProcess)).EndInit();
            this.roundedPanel4.ResumeLayout(false);
            this.roundedPanel4.PerformLayout();
            this.pnImage.ResumeLayout(false);
            this.pnImage.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbResize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDefault)).EndInit();
            this.roundedPanel2.ResumeLayout(false);
            this.roundedPanel2.PerformLayout();
            this.pnSettingParam.ResumeLayout(false);
            this.roundedPanel18.ResumeLayout(false);
            this.roundedPanel18.PerformLayout();
            this.roundedPanel19.ResumeLayout(false);
            this.roundedPanel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam3)).EndInit();
            this.roundedPanel16.ResumeLayout(false);
            this.roundedPanel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam17)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam13)).EndInit();
            this.roundedPanel17.ResumeLayout(false);
            this.roundedPanel17.PerformLayout();
            this.roundedPanel14.ResumeLayout(false);
            this.roundedPanel14.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam7)).EndInit();
            this.roundedPanel15.ResumeLayout(false);
            this.roundedPanel15.PerformLayout();
            this.roundedPanel6.ResumeLayout(false);
            this.roundedPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam6)).EndInit();
            this.roundedPanel11.ResumeLayout(false);
            this.roundedPanel11.PerformLayout();
            this.roundedPanel8.ResumeLayout(false);
            this.roundedPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numParam5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numParam4)).EndInit();
            this.roundedPanel10.ResumeLayout(false);
            this.roundedPanel10.PerformLayout();
            this.roundedPanel7.ResumeLayout(false);
            this.roundedPanel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl single;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage multi;
        private roundedPanel pnImage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private roundedPanel roundedPanel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private roundedPanel pnThinning;
        private System.Windows.Forms.Panel panel2;
        private roundedPanel roundedPanel4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbThinning;
        private System.Windows.Forms.TextBox tbPreProcess;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem filesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem imageProcessingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thinningImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem featureExtractionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loopDetectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lineDetectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem curveDetectionToolStripMenuItem;
        private roundedPanel pnThinned;
        private System.Windows.Forms.Panel panel3;
        private roundedPanel roundedPanel3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private roundedPanel pnLoop;
        private roundedPanel roundedPanel5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnLoop;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbCycle;
        private roundedPanel pnLine;
        private System.Windows.Forms.Panel panel5;
        private roundedPanel roundedPanel9;
        private System.Windows.Forms.Label label27;
        private roundedPanel roundedPanel1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown numThetaStep;
        private System.Windows.Forms.Button btnHT;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.NumericUpDown numMaxGap;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.NumericUpDown numThrVal;
        private roundedPanel pnCurve;
        private roundedPanel roundedPanel12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numNeighborC;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.NumericUpDown numStepC;
        private System.Windows.Forms.Button btnCurve;
        private System.Windows.Forms.Panel panel6;
        private roundedPanel roundedPanel13;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ListView listLine;
        private System.Windows.Forms.ColumnHeader P1X;
        private System.Windows.Forms.ColumnHeader P1Y;
        private System.Windows.Forms.ColumnHeader Nomor;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadImagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem processImagesToolStripMenuItem;
        private System.Windows.Forms.ListView listImg;
        private System.Windows.Forms.ColumnHeader no;
        private System.Windows.Forms.ColumnHeader path;
        private System.Windows.Forms.ColumnHeader loop1;
        private System.Windows.Forms.ColumnHeader loop2;
        private System.Windows.Forms.ColumnHeader line1;
        private System.Windows.Forms.ColumnHeader line2;
        private System.Windows.Forms.ListView listMulti;
        private System.Windows.Forms.ColumnHeader curve1;
        private System.Windows.Forms.ColumnHeader curve2;
        private System.Windows.Forms.Label lblHeight;
        private System.Windows.Forms.Label lblWidth;
        private System.Windows.Forms.NumericUpDown numMinLength;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numNeighbor;
        private System.Windows.Forms.ColumnHeader P2X;
        private System.Windows.Forms.ColumnHeader P2Y;
        private System.Windows.Forms.ListView listCurve;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private roundedPanel pnRunTime;
        private System.Windows.Forms.Label lblRunTime;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label lblImgH;
        private System.Windows.Forms.Label lblImgW;
        private Emgu.CV.UI.ImageBox pbDefault;
        private Emgu.CV.UI.ImageBox pbResize;
        private Emgu.CV.UI.ImageBox ibS2;
        private Emgu.CV.UI.ImageBox ibS1;
        private Emgu.CV.UI.ImageBox ibImgReady;
        private System.Windows.Forms.Label label20;
        private Emgu.CV.UI.ImageBox ibLine2;
        private Emgu.CV.UI.ImageBox ibLineUnion2;
        private System.Windows.Forms.Label label19;
        private Emgu.CV.UI.ImageBox ibLine1;
        private Emgu.CV.UI.ImageBox ibLineUnion1;
        private Emgu.CV.UI.ImageBox ibCycle2;
        private Emgu.CV.UI.ImageBox ibCycle1;
        private Emgu.CV.UI.ImageBox ibThinning;
        private Emgu.CV.UI.ImageBox ibPreProcess;
        private System.Windows.Forms.Label label41;
        private Emgu.CV.UI.ImageBox ibCurve2;
        private Emgu.CV.UI.ImageBox ibCurveUnion2;
        private System.Windows.Forms.Label label42;
        private Emgu.CV.UI.ImageBox ibCurve1;
        private Emgu.CV.UI.ImageBox ibCurveUnion1;
        private System.Windows.Forms.Label label12;
        private Emgu.CV.UI.ImageBox ibCycleFull;
        private System.Windows.Forms.ColumnHeader loopfull;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.NumericUpDown numMinArea;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnResize;
        private System.Windows.Forms.NumericUpDown numH;
        private System.Windows.Forms.NumericUpDown numW;
        private System.Windows.Forms.CheckBox chkRH;
        private System.Windows.Forms.CheckBox chkRW;
        private System.Windows.Forms.Button btnThinning;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.NumericUpDown numWinW;
        private System.Windows.Forms.NumericUpDown numWinH;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label lblMultiResult;
        private System.Windows.Forms.ColumnHeader rho;
        private System.Windows.Forms.ColumnHeader theta;
        private System.Windows.Forms.NumericUpDown numThrPerc;
        private System.Windows.Forms.RadioButton rbPilThrVal;
        private System.Windows.Forms.RadioButton rbPilThrPer;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private Emgu.CV.UI.ImageBox ibHoughLine2;
        private Emgu.CV.UI.ImageBox ibHoughLine1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private Emgu.CV.UI.ImageBox ibDetLine2;
        private Emgu.CV.UI.ImageBox ibLineUni2;
        private Emgu.CV.UI.ImageBox ibDetLine1;
        private Emgu.CV.UI.ImageBox ibLineUni1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ToolStripMenuItem settingParameterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewParameterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadParameterToolStripMenuItem;
        private roundedPanel pnSettingParam;
        private roundedPanel roundedPanel8;
        private System.Windows.Forms.Panel panel9;
        private roundedPanel roundedPanel10;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button btnSaveParam;
        private System.Windows.Forms.Panel panel8;
        private roundedPanel roundedPanel7;
        private System.Windows.Forms.Label label40;
        private roundedPanel roundedPanel16;
        private System.Windows.Forms.Panel panel12;
        private roundedPanel roundedPanel17;
        private System.Windows.Forms.Label label59;
        private roundedPanel roundedPanel14;
        private System.Windows.Forms.Panel panel11;
        private roundedPanel roundedPanel15;
        private System.Windows.Forms.Label label55;
        private roundedPanel roundedPanel6;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.NumericUpDown numParam6;
        private System.Windows.Forms.Panel panel10;
        private roundedPanel roundedPanel11;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.NumericUpDown numParam5;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.NumericUpDown numParam4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown numParam11;
        private System.Windows.Forms.RadioButton rbVal;
        private System.Windows.Forms.RadioButton rbPerc;
        private System.Windows.Forms.NumericUpDown numParam12;
        private System.Windows.Forms.NumericUpDown numParam9;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.NumericUpDown numParam8;
        private System.Windows.Forms.NumericUpDown numParam10;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.NumericUpDown numParam7;
        private System.Windows.Forms.Button btnResetParam;
        private System.Windows.Forms.Button btnSetParam;
        private System.Windows.Forms.NumericUpDown numParam14;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.NumericUpDown numParam13;
        private System.Windows.Forms.Button btnExitParam;
        private roundedPanel roundedPanel18;
        private roundedPanel roundedPanel19;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.CheckBox chkImgH;
        private System.Windows.Forms.NumericUpDown numParam3;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.CheckBox chkImgW;
        private System.Windows.Forms.NumericUpDown numParam2;
        private System.Windows.Forms.NumericUpDown numDistC;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.NumericUpDown numGapC;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numThrPercC;
        private System.Windows.Forms.RadioButton rbValC;
        private System.Windows.Forms.RadioButton rbPercC;
        private System.Windows.Forms.NumericUpDown numThrValC;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown numParam15;
        private System.Windows.Forms.RadioButton rbValMC;
        private System.Windows.Forms.RadioButton rbPercMC;
        private System.Windows.Forms.NumericUpDown numParam16;
        private System.Windows.Forms.NumericUpDown numParam18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numParam17;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.ColumnHeader coord1;
        private System.Windows.Forms.ColumnHeader rhoTheta1;
        private System.Windows.Forms.ColumnHeader coord2;
        private System.Windows.Forms.ColumnHeader rhoTheta2;
        private System.Windows.Forms.ColumnHeader vertex1;
        private System.Windows.Forms.ColumnHeader vertex2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
    }
}