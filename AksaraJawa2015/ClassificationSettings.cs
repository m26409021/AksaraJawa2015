﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AksaraJawa2015
{
    public partial class ClassificationSettings : Form
    {
        private ID3_PNN _bindedForm;
        private ID3_PNN_old _bindedForm_old;

        public ClassificationSettings(ID3_PNN bindedForm, bool enchanceReading = false, bool saveCSV = false)
        {
            InitializeComponent();

            _bindedForm = bindedForm;

            enchancedReading.Checked = enchanceReading;
            if (!enchanceReading)
                standardReading.Checked = true;

            saveCSVRes.Checked = saveCSV;
            if (!saveCSV)
                dontSaveCSVRes.Checked = true;
        }
        public ClassificationSettings(ID3_PNN_old bindedForm, bool enchanceReading = false, bool saveCSV = false)
        {
            InitializeComponent();

            _bindedForm_old = bindedForm;

            enchancedReading.Checked = enchanceReading;
            if (!enchanceReading)
                standardReading.Checked = true;

            saveCSVRes.Checked = saveCSV;
            if (!saveCSV)
                dontSaveCSVRes.Checked = true;
        }

        private void saveChanges_Click(object sender, EventArgs e)
        {
            if (_bindedForm != null)
            {
                _bindedForm._enchanceReading = enchancedReading.Checked;
                _bindedForm._createCSVResult = saveCSVRes.Checked;
            }

            if (_bindedForm_old != null)
            {
                _bindedForm_old._enchanceReading = enchancedReading.Checked;
                _bindedForm_old._createCSVResult = saveCSVRes.Checked;
            }


            this.Close();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
