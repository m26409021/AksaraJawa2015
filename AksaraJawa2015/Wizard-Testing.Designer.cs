﻿namespace AksaraJawa2015
{
    partial class Wizard_Testing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OriginalImage = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.PictureBox_OriginalImage = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.result_txt = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.OriginalImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // OriginalImage
            // 
            this.OriginalImage.Controls.Add(this.button3);
            this.OriginalImage.Controls.Add(this.panel1);
            this.OriginalImage.Controls.Add(this.button2);
            this.OriginalImage.Controls.Add(this.txtFilename);
            this.OriginalImage.Controls.Add(this.btnBrowse);
            this.OriginalImage.Location = new System.Drawing.Point(13, 13);
            this.OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.OriginalImage.Name = "OriginalImage";
            this.OriginalImage.Padding = new System.Windows.Forms.Padding(27, 25, 27, 25);
            this.OriginalImage.Size = new System.Drawing.Size(553, 606);
            this.OriginalImage.TabIndex = 2;
            this.OriginalImage.TabStop = false;
            this.OriginalImage.Text = "Original Image";
            this.OriginalImage.Enter += new System.EventHandler(this.OriginalImage_Enter);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(387, 23);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(154, 31);
            this.button2.TabIndex = 6;
            this.button2.Text = "Load Network";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtFilename
            // 
            this.txtFilename.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFilename.Location = new System.Drawing.Point(8, 23);
            this.txtFilename.Margin = new System.Windows.Forms.Padding(4);
            this.txtFilename.Multiline = true;
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtFilename.Size = new System.Drawing.Size(371, 27);
            this.txtFilename.TabIndex = 7;
            this.txtFilename.TextChanged += new System.EventHandler(this.txtFilename_TextChanged);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(8, 499);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(533, 42);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse Image and Testing";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // PictureBox_OriginalImage
            // 
            this.PictureBox_OriginalImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBox_OriginalImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox_OriginalImage.Location = new System.Drawing.Point(0, 0);
            this.PictureBox_OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox_OriginalImage.Name = "PictureBox_OriginalImage";
            this.PictureBox_OriginalImage.Size = new System.Drawing.Size(533, 434);
            this.PictureBox_OriginalImage.TabIndex = 0;
            this.PictureBox_OriginalImage.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.result_txt);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(574, 13);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(27, 25, 27, 25);
            this.groupBox1.Size = new System.Drawing.Size(553, 606);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Result";
            // 
            // result_txt
            // 
            this.result_txt.Font = new System.Drawing.Font("Java", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.result_txt.Location = new System.Drawing.Point(8, 23);
            this.result_txt.Multiline = true;
            this.result_txt.Name = "result_txt";
            this.result_txt.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.result_txt.Size = new System.Drawing.Size(533, 468);
            this.result_txt.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 498);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(533, 42);
            this.button1.TabIndex = 1;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.PictureBox_OriginalImage);
            this.panel1.Location = new System.Drawing.Point(8, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(533, 434);
            this.panel1.TabIndex = 8;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(8, 549);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(533, 42);
            this.button3.TabIndex = 9;
            this.button3.Text = "Zoom Out";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Wizard_Testing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1139, 749);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.OriginalImage);
            this.Name = "Wizard_Testing";
            this.Text = "Wizard_Testing";
            this.Load += new System.EventHandler(this.Wizard_Testing_Load);
            this.OriginalImage.ResumeLayout(false);
            this.OriginalImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox OriginalImage;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.PictureBox PictureBox_OriginalImage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox result_txt;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button3;
    }
}