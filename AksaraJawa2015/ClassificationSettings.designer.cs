﻿namespace AksaraJawa2015
{
    partial class ClassificationSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.standardReading = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.enchancedReading = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dontSaveCSVRes = new System.Windows.Forms.RadioButton();
            this.saveCSVRes = new System.Windows.Forms.RadioButton();
            this.saveChanges = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // standardReading
            // 
            this.standardReading.AutoSize = true;
            this.standardReading.Location = new System.Drawing.Point(6, 19);
            this.standardReading.Name = "standardReading";
            this.standardReading.Size = new System.Drawing.Size(68, 17);
            this.standardReading.TabIndex = 1;
            this.standardReading.TabStop = true;
            this.standardReading.Text = "Standard";
            this.standardReading.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.enchancedReading);
            this.groupBox1.Controls.Add(this.standardReading);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 68);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alphabet reading method";
            // 
            // enchancedReading
            // 
            this.enchancedReading.AutoSize = true;
            this.enchancedReading.Location = new System.Drawing.Point(6, 42);
            this.enchancedReading.Name = "enchancedReading";
            this.enchancedReading.Size = new System.Drawing.Size(80, 17);
            this.enchancedReading.TabIndex = 2;
            this.enchancedReading.TabStop = true;
            this.enchancedReading.Text = "Enchanced";
            this.enchancedReading.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dontSaveCSVRes);
            this.groupBox2.Controls.Add(this.saveCSVRes);
            this.groupBox2.Location = new System.Drawing.Point(12, 86);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 68);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Save CSV result";
            // 
            // dontSaveCSVRes
            // 
            this.dontSaveCSVRes.AutoSize = true;
            this.dontSaveCSVRes.Location = new System.Drawing.Point(6, 42);
            this.dontSaveCSVRes.Name = "dontSaveCSVRes";
            this.dontSaveCSVRes.Size = new System.Drawing.Size(39, 17);
            this.dontSaveCSVRes.TabIndex = 2;
            this.dontSaveCSVRes.TabStop = true;
            this.dontSaveCSVRes.Text = "No";
            this.dontSaveCSVRes.UseVisualStyleBackColor = true;
            // 
            // saveCSVRes
            // 
            this.saveCSVRes.AutoSize = true;
            this.saveCSVRes.Location = new System.Drawing.Point(6, 19);
            this.saveCSVRes.Name = "saveCSVRes";
            this.saveCSVRes.Size = new System.Drawing.Size(43, 17);
            this.saveCSVRes.TabIndex = 1;
            this.saveCSVRes.TabStop = true;
            this.saveCSVRes.Text = "Yes";
            this.saveCSVRes.UseVisualStyleBackColor = true;
            // 
            // saveChanges
            // 
            this.saveChanges.AutoSize = true;
            this.saveChanges.Location = new System.Drawing.Point(11, 160);
            this.saveChanges.Name = "saveChanges";
            this.saveChanges.Size = new System.Drawing.Size(87, 23);
            this.saveChanges.TabIndex = 4;
            this.saveChanges.Text = "Save Changes";
            this.saveChanges.UseVisualStyleBackColor = true;
            this.saveChanges.Click += new System.EventHandler(this.saveChanges_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.AutoSize = true;
            this.cancelBtn.Location = new System.Drawing.Point(104, 160);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(87, 23);
            this.cancelBtn.TabIndex = 5;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // ClassificationSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 189);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.saveChanges);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ClassificationSettings";
            this.Text = "Classification Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton standardReading;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton enchancedReading;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton dontSaveCSVRes;
        private System.Windows.Forms.RadioButton saveCSVRes;
        private System.Windows.Forms.Button saveChanges;
        private System.Windows.Forms.Button cancelBtn;
    }
}