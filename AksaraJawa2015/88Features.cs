﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AksaraJawa2015
{
    public partial class _88Features : Form
    {
        OpenFileDialog ofd;
        FolderBrowserDialog fbd;
        CImage OImage; // object image
        public _88Features()
        {
            InitializeComponent();

            // init browser dialog
            fbd = new FolderBrowserDialog();
            fbd.SelectedPath = Directory.GetCurrentDirectory();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                System.GC.Collect();
                // percobaan 1
                OImage = new CImage(ofd.FileName);
                PictureBox_OriginalImage.BackgroundImage = OImage.getOriginalImage(); // show the original image
            }

            var result = fbd.ShowDialog();
            OImage.APP_PATH = fbd.SelectedPath + "\\";
            // handle cancel
            if (result == DialogResult.Cancel)
                return;

            OImage.toGrey();
            OImage.mono(Int32.Parse(txtMonoThreshold.Text));
            // set feature extraction method
            int featureExtractionMode;
            if (FeatureExtract1.Checked)
                featureExtractionMode = 1;
            else if (FeatureExtract2.Checked)
                featureExtractionMode = 2;
            else
                featureExtractionMode = 3;

            OImage.BondanAutomateSegmentation(true, featureExtractionMode);
            
        }
    }
}
