﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AksaraJawa2015
{
    public partial class Labeling : Form
    {
        string[][] inputdata;
        int inputcol, inputrow;
        int image_counter;
        string filename;
        CImage OImage; // object image
        public Labeling()
        {
            InitializeComponent();
        }

        private void Labeling_Load(object sender, EventArgs e)
        {
            #region initialize_combobox
            comboBox1.Items.Add("ha");
            comboBox1.Items.Add("na");
            comboBox1.Items.Add("ca");
            comboBox1.Items.Add("ra");
            comboBox1.Items.Add("ka");
            comboBox1.Items.Add("da");
            comboBox1.Items.Add("ta");
            comboBox1.Items.Add("sa");
            comboBox1.Items.Add("wa");
            comboBox1.Items.Add("la");
            comboBox1.Items.Add("pa");
            comboBox1.Items.Add("dha");
            comboBox1.Items.Add("ja");
            comboBox1.Items.Add("ya");
            comboBox1.Items.Add("nya");
            comboBox1.Items.Add("ma");
            comboBox1.Items.Add("ga");
            comboBox1.Items.Add("ba");
            comboBox1.Items.Add("tha");
            comboBox1.Items.Add("nga");
            comboBox1.Items.Add("pasangan ha");
            comboBox1.Items.Add("pasangan na");
            comboBox1.Items.Add("pasangan ca");
            comboBox1.Items.Add("pasangan ra");
            comboBox1.Items.Add("pasangan ka");
            comboBox1.Items.Add("pasangan da");
            comboBox1.Items.Add("pasangan ta");
            comboBox1.Items.Add("pasangan sa");
            comboBox1.Items.Add("pasangan wa");
            comboBox1.Items.Add("pasangan la");
            comboBox1.Items.Add("pasangan pa");
            comboBox1.Items.Add("pasangan dha");
            comboBox1.Items.Add("pasangan ja");
            comboBox1.Items.Add("pasangan ya");
            comboBox1.Items.Add("pasangan nya");
            comboBox1.Items.Add("pasangan ma");
            comboBox1.Items.Add("pasangan ga");
            comboBox1.Items.Add("pasangan ba");
            comboBox1.Items.Add("pasangan tha");
            comboBox1.Items.Add("pasangan nga");
            comboBox1.Items.Add("wulu");
            comboBox1.Items.Add("suku");
            comboBox1.Items.Add("taling");
            comboBox1.Items.Add("pepet");
            comboBox1.Items.Add("tarung");
            comboBox1.Items.Add("layar");
            comboBox1.Items.Add("wignyan");
            comboBox1.Items.Add("cecak");
            comboBox1.Items.Add("pangkon");
            comboBox1.Items.Add("pangku");
            comboBox1.Items.Add("paten");
            comboBox1.Items.Add("pengkal");
            comboBox1.Items.Add("cakra");
            comboBox1.Items.Add("keret");
            comboBox1.Items.Add("0");
            comboBox1.Items.Add("1");
            comboBox1.Items.Add("2");
            comboBox1.Items.Add("3");
            comboBox1.Items.Add("4");
            comboBox1.Items.Add("5");
            comboBox1.Items.Add("6");
            comboBox1.Items.Add("7");
            comboBox1.Items.Add("8");
            comboBox1.Items.Add("9");
            comboBox1.Items.Add("adeg adeg");
            comboBox1.Items.Add("pada lungsi");
            comboBox1.Items.Add("pada lingsa");
            #endregion

            inputcol = 95; //cuma ambil kolom parent dan segment dari file csv
            inputrow = 0;
            image_counter = 0;
            btnNext.Enabled = false;
            btnDelete.Enabled = false;
        }
        public void InputFile(String path)
        {
            inputrow = 0;
            bool skip_first_row = true;
            using (System.IO.StreamReader sr = System.IO.File.OpenText(path))
            {
                String input;
                while ((input = sr.ReadLine()) != null)
                {
                    if (skip_first_row == true)
                    {
                        skip_first_row = false;
                        continue;
                    }
                    inputrow++;
                }
            }
        }
        public void ReadInputData(string path)
        {
            inputdata = new string[inputrow][];
            for (int i = 0; i < inputrow; i++)
            {
                inputdata[i] = new string[inputcol + 1];//+1 buat assign label huruf
            }
            bool skip_first_row = true;
            int rowcounter = 0;
            using (System.IO.StreamReader sr = System.IO.File.OpenText(path))
            {
                String input;
                while ((input = sr.ReadLine()) != null)
                {
                    if (skip_first_row == true)
                    {
                        skip_first_row = false;
                        continue;
                    }
                    string[] value = input.Split(';');
                    int colcounter = 0;
                    for (int i = 0; i < value.Count(); i++)
                    {
                        if (value[i] != "" )
                        {
                            inputdata[rowcounter][colcounter] = value[i];
                            colcounter++;
                        }
                    }
                    rowcounter++;
                }
            }
        }
        private void previewImage(string path, string[] input)
        {
            OImage = new CImage(path + "\\product-" + input[1].Split('\'')[1] + "\\segment-" + input[2].Split('\'')[1] + ".jpg");
            PictureBox_OriginalImage.BackgroundImage = OImage.getOriginalImage(); // show the original image
        }
        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Load the saved CSV";
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog1.FileName != "" && openFileDialog1.CheckFileExists == true)
                    {
                        filename = openFileDialog1.FileName;
                        txtFilename.Text = openFileDialog1.SafeFileName;
                        InputFile(filename);
                        ReadInputData(filename);
                        if (inputdata.Count() > 0)
                        {
                            previewImage(Path.GetDirectoryName(filename), inputdata[0]);
                            image_counter = 0;
                            MessageBox.Show("CSV is Succesfully Loaded");
                            btnNext.Text = "Next (" + (image_counter + 1) + " dari " + inputdata.Count() + ")";
                            btnNext.Enabled = true;
                            btnDelete.Enabled = true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("CSV is NOT Succesfully Loaded");
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Something Wrong with Your CSV \n" + err.Message);
            }
        }
        public void saveIntoFile(string path)
        {

            LinkedList<String[]> csvStr = new LinkedList<String[]>();
            string strToWrite = "Image Data|Parent Folder|Segment Image|Nama Huruf|X|Y|Width|Height";
            for (int i = 0; i < 88; i++)
                strToWrite += "|F" + (i + 1);
            String[] lineCsv = strToWrite.Split('|');
            // insert table title
            csvStr.AddLast(lineCsv);

            for (int i = 0; i < inputdata.Count(); i++)
            {
                if (inputdata[i][inputdata[i].Count() - 1] != "")
                {
                    lineCsv = new String[8 + 88];
                    lineCsv[0] = "imgData.jpg";
                    lineCsv[1] = "'" + inputdata[i][1];
                    lineCsv[2] = "'" + inputdata[i][2];
                    lineCsv[3] = inputdata[i][inputdata[i].Count() - 1];
                    // X and Y
                    lineCsv[4] = inputdata[i][3];
                    lineCsv[5] = inputdata[i][4];
                    // Width and Height
                    lineCsv[6] = inputdata[i][5];
                    lineCsv[7] = inputdata[i][6];
                    // features
                    for (int l = 0; l < 88; l++)
                        lineCsv[8 + l] = inputdata[i][7 + l];

                    // push to linked list
                    csvStr.AddLast(lineCsv);
                }
            }

            String csvPath = path + "\\88result - labeled.csv";
            if (!System.IO.File.Exists(csvPath))
            {
                System.IO.File.Create(csvPath).Close();
            }

            string delimiter = ";";
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < csvStr.Count; i++)
            {
                sb.AppendLine(string.Join(delimiter, csvStr.ElementAt(i)));
            }

            System.IO.File.WriteAllText(csvPath, sb.ToString());
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {
                inputdata[image_counter][inputdata[image_counter].Count() - 1] = comboBox1.SelectedItem.ToString();
                image_counter++;
                if(image_counter < inputdata.Count())
                {
                    previewImage(Path.GetDirectoryName(filename), inputdata[image_counter]);
                    btnNext.Text = "Next (" + (image_counter + 1) + " dari " + inputdata.Count() + ")";
                }
            }

            if(image_counter == inputdata.Count())
            {
                saveIntoFile(Path.GetDirectoryName(filename));
                MessageBox.Show("Labeling process is DONE \n The result is save at \n" + Path.GetDirectoryName(filename));
                btnNext.Enabled = false;
                btnDelete.Enabled = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            inputdata[image_counter][inputdata[image_counter].Count() - 1] = "";
            image_counter++;

            if (image_counter < inputdata.Count())
            {
                previewImage(Path.GetDirectoryName(filename), inputdata[image_counter]);
                btnNext.Text = "Next (" + (image_counter + 1) + " dari " + inputdata.Count() + ")";
            }

            if (image_counter == inputdata.Count())
            {
                saveIntoFile(Path.GetDirectoryName(filename));
                MessageBox.Show("Labeling process is DONE \n The result is save at \n" + Path.GetDirectoryName(filename));
                btnNext.Enabled = false;
                btnDelete.Enabled = false;
            }
        }
    }
}
