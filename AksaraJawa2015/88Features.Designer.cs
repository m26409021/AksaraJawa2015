﻿namespace AksaraJawa2015
{
    partial class _88Features
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OriginalImage = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.FeatureExtract3 = new System.Windows.Forms.RadioButton();
            this.FeatureExtract2 = new System.Windows.Forms.RadioButton();
            this.FeatureExtract1 = new System.Windows.Forms.RadioButton();
            this.txtMonoThreshold = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.PictureBox_OriginalImage = new System.Windows.Forms.PictureBox();
            this.OriginalImage.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).BeginInit();
            this.SuspendLayout();
            // 
            // OriginalImage
            // 
            this.OriginalImage.Controls.Add(this.label3);
            this.OriginalImage.Controls.Add(this.groupBox6);
            this.OriginalImage.Controls.Add(this.txtMonoThreshold);
            this.OriginalImage.Controls.Add(this.btnBrowse);
            this.OriginalImage.Controls.Add(this.PictureBox_OriginalImage);
            this.OriginalImage.Location = new System.Drawing.Point(13, 13);
            this.OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.OriginalImage.Name = "OriginalImage";
            this.OriginalImage.Padding = new System.Windows.Forms.Padding(27, 25, 27, 25);
            this.OriginalImage.Size = new System.Drawing.Size(553, 631);
            this.OriginalImage.TabIndex = 1;
            this.OriginalImage.TabStop = false;
            this.OriginalImage.Text = "Original Image";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 498);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Threshold";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.FeatureExtract3);
            this.groupBox6.Controls.Add(this.FeatureExtract2);
            this.groupBox6.Controls.Add(this.FeatureExtract1);
            this.groupBox6.Location = new System.Drawing.Point(344, 498);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(197, 60);
            this.groupBox6.TabIndex = 32;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Feature Extraction Method";
            // 
            // FeatureExtract3
            // 
            this.FeatureExtract3.AutoSize = true;
            this.FeatureExtract3.Checked = true;
            this.FeatureExtract3.Location = new System.Drawing.Point(108, 25);
            this.FeatureExtract3.Margin = new System.Windows.Forms.Padding(4);
            this.FeatureExtract3.Name = "FeatureExtract3";
            this.FeatureExtract3.Size = new System.Drawing.Size(37, 21);
            this.FeatureExtract3.TabIndex = 2;
            this.FeatureExtract3.TabStop = true;
            this.FeatureExtract3.Text = "3";
            this.FeatureExtract3.UseVisualStyleBackColor = true;
            // 
            // FeatureExtract2
            // 
            this.FeatureExtract2.AutoSize = true;
            this.FeatureExtract2.Location = new System.Drawing.Point(59, 25);
            this.FeatureExtract2.Margin = new System.Windows.Forms.Padding(4);
            this.FeatureExtract2.Name = "FeatureExtract2";
            this.FeatureExtract2.Size = new System.Drawing.Size(37, 21);
            this.FeatureExtract2.TabIndex = 1;
            this.FeatureExtract2.Text = "2";
            this.FeatureExtract2.UseVisualStyleBackColor = true;
            // 
            // FeatureExtract1
            // 
            this.FeatureExtract1.AutoSize = true;
            this.FeatureExtract1.Location = new System.Drawing.Point(9, 25);
            this.FeatureExtract1.Margin = new System.Windows.Forms.Padding(4);
            this.FeatureExtract1.Name = "FeatureExtract1";
            this.FeatureExtract1.Size = new System.Drawing.Size(37, 21);
            this.FeatureExtract1.TabIndex = 0;
            this.FeatureExtract1.Text = "1";
            this.FeatureExtract1.UseVisualStyleBackColor = true;
            // 
            // txtMonoThreshold
            // 
            this.txtMonoThreshold.Location = new System.Drawing.Point(135, 498);
            this.txtMonoThreshold.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonoThreshold.Name = "txtMonoThreshold";
            this.txtMonoThreshold.Size = new System.Drawing.Size(201, 22);
            this.txtMonoThreshold.TabIndex = 15;
            this.txtMonoThreshold.Text = "96";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(17, 566);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(533, 42);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse and Run";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // PictureBox_OriginalImage
            // 
            this.PictureBox_OriginalImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBox_OriginalImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox_OriginalImage.Location = new System.Drawing.Point(8, 23);
            this.PictureBox_OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox_OriginalImage.Name = "PictureBox_OriginalImage";
            this.PictureBox_OriginalImage.Padding = new System.Windows.Forms.Padding(13, 12, 13, 12);
            this.PictureBox_OriginalImage.Size = new System.Drawing.Size(533, 467);
            this.PictureBox_OriginalImage.TabIndex = 0;
            this.PictureBox_OriginalImage.TabStop = false;
            // 
            // _88Features
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 657);
            this.Controls.Add(this.OriginalImage);
            this.Name = "_88Features";
            this.Text = "_88Features";
            this.OriginalImage.ResumeLayout(false);
            this.OriginalImage.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox OriginalImage;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.PictureBox PictureBox_OriginalImage;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton FeatureExtract3;
        private System.Windows.Forms.RadioButton FeatureExtract2;
        private System.Windows.Forms.RadioButton FeatureExtract1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMonoThreshold;
    }
}