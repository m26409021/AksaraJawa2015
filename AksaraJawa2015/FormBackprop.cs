﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Diagnostics;

namespace AksaraJawa2015
{
    public partial class FormBackprop : Form
    {
        Backpropagation TrBackprop;
        Backpropagation loadedNetworks;
        Thread t,prunt,chi2;

        bool TrainNormalized = false,TestNormalized = false, PrunTrainNormalized = false, PrunTestNormalized = false;
        float[][] CopyKtable;
        float inconsistency;
        float thresholdIncons;
        int maxTable;
        int[] gridcounter;
        Stopwatch timespent = new Stopwatch();
        string filename;

        #region aksara_variables
        bool aksara = true; //ngefek ke cara baca file dan generate testing nodes
        bool HoughFeatures = false; //radio button yang dipilih di feature type
        int added_class = 0;
        List<string> aksara_train_list;
        List<int> houghCol, _88featuresCol;//tandai kolom mana saja yang mau dibaca
        #endregion
        // Thread safe updating of control's text property
        //private delegate void SetTextCallback( string text);
        //public void SetText(string text)
        //{
        //    if (textBox8.InvokeRequired)
        //    {
        //        SetTextCallback d = new SetTextCallback(SetText);
        //        Invoke(d, new object[] { text });
        //    }
        //    else
        //    {
        //        textBox8.Text = text;
        //    }
        //}
        public FormBackprop()
        {
            InitializeComponent();
            
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            tabControl2.TabPages[0].Text = "Backpropagation";
            tabControl2.TabPages[1].Text = "Pruning";

            #region _88features_used_columns
            _88featuresCol = new List<int>();
            _88featuresCol.Add(3);
            _88featuresCol.Add(8);
            _88featuresCol.Add(9);
            _88featuresCol.Add(10);
            _88featuresCol.Add(11);
            _88featuresCol.Add(12);
            _88featuresCol.Add(13);
            _88featuresCol.Add(14);
            _88featuresCol.Add(15);
            _88featuresCol.Add(16);
            _88featuresCol.Add(17);
            _88featuresCol.Add(18);
            _88featuresCol.Add(19);
            _88featuresCol.Add(20);
            _88featuresCol.Add(21);
            _88featuresCol.Add(22);
            _88featuresCol.Add(23);
            _88featuresCol.Add(24);
            _88featuresCol.Add(25);
            _88featuresCol.Add(26);
            _88featuresCol.Add(27);
            _88featuresCol.Add(28);
            _88featuresCol.Add(29);
            _88featuresCol.Add(30);
            _88featuresCol.Add(31);
            _88featuresCol.Add(32);
            _88featuresCol.Add(33);
            _88featuresCol.Add(34);
            _88featuresCol.Add(35);
            _88featuresCol.Add(36);
            _88featuresCol.Add(37);
            _88featuresCol.Add(38);
            _88featuresCol.Add(39);
            _88featuresCol.Add(40);
            _88featuresCol.Add(41);
            _88featuresCol.Add(42);
            _88featuresCol.Add(43);
            _88featuresCol.Add(44);
            _88featuresCol.Add(45);
            _88featuresCol.Add(46);
            _88featuresCol.Add(47);
            _88featuresCol.Add(48);
            _88featuresCol.Add(49);
            _88featuresCol.Add(50);
            _88featuresCol.Add(51);
            _88featuresCol.Add(52);
            _88featuresCol.Add(53);
            _88featuresCol.Add(54);
            _88featuresCol.Add(55);
            _88featuresCol.Add(56);
            _88featuresCol.Add(57);
            _88featuresCol.Add(58);
            _88featuresCol.Add(59);
            _88featuresCol.Add(60);
            _88featuresCol.Add(61);
            _88featuresCol.Add(62);
            _88featuresCol.Add(63);
            _88featuresCol.Add(64);
            _88featuresCol.Add(65);
            _88featuresCol.Add(66);
            _88featuresCol.Add(67);
            _88featuresCol.Add(68);
            _88featuresCol.Add(69);
            _88featuresCol.Add(70);
            _88featuresCol.Add(71);
            _88featuresCol.Add(72);
            _88featuresCol.Add(73);
            _88featuresCol.Add(74);
            _88featuresCol.Add(75);
            _88featuresCol.Add(76);
            _88featuresCol.Add(77);
            _88featuresCol.Add(78);
            _88featuresCol.Add(79);
            _88featuresCol.Add(80);
            _88featuresCol.Add(81);
            _88featuresCol.Add(82);
            _88featuresCol.Add(83);
            _88featuresCol.Add(84);
            _88featuresCol.Add(85);
            _88featuresCol.Add(86);
            _88featuresCol.Add(87);
            _88featuresCol.Add(88);
            _88featuresCol.Add(89);
            _88featuresCol.Add(90);
            _88featuresCol.Add(91);
            _88featuresCol.Add(92);
            _88featuresCol.Add(93);
            _88featuresCol.Add(94);
            _88featuresCol.Add(95);
            #endregion

            #region Hough_used_colums
            houghCol = new List<int>();
            houghCol.Add(3);
            houghCol.Add(8);
            houghCol.Add(9);
            houghCol.Add(10);
            houghCol.Add(11);
            houghCol.Add(14);
            houghCol.Add(17);
            houghCol.Add(19);
            #endregion

            //tabcontrol 1
            label16.Text = "No File Selected";
            label17.Text = "-";
            label18.Text = "-";
            label19.Text = "-";
            label20.Text = "-";

            rectangleShape1.Visible = false;
            label21.Visible = false;

            button7.Visible = false;//stop training button

            folderBrowserDialog1.SelectedPath = @"C:\Users\Jeffry\Documents\Visual Studio 2012\Projects\";
            folderBrowserDialog1.Description = "Select a Folder";

            textBox1.WordWrap = true;
            textBox1.ScrollBars = ScrollBars.Vertical;
            
            tabControl1.TabPages[0].Text = "Training Network Info";
            tabControl1.TabPages[1].Text = "Testing Network Info";
            tabControl1.TabPages[2].Text = "Training I/O";
            tabControl1.TabPages[3].Text = "Testing I/O";
            tabControl1.TabPages[4].Text = "Chi2 Output";

            tabControl3.TabPages[0].Text = "Training Pruning Info";
            tabControl3.TabPages[1].Text = "Testing Pruning Info";
            tabControl3.TabPages[2].Text = "Training I/O";
            tabControl3.TabPages[3].Text = "Testing I/O";
            gridcounter = new int[tabControl1.TabPages.Count];
            for (int i = 0; i < tabControl1.TabPages.Count; i++)
            {
                gridcounter[i] = 0;
            }
            
            dataGridView1.Columns.Add("number", "#");
            dataGridView1.Columns.Add("acc", "Accuracy(%)");
            dataGridView1.Columns.Add("euclid", "Euclidean Distance");
            dataGridView1.Columns.Add("error", "SSE value");
            dataGridView1.Columns.Add("epoch", "Number of epoch");
            dataGridView1.Columns.Add("input", "# of input left");
            dataGridView1.Columns.Add("hidden", "# of hidden left");
            dataGridView1.Columns.Add("output", "# of weight left");
            dataGridView1.Columns.Add("time", "TimeSpent");
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            dataGridView2.Columns.Add("number", "#");
            dataGridView2.Columns.Add("acc", "Accuracy(%)");
            dataGridView2.Columns.Add("euclid", "Euclidean Distance");
            dataGridView2.Columns.Add("error", "SSE value");
            dataGridView2.Columns.Add("epoch", "Number of epoch");
            dataGridView2.Columns.Add("input", "# of input left");
            dataGridView2.Columns.Add("hidden", "# of hidden left");
            dataGridView2.Columns.Add("output", "# of weight left");
            dataGridView2.Columns.Add("time", "TimeSpent");
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            dataGridView5.Columns.Add("number", "#");
            dataGridView5.Columns.Add("acc", "Accuracy(%)");
            dataGridView5.Columns.Add("euclid", "Euclidean Distance");
            dataGridView5.Columns.Add("input", "# of input left");
            dataGridView5.Columns.Add("hidden", "# of hidden left");
            dataGridView5.Columns.Add("output", "# of weight left");
            dataGridView5.Columns.Add("time", "TimeSpent");
            dataGridView5.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            dataGridView6.Columns.Add("number", "#");
            dataGridView6.Columns.Add("acc", "Accuracy(%)");
            dataGridView6.Columns.Add("euclid", "Euclidean Distance");
            dataGridView6.Columns.Add("input", "# of input left");
            dataGridView6.Columns.Add("hidden", "# of hidden left");
            dataGridView6.Columns.Add("output", "# of weight left");
            dataGridView6.Columns.Add("time", "TimeSpent");
            dataGridView6.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

            

            checkedListBox1.ScrollAlwaysVisible = true;

            dataGridView1.ReadOnly = true;
            dataGridView2.ReadOnly = true;
            dataGridView3.ReadOnly = true;
            dataGridView4.ReadOnly = true;
            dataGridView5.ReadOnly = true;
            dataGridView6.ReadOnly = true;
            dataGridView7.ReadOnly = true;
            dataGridView8.ReadOnly = true;
            dataGridView9.ReadOnly = true;
            //tabcontrol 1

        }
        private void button1_Click(object sender, EventArgs e)
        {
            //CHOOSE PATH BY USING BROWSE BUTTON
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                checkedListBox1.Items.Clear();
                try
                {               
                    textBox1.Text = folderBrowserDialog1.SelectedPath;

                    //FIND ALL FILES IN FOLDER 
                    System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(folderBrowserDialog1.SelectedPath);
                    foreach (System.IO.FileInfo files in dir.GetFiles("*.*"))
                    {
                        checkedListBox1.Items.Add(files.Name);
                    }
                    
                }catch (Exception err)
                {
                    MessageBox.Show("An error has occurred: " + err.Message);
                }
            }
        }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            //CHOOSE PATH BY PRESSING ENTER
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    MessageBox.Show(textBox1.Text);
                    //FIND ALL FILES IN FOLDER 
                    System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(textBox1.Text);
                    foreach (System.IO.FileInfo files in dir.GetFiles("*.*"))
                    {
                        checkedListBox1.Items.Add(files.Name);
                    }
                    char[] delimiter = { ',' };                   
                }
                catch (Exception err)
                {
                    MessageBox.Show("An error has occurred: " + err.Message);
                }
            }
        }
        #region additional_code
        public int InputFileAksara(string path, bool testing = false)//menghitung jumlah class dalam 1 file
        {
            bool skip_first_row = true;
            if(testing == false || aksara_train_list == null)
                aksara_train_list = new List<string>();

            int index_nama_huruf = 0;
            if (HoughFeatures == true)
            {
                index_nama_huruf = houghCol.First();
            }
            else
            {
                index_nama_huruf = _88featuresCol.First();
            }
            using (System.IO.StreamReader sr = System.IO.File.OpenText(path))
            {
                String input;
                while ((input = sr.ReadLine()) != null)
                {
                    if (skip_first_row == true)
                    {
                        skip_first_row = false;
                        continue;
                    }
                    string[] column = input.Split(';');
                    

                    if (!aksara_train_list.Contains(column[index_nama_huruf]))
                    {
                        aksara_train_list.Add(column[index_nama_huruf]);
                    }
                }
            }
            return aksara_train_list.Count;
        }
        public void diff_fileAksara(string path, bool testing = false)//menghitung jumlah class dalam 1 file
        {
            if (testing == false || aksara_train_list == null)
                aksara_train_list = new List<string>();
            using (System.IO.StreamReader sr = System.IO.File.OpenText(path))
            {
                String input;
                bool first_line = false;
                String last_char = "";
                int index_nama_huruf = 0;
                if (HoughFeatures == true)
                {
                    index_nama_huruf = houghCol.First();
                }
                else
                {
                    index_nama_huruf = _88featuresCol.First();
                }
                while ((input = sr.ReadLine()) != null)
                {
                    if (first_line == false)
                    {
                        first_line = true;
                        continue;
                    }
                    else
                    {
                        string[] column = input.Split(';');
                        if (!aksara_train_list.Contains(column[index_nama_huruf]) && last_char != column[index_nama_huruf])
                        {
                            last_char = column[index_nama_huruf];
                            added_class++;
                        }
                    }
                }
            }
        }
        #endregion

        #region GENERATE CHI2 BUTTON
        public void ChiSquare()
        {

            Backpropagation tempClass = null;
            if (aksara == true)
            {
                int num_of_classes = 0;//number of classes on selected file
                for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                {
                    if (checkedListBox1.GetItemChecked(i))
                    {
                        string tempPath = textBox1.Text;//default path
                        tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                        num_of_classes = InputFileAksara(tempPath);
                        break;
                    }
                }
                tempClass = new Backpropagation(num_of_classes);

                for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                {
                    if (checkedListBox1.GetItemChecked(i))
                    {
                        string tempPath = textBox1.Text;//default path
                        tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                        tempClass.InputFile(tempPath, 1, aksara, false, HoughFeatures);

                    }
                }
                tempClass.GenerateTestingData();
                tempClass.resetclasscounter();
                for (int i = 0; i < checkedListBox1.Items.Count; i++)//read the input file
                {
                    if (checkedListBox1.GetItemChecked(i))
                    {
                        string tempPath = textBox1.Text;//default path
                        tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                        tempClass.ReadInputData(tempPath, aksara, HoughFeatures);
                    }
                }
            }
            else
            {
                tempClass = new Backpropagation(checkedListBox1.CheckedItems.Count);

                for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                {
                    if (checkedListBox1.GetItemChecked(i))
                    {
                        string tempPath = textBox1.Text;//default path
                        tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                        tempClass.InputFile(tempPath, 1, aksara, false, HoughFeatures);

                    }
                }
                tempClass.GenerateTestingData();
                tempClass.resetclasscounter();
                for (int i = 0; i < checkedListBox1.Items.Count; i++)//read the input file
                {
                    if (checkedListBox1.GetItemChecked(i))
                    {
                        string tempPath = textBox1.Text;//default path
                        tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                        tempClass.ReadInputData(tempPath, aksara, HoughFeatures);
                    }
                }
            }
            #region Chi2 Algorithm

            inconsistency = 0;
            float alpha = (float)0.455;//chi-square table df=1,sigLevel=0.5
            //tempClass.roundUP(3);
            
            //phase 1
            do
            {
                tempClass.saveInputData();
                for (int i = 0; i < tempClass.numberofAttribs; i++)
                {
                    tempClass.sortinputdata(i);
                    do
                    {
                        tempClass.Ksquaretable(i);
                        if (tempClass.PKtableCount <= 1)
                            break;
                        tempClass.CalKsquare();
                    } while (tempClass.mergeKvalue(i, alpha,1));
                }
                alpha += (float)0.455;
                tempClass.sortinputdata(0);
                inconsistency = tempClass.calInconsistency(0);
            } while ((float)inconsistency / (float)tempClass.totalData <= 0);
            //phase 1

            //phase 2
            tempClass.retrieveInputData();
            bool[] attributecannotbemerged = new bool[tempClass.numberofAttribs];
            int noattributecanbemerged = 0;
            float[] attribalpha = new float[tempClass.numberofAttribs];
            //for (int i = 0; i < attribalpha.Count(); i++)
            //    attribalpha[i] = alpha;
            //do
            //{
            //    for (int i = 0; i < tempClass.numberofAttribs; i++)
            //    {
            //        if (attributecannotbemerged[i] == false)
            //        {
            //            tempClass.saveInputData();
            //            tempClass.sortinputdata(i);
            //            do
            //            {
            //                tempClass.Ksquaretable(i);
            //                if (tempClass.PKtableCount <= 1)
            //                {
            //                    attributecannotbemerged[i] = true;
            //                    break;
            //                }
            //                tempClass.CalKsquare();
            //            } while (tempClass.mergeKvalue(i, attribalpha[i], 2));
            //            inconsistency = tempClass.calInconsistency(i);

            //            if ((float)inconsistency / (float)tempClass.totalData > 0.05)
            //            {
            //                attributecannotbemerged[i] = true;
            //                tempClass.retrieveInputData();
            //            }
            //            else
            //            {
            //                attribalpha[i] += (float)0.455;
            //            }
            //        }
            //    }
            //    noattributecanbemerged = 0;
            //    for (int i = 0; i < tempClass.numberofAttribs; i++)
            //    {
            //        if (attributecannotbemerged[i] == true)
            //            noattributecanbemerged++;
            //    }
            //} while (noattributecanbemerged < tempClass.numberofAttribs);
            //phase 2

            //phase 3
            tempClass.initializeKsquareStatus();
            for (int i = 0; i < tempClass.numberofAttribs; i++)
            {
                attributecannotbemerged[i] = false;
                tempClass.sortinputdata(i);
                tempClass.Ksquaretable(i);
                tempClass.KsquareStatus(i);//to optimize phase 3, when an interval cannot be merged then leave it from minimum option "removeMinKsquare(i, attribalpha[i]);"
            }
            do
            {
                for (int i = 0; i < tempClass.numberofAttribs; i++)
                {
                    int minIncons = tempClass.totalData, minIndexIncons = 0;

                    if (attributecannotbemerged[i] == false)
                    {
                        tempClass.saveInputData();
                        tempClass.sortinputdata(i);
                        tempClass.Ksquaretable(i);
                        if (tempClass.PKtableCount <= 1)
                        {
                            attributecannotbemerged[i] = true;
                            break;
                        }
                        tempClass.CalKsquare();
                        attribalpha[i] = tempClass.getMaxKsquare(i);
                        while (tempClass.mergeKvalue(i, attribalpha[i], 3))
                        {
                            inconsistency = tempClass.calInconsistency(i);
                            int removeIndexIncons = tempClass.getMinIndexKsquare(i);
                            if (minIncons > inconsistency)
                            {
                                minIncons = (int)inconsistency;
                                minIndexIncons = tempClass.getMinIndexKsquare(i);
                                if (inconsistency == 0)
                                    break;
                            }

                            tempClass.retrieveInputData();
                            tempClass.sortinputdata(i);
                            if ((float)inconsistency / (float)tempClass.totalData <= thresholdIncons)
                                tempClass.removeMinKsquare(i, attribalpha[i], false, removeIndexIncons);
                            else
                                tempClass.removeMinKsquare(i, attribalpha[i], true, removeIndexIncons);
                        }
                        if ((float)minIncons / (float)tempClass.totalData <= thresholdIncons)
                        {
                            tempClass.retrieveInputData();
                            tempClass.sortinputdata(i);
                            tempClass.mergeByIndex(i, minIndexIncons);
                            tempClass.updateKsquareStatus(i, minIndexIncons);
                        }
                        else
                        {
                            attributecannotbemerged[i] = true;
                        }
                    }
                }
                noattributecanbemerged = 0;
                for (int i = 0; i < tempClass.numberofAttribs; i++)
                {
                    if (attributecannotbemerged[i] == true)
                        noattributecanbemerged++;
                }
                Console.WriteLine(noattributecanbemerged);
            } while (noattributecanbemerged < tempClass.numberofAttribs);

            //tempClass.initializeKsquareStatus();
            //for (int i = 0; i < tempClass.numberofAttribs; i++)
            //{
            //    attributecannotbemerged[i] = false;
            //    tempClass.sortinputdata(i);
            //    tempClass.Ksquaretable(i);
            //    tempClass.KsquareStatus(i);//to optimize phase 3, when an interval cannot be merged then leave it from minimum option "removeMinKsquare(i, attribalpha[i]);"
            //}
            //noattributecanbemerged = 0;
            //do
            //{
            //    for (int i = 0; i < tempClass.numberofAttribs; i++)
            //    {
            //        if (attributecannotbemerged[i] == false)
            //        {
            //            tempClass.saveInputData();
            //            tempClass.sortinputdata(i);

            //            tempClass.Ksquaretable(i);
            //            if (tempClass.PKtableCount <= 1)
            //            {
            //                attributecannotbemerged[i] = true;
            //                break;
            //            }
            //            tempClass.CalKsquare();
            //            attribalpha[i] = tempClass.getMaxKsquare(i);

            //            do
            //            {
            //                if (tempClass.mergeKvalue(i, attribalpha[i], 3))
            //                {
            //                    inconsistency = tempClass.calInconsistency(i);

            //                    if ((float)inconsistency / (float)tempClass.totalData > 0)
            //                    {
            //                        tempClass.retrieveInputData();
            //                        tempClass.sortinputdata(i);
            //                        tempClass.removeMinKsquare(i, attribalpha[i]);
            //                    }
            //                    else
            //                    {
            //                        tempClass.updateKsquareStatus(i,);
            //                        break;
            //                    }
            //                }
            //                else
            //                {
            //                    attributecannotbemerged[i] = true;
            //                    break;
            //                }
            //            } while (true);
            //        }
            //    }
            //    noattributecanbemerged = 0;
            //    for (int i = 0; i < tempClass.numberofAttribs; i++)
            //    {
            //        if (attributecannotbemerged[i] == true)
            //            noattributecanbemerged++;
            //    }
            //} while (noattributecanbemerged < tempClass.numberofAttribs);
            //phase 3

            //Chi2 Output
            tempClass.retrieveInputData();
            CopyKtable = new float[tempClass.numberofAttribs][];
            maxTable = 0;
            for (int i = 0; i < tempClass.numberofAttribs; i++)
            {
                tempClass.sortinputdata(i);
                tempClass.Ksquaretable(i);
                CopyKtable[i] = new float[tempClass.PKtableCount];

                if (maxTable < tempClass.PKtableCount)
                    maxTable = tempClass.PKtableCount;

                for (int j = 0; j < tempClass.PKtableCount; j++)
                    CopyKtable[i][j] = tempClass.getKsquaretable(j);
            }
            //for (int i = 0; i < tempClass.numberofAttribs; i++)
            //{
            //    tempClass.sortinputdata(i);
            //    tempClass.Ksquaretable(i);
            //    tempClass.CalKsquare();
            //    tempClass.printtofile(i);
            //}
            //return CopyKtable;
            #endregion
        }
        #endregion


        public void NetworkTestingInfo(int tab)
        {
            try
            {
                //before resetting the SSEvalue and numofepoch
                float SSEvalue = 0;
                int NumofEpochs = 0;
                if (tab == 0)
                {
                    SSEvalue = TrBackprop.PtempSSE;
                    NumofEpochs = TrBackprop.getEpochs();
                }
                //////////////////////////////////////////

                if (tab != 0) //testing setelah training gk perlu regenerate apapun
                {
                    int num_of_classes = 0;//number of classes on selected file, used if aksara == true
                    if (aksara == true)
                    {
                        for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                        {
                            if (checkedListBox1.GetItemChecked(i))
                            {
                                string tempPath = textBox1.Text;//default path
                                tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                                diff_fileAksara(tempPath, true);
                                num_of_classes = InputFileAksara(tempPath, true);//true for testing
                                break;
                            }
                        }
                    }
                    if (TrBackprop == null)
                    {
                        if (aksara == false)
                        {
                            TrBackprop = new Backpropagation(checkedListBox1.CheckedItems.Count);
                        }
                        else
                        {
                            TrBackprop = new Backpropagation(num_of_classes);
                        }
                        tab = 1;//just for testing,so don't generate CValidation data
                    }

                    if (aksara == false)
                    {
                        TrBackprop.resetValue(checkedListBox1.CheckedItems.Count, aksara, true);//true for testing
                    }
                    else
                    {
                        TrBackprop.resetValue(num_of_classes, aksara, true);//true for testing
                    }

                    for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                    {
                        if (checkedListBox1.GetItemChecked(i))
                        {
                            string tempPath = textBox1.Text;//default path
                            tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                            TrBackprop.InputFile(tempPath, tab, aksara, true, HoughFeatures);//true for testing
                        }
                    }

                    TrBackprop.resetclasscounter(); //true for testing
                    TrBackprop.GenerateTestingData(aksara, true); //true for testing

                    for (int i = 0; i < checkedListBox1.Items.Count; i++)//read the input file
                    {
                        if (checkedListBox1.GetItemChecked(i))
                        {
                            string tempPath = textBox1.Text;//default path
                            tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                            TrBackprop.ReadInputData(tempPath, aksara, HoughFeatures); 

                        }
                    }
                }


                float WrongAnswer = 0;
                if (tab == 0)
                {
                    if (TrainNormalized == true)
                    {
                        //TrBackprop.CopyInput();
                        TrBackprop.NormalizeData();
                    }
                    if (TrBackprop.Pdiscrete == true && TrBackprop.PAuto == false)
                        TrBackprop.addManualDiscreteVariable();
                    //if(radioButton1.Checked == true)//slow
                    WrongAnswer = (float)TrBackprop.StartTesting(dataGridView3);
                    //else if(radioButton2.Checked == true)//fast
                    //    WrongAnswer = (float)TrBackprop.New_StartTesting(dataGridView3,2);

                    for (int j = 0; j < dataGridView3.Columns.Count; j++)
                        dataGridView3.Columns[j].Width = 60;
                }
                else if (tab == 1)
                {
                    if (TestNormalized == true)
                    {
                        //TrBackprop.CopyInput();
                        TrBackprop.NormalizeData();
                    }
                    if (TrBackprop.Pdiscrete == true && TrBackprop.PAuto == false)
                        TrBackprop.addManualDiscreteVariable();

                    //if (radioButton1.Checked == true)//slow
                    if (aksara == true)
                        WrongAnswer = (float)TrBackprop.StartTesting(dataGridView4, added_class);
                    else
                        WrongAnswer = (float)TrBackprop.StartTesting(dataGridView4);
                    //else if (radioButton2.Checked == true)//fast
                    //    WrongAnswer = (float)TrBackprop.New_StartTesting(dataGridView4,2);

                    for (int j = 0; j < dataGridView4.Columns.Count; j++)
                        dataGridView4.Columns[j].Width = 60;
                }
                else if (tab == 2)
                {
                    if (PrunTrainNormalized == true)
                    {
                        //TrBackprop.CopyInput();
                        TrBackprop.NormalizeData();
                    }

                    if (TrBackprop.Pdiscrete == true && TrBackprop.PAuto == false)
                        TrBackprop.addManualDiscreteVariable();

                    //if (TrBackprop.Ptype == true)//slow
                    WrongAnswer = (float)TrBackprop.StartTesting(dataGridView7);
                    //else//fast
                    //    WrongAnswer = (float)TrBackprop.New_StartTesting(dataGridView7, 2);

                    for (int j = 0; j < dataGridView7.Columns.Count; j++)
                        dataGridView7.Columns[j].Width = 60;
                }
                else if (tab == 3)
                {
                    if (PrunTestNormalized == true)
                    {
                        //TrBackprop.CopyInput();
                        TrBackprop.NormalizeData();
                    }

                    if (TrBackprop.Pdiscrete == true && TrBackprop.PAuto == false)
                        TrBackprop.addManualDiscreteVariable();

                    //if (TrBackprop.Ptype == true)//slow
                    if (aksara == true)
                        WrongAnswer = (float)TrBackprop.StartTesting(dataGridView8, added_class);
                    else
                        WrongAnswer = (float)TrBackprop.StartTesting(dataGridView8);
                    //else//fast
                    //    WrongAnswer = (float)TrBackprop.New_StartTesting(dataGridView8, 2);

                    for (int j = 0; j < dataGridView8.Columns.Count; j++)
                        dataGridView8.Columns[j].Width = 60;
                }

                float TotalData = (float)TrBackprop.totalData;
                float Accuracy = ((TotalData - WrongAnswer) / TotalData) * 100;

                if (tab == 0)//saat menghitung akurasi dari training
                    TrBackprop.PtrainAcc = Accuracy;

                float EuclidDistance = 0;
                //if(radioButton1.Checked == true)
                EuclidDistance = TrBackprop.getED();
                //else if(radioButton2.Checked == true)
                //    EuclidDistance = TrBackprop.getED(1);

                timespent.Stop();

                gridcounter[tab]++;

                string[] row = null;

                if (tab == 0 || tab == 1)
                    row = new string[] { gridcounter[tab].ToString(), Accuracy.ToString(), EuclidDistance.ToString(), SSEvalue.ToString(), NumofEpochs.ToString(), TrBackprop.countAvailInput().ToString(), TrBackprop.countAvailHidden().ToString(), TrBackprop.countAvailWeight().ToString(), timespent.Elapsed.TotalSeconds.ToString() };
                else if (tab == 2 || tab == 3)
                    row = new string[] { gridcounter[tab].ToString(), Accuracy.ToString(), EuclidDistance.ToString(), (textBox17.Lines.Count() - 1).ToString(), (textBox18.Lines.Count() - 1).ToString(), (textBox19.Lines.Count() - 1).ToString(), timespent.Elapsed.TotalSeconds.ToString() };

                if (tab == 0)
                {
                    dataGridView1.Rows.Add(row);
                    dataGridView1.ClearSelection();//If you want

                    int nRowIndex = dataGridView1.Rows.Count - 1;
                    dataGridView1.Rows[nRowIndex].Selected = true;

                    //In case if you want to scroll down as well.
                    dataGridView1.FirstDisplayedScrollingRowIndex = nRowIndex;
                }
                else if (tab == 1)
                {
                    dataGridView2.Rows.Add(row);
                    dataGridView2.ClearSelection();//If you want

                    int nRowIndex = dataGridView2.Rows.Count - 1;
                    dataGridView2.Rows[nRowIndex].Selected = true;

                    //In case if you want to scroll down as well.
                    dataGridView2.FirstDisplayedScrollingRowIndex = nRowIndex;
                }
                else if (tab == 2)
                {
                    dataGridView5.Rows.Add(row);
                    dataGridView5.ClearSelection();//If you want

                    int nRowIndex = dataGridView5.Rows.Count - 1;
                    dataGridView5.Rows[nRowIndex].Selected = true;

                    //In case if you want to scroll down as well.
                    dataGridView5.FirstDisplayedScrollingRowIndex = nRowIndex;
                }
                else if (tab == 3)
                {
                    dataGridView6.Rows.Add(row);
                    dataGridView6.ClearSelection();//If you want

                    int nRowIndex = dataGridView6.Rows.Count - 1;
                    dataGridView6.Rows[nRowIndex].Selected = true;

                    //In case if you want to scroll down as well.
                    dataGridView6.FirstDisplayedScrollingRowIndex = nRowIndex;
                }

            }
            catch (Exception err)
            {
                MessageBox.Show("Something Wrong !\n" + err.ToString());
            }
        }
        #region START TRAINING
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkedListBox1.CheckedItems.Count > 0)
                {
                    button2.Enabled = false;
                    button3.Enabled = false;
                    button5.Enabled = false;
                    button4.Enabled = false;
                    button7.Visible = true;
                    checkedListBox1.Enabled = false;
                    textBox8.Text = "";
                    textBox10.Text = "";

                    Boolean statusLoad = false;//to check whether the user want to use loaded weight or not
                    int num_of_classes = 0;//number of classes on selected file, used if aksara == true

                    if (checkBox1.Checked == true)
                    {
                        if (MessageBox.Show("Want to use loaded Network ?", "Confirm Network", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            if (aksara == true)
                            {
                                for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                                {
                                    if (checkedListBox1.GetItemChecked(i))
                                    {
                                        string tempPath = textBox1.Text;//default path
                                        tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                                        num_of_classes = InputFileAksara(tempPath);
                                        break;
                                    }
                                }
                                loadedNetworks.resetValue(num_of_classes);
                            }
                            else
                            {
                                loadedNetworks.resetValue(checkedListBox1.CheckedItems.Count);
                            }
                            loadedNetworks.LoadNetworks(filename);
                            aksara = loadedNetworks.PAksara;
                            HoughFeatures = loadedNetworks.PHoughFeatures;
                            TrBackprop = loadedNetworks;
                            for (int j = 0; j < dataGridView9.Columns.Count; j++)
                                dataGridView9.Columns[j].Width = 60;
                            statusLoad = true;
                        }
                    }

                    bool statusNew = true;

                    if (aksara == true && statusLoad == false)
                    {
                        for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                        {
                            if (checkedListBox1.GetItemChecked(i))
                            {
                                string tempPath = textBox1.Text;//default path
                                tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                                num_of_classes = InputFileAksara(tempPath);
                                break;
                            }
                        }
                    }
                    if (TrBackprop == null)
                    {
                        if (aksara == false)
                        {
                            TrBackprop = new Backpropagation(checkedListBox1.CheckedItems.Count);
                        }
                        else
                        {
                            TrBackprop = new Backpropagation(num_of_classes);
                        }
                        
                    }
                    else if (statusLoad == false)
                    {
                        if (MessageBox.Show("Want to generate new network ?", "Confirm Network", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            if (aksara == false)
                            {
                                TrBackprop = new Backpropagation(checkedListBox1.CheckedItems.Count);
                            }
                            else
                            {
                                TrBackprop = new Backpropagation(num_of_classes);
                                added_class = 0;
                            }
                        }
                        else
                        {
                            if (aksara == false)
                            {
                                TrBackprop.resetValue(checkedListBox1.CheckedItems.Count);
                            }
                            else
                            {
                                TrBackprop.resetValue(num_of_classes);
                            }
                            TrBackprop.resetclasscounter();
                            statusNew = false;
                        }

                    }
                    //TrBackprop.formbackprop = this;

                    TrBackprop.Pdiscrete = checkBox2.Checked;//buat menentukan pake discrete atau tidak

                    if (checkBox2.Checked == true && statusLoad == false)
                    {
                        if (radioButton2.Checked == true)
                        {
                            TrBackprop.PKtableCopy = CopyKtable;
                            TrBackprop.PAuto = true;
                            for (int j = 0; j < dataGridView9.Columns.Count; j++)
                                dataGridView9.Columns[j].Width = 60;
                        }
                        else
                        {
                            TrBackprop.PAuto = false;
                            TrBackprop.PNvalue = int.Parse(textBox9.Text);
                        }
                    }

                    //if (checkBox2.Checked == true && statusLoad==false)
                    //{
                    //    if (MessageBox.Show("Automaticaly generate N value ?", "Confirm N value", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    //    {
                    //        TrBackprop.PKtableCopy = ChiSquare();
                    //        TrBackprop.PAuto = true;
                    //        for (int j = 0; j < dataGridView9.Columns.Count; j++)
                    //            dataGridView9.Columns[j].Width = 60;
                    //    }
                    //    else
                    //    {
                    //        TrBackprop.PAuto = false;
                    //        TrBackprop.PNvalue = int.Parse(textBox9.Text);
                    //    }
                    //}

                    for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                    {
                        if (checkedListBox1.GetItemChecked(i))
                        {
                            string tempPath = textBox1.Text;//default path
                            tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                            TrBackprop.InputFile(tempPath, 0, aksara, false, HoughFeatures);

                        }
                    }

                    TrBackprop.GenerateTestingData();
                    if (statusLoad == false && statusNew == true)//if we don't want to use loaded network and we want to generate new network
                        TrBackprop.GenerateNetwork(int.Parse(textBox2.Text), int.Parse(textBox3.Text), float.Parse(textBox4.Text), float.Parse(textBox6.Text), int.Parse(textBox7.Text));//generate all array and network structure
                    else
                        TrBackprop.GenerateNetwork(-1, -1, float.Parse(textBox4.Text), float.Parse(textBox6.Text), int.Parse(textBox7.Text));//generate all array and network structure

                    TrBackprop.resetclasscounter();

                    //Read File input
                    for (int i = 0; i < checkedListBox1.Items.Count; i++)//read the input file
                    {
                        if (checkedListBox1.GetItemChecked(i))
                        {
                            string tempPath = textBox1.Text;//default path
                            tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                            TrBackprop.ReadInputData(tempPath, aksara, HoughFeatures);
                        }
                    }
                    if (MessageBox.Show("Do you want to normalize the data ?", "Normalize Data", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        TrBackprop.NormalizeData();
                        TrainNormalized = true;
                    }
                    else
                        TrainNormalized = false;

                    if (TrBackprop.Pdiscrete == true && TrBackprop.PAuto == false)
                        TrBackprop.addManualDiscreteVariable();

                    //if(radioButton1.Checked == true)
                    t = new Thread(new ThreadStart(TrBackprop.StartTraining));
                    //else if(radioButton2.Checked == true)
                    //    t = new Thread(new ThreadStart(TrBackprop.NewNew_StartTraining));

                    t.Start();
                    timespent.Restart();
                    timer1.Enabled = true;
                    timer1.Start();
                }
                else
                {
                    MessageBox.Show("Choose the input file first");
                }
            }
            catch(Exception err){
                MessageBox.Show("You missed something important.." + err.Message);
                button2.Enabled = true;
                button3.Enabled = true;
                button5.Enabled = true;
                button4.Enabled = true;
                button7.Visible = false;
                checkedListBox1.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (t.ThreadState != System.Threading.ThreadState.Running)
            {
                timer1.Stop();
                this.NetworkTestingInfo(0);//0 means, data is inputed to tab-1 /Training
                tabControl1.SelectedIndex = 0;
                button2.Enabled = true;
                button3.Enabled = true;
                button5.Enabled = true;
                button4.Enabled = true;
                button7.Visible = false;
                checkedListBox1.Enabled = true;
            }


            textBox8.Text = TrBackprop.PtempSSE.ToString();
            textBox10.Text = TrBackprop.PtempEpochs.ToString();
        }
        #endregion

        #region START TESTING
        private void button3_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count > 0)
            {
                button2.Enabled = false;
                button3.Enabled = false;
                button5.Enabled = false;
                button4.Enabled = false;
                if (checkBox1.Checked == true)
                {
                    if (MessageBox.Show("Want to use loaded Network ?", "Confirm Network", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (loadedNetworks != null)
                        {
                            loadedNetworks.resetValue(checkedListBox1.CheckedItems.Count);
                            TrBackprop = loadedNetworks;

                            //if (TrBackprop.PAuto == true)
                            //    TrBackprop.PKtableCopy = ChiSquare();
                        }
                        else
                            MessageBox.Show("No Network file is Loaded");
                    }
                }
                timespent.Restart();
                if (MessageBox.Show("Do you want to normalize the data ?", "Normalize Data", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    TestNormalized = true;
                else
                    TestNormalized = false;

                this.NetworkTestingInfo(1);//1 means, data is inputed to tab-2 /Testing

                tabControl1.SelectedIndex = 1;
                button2.Enabled = true;
                button3.Enabled = true;
                button5.Enabled = true;
                button4.Enabled = true;
            }
            else
                MessageBox.Show("Choose the input data first");
        }
        #endregion



        private void button6_Click(object sender, EventArgs e)
        {
            gridcounter[tabControl1.SelectedIndex] = 0;
            if(tabControl1.SelectedIndex == 0)
                dataGridView1.Rows.Clear();
            else if (tabControl1.SelectedIndex == 1)
                dataGridView2.Rows.Clear();
        }

        #region STOP TRAINING
        private void button7_Click(object sender, EventArgs e)//stop training
        {
            TrBackprop.stopTraining();
            checkedListBox1.Enabled = false;
            button7.Visible = false;
            button2.Enabled = true;
            button3.Enabled = true;
            button5.Enabled = true;
            button4.Enabled = true;
        }
        #endregion

        private void button5_Click(object sender, EventArgs e)//saved file
        {
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            if (TrBackprop != null)
            {
                saveFileDialog1.Title = "Save an Networks File";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    // If the file name is not an empty string open it for saving.
                    if (saveFileDialog1.FileName != "")
                    {
                        string filename = saveFileDialog1.FileName + ".SW";
                        TrBackprop.saveNetworks(filename,checkBox2.Checked,textBox9.Text,radioButton1.Checked, aksara, HoughFeatures);
                        MessageBox.Show("Network is succesfully saved");
                    }
                    else
                        MessageBox.Show("Network is NOT Succesfully saved");
                }
            }
            else
            {
                MessageBox.Show("There isn't any weight in the current network");
            }
        }
        private void button4_Click(object sender, EventArgs e)//load file
        {
            openFileDialog1.Title = "Load the saved networks";
            checkBox1.Checked = false;
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog1.FileName != "" && openFileDialog1.CheckFileExists == true)
                    {
                        loadedNetworks = new Backpropagation();
                        filename = openFileDialog1.FileName;
                        loadedNetworks.LoadNetworks(filename);
                        label16.Text = openFileDialog1.SafeFileName;
                        label17.Text = loadedNetworks.CNetwork.Nlayer.Count().ToString();
                        label18.Text = loadedNetworks.CNetwork.Nlayer[0].Nneuron.Count().ToString();
                        label19.Text = loadedNetworks.CNetwork.Nlayer[1].Nneuron.Count().ToString();
                        label20.Text = loadedNetworks.CNetwork.Nlayer[loadedNetworks.CNetwork.Nlayer.Count() - 1].Nneuron.Count().ToString();
                        label26.Text = loadedNetworks.Pdiscrete.ToString();
                        //if(loadedNetworks.Ptype ==true)
                        //    label28.Text = "Slow";
                        //else
                        //    label28.Text = "Fast";
                        if (loadedNetworks.Pdiscrete == true)
                        {
                            if (loadedNetworks.PAuto == false)
                            {
                                label28.Text = "Manual";
                                label30.Text = loadedNetworks.PNvalue.ToString();
                            }
                            else
                            {
                                label28.Text = "Automatic";
                                label30.Text = "0";
                            }
                        }


                        label43.Text = Math.Floor(loadedNetworks.PtrainAcc).ToString();
                        MessageBox.Show("Network is Succesfully Loaded");
                    }
                    else
                    {
                        MessageBox.Show("Network is NOT Succesfully Loaded");
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Something Wrong with Your Network \n" + err.Message);
            }
            

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (loadedNetworks != null)
            {
                if (checkBox1.Checked == true)
                {
                    if (tabControl2.SelectedIndex == 0)
                    {
                        checkBox2.Checked = loadedNetworks.Pdiscrete;//harus diatas karena bakal masuk fungsi checkbox2.checked
                        textBox2.Enabled = false;
                        textBox3.Enabled = false;
                        checkBox2.Enabled = false;
                        if (loadedNetworks.Pdiscrete == true)
                        {
                            checkBox2.Checked = true;
                            if (loadedNetworks.PAuto == true)
                            {
                                radioButton1.Checked = false;
                                radioButton2.Checked = true;
                            }
                            else
                            {
                                radioButton1.Checked = true;
                                radioButton2.Checked = false;
                            }
                        }
                        else
                        {
                            checkBox2.Checked = false;

                        }
                        radioButton1.Enabled = false;
                        radioButton2.Enabled = false;

                        checkBox3.Enabled = false;
                        if (loadedNetworks.PAksara == true)
                        {
                            checkBox3.Checked = true;
                            if (loadedNetworks.PHoughFeatures == true)
                            {
                                radioButton3.Checked = false;
                                radioButton4.Checked = true;
                            }
                            else
                            {
                                radioButton3.Checked = true;
                                radioButton4.Checked = false;
                            }
                        }
                        else
                        {
                            checkBox3.Checked = false;

                        }
                        radioButton3.Enabled = false;
                        radioButton4.Enabled = false;
                        textBox2.Text = (loadedNetworks.CNetwork.Nlayer.Count() - 2).ToString();
                        textBox3.Text = loadedNetworks.CNetwork.Nlayer[1].Nneuron.Count().ToString();
                        textBox9.Text = loadedNetworks.PNvalue.ToString();
                    }
                    else if (tabControl2.SelectedIndex == 1)
                    {
                        label33.Text = "# of Input Neurons: ";
                        label36.Text = "# of Hidden Neurons: ";
                        label37.Text = "# of Weights: ";
                        label38.Text = "Accuracy: ";
                        label33.Text += loadedNetworks.CNetwork.Nlayer[0].Nneuron.Count();
                        label36.Text += loadedNetworks.CNetwork.Nlayer[1].Nneuron.Count();
                        int totalweightandbias = 0;
                        totalweightandbias += loadedNetworks.CNetwork.Nlayer[0].Nneuron.Count() * loadedNetworks.CNetwork.Nlayer[1].Nneuron.Count();
                        totalweightandbias += loadedNetworks.CNetwork.Nlayer[1].Nneuron.Count() * loadedNetworks.CNetwork.Nlayer[2].Nneuron.Count();

                        label37.Text += totalweightandbias;
                        label38.Text += loadedNetworks.PtrainAcc;
                    }
                }
                else
                {
                    if (tabControl2.SelectedIndex == 0)
                    {
                        textBox2.Enabled = true;
                        textBox3.Enabled = true;
                        checkBox2.Enabled = true;
                        checkBox2.Checked = true;
                        radioButton1.Enabled = true;
                        radioButton2.Enabled = true;
                        checkBox3.Enabled = true;
                        checkBox3.Checked = true;
                        radioButton3.Enabled = true;
                        radioButton4.Enabled = true;
                    }
                    else if(tabControl2.SelectedIndex == 1)
                    {
                        if (TrBackprop != null)
                        {
                            label33.Text = "# of Input Neurons: ";
                            label36.Text = "# of Hidden Neurons: ";
                            label37.Text = "# of Weights: ";
                            label38.Text = "Accuracy: ";
                            label33.Text += TrBackprop.CNetwork.Nlayer[0].Nneuron.Count();
                            label36.Text += TrBackprop.CNetwork.Nlayer[1].Nneuron.Count();
                            int totalweightandbias = 0;
                            totalweightandbias += TrBackprop.CNetwork.Nlayer[0].Nneuron.Count() * TrBackprop.CNetwork.Nlayer[1].Nneuron.Count();
                            totalweightandbias += TrBackprop.CNetwork.Nlayer[1].Nneuron.Count() * TrBackprop.CNetwork.Nlayer[2].Nneuron.Count();

                            //label37.Text += totalweightandbias + TrBackprop.CNetwork.Nlayer[1].Nneuron.Count() + TrBackprop.CNetwork.Nlayer[2].Nneuron.Count();
                            label38.Text += TrBackprop.PtrainAcc;
                        }
                    }
                }
            }
            else
            {
                if(checkBox1.Checked==true)
                    MessageBox.Show("Please LOAD any network first.");
                checkBox1.Checked = false;
            }
            
                
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DataGridView temp = new DataGridView();
            if (tabControl1.SelectedIndex == 0)
                temp = dataGridView1;
            else if (tabControl1.SelectedIndex == 1)
                temp = dataGridView2;

            float avgAcc=0,std=0;
            for (int i = 0; i < temp.Rows.Count-1; i++)
            {
                avgAcc += float.Parse(temp.Rows[i].Cells[1].Value.ToString());
            }
            avgAcc /= (temp.Rows.Count - 1);
            if (temp.Rows.Count - 2 > 0)
            {
                for (int i = 0; i < temp.Rows.Count - 1; i++)
                {
                    std += (float)Math.Pow(avgAcc - float.Parse(temp.Rows[i].Cells[1].Value.ToString()), 2);
                }
                std = (float)Math.Sqrt(std / (temp.Rows.Count - 2));
            }
            MessageBox.Show("Average Accuracy = " + avgAcc.ToString() + "\nStandard Deviation = " + std.ToString());
        }

   
        private void FormBackprop_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(t!= null)
                t.Abort();
            if (prunt != null)
                prunt.Abort();
            if (chi2 != null)
                chi2.Abort();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0 || tabControl1.SelectedIndex == 1)
            {
                label21.Visible = false;
                rectangleShape1.Visible = false;
            }
            else
            {
                label21.Visible = true;
                rectangleShape1.Visible = true;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == false)
            {
                textBox9.Visible = false;
                label24.Visible = false;
                radioButton1.Visible = false;
                radioButton2.Visible = false;
                label23.Visible = false;
                button13.Visible = false;
            }
            else
            {
                textBox9.Visible = true;
                label24.Visible = true;
                radioButton1.Visible = true;
                radioButton2.Visible = true;
                label23.Visible = true;
                button13.Visible = true;
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked == false)
            {
                textBox9.Enabled = true;
                label24.Enabled = true;
   
            }
            else
            {
                textBox9.Enabled = false;
                label24.Enabled = false;
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        

        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl2.SelectedIndex == 1)
            {
                label33.Text = "# of Input Neurons: ";
                label36.Text = "# of Hidden Neurons: ";
                label37.Text = "# of Weights: ";
                label38.Text = "Accuracy: ";
                button14.Visible = false;
                if (TrBackprop == null && (loadedNetworks == null || checkBox1.Checked == false))
                {
                    MessageBox.Show("You don't have any available network, Please Load Network First");
                }
                else
                {
                    if (TrBackprop != null)
                    {
                        label33.Text += TrBackprop.CNetwork.Nlayer[0].Nneuron.Count();
                        label36.Text += TrBackprop.CNetwork.Nlayer[1].Nneuron.Count();
                        int totalweightandbias = 0;
                        totalweightandbias += TrBackprop.CNetwork.Nlayer[0].Nneuron.Count() * TrBackprop.CNetwork.Nlayer[1].Nneuron.Count();
                        totalweightandbias += TrBackprop.CNetwork.Nlayer[1].Nneuron.Count() * TrBackprop.CNetwork.Nlayer[2].Nneuron.Count();

                        //label37.Text += totalweightandbias + TrBackprop.CNetwork.Nlayer[1].Nneuron.Count() + TrBackprop.CNetwork.Nlayer[2].Nneuron.Count();
                        label37.Text += totalweightandbias;
                        label38.Text += TrBackprop.PtrainAcc;

                    }
                    else
                    {
                        label33.Text = "# of Input Neurons: ";
                        label36.Text = "# of Hidden Neurons: ";
                        label37.Text = "# of Weights: ";
                        label38.Text = "Accuracy: ";
                        label33.Text += loadedNetworks.CNetwork.Nlayer[0].Nneuron.Count();
                        label36.Text += loadedNetworks.CNetwork.Nlayer[1].Nneuron.Count();
                        int totalweightandbias = 0;
                        totalweightandbias += loadedNetworks.CNetwork.Nlayer[0].Nneuron.Count() * loadedNetworks.CNetwork.Nlayer[1].Nneuron.Count();
                        totalweightandbias += loadedNetworks.CNetwork.Nlayer[1].Nneuron.Count() * loadedNetworks.CNetwork.Nlayer[2].Nneuron.Count();

                        //label37.Text += totalweightandbias + loadedNetworks.CNetwork.Nlayer[1].Nneuron.Count() + loadedNetworks.CNetwork.Nlayer[2].Nneuron.Count();
                        label37.Text += totalweightandbias;
                        label38.Text += loadedNetworks.PtrainAcc;
                        

                    }
                }
            }
        }
        #region PRUNING - training
        private void button9_Click(object sender, EventArgs e)
        {
            if (checkedListBox1.CheckedItems.Count > 0)
            {
                Boolean status = false;//to check whether the user want to use loaded weight or not
                checkedListBox1.Enabled = false;
                button9.Enabled = false;
                button4.Enabled = false;
                button5.Enabled = false;
                button12.Enabled = false;
                button14.Visible = true;

                int num_of_classes = 0;//number of classes on selected file, used if aksara == true
                if (checkBox1.Checked == true)
                {
                    if (MessageBox.Show("Want to use loaded Network ?", "Confirm Network", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        if (aksara == true)
                        {
                            for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                            {
                                if (checkedListBox1.GetItemChecked(i))
                                {
                                    string tempPath = textBox1.Text;//default path
                                    tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                                    num_of_classes = InputFileAksara(tempPath);
                                    break;
                                }
                            }
                            loadedNetworks.resetValue(num_of_classes);
                        }
                        else
                        {
                            loadedNetworks.resetValue(checkedListBox1.CheckedItems.Count);
                        }
                        loadedNetworks.LoadNetworks(filename);
                        TrBackprop = loadedNetworks;
                        status = true;
                    }
                }
                try
                {
                    if (aksara == true)
                    {
                        for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                        {
                            if (checkedListBox1.GetItemChecked(i))
                            {
                                string tempPath = textBox1.Text;//default path
                                tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                                num_of_classes = InputFileAksara(tempPath);
                                break;
                            }
                        }
                        TrBackprop.resetValue(num_of_classes);
                    }
                    else
                    {
                        TrBackprop.resetValue(checkedListBox1.CheckedItems.Count);
                    }
                    for (int i = 0; i < checkedListBox1.Items.Count; i++)//count the row and column of input file
                    {
                        if (checkedListBox1.GetItemChecked(i))
                        {
                            string tempPath = textBox1.Text;//default path
                            tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                            TrBackprop.InputFile(tempPath, 2, aksara, false, HoughFeatures);
                        }
                    }

                    TrBackprop.GenerateTestingData(aksara);

                    TrBackprop.GenerateNetwork(-1, -1, TrBackprop.ThresholdValue, float.Parse(textBox11.Text), int.Parse(textBox12.Text));//generate all array and network structure

                    TrBackprop.resetclasscounter();
                    for (int i = 0; i < checkedListBox1.Items.Count; i++)//read the input file
                    {
                        if (checkedListBox1.GetItemChecked(i))
                        {
                            string tempPath = textBox1.Text;//default path
                            tempPath += @"\" + checkedListBox1.Items[i].ToString();//selected file path
                            TrBackprop.ReadInputData(tempPath, aksara, HoughFeatures);
                        }
                    }

                    textBox16.Text = "0";
                    textBox15.Text = "0";
                    textBox14.Text = "0";

                    textBox17.Text = "";
                    textBox18.Text = "";
                    textBox19.Text = "";
                    if (MessageBox.Show("Do you want to normalize the data ?", "Normalize Data", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        TrBackprop.NormalizeData();
                        PrunTrainNormalized = true;
                    }
                    else
                        PrunTrainNormalized = false;

                    if (TrBackprop.Pdiscrete == true && TrBackprop.PAuto == false)
                        TrBackprop.addManualDiscreteVariable();

                    prunt = new Thread(new ParameterizedThreadStart(TrBackprop.prunInput));
                    //prunt = new Thread(new ParameterizedThreadStart(TrBackprop.prunHidden));
                    prunt.Start(float.Parse(textBox13.Text));
                    timer2.Start();
                    timespent.Restart();

                }
                catch (Exception err)
                {
                    MessageBox.Show("Something Wrong with Your Network \n" + err.Message);
                   
                    checkedListBox1.Enabled = true;
                    button9.Enabled = true;
                    button4.Enabled = true;
                    button5.Enabled = true;
                    button12.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Choose the input data first");
            }
        }
        
        private void timer2_Tick(object sender, EventArgs e)
        {
            if (prunt.ThreadState != System.Threading.ThreadState.Running)
            {
                timer2.Stop();
                
                tabControl3.SelectedIndex = 0;
                checkedListBox1.Enabled = true;
                button9.Enabled = true;
                button4.Enabled = true;
                button5.Enabled = true;
                button12.Enabled = true;
                //left input index
                for (int i = 0; i < TrBackprop.CNetwork.Nlayer[1].Nneuron[0].weights.Count(); i++)
                {
                    for (int j = 0; j < TrBackprop.CNetwork.Nlayer[1].Nneuron.Count(); j++)
                    {
                        if (TrBackprop.CNetwork.Nlayer[1].Nneuron[j].weights[i] != 0)
                        {
                            textBox17.Text += i.ToString() + "\r\n"; // \r is the escape sequence for carriage return and \n represents a newline or line-feed
                            break;
                        }
                    }
                }
                //left input index
                
                //left hidden index
                for (int i = 0; i < TrBackprop.CNetwork.Nlayer[2].Nneuron[0].weights.Count(); i++)
                {
                    for (int j = 0; j < TrBackprop.CNetwork.Nlayer[2].Nneuron.Count(); j++)
                    {
                        if (TrBackprop.CNetwork.Nlayer[2].Nneuron[j].weights[i] != 0)
                        {
                            textBox18.Text += i.ToString() + "\r\n"; // \r is the escape sequence for carriage return and \n represents a newline or line-feed
                            break;
                        }
                    }
                }
                //left hidden index

                //left weight index
                int weights = 0;
                for (int l = 1; l < TrBackprop.CNetwork.Nlayer.Count(); l++)
                {
                    for (int i = 0; i < TrBackprop.CNetwork.Nlayer[l].Nneuron.Count(); i++)
                    {
                        for (int j = 0; j < TrBackprop.CNetwork.Nlayer[l].Nneuron[i].weights.Count(); j++)
                        {
                            if (TrBackprop.CNetwork.Nlayer[l].Nneuron[i].weights[j] != 0)
                            {
                                textBox19.Text += weights.ToString() + "\r\n"; // \r is the escape sequence for carriage return and \n represents a newline or line-feed
                            }
                            weights++;
                        }
                    }
                }
                //left weight index
                NetworkTestingInfo(2);
                button14.Visible = false;
            }
            textBox16.Text = TrBackprop.PtotalprunWeight.ToString();
            textBox15.Text = TrBackprop.PtotalprunInput.ToString();
            textBox14.Text = TrBackprop.PtotalprunHidden.ToString();
        }
        #endregion
        #region PRUNING - testing
        private void button12_Click(object sender, EventArgs e)
        {
            if (TrBackprop != null)
            {
                timespent.Restart();
                if (MessageBox.Show("Do you want to normalize the data ?", "Normalize Data", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    PrunTestNormalized = true;
                else
                    PrunTestNormalized = false;

                this.NetworkTestingInfo(3);//3 means, data is inputed to tab-2 /Testing
                tabControl3.SelectedIndex = 1;
            }
            else
                MessageBox.Show("Please Process your network through Pruning by clicking Start Pruning button");
        }
        #endregion
        private void button10_Click(object sender, EventArgs e)
        {
            DataGridView temp = new DataGridView();
            if (tabControl3.SelectedIndex == 0)
                temp = dataGridView5;
            else if (tabControl3.SelectedIndex == 1)
                temp = dataGridView6;

            float avgAcc = 0, std = 0,avgInput=0,avgHidden=0,avgWeight=0;
            for (int i = 0; i < temp.Rows.Count - 1; i++)
            {
                avgAcc += float.Parse(temp.Rows[i].Cells[1].Value.ToString());
                avgInput += float.Parse(temp.Rows[i].Cells[3].Value.ToString());
                avgHidden += float.Parse(temp.Rows[i].Cells[4].Value.ToString());
                avgWeight += float.Parse(temp.Rows[i].Cells[5].Value.ToString());
            }
            avgAcc /= (temp.Rows.Count - 1);
            avgInput /= (temp.Rows.Count - 1);
            avgHidden /= (temp.Rows.Count - 1);
            avgWeight /= (temp.Rows.Count - 1);
            if (temp.Rows.Count - 2 > 0)
            {
                for (int i = 0; i < temp.Rows.Count - 1; i++)
                {
                    std += (float)Math.Pow(avgAcc - float.Parse(temp.Rows[i].Cells[1].Value.ToString()), 2);
                }
                std = (float)Math.Sqrt(std / (temp.Rows.Count - 2));
            }
            MessageBox.Show("Average Accuracy = " + avgAcc.ToString() + "\nStandard Deviation = " + std.ToString() + "\nAverage Input = " + avgInput.ToString() + "\nAverage Hidden = " + avgHidden.ToString() + "\nAverage Weight = " + avgWeight.ToString());
        }
        private void button11_Click(object sender, EventArgs e)
        {

            if (tabControl3.SelectedIndex == 0)
            {
                dataGridView5.Rows.Clear();
                gridcounter[2] = 0;
            }
            else if (tabControl3.SelectedIndex == 1)
            {
                dataGridView6.Rows.Clear();
                gridcounter[3] = 0;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkedListBox1.CheckedItems.Count > 0)
                {
                    thresholdIncons = float.Parse(Microsoft.VisualBasic.Interaction.InputBox("Inconsistency Threshold(0.0 - 1.0) = ", "Inconsistency"));
                    chi2 = new Thread(new ThreadStart(ChiSquare));
                    chi2.Start();
                    string temp = "Working...";
                    textBox5.Text = temp;
                    timer3.Enabled = true;
                    timer3.Start();
                    string line;
                    string[] mstats;
                    char[] delimeterChars = { '\\' };
                    line = textBox1.Text;
                    mstats = line.Split(delimeterChars);
                    textBox20.Text = mstats[mstats.Count() - 1];
                }
                else
                {
                    MessageBox.Show("Choose the input file first");
                }
            }
            catch
            {
                MessageBox.Show("Something is missing");
            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            if (chi2.ThreadState != System.Threading.ThreadState.Running)
            {
                dataGridView9.Columns.Clear();

                for (int i = 0; i < CopyKtable.Count(); i++)
                {
                    dataGridView9.Columns.Add("number", "#");
                    dataGridView9.Columns.Add("attrib" + (i + 1), "Attribute " + (i + 1));
                }
                int[] counter = new int[CopyKtable.Count()];
                for (int i = 0; i < CopyKtable.Count(); i++)
                {
                    if (i == 0)
                        counter[i] = CopyKtable.Count();
                    else
                    {
                        counter[i] = counter[i-1] + CopyKtable[i-1].Count();
                    }
                }
                for (int i = 0; i < maxTable; i++)
                {
                    string[] row = new string[CopyKtable.Count() * 2];//*2 because it shows number of each attributes
                    int counter1 = CopyKtable.Count();
                    
                    for (int col = 0; col < row.Count(); col++)
                    {
                        if (col % 2 == 0 && i < CopyKtable[(int)col / 2].Count())
                        {
                            row[col] = (counter[col / 2] + i).ToString();
                        }
                        else if (col % 2 == 1)
                        {
                            if (i < CopyKtable[(int)col / 2].Count())
                                row[col] = CopyKtable[(int)col / 2][i].ToString();
                            else
                                row[col] = "";
                        }
                    }
                    dataGridView9.Rows.Add(row);
                }
                tabControl1.SelectedIndex = 4;

                for (int j = 0; j < dataGridView9.Columns.Count; j++)
                {
                    dataGridView9.Columns[j].Width = 50;
                    dataGridView9.Columns[j].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }
                string temp = "Done...";
                textBox5.Text = temp;
                timer3.Stop();
                chi2.Abort();
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                TrBackprop.stopTraining();
                button14.Visible = false;
            }catch
            {
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                checkedListBox1.SetItemChecked(i, true);
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            HoughFeatures = true;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (aksara == true)
            {
                aksara = false;
                radioButton4.Visible = false;
                radioButton3.Visible = false;
                label54.Visible = false;
            }
            else
            {
                aksara = true;
                radioButton4.Visible = true;
                radioButton3.Visible = true;
                label54.Visible = true;
            }
                
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            HoughFeatures = false;
        }

        private void folderBrowserDialog2_HelpRequest(object sender, EventArgs e)
        {

        }
    }
}
