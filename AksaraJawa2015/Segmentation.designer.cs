﻿namespace AksaraJawa2015
{
    partial class Segmentation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OriginalImage = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.PictureBox_OriginalImage = new System.Windows.Forms.PictureBox();
            this.btnWork = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtLoad = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Convolution = new System.Windows.Forms.CheckBox();
            this.Thinning = new System.Windows.Forms.CheckBox();
            this.hitAndMiss = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbConvolutionType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtKernelSize = new System.Windows.Forms.TextBox();
            this.Resize = new System.Windows.Forms.CheckBox();
            this.Save = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ProjectionProfile = new System.Windows.Forms.CheckBox();
            this.ProjectionProfileSegment = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.mono = new System.Windows.Forms.RadioButton();
            this.invMono = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMonoThreshold = new System.Windows.Forms.TextBox();
            this.LineSegment = new System.Windows.Forms.CheckBox();
            this.RegSegment = new System.Windows.Forms.CheckBox();
            this.automateSegment = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.SkewDetection = new System.Windows.Forms.CheckBox();
            this.pictureBox_ProccessedImage = new System.Windows.Forms.PictureBox();
            this.OriginalImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ProccessedImage)).BeginInit();
            this.SuspendLayout();
            // 
            // OriginalImage
            // 
            this.OriginalImage.Controls.Add(this.btnBrowse);
            this.OriginalImage.Controls.Add(this.PictureBox_OriginalImage);
            this.OriginalImage.Location = new System.Drawing.Point(16, 15);
            this.OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.OriginalImage.Name = "OriginalImage";
            this.OriginalImage.Padding = new System.Windows.Forms.Padding(27, 25, 27, 25);
            this.OriginalImage.Size = new System.Drawing.Size(549, 706);
            this.OriginalImage.TabIndex = 0;
            this.OriginalImage.TabStop = false;
            this.OriginalImage.Text = "Original Image";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(8, 498);
            this.btnBrowse.Margin = new System.Windows.Forms.Padding(4);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(79, 42);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // PictureBox_OriginalImage
            // 
            this.PictureBox_OriginalImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBox_OriginalImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBox_OriginalImage.Location = new System.Drawing.Point(8, 23);
            this.PictureBox_OriginalImage.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox_OriginalImage.Name = "PictureBox_OriginalImage";
            this.PictureBox_OriginalImage.Padding = new System.Windows.Forms.Padding(13, 12, 13, 12);
            this.PictureBox_OriginalImage.Size = new System.Drawing.Size(533, 467);
            this.PictureBox_OriginalImage.TabIndex = 0;
            this.PictureBox_OriginalImage.TabStop = false;
            // 
            // btnWork
            // 
            this.btnWork.Location = new System.Drawing.Point(11, 498);
            this.btnWork.Margin = new System.Windows.Forms.Padding(4);
            this.btnWork.Name = "btnWork";
            this.btnWork.Size = new System.Drawing.Size(79, 42);
            this.btnWork.TabIndex = 2;
            this.btnWork.Text = "Processs";
            this.btnWork.UseVisualStyleBackColor = true;
            this.btnWork.Click += new System.EventHandler(this.btnWork_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtLoad);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.LineSegment);
            this.groupBox2.Controls.Add(this.RegSegment);
            this.groupBox2.Controls.Add(this.automateSegment);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Controls.Add(this.SkewDetection);
            this.groupBox2.Controls.Add(this.btnWork);
            this.groupBox2.Controls.Add(this.pictureBox_ProccessedImage);
            this.groupBox2.Location = new System.Drawing.Point(573, 15);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(27, 25, 27, 25);
            this.groupBox2.Size = new System.Drawing.Size(880, 706);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Proccessed Image";
            // 
            // txtLoad
            // 
            this.txtLoad.AutoSize = true;
            this.txtLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoad.Location = new System.Drawing.Point(64, 210);
            this.txtLoad.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtLoad.Name = "txtLoad";
            this.txtLoad.Size = new System.Drawing.Size(394, 69);
            this.txtLoad.TabIndex = 31;
            this.txtLoad.Text = "Loading ........";
            this.txtLoad.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Convolution);
            this.groupBox5.Controls.Add(this.Thinning);
            this.groupBox5.Controls.Add(this.hitAndMiss);
            this.groupBox5.Controls.Add(this.groupBox1);
            this.groupBox5.Controls.Add(this.Resize);
            this.groupBox5.Controls.Add(this.Save);
            this.groupBox5.Location = new System.Drawing.Point(551, 393);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(300, 244);
            this.groupBox5.TabIndex = 29;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Other Tools";
            this.groupBox5.Visible = false;
            // 
            // Convolution
            // 
            this.Convolution.AutoSize = true;
            this.Convolution.Location = new System.Drawing.Point(8, 103);
            this.Convolution.Margin = new System.Windows.Forms.Padding(4);
            this.Convolution.Name = "Convolution";
            this.Convolution.Size = new System.Drawing.Size(152, 21);
            this.Convolution.TabIndex = 12;
            this.Convolution.Text = "Enable Convolution";
            this.Convolution.UseVisualStyleBackColor = true;
            // 
            // Thinning
            // 
            this.Thinning.AutoSize = true;
            this.Thinning.Location = new System.Drawing.Point(8, 28);
            this.Thinning.Margin = new System.Windows.Forms.Padding(4);
            this.Thinning.Name = "Thinning";
            this.Thinning.Size = new System.Drawing.Size(85, 21);
            this.Thinning.TabIndex = 10;
            this.Thinning.Text = "Thinning";
            this.Thinning.UseVisualStyleBackColor = true;
            // 
            // hitAndMiss
            // 
            this.hitAndMiss.AutoSize = true;
            this.hitAndMiss.Location = new System.Drawing.Point(105, 28);
            this.hitAndMiss.Margin = new System.Windows.Forms.Padding(4);
            this.hitAndMiss.Name = "hitAndMiss";
            this.hitAndMiss.Size = new System.Drawing.Size(107, 21);
            this.hitAndMiss.TabIndex = 15;
            this.hitAndMiss.Text = "Hit and Miss";
            this.hitAndMiss.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbConvolutionType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtKernelSize);
            this.groupBox1.Location = new System.Drawing.Point(19, 132);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(281, 97);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Convolution";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Type";
            // 
            // cbConvolutionType
            // 
            this.cbConvolutionType.FormattingEnabled = true;
            this.cbConvolutionType.Location = new System.Drawing.Point(124, 23);
            this.cbConvolutionType.Margin = new System.Windows.Forms.Padding(4);
            this.cbConvolutionType.Name = "cbConvolutionType";
            this.cbConvolutionType.Size = new System.Drawing.Size(147, 24);
            this.cbConvolutionType.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 65);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Kernel Size";
            // 
            // txtKernelSize
            // 
            this.txtKernelSize.Location = new System.Drawing.Point(124, 62);
            this.txtKernelSize.Margin = new System.Windows.Forms.Padding(4);
            this.txtKernelSize.Name = "txtKernelSize";
            this.txtKernelSize.Size = new System.Drawing.Size(147, 22);
            this.txtKernelSize.TabIndex = 11;
            // 
            // Resize
            // 
            this.Resize.AutoSize = true;
            this.Resize.Location = new System.Drawing.Point(8, 57);
            this.Resize.Margin = new System.Windows.Forms.Padding(4);
            this.Resize.Name = "Resize";
            this.Resize.Size = new System.Drawing.Size(73, 21);
            this.Resize.TabIndex = 16;
            this.Resize.Text = "Resize";
            this.Resize.UseVisualStyleBackColor = true;
            // 
            // Save
            // 
            this.Save.AutoSize = true;
            this.Save.Location = new System.Drawing.Point(105, 57);
            this.Save.Margin = new System.Windows.Forms.Padding(4);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(109, 21);
            this.Save.TabIndex = 24;
            this.Save.Text = "Save to CSV";
            this.Save.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.ProjectionProfile);
            this.groupBox4.Controls.Add(this.ProjectionProfileSegment);
            this.groupBox4.Location = new System.Drawing.Point(551, 208);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(308, 90);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Projection Profile Segmentation";
            // 
            // ProjectionProfile
            // 
            this.ProjectionProfile.AutoSize = true;
            this.ProjectionProfile.Location = new System.Drawing.Point(8, 28);
            this.ProjectionProfile.Margin = new System.Windows.Forms.Padding(4);
            this.ProjectionProfile.Name = "ProjectionProfile";
            this.ProjectionProfile.Size = new System.Drawing.Size(137, 21);
            this.ProjectionProfile.TabIndex = 18;
            this.ProjectionProfile.Text = "Projection Profile";
            this.ProjectionProfile.UseVisualStyleBackColor = true;
            // 
            // ProjectionProfileSegment
            // 
            this.ProjectionProfileSegment.AutoSize = true;
            this.ProjectionProfileSegment.Location = new System.Drawing.Point(8, 57);
            this.ProjectionProfileSegment.Margin = new System.Windows.Forms.Padding(4);
            this.ProjectionProfileSegment.Name = "ProjectionProfileSegment";
            this.ProjectionProfileSegment.Size = new System.Drawing.Size(259, 21);
            this.ProjectionProfileSegment.TabIndex = 19;
            this.ProjectionProfileSegment.Text = "Projection Profile on Segment Image";
            this.ProjectionProfileSegment.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.mono);
            this.groupBox3.Controls.Add(this.invMono);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtMonoThreshold);
            this.groupBox3.Location = new System.Drawing.Point(551, 74);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(309, 97);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Binary Threshold";
            // 
            // mono
            // 
            this.mono.AutoSize = true;
            this.mono.Location = new System.Drawing.Point(8, 23);
            this.mono.Margin = new System.Windows.Forms.Padding(4);
            this.mono.Name = "mono";
            this.mono.Size = new System.Drawing.Size(111, 21);
            this.mono.TabIndex = 7;
            this.mono.TabStop = true;
            this.mono.Text = "Monochrome";
            this.mono.UseVisualStyleBackColor = true;
            // 
            // invMono
            // 
            this.invMono.AutoSize = true;
            this.invMono.Location = new System.Drawing.Point(132, 23);
            this.invMono.Margin = new System.Windows.Forms.Padding(4);
            this.invMono.Name = "invMono";
            this.invMono.Size = new System.Drawing.Size(166, 21);
            this.invMono.TabIndex = 6;
            this.invMono.TabStop = true;
            this.invMono.Text = "Inverted Monochrome";
            this.invMono.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 60);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Threshold";
            // 
            // txtMonoThreshold
            // 
            this.txtMonoThreshold.Location = new System.Drawing.Point(132, 57);
            this.txtMonoThreshold.Margin = new System.Windows.Forms.Padding(4);
            this.txtMonoThreshold.Name = "txtMonoThreshold";
            this.txtMonoThreshold.Size = new System.Drawing.Size(132, 22);
            this.txtMonoThreshold.TabIndex = 13;
            // 
            // LineSegment
            // 
            this.LineSegment.AutoSize = true;
            this.LineSegment.Location = new System.Drawing.Point(551, 334);
            this.LineSegment.Margin = new System.Windows.Forms.Padding(4);
            this.LineSegment.Name = "LineSegment";
            this.LineSegment.Size = new System.Drawing.Size(113, 21);
            this.LineSegment.TabIndex = 20;
            this.LineSegment.Text = "LineSegment";
            this.LineSegment.UseVisualStyleBackColor = true;
            // 
            // RegSegment
            // 
            this.RegSegment.AutoSize = true;
            this.RegSegment.Location = new System.Drawing.Point(551, 306);
            this.RegSegment.Margin = new System.Windows.Forms.Padding(4);
            this.RegSegment.Name = "RegSegment";
            this.RegSegment.Size = new System.Drawing.Size(135, 21);
            this.RegSegment.TabIndex = 22;
            this.RegSegment.Text = "Flood Fill Region";
            this.RegSegment.UseVisualStyleBackColor = true;
            // 
            // automateSegment
            // 
            this.automateSegment.AutoSize = true;
            this.automateSegment.Location = new System.Drawing.Point(551, 362);
            this.automateSegment.Margin = new System.Windows.Forms.Padding(4);
            this.automateSegment.Name = "automateSegment";
            this.automateSegment.Size = new System.Drawing.Size(150, 21);
            this.automateSegment.TabIndex = 23;
            this.automateSegment.Text = "Automate Segment";
            this.automateSegment.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(551, 23);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.label4.Size = new System.Drawing.Size(148, 38);
            this.label4.TabIndex = 21;
            this.label4.Text = "Choose Filter :";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(97, 498);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(112, 42);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Save Image";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // SkewDetection
            // 
            this.SkewDetection.AutoSize = true;
            this.SkewDetection.Location = new System.Drawing.Point(551, 179);
            this.SkewDetection.Margin = new System.Windows.Forms.Padding(4);
            this.SkewDetection.Name = "SkewDetection";
            this.SkewDetection.Size = new System.Drawing.Size(132, 21);
            this.SkewDetection.TabIndex = 8;
            this.SkewDetection.Text = "Skew Correction";
            this.SkewDetection.UseVisualStyleBackColor = true;
            // 
            // pictureBox_ProccessedImage
            // 
            this.pictureBox_ProccessedImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox_ProccessedImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_ProccessedImage.Location = new System.Drawing.Point(8, 23);
            this.pictureBox_ProccessedImage.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox_ProccessedImage.Name = "pictureBox_ProccessedImage";
            this.pictureBox_ProccessedImage.Padding = new System.Windows.Forms.Padding(13, 12, 13, 12);
            this.pictureBox_ProccessedImage.Size = new System.Drawing.Size(533, 467);
            this.pictureBox_ProccessedImage.TabIndex = 0;
            this.pictureBox_ProccessedImage.TabStop = false;
            // 
            // Segmentation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1469, 736);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.OriginalImage);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Segmentation";
            this.Text = "Segmentation";
            this.Load += new System.EventHandler(this.Segmentation_Load);
            this.OriginalImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_OriginalImage)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_ProccessedImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox OriginalImage;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.PictureBox PictureBox_OriginalImage;
        private System.Windows.Forms.Button btnWork;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox_ProccessedImage;
        private System.Windows.Forms.RadioButton mono;
        private System.Windows.Forms.RadioButton invMono;
        private System.Windows.Forms.CheckBox SkewDetection;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox Convolution;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbConvolutionType;
        private System.Windows.Forms.CheckBox Thinning;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtKernelSize;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtMonoThreshold;
        private System.Windows.Forms.CheckBox hitAndMiss;
        private System.Windows.Forms.CheckBox Resize;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.CheckBox ProjectionProfile;
        private System.Windows.Forms.CheckBox ProjectionProfileSegment;
        private System.Windows.Forms.CheckBox LineSegment;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox RegSegment;
        private System.Windows.Forms.CheckBox automateSegment;
        private System.Windows.Forms.CheckBox Save;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label txtLoad;
    }
}

