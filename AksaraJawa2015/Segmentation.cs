﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace AksaraJawa2015
{
    public partial class Segmentation : Form
    {
        OpenFileDialog ofd;
        CImage OImage; // object image

        public Segmentation()
        {
            InitializeComponent();
            String[] convolutionType;
            convolutionType = new string[3];
            convolutionType[0] = "Mean Filter Blur";
            convolutionType[1] = "Edge Detection";
            convolutionType[2] = "";

            // Add value to combo box
            for (int i = 0; i < convolutionType.Length; i++)
            {
                cbConvolutionType.Items.Add(convolutionType[i]);
            }

            // init monochrome threshold
            txtMonoThreshold.Text = "96";

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                System.GC.Collect();
                // percobaan 1
                OImage = new CImage(ofd.FileName);
                PictureBox_OriginalImage.BackgroundImage = OImage.getOriginalImage(); // show the original image
            }
        }

        private void btnWork_Click(object sender, EventArgs e)
        {
            txtLoad.Visible = true;
            Application.DoEvents();
            Stopwatch s = new Stopwatch();
            s.Start();
            if (OImage == null)
            {
                MessageBox.Show("Please open the image you want to process"); return;
            }

            //// here process the gray scale
            OImage.toGrey();
            OImage.Histogram();

            //if (txtResultPath.Text == "")
            //{
            //    String pth = System.IO.Path.GetDirectoryName(System.IO.Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            //    MessageBox.Show("Path: " + pth); return;
            //}
            if (mono.Checked)
            {
                // monochrome process
                // black is object, white is background
                OImage.mono(Int32.Parse(txtMonoThreshold.Text)); // accept 2 param, param 1 is threshold and param 2 is invert flag
            }
            else if (invMono.Checked)
            {
                // invert monochrome
                // if the resource image have black background, and white object so we invert the resource
                // then we have black is object, and white is background
                OImage.mono(Int32.Parse(txtMonoThreshold.Text), true); // 128 is standard threshold
            }

            if (Convolution.Checked)
            {
                // here proccess the noise reduction method
                int kernelSize = Int32.Parse(txtKernelSize.Text);
                int tempType = cbConvolutionType.SelectedIndex;
                if (tempType == 0)
                {
                    OImage.meanFilter(kernelSize);
                }
                Console.WriteLine(kernelSize + ", " + tempType);
            }

            if (hitAndMiss.Checked)
            {
                // hit and miss process
                OImage.hitandmiss();
            }

            if (Thinning.Checked)
            {
                // thinning process
                OImage.thinning();
            }

            if (Resize.Checked)
            {
                OImage.bilinearResize(8);
            }

            if (SkewDetection.Checked)
            {
                OImage.houghTransform();
            }

            if (ProjectionProfile.Checked)
            {
                OImage.projectionProfile();
            }

            if (LineSegment.Checked)
            {
                OImage.lineSegmentTest();
            }

            if (ProjectionProfileSegment.Checked)
            {
                OImage.projectionProfileOnSegment();
            }

            if (RegSegment.Checked)
            {
                OImage.searchRegion();
            }

            if (automateSegment.Checked)
            {
                OImage.automateSegmentation();
            }

            if (Save.Checked)
            {
                OImage.writeCSV();
            }

            // stop stopwatch;
            s.Stop();
            TimeSpan ts = s.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);

            // set background image to null first, so it would change everytime we run the process button
            pictureBox_ProccessedImage.BackgroundImage = null;
            pictureBox_ProccessedImage.BackgroundImage = OImage.getProccessedImage();
            // MessageBox.Show("done!" + elapsedTime);
            txtLoad.Visible = false;
            Application.DoEvents();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Bitmap t;
            // handle exception
            try
            {
                t = OImage.getProccessedImage();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please open the image you want to process");
                return;
            }

            String filePath = OImage.APP_PATH;
            // create folder if not exists
            bool isExists = System.IO.Directory.Exists(filePath);
            if (!isExists)
                System.IO.Directory.CreateDirectory(filePath);
            String filename = "hasil-" + DateTime.Now.ToString("dd-MM-yyyy hhmmss") + ".jpg";
            t.Save(filePath + filename, System.Drawing.Imaging.ImageFormat.Jpeg);
            MessageBox.Show("Save Success filename : " + filename);
        }

        private void Segmentation_Load(object sender, EventArgs e)
        {

        }
    }
}
