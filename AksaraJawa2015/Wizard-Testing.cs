﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AksaraJawa2015
{
    public partial class Wizard_Testing : Form
    {
        Backpropagation loadedNetworks;
        
        FolderBrowserDialog fbd;
        OpenFileDialog ofd;
        CImage OImage; // object image
        string ori_test_filePath;

        public Wizard_Testing()
        {
            InitializeComponent();
            fbd = new FolderBrowserDialog();
            fbd.SelectedPath = Directory.GetCurrentDirectory();
            
        }

        private void OriginalImage_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Load the saved networks";
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog1.FileName != "" && openFileDialog1.CheckFileExists == true)
                    {
                        loadedNetworks = new Backpropagation();
                        string filename = openFileDialog1.FileName;
                        txtFilename.Text = openFileDialog1.SafeFileName;
                        loadedNetworks.LoadNetworks(filename);
                        MessageBox.Show("Network is Succesfully Loaded");
                    }
                    else
                    {
                        MessageBox.Show("Network is NOT Succesfully Loaded");
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Something Wrong with Your Network \n" + err.Message);
            }
        }
        public void TestBackprop(string filePath)
        {
            try
            {
                loadedNetworks.wizardInputFile(filePath);
                loadedNetworks.resetclasscounter();
                loadedNetworks.wizardGenerateTestingData();
                loadedNetworks.wizardReadInputData(filePath);
                if (MessageBox.Show("Do you want to normalize the data ?", "Normalize Data", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    loadedNetworks.wizardNormalizeData();
                }
                loadedNetworks.wizardStartTesting(result_txt);
                MessageBox.Show("Done");
            }
            catch (Exception err)
            {
                MessageBox.Show("Something Wrong !\n" + err.ToString());
            }
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                ofd = new OpenFileDialog();
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    System.GC.Collect();
                    // percobaan 1
                    OImage = new CImage(ofd.FileName);
                    PictureBox_OriginalImage.Image = OImage.getOriginalImage(); // show the original image
                    PictureBox_OriginalImage.SizeMode = PictureBoxSizeMode.AutoSize;
                    button3.Visible = true;
                }

                var result = fbd.ShowDialog();
                OImage.APP_PATH = fbd.SelectedPath + "\\";
                // handle cancel
                if (result == DialogResult.Cancel)
                    return;

                string input = Microsoft.VisualBasic.Interaction.InputBox("Threshold value", "Important", "96");
                OImage.toGrey();
                OImage.mono(Int32.Parse(input));
                // set feature extraction method

                ori_test_filePath = OImage.BondanAutomateSegmentation(true, 3);
                //ori_test_filePath = "C:\\Users\\Casper\\Desktop\\P. Greg\\AksaraJawa2015\\Hasil Segmentasi\\imgData\\";
                if (ori_test_filePath != "none")
                {
                    string test_filePath = ori_test_filePath + "88result.csv";
                    TestBackprop(test_filePath);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Something Wrong with Your Data \n" + err.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ori_test_filePath != null || ori_test_filePath != "none")
            {
                saveFileDialog1.Title = "Export text into file";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    // If the file name is not an empty string open it for saving.
                    if (saveFileDialog1.FileName != "")
                    {
                        string filename = saveFileDialog1.FileName + ".txt";
                        TextWriter tw = new StreamWriter(ori_test_filePath + Path.GetFileName(filename));
                        for (int i = 0; i < result_txt.Lines.Length; i++)
                        {
                            tw.WriteLine(result_txt.Lines[i]);
                        }
                        tw.Close();

                        MessageBox.Show("Result is succesfully saved");
                    }
                    else
                        MessageBox.Show("Result is NOT Succesfully saved");
                }
                
            }
        }

        private void txtFilename_TextChanged(object sender, EventArgs e)
        {

        }

        private void Wizard_Testing_Load(object sender, EventArgs e)
        {

        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (button3.Text == "Zoom Out")
            {
                Size newSize = new Size((int)(OImage.getOriginalImage().Width * 0.3), (int)(OImage.getOriginalImage().Height * 0.3));
                Bitmap bmp = new Bitmap(OImage.getOriginalImage(), newSize);
                PictureBox_OriginalImage.Image = bmp;
                button3.Text = "Zoom In";
            }
            else
            {
                Size newSize = new Size((int)(OImage.getOriginalImage().Width * 1), (int)(OImage.getOriginalImage().Height * 1));
                Bitmap bmp = new Bitmap(OImage.getOriginalImage(), newSize);
                PictureBox_OriginalImage.Image = bmp;
                button3.Text = "Zoom Out";
            }
        }
    }
}