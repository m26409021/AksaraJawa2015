﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.IO;

namespace AksaraJawa2015
{
    public partial class HoughFeature : Form
    {
        //private NumericUpDown[] numPar;
        public HoughFeature()
        {
            InitializeComponent();
        }
       /* public ICollection<NumericUpDown> numParam
        {
            get { return numPar; }
        }*/
        //-----------------------------------------------------\\
        //---------------------SINGLE TAB----------------------\\
        //-----------------------------------------------------\\

        Image<Gray, byte> exp, data, img;
        Image<Gray, byte>[] crop=new Image<Gray,byte>[2];
        ZhangSuenThinning thin = new ZhangSuenThinning();

        private void loadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnThinning.Visible = false;
            pnThinned.Visible = false;
            pnLoop.Visible = false;
            pnLine.Visible = false;
            pnCurve.Visible = false;
            int width,height;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image Files (*.jpg; *.jpeg; *.bmp; *.png)|*.jpg; *.jpeg; *.bmp; *.png";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    exp = new Image<Gray, byte>(ofd.FileName);
                    pbDefault.Image = exp;
                    //resize
                    height = 22;
                    width = exp.Width * height / exp.Height;
                    data = exp.Resize(width, height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    pbResize.Image = data;
                    //interface
                    numH.Value = height;
                    numW.Value = width;
                    lblImgH.Text = "Height : " + height.ToString() + " px";
                    lblImgW.Text = "Width  : " + width.ToString() + " px";
                    imageProcessingToolStripMenuItem.Enabled = true;
                    pnImage.Visible = true;
                    pnImage.Location = new Point(6, 32);
                }
                catch
                {
                    MessageBox.Show("Bukan File Gambar", "Open File Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void chkRW_CheckedChanged(object sender, EventArgs e)
        {
            if(chkRW.Checked) numW.Enabled = true;
            else numW.Enabled = false;
            if (chkRW.Checked || chkRH.Checked) btnResize.Enabled = true;
            else btnResize.Enabled = false;
        }

        private void chkRH_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRH.Checked) numH.Enabled = true;
            else numH.Enabled = false;
            if (chkRW.Checked || chkRH.Checked) btnResize.Enabled = true;
            else btnResize.Enabled = false;
        }

        private void btnResize_Click(object sender, EventArgs e)
        {
            if (chkRH.Checked || chkRW.Checked)
            {
                int width = 1, height = 1;
                if (chkRH.Checked && chkRW.Checked)
                {
                    height = (int)numH.Value;
                    width = (int)numW.Value;
                }
                else if (chkRH.Checked)
                {
                    height = (int)numH.Value;
                    width = exp.Width * height / exp.Height;
                }
                else if (chkRW.Checked)
                {
                    width = (int)numW.Value;
                    height = exp.Height * width / exp.Width;
                }
                data = exp.Resize(width, height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                pbResize.Image = data;
                lblImgH.Text = "Height :" + height.ToString() + " px";
                lblImgW.Text = "Width  :" + width.ToString() + " px";
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult msg = MessageBox.Show("Do you really want to exit Application?", "Exit Application", MessageBoxButtons.YesNo);
            if (msg == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void thinningImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnThinning.Location = new Point(413, 32);
            pnThinning.Visible = true;
        }

        private void btnThinning_Click(object sender, EventArgs e)
        {
            tbPreProcess.Text = "";
            tbThinning.Text = "";

            int i, j, w, h;
            ZhangSuenThinning thin = new ZhangSuenThinning();
            var watch = new Stopwatch();
            long runtime = 0;
            int winW = (int)((numWinW.Value - 1) / 2);
            int winH = (int)((numWinH.Value - 1) / 2);

            //pre_process
            watch = Stopwatch.StartNew();
            thin.setImage(data);
            img = thin.pre_process(winW, winH);
            watch.Stop();
            runtime = watch.ElapsedMilliseconds;
            ibPreProcess.Image = binToMono(img);
            h = img.Height;
            w = img.Width;
            for (i = 0; i < h; i++)
            {
                for (j = 0; j < w; j++)
                    tbPreProcess.Text += img.Data[i, j, 0] + " ";
                tbPreProcess.Text += "\r\n";
            }

            //thinning
            watch = Stopwatch.StartNew();
            img = thin.skeletonizing();
            watch.Stop();
            runtime += watch.ElapsedMilliseconds;
            ibThinning.Image = binToMono(img);
            for (i = 0; i < img.Height; i++)
            {
                for (j = 0; j < img.Width; j++)
                    tbThinning.Text += img.Data[i, j, 0] + " ";
                tbThinning.Text += "\r\n";
            }

            img = cropImage(img);

            //interface
            lblRunTime.Text = "Run Time: " + runtime + " ms.";
            pnRunTime.Visible = true;
            featureExtractionToolStripMenuItem.Enabled = true;

            ibImgReady.Image = binToMono(img);
            lblWidth.Text = "Image Width  : " + img.Width.ToString() + " px";
            lblHeight.Text = "Image Height : " + img.Height.ToString() + " px";
            ibS1.Image = binToMono(crop[0]);
            ibS2.Image = binToMono(crop[1]);
        }

        private Image<Gray, byte> binToMono(Image<Gray, byte> im)
        {
            int h = im.Height, w = im.Width;
            Image<Gray, byte> temp = new Image<Gray,byte>(w,h);
            for (int i = 0; i < h; i++)
                for (int j = 0; j < w; j++)
                    if (im.Data[i, j, 0] == 1)
                        temp.Data[i, j, 0] = 0;
                    else if(im.Data[i, j, 0] == 0)
                        temp.Data[i, j, 0] = 255;
            return temp;
        }

        private Image<Gray, byte> cropImage(Image<Gray, byte> im)
        {
            int i, j, ii;
            int w = im.Width, h = im.Height;
            if (h % 2 == 1)
            {
                h++;
                Image<Gray, byte> temp = new Image<Gray, byte>(w, h); //larger image
                for (i = 0; i < h-1; i++)
                    for (j = 0; j < w; j++)
                        temp.Data[i, j, 0] = im.Data[i, j, 0];
                im = temp.Clone();
            }
            for (i = 0; i < 2; i++)
                crop[i] = new Image<Gray, byte>(w, h / 2);
            //atas
            for (i = 0, ii = 0; i < h / 2; i++, ii++)
                for (j = 0; j < w; j++)
                    crop[0].Data[ii, j, 0] = im.Data[i, j, 0];
            //bawah
            for (i = h / 2, ii = 0; i < h; i++, ii++)
                for (j = 0; j < w; j++)
                    crop[1].Data[ii, j, 0] = im.Data[i, j, 0];
            return im;
        }

        private void cekFeaturePanel()
        {
            if (pnThinned.Visible == false)
            {
                pnThinned.Location = new Point(6, 32);
                pnThinned.Visible = true;
                pnImage.Visible = false;
            }
            pnThinning.Visible = false;
        }
        private void loopDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cekFeaturePanel();
            pnLoop.Location = new Point(413, 32);
            pnLoop.Visible = true;
            pnLine.Visible = false;
            pnCurve.Visible = false;
            lblRunTime.Text = "Run Time: ";
        }

        private void btnLoop_Click(object sender, EventArgs e)
        {
            CycleDetect cycle = new CycleDetect();
            int minArea = (int)numMinArea.Value;
            tbCycle.Text = "Jumlah Loop pada:";

            var watch = Stopwatch.StartNew();
            //binToMono( img).ToBitmap().Save(@"D:\Meili\TA\hasil\line\imgcycle0.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
            //full image
            cycle.setImage(img, minArea);
            tbCycle.Text = tbCycle.Text + "\r\nFull Image: " + cycle.countCycle().ToString();
            ibCycleFull.Image = cycle.getHasil();
            //cycle.getHasil().ToBitmap().Save(@"D:\Meili\TA\hasil\line\imgcycle1.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
            //segment 1
            cycle.setImage(crop[0], minArea);
            tbCycle.Text = tbCycle.Text + "\r\nSegment 1: " + cycle.countCycle().ToString();
            ibCycle1.Image = cycle.getHasil();
            //cycle.getHasil().ToBitmap().Save(@"D:\Meili\TA\hasil\line\imgcycle2.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
            //segment 2
            cycle.setImage(crop[1], minArea);
            tbCycle.Text = tbCycle.Text + "\r\nSegment 2: " + cycle.countCycle().ToString();
            ibCycle2.Image = cycle.getHasil();
            //cycle.getHasil().ToBitmap().Save(@"D:\Meili\TA\hasil\line\imgcycle3.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
            watch.Stop();
            lblRunTime.Text = "Run Time: " + watch.ElapsedMilliseconds + " ms.";
        }
        private void lineDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cekFeaturePanel();
            pnLine.Location = new Point(413, 32);
            pnLine.Visible = true;
            pnLoop.Visible = false;
            pnCurve.Visible = false;
            lblRunTime.Text = "Run Time: ";
        }

        private void rbPilThrPer_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPilThrPer.Checked)
            {
                numThrPerc.Enabled = true;
                numThrVal.Enabled = false;
            }
            if (rbPilThrVal.Checked)
            {
                numThrVal.Enabled = true;
                numThrPerc.Enabled = false;
            }
        }

        private void rbPilThrVal_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPilThrPer.Checked)
            {
                numThrPerc.Enabled = true;
                numThrVal.Enabled = false;
            }
            if (rbPilThrVal.Checked)
            {
                numThrVal.Enabled = true;
                numThrPerc.Enabled = false;
            }
        }

        private void btnHT_Click(object sender, EventArgs e)
        {
            int c, w = crop[0].Width, h = crop[0].Height;
            LineSegment2D[] lines;
            Image<Gray, byte> imgLine, imgUnion;
            listLine.Items.Clear();

            //parameter
            int neighbor = (int)numNeighbor.Value;
            int step = (int)numThetaStep.Value;
            int minLength = (int)numMinLength.Value;
            int maxGap = (int)numMaxGap.Value;
            int thresh = 0, pilThresh = 0;
            if (rbPilThrPer.Checked) { pilThresh = 0; thresh = (int)numThrPerc.Value; }
            else if (rbPilThrVal.Checked) { pilThresh = 1; thresh = (int)numThrVal.Value; }

            /////////-----------------------
            /*DialogResult msg = MessageBox.Show("Do you want to save the result?", "Save File", MessageBoxButtons.YesNo);
            string folderName = @"Desktop";
            bool ceksave = false;
            if (msg == DialogResult.Yes)
            {
                FolderBrowserDialog fbd = new FolderBrowserDialog();
                fbd.ShowNewFolderButton = true;
                fbd.SelectedPath = @"Desktop";
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    folderName = fbd.SelectedPath;
                    ceksave = true;
                }
            }
            int a = 1;*/
            //--------------------------------

            //BIKIN SENDIRI
            var watch=new Stopwatch();
            long runTimeLine = 0;
            HoughLine HL = new HoughLine();
            for (int i = 0; i < 2; i++)
            {
                HL.setImage(crop[i], step);
                watch.Reset(); watch.Start();
                List<HoughInfo> foundLines = HL.getLines(pilThresh, thresh, neighbor, minLength, maxGap);
                watch.Stop();
                runTimeLine += watch.ElapsedMilliseconds;
                //add ke list
                c = 1;
                foreach (HoughInfo line in foundLines)
                {
                    ListViewItem item = new ListViewItem(new String[] { (c++).ToString(), line._line.P1.X.ToString(), line._line.P1.Y.ToString(), line._line.P2.X.ToString(), line._line.P2.Y.ToString(), line._rho.ToString(), line._theta.ToString() });
                    item.Group = listLine.Groups[i];
                    listLine.Items.Add(item);
                    //abc += line._line.P1.X.ToString() + " " + line._line.P1.Y.ToString() + " " + line._line.P2.X.ToString() + " " + line._line.P2.Y.ToString() + " " + line._rho.ToString() + " " + line._theta.ToString()+"\r\n";
                }
                //get image
                Image<Gray, byte> hasil = HL.getImageHasil(foundLines);
                if (i == 0)
                {
                    ibDetLine1.Image = hasil;
                    ibLineUni1.Image = HL.getImageUnion(hasil);
                    ibHoughLine1.Image = HL.getAccArrayImage();
                }
                else
                {
                    ibDetLine2.Image = hasil;
                    ibLineUni2.Image = HL.getImageUnion(hasil);
                    ibHoughLine2.Image = HL.getAccArrayImage();
                }
                //textBox1.Text += " " + HL.maxValue().ToString();
               /* if (ceksave)
                {
                    hasil.ToBitmap().Save(folderName + "/" + (a++).ToString() + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    HL.getImageUnion(hasil).ToBitmap().Save(folderName + "/" + (a++).ToString() + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    HL.getAccArrayImage().ToBitmap().Save(folderName + "/" + (a++).ToString() + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }*/
            }

            //LIBRARY
            //public LineSegment2D[][] HoughLinesBinary(double rhoResolution,double thetaResolution,int threshold,double minLineWidth,double gapBetweenLines)
            long runTimeLineLib = 0;
            for (int i = 0; i < 2; i++)
            {
                watch.Reset(); watch.Start();
                lines = crop[i].HoughLinesBinary(1, step * Math.PI / 180, (int)numThrVal.Value, minLength, neighbor)[0]; //chanel 0 -> gray
                watch.Stop();
                runTimeLineLib += watch.ElapsedMilliseconds;
                c = 1;
                imgLine = new Image<Gray, byte>(w, h);
                imgUnion = binToMono(crop[i]);
                foreach (LineSegment2D line in lines)
                {
                    ListViewItem item = new ListViewItem(new[] { (c++).ToString(), line.P1.X.ToString(), line.P1.Y.ToString(), line.P2.X.ToString(), line.P2.Y.ToString() });
                    item.Group = listLine.Groups[i + 2];
                    listLine.Items.Add(item);
                    imgLine.Draw(line, new Gray(128), 1);
                    imgUnion.Draw(line, new Gray(128), 1);
                    //abc += line.P1.X.ToString() + " " + line.P1.Y.ToString() + " " + line.P2.X.ToString() + " " + line.P2.Y.ToString() + "\r\n";
                }
                /*if (ceksave)
                {
                    binToMono(crop[i]).ToBitmap().Save(folderName + "/" + (a++).ToString() + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    imgLine.ToBitmap().Save(folderName + "/" + (a++).ToString() + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                    imgUnion.ToBitmap().Save(folderName + "/" + (a++).ToString() + ".jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }*/

                if (i == 0)
                {
                    ibLineUnion1.Image = imgUnion;
                    ibLine1.Image = imgLine;
                }
                else if (i == 1)
                {
                    ibLineUnion2.Image = imgUnion;
                    ibLine2.Image = imgLine;
                }
            }
            lblRunTime.Text = "Run Time: " + runTimeLine + " ms. Run Time Library: "+runTimeLineLib +" ms.";
        }

        private void curveDetectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cekFeaturePanel();
            pnCurve.Location = new Point(413, 32);
            pnLoop.Visible = false;
            pnLine.Visible = false;
            pnCurve.Visible = true;
            lblRunTime.Text = "Run Time: ";
        }

        private void rbPercC_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPercC.Checked)
            {
                numThrPercC.Enabled = true;
                numThrValC.Enabled = false;
            }
            if (rbValC.Checked)
            {
                numThrValC.Enabled = true;
                numThrPercC.Enabled = false;
            }
        }

        private void rbValC_CheckedChanged(object sender, EventArgs e)
        {
            if (rbPercC.Checked)
            {
                numThrPercC.Enabled = true;
                numThrValC.Enabled = false;
            }
            if (rbValC.Checked)
            {
                numThrValC.Enabled = true;
                numThrPercC.Enabled = false;
            }
        }

        private void btnCurve_Click(object sender, EventArgs e)
        {
            HoughCurve curve = new HoughCurve();
            Image<Gray, byte> hsl,union;
            int step = (int)numStepC.Value;
            int neighbor = (int)numNeighborC.Value;
            int dist = (int)numDistC.Value;
            int gap = (int)numGapC.Value;
            int pilThresh=1;
            int threshold = (int)numThrValC.Value;
            if (rbPercC.Checked)
            {
                pilThresh = 0;
                threshold = (int)numThrPercC.Value;
            }
            int color = 128;
            listCurve.Items.Clear();

            var watch = Stopwatch.StartNew();
            for (int i = 0; i < 2; i++)
            {
                int c = 1;
                curve.setImage(crop[i], step);
                hsl = curve.getCurve(neighbor, pilThresh, threshold, color, dist, gap);
                union = curve.getImage();
                List<int[]> param = curve.getParam();
                foreach (int[] par in param)
                {
                    ListViewItem item = new ListViewItem(new[] { c++.ToString(), par[0].ToString(), par[1].ToString(), par[2].ToString() });
                    item.Group = listCurve.Groups[i];
                    listCurve.Items.Add(item);
                }
                if (i == 0)
                {
                    ibCurve1.Image = binToMono(hsl);
                    ibCurveUnion1.Image = binToMono(union);
                }
                else if (i == 1)
                {
                    ibCurve2.Image = binToMono(hsl);
                    ibCurveUnion2.Image = binToMono(union);
                }
            }
            watch.Stop();
            lblRunTime.Text = "Run Time: " + watch.ElapsedMilliseconds + " ms.";
        }

        //--------------------------------------------------------\\
        //---------------------MULTIPLE TAB-----------------------\\
        //--------------------------------------------------------\\

        String[] files;
        int[] cekFile;
        List<int> numSelect = new List<int>();
        int[] param = new int[19];
        bool load = false;

        private void loadImagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //-----LOAD IMAGES-----\\
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = @"D:\Meili\TA\sample image\sample hasil singgih\";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                numSelect.Clear();
                //load image
                listImg.Items.Clear();
                ImageList imageList = new ImageList();
                files = Directory.GetFiles(fbd.SelectedPath, "*", SearchOption.AllDirectories);
                cekFile = new int[files.Length];
                int n = files.Length;
                for (int dn = 0; dn < n; dn++)
                {
                    String nama = Path.GetFileName(files[dn]);
                    int cekSegmen = nama.IndexOf("segmen");
                    int cekHist = nama.IndexOf("histogram");
                    String dir = new DirectoryInfo(Path.GetDirectoryName(files[dn])).Name;
                    int cekFolder = dir.IndexOf("product");
                    if (cekFolder !=-1 && cekSegmen != -1 && cekHist == -1)
                        cekFile[dn] = 1;
                    else
                        cekFile[dn] = 0;
                    //cekFile[dn] = 1;
                    if (cekFile[dn] == 1)
                    {
                        try
                        {
                            Image im = Image.FromFile(files[dn]);
                            if (im.Width < 10 && im.Width < 10)
                                cekFile[dn] = 0;
                            if (cekFile[dn] == 1)
                            {
                                imageList.Images.Add(im);
                                numSelect.Add(dn);
                            }
                        }
                        catch
                        {
                        }
                    }
                }
                imageList.ImageSize = new Size(50, 30);
                listImg.LargeImageList = imageList;
                //add to list box
                int hitungFile=0;
                n = imageList.Images.Count;
                for (int dn = 0; dn < n; dn++)
                {
                    String dir = new DirectoryInfo(Path.GetDirectoryName(files[numSelect[dn]])).Name;
                    String nama = Path.GetFileName(files[numSelect[dn]]);
                    listImg.Items.Add(new ListViewItem { ImageIndex = dn, Text = dir + "/" + nama, Tag = files[dn] });
                    hitungFile++;
                }
                processImagesToolStripMenuItem.Enabled = true;
                listImg.Visible = true;
                lblMultiResult.Text = hitungFile.ToString() + " files image Loaded";
                lblMultiResult.Visible = true;
                load = true;
            }
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DialogResult msg = MessageBox.Show("Do you really want to exit Application?", "Exit Application", MessageBoxButtons.YesNo);
            if (msg == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void addNewParameterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] text = File.ReadAllLines(@"../../defParam.txt");
            setParamtoNum(text);
            pnSettingParam.Location = new Point(6, 31);
            pnSettingParam.Visible = true;
            pnSettingParam.BringToFront();
            fileToolStripMenuItem.Enabled = false;
            processImagesToolStripMenuItem.Enabled = false;
        }

        private void btnExitParam_Click(object sender, EventArgs e)
        {
            DialogResult msg = MessageBox.Show("Discard all changes and go back to Application Form?", "Exit Setting Parameter", MessageBoxButtons.YesNo);
            if (msg == DialogResult.Yes)
            {
                pnSettingParam.Visible = false;
                fileToolStripMenuItem.Enabled = true;
                if (load) processImagesToolStripMenuItem.Enabled = true;
            }
        }

        private void chkImgW_CheckedChanged(object sender, EventArgs e)
        {
            if (chkImgW.Checked) numParam2.Enabled = true;
            else numParam2.Enabled = false;
            if (chkImgH.Checked) numParam3.Enabled = true;
            else numParam3.Enabled = false;
        }

        private void chkImgH_CheckedChanged(object sender, EventArgs e)
        {
            if (chkImgW.Checked) numParam2.Enabled = true;
            else numParam2.Enabled = false;
            if (chkImgH.Checked) numParam3.Enabled = true;
            else numParam3.Enabled = false;
        }

        private void rbPerc_CheckedChanged(object sender, EventArgs e)
        {
            numParam12.Enabled = false;
            numParam11.Enabled = true;
        }

        private void rbVal_CheckedChanged(object sender, EventArgs e)
        {
            numParam12.Enabled = true;
            numParam11.Enabled = false;
        }

        private void rbPercMC_CheckedChanged(object sender, EventArgs e)
        {
            numParam16.Enabled = false;
            numParam15.Enabled = true;
        }

        private void rbValMC_CheckedChanged(object sender, EventArgs e)
        {
            numParam15.Enabled = false;
            numParam16.Enabled = true;
        }

        private void btnSaveParam_Click(object sender, EventArgs e)
        {
            NumericUpDown[] numPar = new NumericUpDown[] { numParam2, numParam3, numParam4, numParam5, numParam6, numParam7, numParam8, numParam9, numParam10, numParam11, numParam12, numParam13, numParam14, numParam15, numParam16, numParam17, numParam18 };
            SaveFileDialog save = new SaveFileDialog();
            save.DefaultExt = ".txt";
            save.Filter = "Text|*.txt|All|*.*";
            if (save.ShowDialog() == DialogResult.OK)
            {
                String[] par = setNumtoParam();
                File.WriteAllLines(save.FileName, par, Encoding.UTF8);
                MessageBox.Show("Saved to " + save.FileName);
            }
        }

        private void btnSetParam_Click(object sender, EventArgs e)
        {
            NumericUpDown[] numPar = new NumericUpDown[] { numParam2, numParam3, numParam4, numParam5, numParam6, numParam7, numParam8, numParam9, numParam10, numParam11, numParam12, numParam13, numParam14, numParam15, numParam16, numParam17, numParam18 };

            String[] par = setNumtoParam();
            for (int i = 0; i <= 18; i++)
                param[i] = Convert.ToInt32(par[i]);
            DialogResult msg = MessageBox.Show("Set Success. \n\rDo you want to go back to Application Form?", "Back to Application Form", MessageBoxButtons.YesNo);
            if (msg == DialogResult.Yes)
            {
                pnSettingParam.Visible = false;
                fileToolStripMenuItem.Enabled = true;
                if (load) processImagesToolStripMenuItem.Enabled = true;
            }
        }

        private void btnResetParam_Click(object sender, EventArgs e)
        {
            DialogResult msg = MessageBox.Show("Reset to default parameters?", "Reset Parameter", MessageBoxButtons.YesNo);
            if (msg == DialogResult.Yes)
            {
                string[] text = File.ReadAllLines(@"../../defParam.txt");
                setParamtoNum(text);
            }
        }

        private void setParamtoNum(string[] text)
        {
            NumericUpDown[] numPar = new NumericUpDown[] { numParam2, numParam3, numParam4, numParam5, numParam6, numParam7, numParam8, numParam9, numParam10, numParam11, numParam12, numParam13, numParam14, numParam15, numParam16, numParam17, numParam18 };

            if (Convert.ToInt32(text[0]) == 0) chkImgW.Checked = false;
            else chkImgW.Checked = true;
            if (Convert.ToInt32(text[1]) == 0) chkImgH.Checked = false;
            else chkImgH.Checked = true;
            for (int i = 2, j = 0; i <= 18; i++)
            {
                if (i == 11)
                {
                    if (Convert.ToInt32(text[i]) == 0)
                    {
                        rbPerc.Checked = true;
                        numPar[9].Value = Convert.ToInt32(text[12]);
                    }
                    else
                    {
                        rbVal.Checked = true;
                        numPar[10].Value = Convert.ToInt32(text[12]);
                    }
                    i++; j = 11;
                }
                else if (i == 15)
                {
                    if (Convert.ToInt32(text[i]) == 0)
                    {
                        rbPercMC.Checked = true;
                        numPar[13].Value = Convert.ToInt32(text[16]);
                    }
                    else
                    {
                        rbValMC.Checked = true;
                        numPar[14].Value = Convert.ToInt32(text[16]);
                    }
                    i++; j = 15;
                }
                else
                    numPar[j++].Value = Convert.ToInt32(text[i]);
            }
        }

        private string[] setNumtoParam()
        {
            NumericUpDown[] numPar = new NumericUpDown[] { numParam2, numParam3, numParam4, numParam5, numParam6, numParam7, numParam8, numParam9, numParam10, numParam11, numParam12, numParam13, numParam14, numParam15, numParam16, numParam17, numParam18 };
            String[] par = new String[19];

            if (chkImgW.Checked) par[0] = "1";
            else par[0] = "0";
            if (chkImgH.Checked) par[1] = "1";
            else par[1] = "0";
            for (int i = 2, j = 0; i <= 18; i++)
            {
                if (i == 11)
                {
                    if (rbPerc.Checked)
                    {
                        par[11] = "0";
                        par[12] = numPar[9].Value.ToString();
                    }
                    else
                    {
                        par[11] = "1";
                        par[12] = numPar[10].Value.ToString();
                    }
                    i = 12; j = 11;
                }
                else if (i == 15)
                {
                    if (rbPerc.Checked)
                    {
                        par[15] = "0";
                        par[16] = numPar[13].Value.ToString();
                    }
                    else
                    {
                        par[15] = "1";
                        par[16] = numPar[14].Value.ToString();
                    }
                    i = 16; j = 15;
                }
                else
                    par[i] = numPar[j++].Value.ToString();
            }
            return par;
        }

        private void loadParameterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text|*.txt|All|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string[] text = File.ReadAllLines(ofd.FileName);
                for (int i = 0; i < 12; i++)
                    param[i] = Convert.ToInt32(text[i]);
                setParamtoNum(text);
                pnSettingParam.Location = new Point(6, 31);
                pnSettingParam.Visible = true;
                fileToolStripMenuItem.Enabled = false;
                processImagesToolStripMenuItem.Enabled = false;
            }
        }

        private void processImagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int n, w, h, no = 1;
            string[] listFitur = new string[20];
            bool cekSave = false;
            List<CsvRow> allRow = new List<CsvRow>();
            List<CsvRow> template = new List<CsvRow>();
            listMulti.Items.Clear();
            /*string pars="";
            foreach (int par in param)
                pars += par.ToString()+" ";
            MessageBox.Show(pars);*/

            //-----OPEN FILE CSV-----\\
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult msg = MessageBox.Show("Do you want to save the result?", "Save File", MessageBoxButtons.YesNo);
            if (msg == DialogResult.Yes)
            {
                ofd.Filter = "CSV files (*.csv)|*.csv|XML files (*.xml)|*.xml";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    using (CsvFileReader reader = new CsvFileReader(ofd.FileName))
                    {
                        CsvRow row = new CsvRow();
                        while (reader.ReadRow(row))
                        {
                            CsvRow tempRow = new CsvRow();
                            foreach (String a in row)
                                tempRow.Add(a);
                            allRow.Add(tempRow);
                        }
                        cekSave = true;
                        row = allRow.ElementAt(0);
                        int rn=row.Count;
                        for (int i = rn-1; i >= 7; i--)
                            row.RemoveAt(i);
                        row.Add("Jumlah Loop pada Gambar");
                        row.Add("Jumlah Loop pada Segmen Atas");
                        row.Add("Jumlah Loop pada Segmen Bawah");
                        row.Add("Jumlah Line pada Segmen Atas");
                        row.Add("Koordinat titik ujung garis (x1,y1,x2,y2)");
                        row.Add("Rho, theta");
                        row.Add("Jumlah Line pada Segmen Bawah");
                        row.Add("Koordinat titik ujung garis (x1,y1,x2,y2)");
                        row.Add("Rho, theta");
                        row.Add("Jumlah Curve pada Segmen Atas");
                        row.Add("Koordinat vertex, sudut orientasi (Xo,Yo,Theta)");
                        row.Add("Jumlah Curve pada Segmen Bawah");
                        row.Add("Koordinat vertex, sudut orientasi (Xo,Yo,Theta)");
                    }
                }
            }
            //-----READ TEMPLATE-----\\
            using (CsvFileReader reader = new CsvFileReader(@"../../template.csv"))
            {
                CsvRow row = new CsvRow();
                while (reader.ReadRow(row))
                {
                    CsvRow tempRow = new CsvRow();
                    foreach (String a in row)
                        tempRow.Add(a);
                    template.Add(tempRow);
                }
            }

            //-----PROCESS IMAGE-----\\
            n = files.Length;
            for (int dn = 0; dn < n; dn++)
            {
                if (cekFile[dn] == 1)
                {
                    //-----LOAD IMAGE-----\\
                    exp = new Image<Gray, byte>(files[dn]);
                    //resize
                    if (param[0]==1 && param[1]==1)
                    {
                        w = param[2];
                        h = param[3];
                    }
                    else if (param[0] == 1)
                    {
                        w = param[2];
                        h = exp.Height * w / exp.Width;
                    }
                    else if (param[1] == 1)
                    {
                        h = param[3];
                        w = exp.Width * h / exp.Height;
                    }
                    else
                    {
                        h = 22;
                        w = exp.Width * h / exp.Height;
                    }
                    data = exp.Resize(w, h, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                    //add list
                    listFitur[0] = no++.ToString();
                    String namaFile = Path.GetFileName(files[dn]);
                    String namaDir = new DirectoryInfo(Path.GetDirectoryName(files[dn])).Name;
                    listFitur[1] = namaDir + "/" + namaFile;

                    //-----THINNING-----\\
                    ZhangSuenThinning thin = new ZhangSuenThinning();
                    //pre_process
                    thin.setImage(data);
                    thin.pre_process(param[4], param[5]);
                    //thinning
                    img = thin.skeletonizing();
                    //crop segment
                    img = cropImage(img);

                    //-----LOOP DETECTION-----\\
                    CycleDetect cycle = new CycleDetect();
                    //full
                    cycle.setImage(img, param[6]);
                    listFitur[2] = cycle.countCycle().ToString();
                    //segment 1
                    cycle.setImage(crop[0], param[6]);
                    listFitur[3] = cycle.countCycle().ToString();
                    //segment 2
                    cycle.setImage(crop[1], param[6]);
                    listFitur[4] = cycle.countCycle().ToString();

                    //-----LINE DETECTION-----\\
                    int step = param[7];
                    int neighbor = param[8];
                    int minLength = param[9];
                    int maxGap = param[10];
                    int thresh = param[12], pilThresh = param[11] ;

                    HoughLine HL = new HoughLine();
                    List<HoughInfo> foundLines = new List<HoughInfo>();
                    int ii = 0;
                    for (int i = 0; i < 2; i++)
                    {
                        HL.setImage(crop[i], step);
                        foundLines = HL.getLines(pilThresh, thresh, neighbor, minLength, maxGap);
                        if (i == 0) ii = 5; //atas
                        else ii = 8; // bawah
                        listFitur[ii + 1] = "";
                        listFitur[ii + 2] = "";
                        listFitur[ii] = foundLines.Count.ToString();
                        foreach (HoughInfo line in foundLines)
                        {
                            listFitur[ii + 1] += "(" + line._line.P1.X.ToString() + ", " + line._line.P1.Y.ToString() + ", " + line._line.P2.X.ToString() + ", " + line._line.P2.Y.ToString() + "), ";
                            listFitur[ii + 2] += "(" + line._rho.ToString() + ", " + line._theta.ToString() + "), ";
                        }
                    }

                    //-----CURVE DETECTION-----\\
                    HoughCurve curve = new HoughCurve();
                    int stepC = param[13];
                    int neighborC = param[14];
                    int pilThreshC = param[15], threshC = param[16];
                    int distC = param[17];
                    int gapC = param[18];
                    for (int i = 0; i < 2; i++)
                    {
                        curve.setImage(crop[i], stepC);
                        curve.getCurve(neighborC, pilThreshC, threshC, 128, distC, gapC);
                        if (i == 0) ii = 11; //atas
                        else ii = 13; //bawah
                        listFitur[ii] = curve.getNumCurve().ToString();
                        List<int[]> parCurve = curve.getParam();
                        listFitur[ii + 1] = "";
                        foreach (int[] par in parCurve)
                        {
                            listFitur[ii + 1] += "(" + par[0].ToString() + ", " + par[1].ToString() + ", " + par[2].ToString() + "), ";
                        }
                    }

                    //-----CEK SUGGESTION-----\\
                   /* int countTemp = template.Count-1;
                    double[] similarity = new double[countTemp];
                    int[] idx = new int[countTemp];string outpu="";
                    for (int c = 0; c < countTemp; c++)
                    {
                        CsvRow row = template.ElementAt(c + 1);
                        int countRow = row.Count - 1;
                        int[] vecA = new int[countRow], vecB = new int[countRow];
                        for (int cv = 0; cv < countRow; cv++)
                        {
                            vecA[cv] = Convert.ToInt32(row.ElementAt(cv + 1));
                            vecB[cv] = Convert.ToInt32(listFitur[cv + 2]);
                        }
                        similarity[c] = dotProduct(vecA, vecB) / (Math.Sqrt(dotProduct(vecA,vecA)) * Math.Sqrt(dotProduct(vecB,vecB)));
                        idx[c] = c;
                    }
                    //MessageBox.Show(outpu);
                    Array.Sort(similarity, idx);
                    Array.Reverse(similarity);
                    Array.Reverse(idx);*/

                    /*listFitur[9] = (Math.Round(similarity[0] * 100, 2)).ToString() + "% " + template.ElementAt(idx[0]).ElementAt(0) + ", ";
                    listFitur[9] += (Math.Round(similarity[1] * 100, 2)).ToString() + "% " + template.ElementAt(idx[1]).ElementAt(0) + ", ";
                    listFitur[9] += (Math.Round(similarity[2] * 100, 2)).ToString() + "% " + template.ElementAt(idx[2]).ElementAt(0);*/

                    //-----ADD TO LIST VIEW-----\\
                    ListViewItem item = new ListViewItem(listFitur);
                    listMulti.Items.Add(item);

                    //-----ADD TO CSV-----\\
                    if (cekSave)
                    {
                        /*CsvRow row = new CsvRow();
                        for (int i = 1; i <= 14; i++)
                            row.Add(listFitur[i]);
                        allRow.Add(row);*/
                        String dir = new DirectoryInfo(Path.GetDirectoryName(files[dn])).Name;
                        dir = dir.Replace("product-", "'");
                        String nama = Path.GetFileNameWithoutExtension(files[dn]);
                        nama = nama.Replace("segment-", "'");
                        int countRow = allRow.Count;
                        for (int c = 1; c < countRow; c++)
                        {
                            CsvRow row = allRow.ElementAt(c);
                            //CsvRow row = new CsvRow();
                            //String a = dir + " " + row.ElementAt(1) + " " + nama + " " + row.ElementAt(2);
                            //MessageBox.Show(a);
                            if (dir.Equals(row.ElementAt(1)) && nama.Equals(row.ElementAt(2)))
                            {
                                int rn = row.Count;
                                for (int i = rn-1; i >= 7; i--)
                                    row.RemoveAt(i);
                                for (int i = 2; i <= 14; i++)
                                    row.Add(listFitur[i]);
                                //allRow.Add(row);
                                allRow.RemoveAt(c);
                                allRow.Insert(c, row);
                            }
                        }
                    }
                }
            }
            if (cekSave)
            {
                using (CsvFileWriter writer = new CsvFileWriter(ofd.FileName))
                {
                    foreach (CsvRow row in allRow)
                        writer.WriteRow(row);
                }
            }
            listMulti.Visible = true;
        }

        private int dotProduct(int[] vecA, int[] vecB)
        {
            int hasil = 0;
            int n=vecA.Length;
            for (int i = 0; i < n; i++)
                hasil += (vecA[i] * vecB[i]);
            return hasil;
        }

        private void FeatureExtractApp_Load(object sender, EventArgs e)
        {
            //-----SET DEFAULT PARAM-----\\
            string[] text = File.ReadAllLines(@"../../defParam.txt");
            for (int i = 0; i <= 18; i++)
                param[i] = Convert.ToInt32(text[i]);
        }
    }
}
